/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	vvi adj(n);
	for (int i = 0; i < n - 1; i++) {
		cini(u);
		cini(v);
		u--;
		v--;
		adj[u].push_back(v);
		adj[v].push_back(u);
	}
	vb s(n);
	for (int i = 0; i < n; i++) {
		cini(aux);
		s[i] = aux;
	}

	/* We create a tree dp. On every node we need to have a table
	 * per child with number of pairs of nodes and the distances,
	 * and number of nodes per level.
	 * With these, we can create all pairs of childs, and build
	 * the number of valid triangles.
	 * Properties of valid triangles:
	 * All of them have one "root" node where every single of the vertices is
	 * located on another child of the node, on same level.
	 * We just have to sum up efficiently.
	 *
	 * Creating the level map per node is creating it for one (root) node, and
	 * then step forward dfs through all others.
	 * Creating for root node is simple dfs.
	 *
	 * We need  to create to maps per child.
	 * One 2-dimensional, for pairs of nodes with
	 * x=dist to each other, and y=dist to node
	 *
	 * One 1-dimensional for single nodes by level
	 */
	struct nodestat {
		vi lvl;	// count per level.
		vvi pairs; // pairs[i][j]=count of pairs on level i with dist to each other j
	};
	function<void(int, int, nodestat&)> dfs1 = [&](int node, int parent, nodestat &ret) {
		ret.lvl.assign(1, 1);
		for (int chl : adj[node]) {
			if (chl == parent)
				continue;
			nodestat chlStat;
			dfs1(chl, node, chlStat);

			if (ret.lvl.size() < chlStat.lvl.size() + 1)
				ret.lvl.resize(chlStat.lvl.size() + 1);

			for (size_t i = 0; i < chlStat.lvl.size(); i++)
				ret.lvl[i + 1] += chlStat.lvl[i];

			// TODO construct number of pairs, pairs matrix

		}
	};

	nodestat stat;
	dfs1(0, -1, stat);
	cout << stat.lvl.size() << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

