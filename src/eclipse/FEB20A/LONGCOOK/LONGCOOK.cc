/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl;}
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << " ";
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

#define MONDAY 0
const int D = 0;	// weekday of first january in year 1, 401, 801,...

/*
 * A year is a leap year if it a multiple of 400,
 * or if it is a multiple of 4 but not a multiple of 100.
 * For example, the year 2100 is not a leap year, but the year 2400 is a leap year.
 */
bool isLeap(const int y) {
	return (y + 1) % 400 == 0 || ((y + 1) % 4 == 0 && (y + 1) % 100 != 0);
}

/* @return previous year of 400 cycle start, ie 0, 400, 800... */
int prev400(int y) {
	return (y / 400) * 400;
}

vi month = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

int firstOfMonth(int m, int y) {
	log("firstOfMonth", m, y);
	int baseY = prev400(y);
	int d = D;
	while (baseY < y) {
		if (isLeap(y)) {
			d += (366 % 7);
		} else
			d += (365 % 7);
		d %= 7;
		baseY++;
	}

	int baseM = 0;
	while (baseM < m) {
		int md = month[baseM];
		if (baseM == 1 && isLeap(y))
			md++;
		d += md;
		d %= 7;
		baseM++;
	}

	return d;
}

/*
 * @return day of first friday in month m in year y
 */
int firstFriday(int m, int y) {
	log("firstFriday", m, y);
	int d = firstOfMonth(m, y);
	int ans = 1;
	while (d != 4) {
		ans++;
		d++;
		d %= 7;
	}
	return ans;
}

int secondToLastSunday(int m, int y) {
	log("secondToLastSunday", m, y);
	int m2 = m + 1;
	int y2 = y;
	if (m2 == 12) {
		m2 = 0;
		y2++;
	}

	int d = firstOfMonth(m2, y2);
	d--;
	if (d < 0)
		d += 7;

	int dom = month[m];
	if (m == 1 && isLeap(y))
		dom++;

	int cnt = 0;
	if (d == 6)
		cnt = 1;
	dom -= 7;
	while (d != 6) {
		dom--;
		d--;
		if (d < 0)
			d += 7;
	}
	return dom;
}

const int N = 400 * 12;
vb isOverlap(N);
vi ovsum(N + 1);
int ov400 = 0;

/* day of month of first friday if 1st of month is i */
vi ffOffs = { 5, 4, 3, 2, 1, 7, 6 };
/* day of month of first sunday if 1st of month is i */
vi fsOffs = { 7, 6, 5, 4, 3, 2, 1 };

/* find day of January 1 in year 1, so the first day
 * we have to consider in history of the monthly challange. */
void init() {
	int d = D;
	for (int y = 0; y < 400; y++) {
		bool leap = isLeap(y);
		for (int m = 0; m < 12; m++) {
			int idx = y * 12 + m;
			int daysOfMonth = month[m];
			if (m == 1 && leap)
				daysOfMonth++;

			int fF = ffOffs[d];	// day of month of first friday
			int fS = fsOffs[d]; // day of month of first sunday
			stack<int> sundays;
			while (fS + 7 <= daysOfMonth) {
				fS += 7;
				sundays.push(fS);
			}
			sundays.pop();
			const int secondLastSunday = sundays.top();

			isOverlap[idx] = fF + 10 > secondLastSunday;
			ovsum[idx + 1] = ovsum[idx] + isOverlap[idx];
			ov400 += isOverlap[idx];

			d += month[m];
			if (m == 2 && leap)
				d++;
			d %= 7;
		}
	}
}

/* return ovsum for interval (l,r] */
ll inter(ll l, ll r) {
	assert(l < r);
	l %= N;
	r %= N;
	if (r == 0)
		r = N;
	return ovsum[r] - ovsum[l];
}

void solve() {
	cinll(m1);
	cinll(y1);
	cinll(m2);
	cinll(y2);
	m1--;
	y1--;
	m2--;
	y2--;

	ll idx1 = y1 * 12 + m1;
	ll idx2 = y2 * 12 + m2;

	/* first start of 400*12 cycle >=idx1 */
	ll cy1 = (idx1 / N) * N;
	if (cy1 < idx1)
		cy1 += N;

	/* last start of 400*12 cycle <= idx2 */
	ll cy2 = (idx2 / N) * N;
	if (cy2 < cy1)
		cy2 = cy1;

	ll ans = ((cy2 - cy1) / N) * ov400;

	if (cy1 > idx1 && cy1 < idx2) {
		ans += inter(idx1, cy1);
		ans += inter(cy2, idx2 + 1);
	} else
		ans += inter(idx1, idx2 + 1);

	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	init();
	//test();
	//return 0;
	cini(t);
	while (t--)
		solve();
}

