/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl;}
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << "  ";
	logf(++it, args...);
}
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(int i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<vll> vvll;

/* For two segments to have a right angle
 * following conditions must hold:
 * 1. slope must fit, slope(segA)=-slope(segB)
 * 2. there must be a station at the crosspoint of both segments.
 *
 * On both axes,
 * we need to check for every x0 if there is a x1 with
 * 1. opposite sign
 * 2. and a quadratic multiple of x0
 * 3. and if on the other axis there is a value x1/sqrt(quad)
 *
 * example, x0=-3, x1=12, y1=6, y2=-6
 *
 * Since |x|,|y| are limited to 1e5, we simply check all quads.
 * Note that for quad==1 we need to make sure we do not count twice.
 *
 * Note: For x0=0 there are more rect angles.
 */

/* brute force solution to find what is wrong with the actual one. */
int solve2(vi dx, vi dy) {
	vector<pii> p;

	for (auto x : dx)
		p.emplace_back(0, x);

	for (auto y : dy)
		p.emplace_back(y, 0);

	/* return quad of distance between points */
	function<ll(pii, pii)> dist2 = [](pii p1, pii p2) {
		ll yd = p1.first - p2.first;
		ll xd = p1.second - p2.second;
		return yd * yd + xd * xd;
	};

	function<bool(int, int, int)> isRight = [&](int i, int j, int k) {
		vll lens(3);
		lens[0] = dist2(p[i], p[j]);
		lens[1] = dist2(p[j], p[k]);
		lens[2] = dist2(p[i], p[k]);
		sort(lens.begin(), lens.end());
		return lens[0] + lens[1] == lens[2];
	};

	int ans = 0;
	for (size_t i = 0; i < p.size(); i++)
		for (size_t j = i + 1; j < p.size(); j++)
			for (size_t k = j + 1; k < p.size(); k++)
				if (isRight(i, j, k))
					ans++;

	cout << ans << endl;
	return ans;
}
const int N = 100005;

vvi divs(N);

void calcDivs(int x, vi &res) {
	cout << "calcDivs, x=" << x << endl;
	x = abs(x);
	stack<int> st;
	for (int i = 1; i * i <= x; i++) {
		if (x % i == 0) {
			res.push_back(x);
			divs[x].push_back(i);
			int i2 = x / i;
			if (i2 != i) {
				st.push(i2);
				divs[i2].push_back(x);
			}
		}
	}
	while (st.size()) {
		res.push_back(st.top());
		st.pop();
	}
}

/* ok, it is not that simple.
 * Let be the 3 verts
 * A the smallest one on Y axis
 * B the one on X axis
 * C the bigger one on Y axis
 * For 3 such vertices to be right tri it must be (ignoring signs)
 * A,B,C integer
 *  A*F=B
 *  B*F=C
 *  A*F*F=c
 * How to find the possible Fs for a given A to make B and C integers?
 * Since |C| >= |A| it holds that F>=1, ie f/d -> f>=d
 * Example F=3/2
 * 1. denominator must be divisor of A
 * 2. nominator must be divisor of C
 *
 */
int solve(vi dx, vi dy) {
	cout << "solve()" << endl;
	vb idxx(2 * N);
	vb idxy(2 * N);

	vi divX;

	for (size_t i = 0; i < dx.size(); i++) {
		idxx[dx[i] + N] = true;
	}

	for (auto y : dy)
		idxy[y + N] = true;

	// we want to calcDivs() here for all x,y, then and store the
	// divisors per x.
	// Then loop over the devisors, and per devisor over all x of that divisor.

	ll ans = 0;
	for (int loop = 0; loop < 2; loop++) {
		for (auto x : dx) {
			calcDivs(x, divX);
			cout << "x=" << x << " divX.size()=" << divX.size() << endl;
			if (x == 0) {
				ans += ((1LL * dy.size()) * (dx.size() - 1));
			} else {
				for (int k = 1;; k++) {
					const ll x1kk = -x * k * k;
					if (abs(x1kk) > N * abs(x))
						break;
					/* we need to insert a loop over all divisors of x here */
					for (int d : divX) {
						if (x1kk % (d * d) != 0)
							continue;

						const ll x1kkd = x1kk / (d * d);
						cout << "x1kkd=" << x1kkd << endl;

						if (abs(x1kkd) >= N)
							break;

						if (idxx[x1kkd + N]) {
							if (k != 1 || x > 0) {
								const int ii = x * k / d;
								if (idxy[N + ii])
									ans++;

								if (idxy[N - ii])
									ans++;
							}
						}
					}
				}
			}
		}
		swap(dx, dy);
		swap(idxx, idxy);
	}

	cout << ans << endl;
	return ans;
}

int main() {
	//cin.tie(nullptr);
	//std::ios::sync_with_stdio(false);

	cini(t);
	while (t--) {
		cini(n);
		cini(m);
		cout << "t=" << t << " n=" << n << " m=" << m << endl;
		vi x(n);
		for (int i = 0; i < n; i++)
			cin >> x[i];
		cout << "did parse x" << endl;
		vi y(m);
		for (int i = 0; i < m; i++) {
			cout << "hallo1: " << i << endl;
			cin >> y[i];
			cout << "hallo2: " << y[i] << endl;
		}

		cout << "did parse y" << endl;

		if (solve(x, y) != solve2(x, y)) {
			cout << "HIT!!!" << endl;
			break;
		}
	}
}

