/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

#define MOD 1000000007

int mul(const int v1, const int v2, int mod = MOD) {
	return (int) ((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod = MOD) {
	int res = 1;
	while (p != 0) {
		if (p & 1)
			res = mul(res, a, mod);
		p >>= 1;
		a = mul(a, a, mod);
	}
	return res;
}

int pl(int v1, int v2, int mod = MOD) {
	int res = v1 + v2;

	if (res < 0)
		res += mod;

	else if (res >= mod)
		res -= mod;

	return res;
}

int inv(const int x, const int mod = MOD) {
	return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
	return mul(zaehler, inv(nenner));
}

template<typename E>
class SegmentTree {
private:
	vector<E> tree;
	E neutral;
	function<E(E, E)> cumul;

	int treeDataOffset;
	const int ROOT = 1;

	inline int leftChild(const int treeIdx) {
		return treeIdx * 2;
	}
	inline int rightChild(const int treeIdx) {
		return treeIdx * 2 + 1;
	}
	inline int parent(const int treeIdx) {
		return treeIdx / 2;
	}
	inline bool isOdd(const int idx) {
		return (idx & 0x1);
	}

public:
	/** SegmentTree. Note that you can hold your data in your own storage and give
	 * an Array of indices to a SegmentTree.
	 * @param beg, end The initial data.
	 * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
	 * @param pCumul The cumulative function to create the "sum" of two nodes.
	 **/
	template<typename Iterator>
	SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E(E, E)> pCumul) {
		neutral = pNeutral;
		cumul = pCumul;
		treeDataOffset = (int) distance(beg, end);
		tree = vector<E>(treeDataOffset * 2, pNeutral);

		int i = treeDataOffset;
		for (auto it = beg; it != end; it++)
			tree[i++] = *it;

		for (int j = treeDataOffset - 1; j >= 1; j--)
			tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
	}

	/** Updates all elements in interval(idxL, idxR].
	 * iE add 42 to all elements from 4 to inclusive 7:
	 * update(4, 8, [](int i1) { return i1+42; });
	 */
	void updateRange(int pIdxL, int pIdxR, function<E(E)> apply) {
		pIdxL += treeDataOffset;
		pIdxR += treeDataOffset;
		for (int i = pIdxL; i < pIdxR; i++)
			tree[i] = apply(tree[i]);

		while (pIdxL != ROOT) {
			pIdxL = parent(pIdxL);
			pIdxR = max(pIdxL, parent(pIdxR - 1));
			for (int i = pIdxL; i <= pIdxR; i++)
				tree[i] = cumul(tree[leftChild(i)], tree[rightChild(i)]);
		}
	}

	/** Updates the data at dataIdx to value returned by apply. */
	void updateApply(int dataIdx, function<E(E)> apply) {
		updateSet(dataIdx, apply(get(dataIdx)));
	}

	/** Updates the data at dataIdx by setting it to value. */
	void updateSet(int dataIdx, E value) {
		int treeIdx = treeDataOffset + dataIdx;
		tree[treeIdx] = value;

		while (treeIdx != ROOT) {
			treeIdx = parent(treeIdx);
			tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
		}
	}

	/** @return value at single position, O(1) */
	E get(int idx) {
		return tree[idx + treeDataOffset];
	}

	/** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
	E get(int pIdxL, int pIdxR) {
		int idxL = pIdxL + treeDataOffset;
		int idxR = pIdxR + treeDataOffset;
		E cum = neutral;
		while (idxL < idxR) {
			if (isOdd(idxL)) { // left is odd
				cum = cumul(cum, tree[idxL]);
				idxL++;
			}
			if (isOdd(idxR)) {
				idxR--;
				cum = cumul(cum, tree[idxR]);
			}
			idxL = parent(idxL);
			idxR = parent(idxR);
		}
		return cum;
	}

};

const int INF = 1e9;

/* In loop over s
 * we need to calc the values for all prefixes
 * ending in current char.
 * So, for every single i the val is calculated based on the
 * between pos[i] and the min(pos[i..cur]).
 */
void solve3() {
	cini(n);
	cins(s);

	int cnt = 0;
	for (int i = 0; i < n; i++) {
		if (s[i] == '(')
			cnt++;
		else if (s[i] == ')')
			cnt--;
	}
	// TODO...
}

void solve2() {
	cini(n);
	cins(s);

	vi bal(n + 1);
	int cnt = 0;
	for (int i = 0; i < n; i++) {
		if (s[i] == '(')
			cnt++;
		else if (s[i] == ')')
			cnt--;
		bal[i + 1] = cnt;
	}

	SegmentTree<int> seg(bal.begin(), bal.end(), INF, [](int i1, int i2) {
		return min(i1, i2);
	});

	/* return bal[l]-main(bal[l]...bal[r])  */
	function<int(int, int)> cntSwaps = [&](const int l, const int r) {
		int c1 = bal[l];
		int c2 = seg.get(l, r + 2);
		c2 -= c1;
		c2 = min(0, c2);
		return (abs(c2) + 1) / 2;
	};

	int ansf = 0;
	int ansd = 0;
	for (int i = 0; i < n; i++) {
		for (int j = i; j < n; j++) {
			ansf += cntSwaps(i, j);
			ansd++;
		}
	}

	cout << fraction(ansf, ansd) << endl;
}

/* brute force simulate the process */
void solve1() {
	cini(n);
	cins(s);

	function<int(int, int)> cnt = [&](const int l, const int r) {
		int ans = 0;
		int c = 0;
		for (int k = min(l, r); k <= max(l, r); k++) {
			if (c == 0 && s[k] == ')') {
				ans++;
				c++;
			} else {
				if (s[k] == ')')
					c--;
				else if (s[k] == '(')
					c++;
			}
		}
		return ans;
	};

	int ansf = 0;
	int ansd = 0;
	for (int i = 0; i < n; i++) {
		for (int j = i; j < n; j++) {
			ansf += cnt(i, j);
			ansd++;
		}
	}

	cout << fraction(ansf, ansd) << endl;
}

int main() {
	cini(t);
	while (t--)
		solve2();
}

