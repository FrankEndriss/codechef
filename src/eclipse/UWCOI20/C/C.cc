/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<vb> vvb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct Dsu {
	vector<int> p;
	vector<int> sz;
	/* initializes each (0..size-1) to single set */
	Dsu(int size) {
		p.resize(size);
		sz.resize(size);
		for (int i = 0; i < size; i++) {
			p[i] = i;
			sz[i] = 1;
		}
	}

	/* finds set representative for member v */
	int find_set(int v) {
		if (v == p[v])
			return v;
		return p[v] = find_set(p[v]);
	}

	/* makes v a single set, removes it from other set if contained there. */
	void make_set(int v) {
		p[v] = v;
	}

	/* combines the sets of two members.
	 * Use the bigger set as param a for optimized performance.  */
	void union_sets(int a, int b) {
		a = find_set(a);
		b = find_set(b);
		if (a != b) {
			p[b] = a;
			sz[a] += sz[b];
		}
	}
};

const vector<pii> offs = { { -1, 0 }, { 1, 0 }, { 0, -1 }, { 0, 1 } };

void solve() {
	cini(h);
	cini(w);
	cini(q);

	//map<int, vector<pii>> hmap;
	vvi a(h, vi(w));
	for (int i = 0; i < h; i++) {
		for (int j = 0; j < w; j++) {
			cin >> a[i][j];
			//hmap[a[i][j]].emplace_back(i, j);
		}
	}

	vector<vi> qlist(q, vi(5));	// qlist[i][4]=ans
	for (int i = 0; i < q; i++) {
		qlist[i][0] = i;
		cin >> qlist[i][1] >> qlist[i][2] >> qlist[i][3];
	}
	sort(qlist.begin(), qlist.end(), [](vi &v1, vi &v2) {
		return v1[3] < v2[3];	// by power
	});

	Dsu dsu(h * w);
	vvb vis(h, vb(w));
	vector<set<vi>> adj;	//  adj[i]=set of unvisited cells adjent to group i { hight, r, c }

	for (int i = 0; i < q; i++) {
		if (qlist[i][3] <= a[qlist[i][1]][qlist[i][2]]) {	// power less than hight of source
			qlist[i][4] = 0;
			continue;
		}

#define cellidx(i) (qlist[i][1] * h + qlist[i][2])
		int group = dsu.find_set(cellidx(i));

		pii s = { qlist[i][1], qlist[i][2] };
		if (!vis[s.first][s.second]) {
			adj[group].insert( { a[s.first][s.second], s.first, s.second });
		}

		for (pii p2 : offs) {
			p2.first += s.first;
			p2.second += s.second;
			if (!vis[p2.first][p2.second])
				adj[group].insert( { a[p2.first][p2.second], p2.first, p2.second });
		}

		queue<pii> q;
		q.push( { v[1], v[2] });

		for (vi v : adj[group]) {
			if (v[0] < qlist[i][3]) { // height < power
				dsu.union_sets(group, dsu.find_set)
			}

			if (qlist[i][3] > a[qlist[i][1]][qlist[i][2]]) {
				q.push( { qlist[i][1], qlist[i][2] });
			} else {
				qlist[i][4] = 0;
				continue;
			}
			while (q.size()) {
			}

			cini(r);
			cini(c);
			cini(power);
			r--;
			c--;

//cout << "r=" << r << " c=" << c << " power=" << power << endl;

			int ans = 0;
			queue<pii> q;
			if (power > a[r][c])
				q.push( { r, c });
			vis[r][c] = true;
			while (q.size()) {
				ans++;
				pii p = q.front();
				q.pop();
				for (pii off : offs) {
					pii pnext = { p.first + off.first, p.second + off.second };
					if (pnext.first >= 0 && pnext.first < h && pnext.second >= 0 && pnext.second < w && !vis[pnext.first][pnext.second]
							&& a[pnext.first][pnext.second] < power) {
						vis[pnext.first][pnext.second] = true;
						q.push(pnext);
					}
				}
			}
			cout << ans << endl;
		}

	}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
	solve();
}

