/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void divs(int n, vi &ans) {
	for (int i = 2; i * i < n; i++) {
		while (n % i == 0) {
			ans.push_back(i);
			n /= i;
		}
	}
	if (n > 1)
		ans.push_back(n);
}

int req(int i) {
	cout << "? " << i << endl;
	cini(resp);
	return resp;
}
void solve() {
	cini(n);
	cini(q);

	vi d;
	divs(n, d);
	sort(d.begin(), d.end());

	if (d.size() == 1) {
		cout << "! 1" << endl;
		cini(x);
		return;
	}

	while (d.size() > 2) {
		int myq = d[0] * d[1];
		int resp = req(myq);
		if (resp == n - myq) {
			resp = req(d[0]);
			if (resp == n - d[0]) {
				cout << "! " << n / d[0] << endl;
				cini(x);
				return;
			}
			if (d[0] != d[1]) {
				resp = req(d[1]);
				if (resp == n - d[1]) {
					cout << "! " << n / d[1] << endl;
				} else {
					cout << "! " << n / (d[0] * d[1]) << endl;
				}
				cini(x);
				return;
			}

		} else {
			d.erase(d.begin());
			d.erase(d.begin());
		}
	}

	if (d.size() == 2) {
		int resp = req(d[0]);
		if (resp == n - d[0]) {
			cout << "! " << n / d[0] << endl;
		} else
			cout << "! " << n / d[1] << endl;
	} else {
		cout << "! " << n / d[0] << endl;
	}
	cini(x);

	return;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

