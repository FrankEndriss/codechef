/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF = 1e9;

void solve() {
	cini(r);
	cini(c);
	cini(x1);
	cini(y1);
	cini(x2);
	cini(y2);
	vs s(r);
	for (int i = 0; i < r; i++)
		cin >> s[i];

	vvi dp(r, vi(c, INF));
	queue<pii> q;
	q.push( { x1 - 1, y1 - 1 });
	dp[x1 - 1][y1 - 1] = 0;
	const vector<pii> offs = { { 0, 1 }, { 0, -1 }, { 1, 0 }, { -1, 0 } };
	while (q.size()) {
		pii p = q.front();
		q.pop();
		for (pii o : offs) {
			pii pn = { p.first + o.first, p.second + o.second };
			if (pn.first >= 0 && pn.first < r && pn.second >= 0 && pn.second < c) {
				int val = dp[p.first][p.second];
				if (s[pn.first][pn.second] == '1')
					val++;
				if (val < dp[pn.first][pn.second]) {
					dp[pn.first][pn.second] = val;
					q.push( { pn.first, pn.second });
				}
			}
		}
	}
	int ans = dp[x2 - 1][y2 - 1];
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

