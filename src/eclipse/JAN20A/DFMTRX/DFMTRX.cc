/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* brute force.
 * seems to work.
 * for n=powof2 its fairly fase.
 * but stackoverflow for n>=128
 * for other n somewhat slow :/ */
void solveBF(const int n) {
	vvi ans(n, vi(n, -1));

	/* return true if sol found, in ans */
	function<bool(int, vb&, int)> dfs = [&](const int i, vb &vis, const int j) {
		if (i >= n) /* found distribution for all 1..n-1 */
			return true;

		/*
		 cout << "dfs, i=" << i << " n=" << n << " j=" << j << " vis=";
		 for (int b : vis)
		 cout << b << " ";
		 cout << endl;
		 */

		for (int f = 1; f < n; f++) { /* first of pair */
			if (vis[f])
				continue;
			vis[f] = true;

			for (int s = 0; s < f; s++) { /* second of pair */
				//cout << "inner1, f=" << f << " s=" << s << endl;
				if (vis[s])
					continue;
				//cout << "inner2, f=" << f << " s=" << s << endl;

				if (ans[f][s] > -1)
					continue;
				//cout << "inner3, f=" << f << " s=" << s << endl;

				vis[s] = true;
				ans[f][s] = i;

				if (j + 1 < n / 2) {
					if (dfs(i, vis, j + 1))
						return true;
				} else {
					vb nvis(n);
					if (dfs(i + 1, nvis, 0))
						return true;
				}
				ans[f][s] = -1;
				vis[s] = false;
			}
			vis[f] = false;
		}
		return false;
	};

	vb vis(n);
	if (dfs(1, vis, 0)) {
		for (int i = 0; i < n; i++)
			ans[i][i] = n;

		for (int i = 1; i < n; i++)
			for (int j = 0; j < i; j++)
				ans[j][i] = ans[i][j] + n;

		cout << "Hooray" << endl;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				cout.width(2);
				cout << ans[i][j] << " ";
			}
			cout << endl;
		}
	} else {
		cout << "Boo" << endl;
	}

}

/* Odd n are Boo, even are Hooray.
 * In even grid we need to distribute gaus(n-1) fields in a way
 * that every number 1..n "gets" (n+1)/2 of the fields.
 * And the indexes of the field must be distinct to each other.
 */
void solve() {
	cini(n);

	if (n == 1) {
		cout << "Hooray\n1" << endl;
		return;
	}

	if (n & 1) {
		cout << "Boo" << endl;
		return;
	}

	solveBF(n);

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

