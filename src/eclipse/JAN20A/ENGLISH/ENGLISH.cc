/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
	cini(n);
	cinas(s, n);

	vi srev(n);
	iota(srev.begin(), srev.end(), 0);

	sort(s.begin(), s.end());

	sort(srev.begin(), srev.end(), [&](int i1, int i2) {
		auto it1 = s[i1].rbegin();
		auto it2 = s[i2].rbegin();
		while (it1 != s[i1].rend() && it2 != s[i2].rend()) {
			if (*it1 < *it2)
				return true;
			else if (*it1 > *it2)
				return false;

			it1++;
			it2++;
		}
		if (it2 == s[i2].rend())
			return true;

		return false;
	});

#ifdef DEBUG
	cout << "srev" << endl;
	for (int i = 0; i < n; i++)
		cout << s[srev[i]] << endl;
#endif

	/* Now we create set of pair<-beauty,index>
	 * We then erase biggest beauty pair, and update the pair which
	 * changes by that erase.
	 * For pre/post we use two sets, and remove from the better one.
	 * Do until no more matches.
	 *
	 * Note that the updates are bit complicated, since we need to update the "other" set, too.
	 * TODO
	 */
	cout << 42 << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

