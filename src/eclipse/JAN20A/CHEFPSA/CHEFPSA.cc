/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD = 1e9 + 7;

void solve() {
	cini(n);
	map<int, int> f;
	vi a(n * 2);
	set<int> f2;
	for (int i = 0; i < n * 2; i++) {
		cin >> a[i];
		f[a[i]]++;
		if (f[a[i]] % 2 == 0)
			f2.insert(a[i]);
		else
			f2.erase(a[i]);
	}

	int preSum = 0;
	int postSum = 0;

	function<int(void)> count = [&]() {
		if (f.size() == 0)
			return 1;

		int ans = 0;
		int fcnt = 0;
		for (auto ent : f) {
			if (ent.second == 0)
				continue;
			fcnt++;

			/* TODO this not correct */
			int nextPre = preSum - ent.first;
			if (f[nextPre] > 0 && f[postSum - nextPre] > 0) {
				f[nextPre]--;
				f[postSum - nextPre]--;
				if (nextPre != postSum - nextPre)
					ans += 2 * count();
				else
					ans += count();
			}
		}
		if (fcnt == 0)
			return 1;
		else
			return ans;
	};

	int ans = 0;
	for (int sum : f2) { /* possible sums over all */
		f[sum] -= 2;
		/* There must be some number be the diff of sums and some other number. Twice, one pre, one post. */
		preSum = sum;
		postSum = sum;
		ans += count();
		f[sum] += 2;
	}

	cout << ans << endl;

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

