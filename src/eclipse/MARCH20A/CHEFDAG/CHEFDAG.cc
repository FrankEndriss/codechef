/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << "   ";
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* see https://cp-algorithms.com/graph/edmonds_karp.html */
const int INF = 1e9;

int bfs(int s, int t, vi &parent, vvi &adj, vvi &capacity) {
	log("bfs");
	fill(parent.begin(), parent.end(), -1);
	parent[s] = -2;
	queue<pii> q;
	q.push( { s, INF });

	while (!q.empty()) {
		int cur = q.front().first;
		int flow = q.front().second;
		q.pop();
		log(cur, flow);

		for (int next : adj[cur]) {
			if (parent[next] == -1 && capacity[cur][next]) {
				parent[next] = cur;
				int new_flow = min(flow, capacity[cur][next]);
				if (next == t)
					return new_flow;
				q.push( { next, new_flow });
			}
		}
	}

	return 0;
}

int maxflow(int s, int t, vvi &adj, vvi &capacity, int n) {
	log("maxflow");
	int flow = 0;
	vi parent(n);
	int new_flow;

	while (new_flow = bfs(s, t, parent, adj, capacity)) {
		flow += new_flow;
		int cur = t;
		while (cur != s) {
			int prev = parent[cur];
			capacity[prev][cur] -= new_flow;
			capacity[cur][prev] += new_flow;
			cur = prev;
		}
	}

	return flow;
}

/* We should create a graph where
 * any node is connected to "some" connectable node.
 * So which nodes are connectable?
 * ->All nodes not "blocked" by the input permutations
 * Then, we have for every node a list of nodes
 * we can connect to.
 * Now we need to find most possible pairs. ie
 * a max flow network.
 *
 */
void solve() {
	cini(n);
	cini(k);
	vvi a(k, vi(n));
	vvi pos(k, vi(n));

	for (int i = 0; i < k; i++)
		for (int j = 0; j < n; j++) {
			cini(aux);
			aux--;
			a[i][j] = aux;
			pos[i][aux] = j;
		}

	vvi adj(2 * n + 2);
	vvi cap(2 * n + 2, vi(2 * n + 2));
	const int S = 2 * n;
	const int T = 2 * n + 1;

	for (int v = 0; v < n; v++) {
		vi vis(n);
		for (int i = 0; i < k; i++) {
			for (int j = pos[i][v] + 1; j < n; j++) {
				vis[a[i][j]]++;
			}
		}

		adj[S].push_back(v);
		adj[v].push_back(S);
		cap[S][v] = 1;
		adj[v + n].push_back(T);
		adj[T].push_back(v + n);
		cap[v + n][T] = 1;

		for (int i = 0; i < n; i++) {
			if (vis[i] == k) {
				log("connnect", v, i);
				adj[v].push_back(i + n);
				adj[i + n].push_back(v);
				cap[v][i + n] = 1;
			}
		}
	}

	log("callmaxflow");
	int con = maxflow(S, T, adj, cap, adj.size());
	log("finimaxflow", con);

#ifdef DEBUG
	cout << "cap:" << endl;
	for (int i = n; i < n * 2; i++) {
		for (int j = 0; j < n; j++) {
			cout << cap[i][j] << " ";
		}
		cout << endl;
	}
#endif

	vi ans(n, -1);
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			if (cap[j + n][i])
				ans[i] = j;
		}
	}

	cout << n - con << endl;
	for (int i = 0; i < n; i++)
		cout << ans[i] + 1 << " ";
	cout << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--) {
		log(t);
		solve();
	}
}

