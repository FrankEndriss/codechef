/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int NONE = -2;
const int ERR = -1;

int query(int x1, int x2, int y) {
	cout << 1 << " " << x1 << " " << x2 << " " << y << endl;
	cini(aux);
	return aux;
}

/** This is impossible to solve.
 * Lets try to profe that it is unsolvable.
 * Assume K=3 and Q=1, M=10.
 * a[] could be like all values same, ie ans to (all) Q would be 0.
 * To answer that, we would have to be sure that there is no single value
 * in a[] other than some x.
 *
 * That x coulld be on any hight (1,10)
 * With first query (ie 5) we will get ans==-2, so we know
 * a[] are in range (6,10) or (1,4).
 * Second query can be 8, which again will ans==-2, so we know
 * values are in range 9-10, 6-7 or 1-4
 * With third query we cannot query all these 3 ranges,
 * so we cannot tell if final ans is 0 or 1.
 * And that is only first Q, there could be 10.
 *
 * Basically the same is true for M=1e9 and K==10.
 *
 * Lets try to find strat for K=100 (and M=1e9, N/(R-L)==20
 * Note that all (L,R) can be without intersection.
 * So for first (L,R).
 * Query M/2. if resp>M/2 thats ans
 * else if resp==-2 ans<M/2
 * else
 *
 * Apart from that it is true that if we query for a range (l,r), we do know
 * nothing about the other ranges.
 * On the other hand, if we query two (or more ranges) including the space
 * between the ranges, we cannot tell to which range an answer refers.
 * So, what we can say by query bigger rantes (i.e (1,N)) is that the
 * upper bound of hight set by the response to the query applies to
 * all Q ranges.
 * But most likely these responses will set bounds of limited use (ie resp==M)
 *
 * Assume a more simple version of the riddle:
 * K==3, Q=1, M=10, R-L==1000
 * All a[i]=same value, but one a[i] is +1 or -1.
 * So the ans to the single Q is 0 or 1, depending on
 * the fact that the single i is in range (L,R), or not.
 *
 * ===================
 * Aprils fool.
 * We are given T test cases. a[i] does not change in between.
 *
 * So, how do we find hight for at least first segment (L,R) ?
 * Ask at M/2. 
 * If resp>=M/2, thats ans.
 * Else If resp<M/2 we can exclude ranges at top and bottom.
 * Else resp==-2, and
 * Again we use middle of top and bottom to query again.
 *
 * ...again...this does not help. There must be more aprils fool.
 * Assume further that max(a[i])==M and min(a[i])==1.
 *
 * We could implement something which depends on the fact that if 
 * the values are really random, the biggest interval would be fairly
 * big.
 * With that knowledge we can go for subtask1 and query y=M/2 and use
 * this as ans. if resp==-2, we would try...
 *
 * If we maintain upper and lower bound of ans for every Q-range.
 * Before first it is (1,M)
 * After query M/2 it is resp, or 1,M/2.
 * What if we query at M/4?:
 * ===================
 *
 * The segments build a rectangle...
 **/
void solve() {
	cini(n);
	cini(m);
	cini(k);
	cini(q);

	vi l(q);
	vi r(q);
	for (int i = 0; i < q; i++)
		cin >> l[i] >> r[i];

}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

