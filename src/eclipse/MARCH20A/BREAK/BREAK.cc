/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* In this riddle we have to consider a bunch of stupid cornercases...
 * no fun.
 */

/* one turn
 * to get a draw all cards must be played.
 * 1. For every card a[i] there must be a higher card b[i], so simply sort them.
 * 2. There must be an order, except the first a[i] all a[i] can be used only if
 * the same number was used before.
 * ie we must start with the smallest a because else we never ever can play a smaller
 * card than the first.
 * It is greedy, a should always play smallest possible card, and b must choose
 * a card smallest so that a can play a next card. Which is in fact the smallest b.
 *  */
void solve1() {
	cini(n);
	cinai(a, n);
	cinai(b, n);
	sort(a.begin(), a.end());
	sort(b.begin(), b.end());

	set<int> vis;
	bool ok = true;
	vis.insert(a[0]);
	if (b[0] > a[0]) {
		vis.insert(b[0]);
		for (int i = 1; ok && i < n; i++) {
			if (vis.count(a[i]))
				vis.insert(a[i]);
			else
				ok = false;

			if (b[i] > a[i])
				vis.insert(b[i]);
			else
				ok = false;
		}
	} else
		ok = false;

	if (ok)
		cout << "YES" << endl;
	else
		cout << "NO" << endl;

}

/* any number of turns.
 * Note that we can swap cards from one player to the other
 * by simply play that card as an attacker, and give up as the defender.
 * But to change the roles at least one card must be beaten, ie with N<3
 * We cannot exchange cards.
 *
 * So, the question is if there exist any bipartition of the
 * cards so that solve1() gives yes.
 * The rules if we really can swap are more complicated, we need to consider
 *
 * n<=2
 * More than n a[] are equal
 * max(b[])>min(a[])
 * More corner cases:
 * n=3 and a has tree equal, b all less
 */
void solve2() {
	cini(n);
	if (n == 1) {
		cini(a);
		cini(b);
		if (b > a)
			cout << "YES" << endl;
		else
			cout << "NO" << endl;
		return;
	}

	map<int, int> f;
	vi a;
	vi b;
	int mab = 0;
	for (int i = 0; i < 2 * n; i++) {
		cini(aux);
		if (i < n)
			a.push_back(aux);
		else {
			b.push_back(aux);
			mab = max(mab, b.back());
		}
		f[aux]++;
	}

	bool ok = true;

	if (n == 2) { // ???
		sort(a.begin(), a.end());
		sort(b.begin(), b.end());
		if (b[0] > a[0] && (a[1] > b[1] || b[1] > a[1]))
			;
		else
			ok = false;
	}

	for (auto ent : f) {
		/* After any attacker card, defender can give up. So they can create any card distribution als long as
		 * both have more than one card, ie they can change roles at least once.
		 *
		 * Changeing roles is possible if both have two cards.
		 *
		 * We can remove any pair of unequal cards by: Attacker play it, and the defender
		 * defends it, and attacker gives up. Role changes.
		 *
		 * if a[] is all same rank, and b[] all less then the roles can never change.
		 */

		if (ent.second > n) {
			ok = false;
		} else {
			if (ent.second == n) {
				sort(a.begin(), a.end());
				if (a[0] == a.back() && mab < a.back())
					ok = false;
			}
		}
	}

	if (ok)
		cout << "YES" << endl;
	else
		cout << "NO" << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	cini(s);
	while (t--) {
		if (s == 1)
			solve1();
		else if (s == 2)
			solve2();
		else
			assert(false);
	}
}

