/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

typedef pii pt;
#define x first
#define y second

bool cmp(pt a, pt b) {
	return a.x < b.x || (a.x == b.x && a.y < b.y);
}

bool cw(pt a, pt b, pt c) {
	return a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y) < 0;
}

bool ccw(pt a, pt b, pt c) {
	return a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y) > 0;
}

/** see https://cp-algorithms.com/geometry/grahams-scan-convex-hull.html
 * After return a contains a list of points forming a polygon which
 * includes all points.
 */
void convex_hull(vector<pt> &a) {
	if (a.size() == 1)
		return;

	sort(a.begin(), a.end(), &cmp);
	pt p1 = a[0], p2 = a.back();
	vector<pt> up, down;
	up.push_back(p1);
	down.push_back(p1);
	for (int i = 1; i < (int) a.size(); i++) {
		if (i == a.size() - 1 || cw(p1, a[i], p2)) {
			while (up.size() >= 2 && !cw(up[up.size() - 2], up[up.size() - 1], a[i]))
				up.pop_back();
			up.push_back(a[i]);
		}
		if (i == a.size() - 1 || ccw(p1, a[i], p2)) {
			while (down.size() >= 2 && !ccw(down[down.size() - 2], down[down.size() - 1], a[i]))
				down.pop_back();
			down.push_back(a[i]);
		}
	}

	a.clear();
	for (int i = 0; i < (int) up.size(); i++)
		a.push_back(up[i]);
	for (int i = down.size() - 2; i > 0; i--)
		a.push_back(down[i]);
}

/* if ants v1 and v2 are connected, return
 * point where ant v1 will go to.
 */
pt mv(pt v1, pt v2) {
	pii ans;
	int dx = v2.x - v1.x;
	int dy = v2.y - v1.y;

	if (dx == 0)
		dy /= abs(dy);
	else if (dy == 0)
		dx /= abs(dx);
	else {
		int g = __gcd(dx, dy);
		dx /= g;
		dy /= g;
	}
	return {v1.x+dx, v1.y+dy};
}

/* Strat 1:
 * Create a polygon by circling arround all ants. Note that this is way to slow.
 * But it would put in best case all ants in that circle into one point.
 *
 * Strat 2:
 * Find next ant by slopes. Allways use the biggest slope avail. Start
 * searching in list sortet by x or y.
 * TODO analyse solutions for create convex hull of point cloud.
 * https://en.wikipedia.org/wiki/Convex_hull_algorithms
 * https://en.wikipedia.org/wiki/Graham_scan
 *
 * Strat 3:
 * Find a polygon with most possible vertexes. Repeat.
 * This is kind of "reverse graham". For any three vertex on convex hull
 * we can try to find two other points substituting one point.
 * How?
 *
 * Strat 4:
 * Avoid problematic vertices, ie for 3 vertex, two can be on one axis, so
 * it would be helpfull to move one by some position.
 * And hardly avoid to have the last two vertex on one line parallel to
 * one axis.
 * We want to avoid to connect vertex with a lot of in between stations,
 * ie two vertex with slope 0 or 1 or 1/2
 *
 * Strat 5:
 * Super simple, find 3 "good fitting" vertex, connect them.
 * Must not be colinear.
 * Must be in clockwise order.
 * Should be direct connect (mv(...) should return v2).
 */
void solve() {
	cini(n);
	vector<pii> a(n);
	vb vis(n); /* killed ants */

	for (int i = 0; i < n; i++)
		cin >> a[i].x >> a[i].y;

	vvi ans;

	int alive = n;	// alife ants
	while (alive > 1) {
		/* We implement Strat5 to get a working solution.
		 * This is, on every loop we find 3 vertexes and connect them.
		 * ... TODO ...
		 **/

		vi lans;
		int lcnt = 0;
		int prev = -1;
		for (int i = 0; lcnt < 3 && i < n; i++) {
			if (vis[i])
				continue;

			if (prev < 0) {
				prev = i;
				lcnt = 1;
				continue;
			}

			pt p = mv(a[prev], a[i]);
			if (p == a[i]) {
				prev = i;
				lcnt++;
			}
		}

		cout << lans.size() << " ";
		for (int i : lans)
			cout << i + 1 << " ";
		cout << endl;
	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	solve();
}

