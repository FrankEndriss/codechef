/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl; }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << "=" << a << "  ";
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template<typename E>
class SegmentTree {
private:
	vector<E> tree;
	E neutral;
	function<E(E, E)> cumul;

	int treeDataOffset;
	const int ROOT = 1;

	inline int leftChild(const int treeIdx) {
		return treeIdx * 2;
	}
	inline int rightChild(const int treeIdx) {
		return treeIdx * 2 + 1;
	}
	inline int parent(const int treeIdx) {
		return treeIdx / 2;
	}
	inline bool isOdd(const int idx) {
		return (idx & 0x1);
	}

public:
	/** SegmentTree. Note that you can hold your data in your own storage and give
	 * an Array of indices to a SegmentTree.
	 * @param beg, end The initial data.
	 * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
	 * @param pCumul The cumulative function to create the "sum" of two nodes.
	 **/
	template<typename Iterator>
	SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E(E, E)> pCumul) {
		neutral = pNeutral;
		cumul = pCumul;
		treeDataOffset = (int) distance(beg, end);
		tree = vector<E>(treeDataOffset * 2, pNeutral);

		int i = treeDataOffset;
		for (auto it = beg; it != end; it++)
			tree[i++] = *it;

		for (int j = treeDataOffset - 1; j >= 1; j--)
			tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
	}

	/** Updates all elements in interval(idxL, idxR].
	 * iE add 42 to all elements from 4 to inclusive 7:
	 * update(4, 8, [](int i1) { return i1+42; });
	 */
	void updateRange(int pIdxL, int pIdxR, function<E(E)> apply) {
		pIdxL += treeDataOffset;
		pIdxR += treeDataOffset;
		for (int i = pIdxL; i < pIdxR; i++)
			tree[i] = apply(tree[i]);

		while (pIdxL != ROOT) {
			pIdxL = parent(pIdxL);
			pIdxR = max(pIdxL, parent(pIdxR - 1));
			for (int i = pIdxL; i <= pIdxR; i++)
				tree[i] = cumul(tree[leftChild(i)], tree[rightChild(i)]);
		}
	}

	/** Updates the data at dataIdx to value returned by apply. */
	void updateApply(int dataIdx, function<E(E)> apply) {
		updateSet(dataIdx, apply(get(dataIdx)));
	}

	/** Updates the data at dataIdx by setting it to value. */
	void updateSet(int dataIdx, E value) {
		int treeIdx = treeDataOffset + dataIdx;
		tree[treeIdx] = value;

		while (treeIdx != ROOT) {
			treeIdx = parent(treeIdx);
			tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
		}
	}

	/** @return value at single position, O(1) */
	E get(int idx) {
		return tree[idx + treeDataOffset];
	}

	/** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
	E get(int pIdxL, int pIdxR) {
		int idxL = pIdxL + treeDataOffset;
		int idxR = pIdxR + treeDataOffset;
		E cum = neutral;
		while (idxL < idxR) {
			if (isOdd(idxL)) { // left is odd
				cum = cumul(cum, tree[idxL]);
				idxL++;
			}
			if (isOdd(idxR)) {
				idxR--;
				cum = cumul(cum, tree[idxR]);
			}
			idxL = parent(idxL);
			idxR = parent(idxR);
		}
		return cum;
	}
};

const int MOD = 998244353;

inline int mul(const int v1, const int v2, const int mod = MOD) {
	return (int) ((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, const int mod = MOD) {
	int res = 1;
	while (p != 0) {
		if (p & 1)
			res = mul(res, a, mod);
		p >>= 1;
		a = mul(a, a, mod);
	}
	return res;
}

inline int inv(const int x, const int mod = MOD) {
	return toPower(x, mod - 2);
}

inline int pl(const int v1, const int v2, const int mod = MOD) {
	int res = v1 + v2;

	if (res < 0)
		res += mod;

	else if (res >= mod)
		res -= mod;

	return res;
}

const int N = 100007;
vector<int> fac(N);

void initFac(const int mod = MOD) {
	fac[1] = 1;
	for (int i = 2; i < N; i++) {
		fac[i] = mul(fac[i - 1], i, mod);
	}
}

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(const int n, const int k) {
	if (k == 0 || k == n)
		return 1;
	if (k == 1 || k == n - 1)
		return n;
	if (k > n / 2)
		return nCr(n, n - k);

	return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

/*
 * There are n piles of stones, player can take any amount of stones from one pile,
 * must take at least one.
 * Taking last stone wins.
 * So, having one pile is a winning position...
 * Nim says a winning position is if
 * "the xor-sum of the pile sizes is non zero".
 * So, the number of winning moves is the number of
 * moves which makes the xorsum zero.
 *
 * To answer queries we need to know
 * -the xorsum
 * -the number and sizes of the piles bteq the xorsum
 * Then, ans is calculated as the sum of number of ways to take xorsum stones from that piles.
 *
 * That number of ways for one pile of size>=xorsum is nCr(size, xorsum). NO!!!
 * We can also take less than xorsum stones to make xorsum zero.
 *
 * From a pile of size sz we can take 1..sz stones.
 * let t number of taken stones, then we can take
 * every t for which is true:
 * 0 = xorsum ^ sz ^ (sz-t);
 * t = sz - (xorsum ^ sz) :if(t>0)
 * So, there is at most one t per pile size.
 * t is positive if and only if
 * the highest bit of xorsum is set in sz, too.
 * The highest bit of xorsum is the highest bit
 * of sizes with an odd count.
 *
 * -> Howto find the sizes of the piles (and freq of sizes) efficiently?
 * What about SegmentTree<set<pii>>, where pii.first=a[i], pii.second=freq
 * Whith this we can efficiently query the freq-map for a given (l,r).
 * By sorting the pairs (note that they are sortet in the set) we might
 * opitmize the loop arround the freqs?
 * Can we compress the set<pii> somehow and maintain the compressed from in
 * the SegmentTree?
 *
 * First implement brute force for some points.
 */

void solve2() {
	cini(n);
	cinai(a, n);
	cini(q);
	vector<pair<pii, int>> vq;
	for (int i = 0; i < q; i++) {
		cin >> vq[i].first.first >> vq[i].first.second;
		vq[i].second = i;
	}

	map<int, int> f;
	map<int, int> ff;
	for (int i = 0; i < n; i++) {
		auto it = f.find(a[i]);
		if (it == f.end()) {
			f[a[i]]++;
			ff[1]++;
		} else {
			auto it2 = ff.find(it->second);
			if (it2->second == 1)
				ff.erase(it2);
			else
				it2->second--;

			it->second++;
			ff[it->second]++;
		}
	}
	/* we can remove an a[i] from the maps, too, in O(log n)
	 * but how does it help?
	 * If we calculate one interval, then another one with "small" distance
	 * to first one, that would help. But to calc all intervals, we would have
	 * to sort them by "distance". like traveling salesman, which is difficult
	 * for 1e5 intervals.
	 *
	 * What about SQRT-decomposition. We would split a[] into subarrays, and
	 * calc the maps for each one.
	 * Then, for every query, we add them up.
	 **/
	// ???
	typedef pair<map<int, int>, map<int, int>> pmm;
	vector<pmm> data(n);
	for (int i = 0; i < n; i++) {
		data[i].first[a[i]] = 1;
		data[i].second[1] = 1;
	}

	pmm neutral;
	SegmentTree<pmm> stree(data.begin(), data.end(), neutral, [](pmm p1, pmm p2) {
		return p1;
	});
}

void solve() {
	cini(n);
	cinai(a, n);
	cini(q);

	for (int i = 0; i < q; i++) {
		cini(l);
		cini(r);
		l--;
		r--;
		map<int, int> f;
		for (int j = l; j <= r; j++)
			f[a[j]]++;

		int xorsum = 0;
		for (auto ent : f)
			xorsum ^= ent.second;
		log(xorsum);

		if (xorsum == 0) {
			cout << 0 << endl;
			continue;
		}

		int ans = 0;
		for (auto ent : f) {
			log(ent.second, xorsum);
			int t = ent.second - (xorsum ^ ent.second);
			if (t > 0)
				ans = pl(ans, nCr(ent.second, t));
		}
		cout << ans << endl;
	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	initFac();

	cini(t);
	while (t--)
		solve();
}

