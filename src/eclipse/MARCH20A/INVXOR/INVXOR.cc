/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* sum of xor values of all (nonempty) subseqs of a.
 * 2^n -1 subseqs (sum of nCr(n, 1..n))
 *
 * beauty of these subseqs?
 * Bit by bit.
 * Consider first bit:
 * There are nodd number in a[] with first bit set, and neven numbers with bit not set.
 * We need to consider how much subseqs exist with odd/even number of nodd elements.
 * ...
 */
int beauty(vi &a) {

}
/* defined to be the
 * number of non-negative integer sequences with length N which have beauty B
 * Note that there is an upper bound for values in that seq: No element can
 * be bigger than b, since if it would, beauty would be bigger than b.
 *
 * How to construct seqs (of len n) with beauty b?
 * Bit by bit.
 * To have first bit set, number of elements with first bit set must be:
 * If there is 1 element with first bit set, that element is contained in
 * 2^(n-1) subseqs (all subseqs without that element, with that element added.
 * So first bit of b is set if 2^(n-1) is odd, which is the case only for
 * n==1.
 * So, basically the same is true for second bit, but:
 * Two first bit count as one second bit, too!
 * To have 2^(n-1)=2 obviously n==2
 * So there must be 1 elements with first bit set,
 * or n==1 (and second bit set in that one element).
 * ...
 */
int f(int n, int b) {

}

inline string remove_pre_zero(const string &a) {
	auto t = a.find_first_not_of('\0', 0);
	if (t == a.npos)
		return string("0");
	else
		return a.substr(t);
}

/* convert decimal string to binary string...
 * add dec-digit by dec-digit
 * multiply by 10 is: x*10== x<<1 + x<<3
 */
/* https://stackoverflow.com/questions/5372279/how-can-i-convert-very-large-decimal-numbers-to-binary-in-java */
string convert_to_bin(const string &_s) {
	const static string str[] = { "0", "1" };
	string s(_s.size(), '0');
	string binary;
	binary.reserve(_s.size() * 3);
	int i = 0;
	for (const auto &c : _s)
		s[i++] = (c - '0');

	while (s != "0") //simulate divide by 2
	{
		int t = 0, old_t = 0;
		for (auto &ch : s) {
			t = ((old_t * 10 + ch) & 1);
			ch = (ch + old_t * 10) >> 1;
			old_t = t;
		}
		binary += str[t];
		if (s[0] == 0)
			s = remove_pre_zero(s);
	}
	return string(binary.rbegin(), binary.rend());
}

/* to work this out, we have to convert n to binary string.
 */
void solve() {
	cins(n);	// note: string
	cini(x);
	cini(m);

	n = convert_to_bin(n);
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

