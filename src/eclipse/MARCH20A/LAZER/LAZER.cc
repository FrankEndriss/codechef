/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); cout<<endl;}
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << "=" << a << "  ";
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

template<typename E>
class SegmentTree {
private:
	vector<E> tree;
	E neutral;
	function<E(E, E)> cumul;

	int treeDataOffset;
	const int ROOT = 1;

	inline int leftChild(const int treeIdx) {
		return treeIdx * 2;
	}
	inline int rightChild(const int treeIdx) {
		return treeIdx * 2 + 1;
	}
	inline int parent(const int treeIdx) {
		return treeIdx / 2;
	}
	inline bool isOdd(const int idx) {
		return (idx & 0x1);
	}

public:
	/** SegmentTree. Note that you can hold your data in your own storage and give
	 * an Array of indices to a SegmentTree.
	 * @param beg, end The initial data.
	 * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
	 * @param pCumul The cumulative function to create the "sum" of two nodes.
	 **/
	template<typename Iterator>
	SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E(E, E)> pCumul) {
		neutral = pNeutral;
		cumul = pCumul;
		treeDataOffset = (int) distance(beg, end);
		tree = vector<E>(treeDataOffset * 2, pNeutral);

		int i = treeDataOffset;
		for (auto it = beg; it != end; it++)
			tree[i++] = *it;

		for (int j = treeDataOffset - 1; j >= 1; j--)
			tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
	}

	/** Updates all elements in interval(idxL, idxR].
	 * iE add 42 to all elements from 4 to inclusive 7:
	 * update(4, 8, [](int i1) { return i1+42; });
	 */
	void updateRange(int pIdxL, int pIdxR, function<E(E)> apply) {
		pIdxL += treeDataOffset;
		pIdxR += treeDataOffset;
		for (int i = pIdxL; i < pIdxR; i++)
			tree[i] = apply(tree[i]);

		while (pIdxL != ROOT) {
			pIdxL = parent(pIdxL);
			pIdxR = max(pIdxL, parent(pIdxR - 1));
			for (int i = pIdxL; i <= pIdxR; i++)
				tree[i] = cumul(tree[leftChild(i)], tree[rightChild(i)]);
		}
	}

	/** Updates the data at dataIdx to value returned by apply. */
	void updateApply(int dataIdx, function<E(E)> apply) {
		updateSet(dataIdx, apply(get(dataIdx)));
	}

	/** Updates the data at dataIdx by setting it to value. */
	void updateSet(int dataIdx, E value) {
		int treeIdx = treeDataOffset + dataIdx;
		tree[treeIdx] = value;

		while (treeIdx != ROOT) {
			treeIdx = parent(treeIdx);
			tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
		}
	}

	/** @return value at single position, O(1) */
	E get(int idx) {
		return tree[idx + treeDataOffset];
	}

	/** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
	E get(int pIdxL, int pIdxR) {
		int idxL = pIdxL + treeDataOffset;
		int idxR = pIdxR + treeDataOffset;
		E cum = neutral;
		while (idxL < idxR) {
			if (isOdd(idxL)) { // left is odd
				cum = cumul(cum, tree[idxL]);
				idxL++;
			}
			if (isOdd(idxR)) {
				idxR--;
				cum = cumul(cum, tree[idxR]);
			}
			idxL = parent(idxL);
			idxR = parent(idxR);
		}
		return cum;
	}

};

/* We need to find the number of intervals for a value.
 * But only an interval of intervals should be considered.
 *
 * So, for all intervals...
 * We need a way to nicly model the intervals given in a[].
 *
 * In q, we need to find the count of Intervals
 * where first<q<second.
 * Lets implement brute force first. -> ok
 *
 * If we sort queryies by y...
 * Then we use a SegmentTree 0..xMax
 * and sort interval events by a[x].
 * On event we add/remove 1 at position of interval.
 * Then we can tell on each y (after every evt) how
 * much intervals exist in segment tree.
 */
void solve() {
	cini(n);
	cini(q);
	vi a(n);

	SegmentTree<int> stree(a.begin(), a.end(), 0, [](int i1, int i2) {
		return i1 + i2;
	});

	vector<pair<int, pii>> evt; /* evt[i]=<a,<+-1,x>> */
	for (int i = 0; i < n; i++) {
		cin >> a[i];
		if (i > 0) {
			//if (a[i - 1] < a[i]) {
			evt.push_back( { min(a[i - 1], a[i]), { -1, i - 1 } });	// note +-1 inversed
			evt.push_back( { max(a[i - 1], a[i]), { +1, i - 1 } });
			//} else {
			//	evt.push_back( { a[i], { -1, i } });
			//	evt.push_back( { a[i - 1], { 1, i - 1 } });
			//}
		}
	}
	sort(evt.begin(), evt.end());

	vector<pair<pii, pii>> vq(q);
	for (int i = 0; i < q; i++) {
		cin >> vq[i].second.first >> vq[i].second.second >> vq[i].first.first;
		vq[i].first.second = i;
	}
	sort(vq.begin(), vq.end());

	int qidx = 0;
	int evtidx = 0;
	while (qidx < q) {
		log("query", vq[qidx].first.first);
		/* to finish query qidx, we need to integrate all evt with y<q.y
		 * and all evt with y=q.y and evt=-1 (ie all interval starts, but not ends)
		 */
		while (evtidx < evt.size() && evt[evtidx].first < vq[qidx].first.first) {
			int x = evt[evtidx].second.second;
			log(x, evt[evtidx].second.first);
			stree.updateSet(x, stree.get(x) - evt[evtidx].second.first);
			evtidx++;
		}
		while (evtidx < evt.size() && evt[evtidx].first == vq[qidx].first.first && evt[evtidx].second.first == -1) {
			int x = evt[evtidx].second.second;
			log(x, evt[evtidx].second.first);
			stree.updateSet(x, stree.get(x) - evt[evtidx].second.first);
			evtidx++;
		}

		log(vq[qidx].second.first, vq[qidx].second.second);

		int ans = stree.get(vq[qidx].second.first - 1, vq[qidx].second.second - 1);
		vq[qidx].first.first = vq[qidx].first.second;
		vq[qidx].first.second = ans;
		qidx++;
	}
	sort(vq.begin(), vq.end());

	for (int i = 0; i < vq.size(); i++)
		cout << vq[i].first.second << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

