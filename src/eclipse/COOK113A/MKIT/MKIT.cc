/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

vi a;
set<pii> kf;	// factors of k, first=factor, second=cnt

vector<pii> f;	// f[j]= list of i for which a[i] has factor j (first), second times

void index(int i, function<void(int, int)> cb) {
	int aux = a[i];
	for (int j = 2; j * j <= aux; j++) {
		int cnt = 0;
		while (aux % i == 0) {
			cnt++;
			aux /= i;
		}
		if (cnt > 0)
			cb(i, cnt);
	}
	if (aux != 1)
		cb(aux, 1);
}

void solve() {
	cini(n);
	cini(q);
	cini(k);
	a.resize(n);

	index(k, [](int f, int cnt) {
		kf.insert( { f, cnt });
	});

	for (int i = 0; i < n; i++) {
		cin >> a[i];
		// todo index(i);
	}

	for (int i = 0; i < q; i++) {
		/* now find smallest sum of a[i]s where factors contain
		 * all factors of kf.
		 */

	}
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

