/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI = 3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

map<ll, ll> memo;

/* just the sum of the bits, without the lowest bit, but no -1 or the like.
 * Memoization might be useful. */
ll sumbits(ll n) {
	auto it = memo.find(n);
	if (it != memo.end())
		return it->second;

	ll ans = 0;
	ll mask = 1LL << 40;
	while (mask) {
		if (n & mask) {
			ans += mask * (n & (~mask));
			ans += sumbits(mask - 1);
			ans += sumbits(n & (~mask));
			memo[n] = ans;
			return ans;
		}
		mask >>= 1;
	}
	return ans;
}
/* We create the sum from 1 to n bit by bit.
 * We do this by counting the number of times that bit is
 * set and any of the lower bits are set.
 * Let b be the nth bit.
 * The first time b is set it is -1.
 * Then it is (b>>1)-1 times set.
 * The ones it is the lowest bit.
 * Then it is (b>>1)-1 times set...
 *
 */
ll mysum(ll n) {
	if (n == 0)
		return 0;
	else if (n == 1)
		return -1;
	else if (n == 2)
		return -2;

	//cout << "sum n=" << n << endl;
	ll ans = 0;
	ll mask = 1LL << 40;
	while (mask) {
		if (n & mask) {
			ans--;	// once it is the only one bit and counts therefore as -1
			ans += mask * (n & (~mask));
			ans += mysum(mask - 1);
			ans += sumbits(n & (~mask));
			return ans;
			//cout << "masked mask=" << mask << " n=" << n << endl;
		}
		mask >>= 1;
	}
	assert(false);
	return -1;
}

void solve() {
	cini(l);
	cini(r);
	/* A==Smallest bit of N.
	 * B==Other bits of N.
	 * Sum over all (l,r) without the smallest bit?
	 * Sum over all smallest bits of (l,r)
	 * Numbers with only one (or zero) bit count as -1.
	 */

	ll ans = mysum(r) - mysum(l - 1);
	cout << ans << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	/*
	 for (int i = 1; i < 20; i++) {
	 cout << i << " " << mysum(i) << endl;
	 }
	 */
	cini(t);
	while (t--)
		solve();
}

