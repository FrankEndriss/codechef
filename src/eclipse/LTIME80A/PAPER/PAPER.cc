/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#define log(args...)
#else
#define log(args...) { string _s = #args; replace(_s.begin(), _s.end(), ',', ' '); stringstream _ss(_s); istream_iterator<string> _it(_ss); logf(_it, args); }
void logf(istream_iterator<string> it) {
}
template<typename T, typename ... Args>
void logf(istream_iterator<string> it, T a, Args ... args) {
	cout << *it << " = " << a << endl;
	logf(++it, args...);
}
#endif

#define forn(i,k,n) for(ll i=(k); i<(n); i++)

const double PI = acos(-1);
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* find nearest pair of points.
 * On any unfold all points are copied along
 * the folding axix.
 * After such unfold nearest distance between old
 * and new points is nearest distand to the foldings axix
 * times 2.
 * We can simulate the unfold, by copying only the
 * interesting points. That are the ones nearest to the borders,
 * 2 per border, since any point in between that two can be ignored.
 *
 * Since numbers get huge, we need to use an alternate coordinate system.
 * This is, we use two borders, and the distance two those two
 * borders. LET border numbers be
 * U=0, D=1; and R=1; L=0;
 * 00==UL, 01==UR, 10==DL, 11==DR
 *
 * no,no,no...
 * We only need to find if one of the initial borders is a bolded
 * folder or not. it does not matter how often there are foldings.
 * Then, if it is a folding border, the folding results in having
 * two points int the distance 2 times the distance to the border.
 *
 * So, how to find if a border is a folded border?
 * For first 50% trivial, U border is folded, others not.
 * -> if at least once that border was mentioned in s.
 *
 **/

// 0,0 == bottom/left
void solve() {
	cini(n);
	cini(m);
	cini(w);
	cini(h);
	cins(s); // length==n; RLUD

	vi f(256);
	for (char c : s)
		f[c]++;

	vector<pii> ip(m);
	int maY = 0;	// max Y
	int miY = 1e9;	// min Y
	int maX = 0;
	int miX = 1e9;
	for (int i = 0; i < m; i++) {
		cin >> ip[i].first >> ip[i].second;
		miY = min(miY, ip[i].second);
		maY = max(maY, ip[i].second);
		miX = min(miX, ip[i].first);
		maX = max(maX, ip[i].first);
	}

	double dist = 1e9;
	for (int i = 0; i < m; i++) {
		for (int j = i + 1; j < m; j++) {
			dist = min(dist, hypot(abs(ip[i].first - ip[j].first), abs(ip[i].second - ip[j].second)));
		}
	}

	if (f['U'] > 0 || f['D'] > 1) {
		dist = min(dist, (double) ((h - maY) * 2));
	}
	if (f['D'] > 0 || f['U'] > 1) {
		dist = min(dist, (double) (miY * 2));
	}

	if (f['L'] > 0 || f['R'] > 1) {
		dist = min(dist, (double) (miX * 2));
	}
	if (f['R'] > 0 || f['L'] > 1) {
		dist = min(dist, (double) ((w - maX) * 2));
	}

	cout << setprecision(10) << fixed << dist << endl;
}

int main() {
	cin.tie(nullptr);
	std::ios::sync_with_stdio(false);
	cini(t);
	while (t--)
		solve();
}

