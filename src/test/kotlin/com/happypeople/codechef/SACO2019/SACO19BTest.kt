package com.happypeople.codechef.SACO2019

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class SACO19BTest {

    @Before
    fun setUp() {
        SACO19B.printLog=true
    }

    @Test
    fun run() {
        SACO19B.inputStr="1\n" +
                "4 3\n" +
                "1 4 3 5"
        SACO19B().run()
    }

    @Test
    fun run2() {
        SACO19B.inputStr="1\n" +
                "4 2\n" +
                "3 2 3 2"
        SACO19B().run()
    }
}