package com.happypeople.codechef.SACO2019

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class SACO19ATest {

    @Before
    fun setUp() {
        SACO19A.printLog=true
    }

    @Test
    fun run() {
        SACO19A.inputStr="1\n" +
                "2 4\n" +
                "1 abhishek N\n" +
                "1 oihir N\n" +
                "1 mayank Y\n" +
                "2 brafull N\n" +
                "2 zzoomi Y\n" +
                "2 piyush N\n" +
                "1 1\n" +
                "1 3\n" +
                "2 2\n" +
                "2 3"
        SACO19A().run()
    }
}