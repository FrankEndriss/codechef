package com.happypeople.codechef.JAN19B

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class DIFNEIGHTest {

    @Before
    fun setUp() {
        //DIFNEIGH.printLog = true
        //DIFNEIGH.outputStr = StringBuffer("")
    }

    @Test
    fun run() {
        DIFNEIGH.inputStr = "3\n" +
                "1 1\n" +
                "3 2\n" +
                "2 3"
        DIFNEIGH().run()
        val expStr = "1\n" +
                "1\n" +
                "3\n" +
                "11\n" +
                "22\n" +
                "33\n" +
                "3\n" +
                "123\n" +
                "123"
        assertEquals("blub", expStr.trim(), DIFNEIGH.outputStr.toString().trim())
    }

    @Test
    fun run4x2() {
        DIFNEIGH.inputStr = "1\n" +
                "4 2\n"
        DIFNEIGH().run()

    }

    @Test
    fun run2() {
        DIFNEIGH.inputStr = "2\n" +
                "4 4\n" +
                "7 1"
        DIFNEIGH().run()
    }

    @Test
    fun run3() {
        DIFNEIGH.inputStr = "1\n" +
                "7 2"
        DIFNEIGH().run()
    }

    @Test
    fun run4() {
        DIFNEIGH.inputStr = "1\n" +
                "7 3"
        DIFNEIGH().run()
    }

    @Test
    fun run5() {
        DIFNEIGH.inputStr = "1\n" +
                "3 7"
        DIFNEIGH().run()
    }

    @Test
    fun run7() {
        DIFNEIGH.inputStr = "9\n" +
                "1 1\n" +
                "1 2\n" +
                "2 1\n" +
                "2 2\n" +
                "3 1\n" +
                "3 2\n" +
                "3 3\n" +
                "1 3\n" +
                "2 3\n"
        DIFNEIGH().run()

    }

    @Test
    fun run44() {
        DIFNEIGH.inputStr = "7\n" +
                "1 4\n" +
                "2 4\n" +
                "3 4\n" +
                "4 4\n" +
                "4 1\n" +
                "4 2\n" +
                "4 3\n"
        DIFNEIGH().run()
    }

    @Test
    fun run55() {
        DIFNEIGH.inputStr = "1\n" +
                "43 47\n"
        DIFNEIGH().run()
    }

    @Test
    fun runAll() {
        for (m in 1..9)
            for (n in 1..9) {
                DIFNEIGH.inputStr = "1\n" +
                        "$n $m\n"
                //println("*** $n X $m")
                DIFNEIGH().run()
            }
    }

}