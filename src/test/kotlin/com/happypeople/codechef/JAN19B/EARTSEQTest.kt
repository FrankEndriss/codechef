package com.happypeople.codechef.JAN19B

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import java.util.*

class EARTSEQTest {

    @Before
    fun setUp() {
        EARTSEQ.printLog = true
    }

    @Test
    fun run50000() {
        EARTSEQ.printLog = false
        EARTSEQ.inputStr = "1\n" +
                "50000\n"
        //EARTSEQ.outBuf=StringBuffer("")
        EARTSEQ().run()
        //checkResult(EARTSEQ.outBuf.toString())
    }

    @Test
    fun runx() {
        for (i in 3..13) {
            EARTSEQ.printLog = false
            EARTSEQ.inputStr = "1\n" +
                    "$i\n"
            EARTSEQ().run()
        }

    }

    @Test
    fun runXonce() {
        var str = "11\n"
        for (i in 33..43) {
            str = str + "$i\n"
        }
        EARTSEQ.printLog = false
        EARTSEQ.inputStr = str
        EARTSEQ().run()
    }

    fun checkResult(str:String) {
        val sc = Scanner(str)
        while (sc.hasNext()) {
            val line = sc.nextLine().trim()
            val scLine = Scanner(line)
            val list = mutableListOf<Int>()
            while (scLine.hasNextInt()) {
                list.add(scLine.nextInt())
            }
            println("checking: $list")
            assertEquals("Uniqueness", list.size, list.toSet().size)
            assertTrue("Pairs", checkPairs(list))
            assertTrue("Triples", checkTriples(list))
        }
    }

    @Test
    fun run() {
        EARTSEQ.printLog = false
        for (i in 3..33) {
            EARTSEQ.inputStr = "1\n" +
                    "$i\n"
            EARTSEQ().run()
        }
    }


    fun checkPairs(a: List<Int>): Boolean {
        for (i in a.indices)
            if (!checkPair(a[i], a[(i + 1) % a.size]))
                return false
        return true
    }

    fun checkPair(a1: Int, a2: Int): Boolean {
        val gcd = Gcd.gcd(a1.toLong(), a2.toLong())
        println("gcd of $a1 $a2 == $gcd")
        return gcd != 1L
    }

    fun checkTriples(a: List<Int>): Boolean {
        for (i in a.indices)
            if (!checkTriple(a[i], a[(i + 1) % a.size], a[(i + 2) % a.size]))
                return false
        return true
    }

    fun checkTriple(a1: Int, a2: Int, a3: Int): Boolean {
        val gcd = Gcd.gcd(listOf(a1.toLong(), a2.toLong(), a3.toLong()))
        println("gcd of $a1 $a2 $a3 == $gcd")
        return gcd == 1L
    }

    class Gcd<E> {
        companion object {

            fun lcm(a: Long, b: Long): Long {
                return a * (b / gcd(a, b))
            }

            fun lcm(input: List<Long>): Long {
                var result = input[0]
                for (i in input.indices)
                    result = lcm(result, input[i])
                return result
            }

            fun gcd(pA: Long, pB: Long): Long {
                var b = pB
                var a = pA
                while (b > 0) {
                    val temp = b
                    b = a % b
                    a = temp
                }
                return a
            }

            fun gcd(input: List<Long>): Long {
                var result = input[0]
                for (i in input.indices)
                    result = gcd(result, input[i])
                return result
            }
        }
    }
}