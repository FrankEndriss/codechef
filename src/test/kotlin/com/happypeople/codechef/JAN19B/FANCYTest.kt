package com.happypeople.codechef.JAN19B

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class FANCYTest {

    @Before
    fun setUp() {
        FANCY.printLog=true
    }

    @Test
    fun run() {
        FANCY.inputStr="2\n" +
                "i do not have any fancy quotes\n" +
                "when nothing goes right go left"
        FANCY().run()
    }
}