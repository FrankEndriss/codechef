package com.happypeople.codechef.JAN19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class MGAMETest {

    @Before
    fun setUp() {
        MGAME.printLog = true
    }

    @Test
    fun run() {
        val sb = StringBuffer("1000000\n")
        for (p in 1..1000000) {
            sb.append("1 $p\n")
        }
        MGAME.inputStr = sb.toString()
        MGAME.outBuffer = null
        MGAME().run()
    }

    fun counted(N: Int, P: Int): Long {
        // 1. find maximum possible score
        // Obviously, if choosen 1 for any i,j,k the score cannot be gt 1.
        var lMax = Long.MIN_VALUE
        for (i in 1..P) {
            for (j in 1..P) {
                for (k in 1..P) {
                    val res = (((N % i) % j) % k) % N
                    if (res > lMax) {
                        lMax = res.toLong()
                        //log("maxFound: $lMax @ $i $j $k")
                    }
                }
            }
        }
        //log("max: $lMax")
        var count = 0L
        for (i in 1..P) {
            for (j in 1..P) {
                for (k in 1..P) {
                    val res = (((N % i) % j) % k) % N
                    if (res.toLong() == lMax) {
                        //log("@: $i $j $k")
                        count++
                    }
                }
            }
        }
        // println("ans counted: $count")
        return count
    }
}