package com.happypeople.codechef.JAN19B

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class HP18Test {

    @Before
    fun setUp() {
        HP18.printLog=true
    }

    @Test
    fun run() {
        HP18.inputStr="2\n" +
                "5 3 2\n" +
                "1 2 3 4 5\n" +
                "5 2 4\n" +
                "1 2 3 4 5"
        HP18().run()
    }

    @Test
    fun run2() {
        HP18.inputStr = "2\n" +
                "5 1 1\n" +
                "1 1 3 4 5\n"+
                "3 3 3\n"+
                "3 3 3"

        HP18().run()
    }
}