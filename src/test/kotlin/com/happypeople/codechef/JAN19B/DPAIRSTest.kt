package com.happypeople.codechef.JAN19B

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class DPAIRSTest {

    @Before
    fun setUp() {
        DPAIRS.printLog=true
    }

    @Test
    fun run() {
        DPAIRS.inputStr="3 2\n" +
                "10 1 100\n" +
                "4 3"
        DPAIRS().run()
    }
}