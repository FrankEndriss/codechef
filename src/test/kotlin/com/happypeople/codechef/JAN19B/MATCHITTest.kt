package com.happypeople.codechef.JAN19B

import org.junit.Before
import org.junit.Test
import java.util.*

class MATCHITTest {

    @Before
    fun setUp() {
        MATCHIT.printLog = true
    }

    fun rand(r: Random, min: Int, max: Int): Int {
        return r.nextInt(max - min) + min
    }

    var gN = 0
    var gM = 0
    var points = listOf<Pair<Int, Int>>()
    var V = arrayOf(IntArray(0))

    fun generateTestData(n: Int, m: Int, seed: Long) {
        val r = Random(seed)

        // create 2*m distinct points
        val pset = mutableSetOf<Pair<Int, Int>>()
        while (pset.size < 2 * m) {
            pset.add(Pair(rand(r, 1, n), rand(r, 1, n)))
        }
        points = pset.toList()

        V = Array(n) { IntArray(n) }
        for (y in 0..n - 1)
            for (x in 0..n - 1)
                V[y][x] = rand(r, -1000000000, 1000000000)

        gN = n
        gM = m
    }

    fun dataToString(): String {
        val sb = StringBuffer("")
        sb.append("$gN $gM\n")
        for (p in points)
            sb.append("${p.first} ${p.second}\n")
        for (y in V) {
            sb.append(y.joinToString(" "))
            sb.append("\n")
        }
        return sb.toString()
    }

    @Test
    fun runG() {
        generateTestData(666, 10, 42)
        // MATCHIT.printLog = false
        MATCHIT.inputStr = dataToString()
        val ts=System.currentTimeMillis()
        MATCHIT().run()
        val needed=System.currentTimeMillis()-ts
        println("needed: $needed")
    }

    @Test
    fun run1() {
        MATCHIT.inputStr = "4 2\n" +
                "1 1\n" +
                "2 2\n" +
                "3 3\n" +
                "4 4\n" +
                "1 -1 -1 -1\n" +
                "1 1 1 1\n" +
                "1 1 1 1\n" +
                "-1 -1 -1 1"
        MATCHIT().run()
    }

}