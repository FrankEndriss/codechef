package com.happypeople.codechef.LTIME69A

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ROLLBARTest {

    @Before
    fun setUp() {
        ROLLBAR.printLog=true
    }

    @Test
    fun run() {
        ROLLBAR.inputStr="2\n" +
                "2 4\n" +
                "1 1\n" +
                "1111\n" +
                "0111\n" +
                "2 4\n" +
                "1 1\n" +
                "1111\n" +
                "0011"
        ROLLBAR().run()
    }
}