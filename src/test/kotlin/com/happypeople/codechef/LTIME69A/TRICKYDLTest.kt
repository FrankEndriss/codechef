package com.happypeople.codechef.LTIME69A

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class TRICKYDLTest {

    @Before
    fun setUp() {
        TRICKYDL.printLog=true
    }

    @Test
    fun run() {
        TRICKYDL.inputStr="4\n" +
                "5\n" +
                "8\n" +
                "9\n" +
                "1000000000"
        TRICKYDL().run()
    }
}