package com.happypeople.codechef.LTIME69A

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CHFCHTest {

    @Before
    fun setUp() {
        CHFCH.printLog=true
    }

    @Test
    fun run() {
        CHFCH.inputStr="2\n" +
                "8 3\n" +
                "1 2 2 1 3 3 2 1\n" +
                "5 3\n" +
                "1 1 2 2 3"
        CHFCH().run()
    }
}