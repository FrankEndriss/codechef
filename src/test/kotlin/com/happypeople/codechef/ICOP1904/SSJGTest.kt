package com.happypeople.codechef.ICOP1904

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class SSJGTest {

    @Before
    fun setUp() {
        SSJG.printLog=true
    }

    @Test
    fun run() {
        SSJG.inputStr="    1\n" +
                "    6 13\n" +
                "    1 2\n" +
                "    1 3\n" +
                "    2 1\n" +
                "    2 4\n" +
                "    2 5\n" +
                "    3 2\n" +
                "    4 2\n" +
                "    4 3\n" +
                "    4 6\n" +
                "    5 1\n" +
                "    5 4\n" +
                "    5 5\n" +
                "    6 2"
        SSJG().run()
    }

    @Test
    fun run2() {
        SSJG.inputStr="1\n" +
                "4 4\n"+
                "1 1\n"+
                "2 2\n"+
                "1 3\n"+
                "1 4"
        SSJG().run()
    }
}