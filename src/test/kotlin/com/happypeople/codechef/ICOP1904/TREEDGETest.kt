package com.happypeople.codechef.ICOP1904

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class TREEDGETest {

    @Before
    fun setUp() {
        TREEDGE.printLog=true
    }

    @Test
    fun run() {
        TREEDGE.inputStr="1\n" +
                "7 3\n" +
                "1 2 1\n" +
                "1 3 -2\n" +
                "2 4 3\n" +
                "2 5 -4\n" +
                "5 7 5\n" +
                "3 6 6\n" +
                "2 3 1\n" +
                "5 4 2\n" +
                "5 6 0"
        TREEDGE().run()
    }
}