package com.happypeople.codechef.ICOP1904

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class JVPHOBIATest {

    @Before
    fun setUp() {
        JVPHOBIA.printLog=true
    }

    @Test
    fun run() {
        JVPHOBIA.inputStr="5 5\n" +
                "1 2 6\n" +
                "1 3 3\n" +
                "2 3 2\n" +
                "3 4 6\n" +
                "3 5 5\n" +
                "\n" +
                "2 5\n" +
                "1 5"
        JVPHOBIA().run()
    }
}