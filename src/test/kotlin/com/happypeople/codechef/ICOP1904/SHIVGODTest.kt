package com.happypeople.codechef.ICOP1904

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class SHIVGODTest {

    @Before
    fun setUp() {
        SHIVGOD.printLog=true
    }

    @Test
    fun run() {
        SHIVGOD.inputStr="   1\n" +
                "5 3 2\n" +
                "1 2 3 4 5"
        SHIVGOD().run()
    }

    @Test
    fun run2() {
        SHIVGOD.inputStr="1\n" +
                "5 3 2\n" +
                "1 2 3 4 3"
        SHIVGOD().run()
    }

    @Test
    fun run3() {
        SHIVGOD.inputStr="1\n" +
                "6 4 2\n" +
                "1 7 1 8 2 3"
        SHIVGOD().run()
    }

    @Test
    fun run4() {
        SHIVGOD.inputStr="3\n" +
                "6 4 2\n" +
                "6 5 5 5 5 5\n"+
                "6 4 2\n" +
                "6 5 6 5 5 5\n"+
                "6 4 2\n" +
                "5 5 5 5 5 6"
        SHIVGOD().run()
    }
}