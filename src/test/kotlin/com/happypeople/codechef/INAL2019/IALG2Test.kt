package com.happypeople.codechef.INAL2019

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class IALG2Test {

    @Before
    fun setUp() {
        IALG2.printLog=true
    }

    @Test
    fun run() {
        IALG2.inputStr="   10\n" +
                "   abo  AJKHY\n" +
                "   a    HSFSJ\n" +
                "   abce DHGTF\n" +
                "   abzaei HAJIU\n" +
                "   abaeiou ZJJHA\n" +
                "   abcs  AHJUG\n" +
                "   aghyr AGHJS\n" +
                "   agoiag FGHYD\n" +
                "   abgiuav VHSJU\n" +
                "   ab  VAGSH\n" +
                "   5\n" +
                "   a\n" +
                "   ab\n" +
                "   abc\n" +
                "   ag\n" +
                "   abd  "
        IALG2().run()
    }
}