package com.happypeople.codechef.INAL2019

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class IALG1Test {

    @Before
    fun setUp() {
        IALG1.printLog=true
    }

    @Test
    fun run() {
        IALG1.inputStr=" 3\n" +
                "   2 4\n" +
                "   15 15\n" +
                "   20 3"
        IALG1().run()
    }
}