package com.happypeople.codechef.INAL2019

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class IALG5Test {

    @Before
    fun setUp() {
        IALG5.printLog=true
    }

    @Test
    fun run() {
        IALG5.inputStr="5 17 5\n"
        IALG5().run()
    }
}