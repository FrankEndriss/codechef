package com.happypeople.codechef.INAL2019

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class IALG4Test {

    @Before
    fun setUp() {
        IALG4.printLog=true
    }

    @Test
    fun run() {
        IALG4.inputStr="   1\n" +
                "    15 3\n" +
                "    28 15 14 11 9 77 8 12 30 31 32 33 34 35 36"
        IALG4().run()
    }
}