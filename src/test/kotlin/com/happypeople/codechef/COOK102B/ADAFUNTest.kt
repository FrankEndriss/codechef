package com.happypeople.codechef.COOK102B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ADAFUNTest {

    @Before
    fun setUp() {
        ADAFUN.printLog=true
    }

    @Test
    fun run() {
        ADAFUN.inputStr="1\n" +
                "6\n" +
                "0 1 2 5 7 8"
        ADAFUN().run()
    }
}