package com.happypeople.codechef.COOK102B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ADAKNGTest {

    @Before
    fun setUp() {
        ADAKNG.printLog=true
    }

    @Test
    fun run() {
        ADAKNG.inputStr="1\n" +
                "1 3 1"
        ADAKNG().run()
    }
}