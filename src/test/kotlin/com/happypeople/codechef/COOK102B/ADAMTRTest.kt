package com.happypeople.codechef.COOK102B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ADAMTRTest {

    @Before
    fun setUp() {
        ADAMTR.printLog=true
    }

    @Test
    fun run() {
        ADAMTR.inputStr="1\n" +
                "3\n" +
                "1 2 3\n" +
                "4 5 6\n" +
                "7 8 9\n" +
                "1 2 7\n" +
                "4 5 8\n" +
                "3 6 9"
        ADAMTR().run()
    }
}