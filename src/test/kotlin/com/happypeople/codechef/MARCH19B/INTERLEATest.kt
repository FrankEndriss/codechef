package com.happypeople.codechef.MARCH19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test
import java.lang.Math.random

class INTERLEATest {

    @Before
    fun setUp() {
        INTERLEA.printLog=true
    }

    @Test
    fun run() {
        INTERLEA.inputStr="3\n" +
                "123\n" +
                "456\n" +
                "789"
        INTERLEA().run()
    }

    @Test
    fun run2() {
        INTERLEA.inputStr="3\n" +
                "456\n" +
                "123\n" +
                "789"
        INTERLEA().run()
    }

    @Test
    fun run3() {
        INTERLEA.inputStr="3\n" +
                "123\n" +
                "129\n" +
                "127"
        INTERLEA().run()
    }

    @Test
    fun run4() {
        var s="9\n"

        for(i in 1..9) {
            for (j in 1..9) {
                val c = (random() * 10).toInt();
                s = s + c
            }
            s=s+"\n"
        }
        INTERLEA.inputStr=s
        INTERLEA().run()
    }
}