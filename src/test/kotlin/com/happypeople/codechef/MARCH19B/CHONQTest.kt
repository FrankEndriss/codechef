package com.happypeople.codechef.MARCH19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CHONQTest {

    @Before
    fun setUp() {
        CHONQ.printLog=true
    }

    @Test
    fun run() {
        CHONQ.inputStr="1\n" +
                "3 3\n" +
                "1 2 3"
        CHONQ().run()
    }

    @Test
    fun run2() {
        CHONQ.inputStr="1\n" +
                "3 3\n" +
                "2 4 3"
        CHONQ().run()
    }
}