package com.happypeople.codechef.MARCH19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class SUBPRNJLTest {

    @Before
    fun setUp() {
        SUBPRNJL.printLog=true
    }

    @Test
    fun run() {
        SUBPRNJL.inputStr="1\n" +
                "3 3\n" +
                "1 3 2"
        SUBPRNJL().run()
    }

    @Test
    fun run2() {
        SUBPRNJL.inputStr="1\n" +
                "5 17\n" +
                "3 3 2 2 3"
        SUBPRNJL().run()
    }
}