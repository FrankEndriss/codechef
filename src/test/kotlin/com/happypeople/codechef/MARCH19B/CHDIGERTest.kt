package com.happypeople.codechef.MARCH19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CHDIGERTest {

    @Before
    fun setUp() {
        CHDIGER.printLog=true
    }

    @Test
    fun run() {
        CHDIGER.inputStr="3\n" +
                "35 4\n" +
                "42 4\n" +
                "2345678911332 5"
        CHDIGER().run()
    }
}