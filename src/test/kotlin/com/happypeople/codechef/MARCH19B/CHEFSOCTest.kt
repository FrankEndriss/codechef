package com.happypeople.codechef.MARCH19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CHEFSOCTest {

    @Before
    fun setUp() {
        CHEFSOC.printLog=true
    }

    @Test
    fun run() {
        CHEFSOC.inputStr="4\n" +
                "4\n" +
                "1 1 1 1\n" +
                "3\n" +
                "2 2 2\n" +
                "4\n" +
                "1 2 1 1\n" +
                "4\n"+
                "2 2 1 1\n"
        CHEFSOC().run()
    }

    @Test
    fun run2() {
        CHEFSOC2.inputStr="4\n" +
                "4\n" +
                "1 1 1 1\n" +
                "3\n" +
                "2 2 2\n" +
                "4\n" +
                "1 2 1 1\n" +
                "4\n"+
                "2 2 1 1\n"
        CHEFSOC2().run()
    }

    @Test
    fun run3() {
        CHEFSOC2.printLog=true
        CHEFSOC2.inputStr="1\n" +
                "3\n" +
                "2 2 2\n"
        CHEFSOC2().run()
    }

    @Test
    fun run4() {
        CHEFSOC2.printLog=true
        CHEFSOC2.inputStr="1\n" +
                "4\n" +
                "2 2 1 1\n"
        CHEFSOC2().run()
    }

}