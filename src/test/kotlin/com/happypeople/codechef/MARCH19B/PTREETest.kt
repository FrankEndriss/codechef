package com.happypeople.codechef.MARCH19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class PTREETest {

    @Before
    fun setUp() {
        PTREE.printLog=true
    }

    @Test
    fun run() {
        PTREE.inputStr="1\n" +
                "6 3\n" +
                "1 2\n" +
                "1 3\n" +
                "2 4\n" +
                "2 5\n" +
                "3 6\n" +
                "1 1\n" +
                "10 10\n" +
                "5 5"
        PTREE().run()
    }

    @Test
    fun run2() {
        PTREE.inputStr="1\n" +
                "9 3\n" +
                "1 2\n" +
                "1 3\n" +
                "2 4\n" +
                "2 5\n" +
                "3 6\n" +
                "4 7\n" +
                "5 8\n" +
                "6 9\n" +
                "3 3\n" +
                "${21 xor 3} 362\n" +
                "${293761 xor 2} 5"
        PTREE().run()
        // 21
        // 293761
        // 494896756

        // should be
        // 21
        // 293761
        // 68736890
    }

    @Test
    fun geomSumPerf() {
        val sut=PTREE()
        var sum=0
        for(i in 1..100000) {
            sum=PTREE.Inv.plus(sum, sut.geomSum(123, i, PTREE.Inv.MOD))
        }
    }

    @Test
    fun geomSum() {
        val sut=PTREE()
        val r1=sut.geomSum(3, 33, PTREE.Inv.MOD)
        val r2=sut.geomSum_simple(3, 33)
        assertEquals("same1 ", r1, r2)
        val r3=sut.geomSum(3, 22, PTREE.Inv.MOD)
        val r4=sut.geomSum_simple(3, 22)
        assertEquals("same2 ", r3, r4)
    }

    @Test
    fun run3() {
        PTREE.inputStr="3\n" +
                "6 3\n" +
                "1 2\n" +
                "1 3\n" +
                "2 4\n" +
                "2 5\n" +
                "3 6\n" +
                "1 1\n" +
                "10 10\n" +
                "5 5\n" +
                "9 3\n" +
                "1 2\n" +
                "1 3\n" +
                "2 4\n" +
                "2 5\n" +
                "2 9\n" +
                "3 6\n" +
                "3 7\n" +
                "3 8\n" +
                "1 1\n" +
                "10 10\n" +
                "7 7\n" +
                "10 2\n" +
                "1 2\n" +
                "2 3\n" +
                "3 4\n" +
                "4 5\n" +
                "1 6\n" +
                "6 7\n" +
                "7 8\n" +
                "8 9\n" +
                "9 10\n" +
                "1 1\n" +
                "33 1\n"
        PTREE().run()
    }
}