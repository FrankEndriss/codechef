package com.happypeople.codechef.MARCH19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class JAINTest {

    @Before
    fun setUp() {
        JAIN.printLog=true
    }

    @Test
    fun run() {
        JAIN.inputStr="2\n" +
                "3\n" +
                "aaooaoaooa\n" +
                "uiieieiieieuuu\n" +
                "aeioooeeiiaiei\n"+
               "7\n" +
                       "aeo\n" +
                       "aeo\n" +
                       "aeiou\n" +
                       "aeiou\n" +
                       "aeiou\n" +
                       "aeiou\n" +
                       "aeiou\n"
        JAIN().run()
    }
}