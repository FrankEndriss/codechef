package com.happypeople.codechef.CONE2018

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class AE04Test {

    @Before
    fun setUp() {
        AE04.printLog=true
    }

    @Test
    fun run() {
        AE04.inputStr="8 4 2\n" +
                "5 9 9 2 4 4 5 4\n" +
                "1 4 2\n" +
                "1 4 3\n" +
                "1 8 7\n" +
                "2 3 8"
        AE04().run()
    }

    @Test
    fun run2() {
        AE04.inputStr="3 1 2\n" +
                "1 2 4\n"+
                "1 1 1"
        AE04().run()
    }
}