package com.happypeople.codechef.CONE2018

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class SATest {

    @Before
    fun setUp() {
        SA.printLog=true
    }

    @Test
    fun run() {
        SA.inputStr="2\n" +
                "3\n" +
                "2 3 1\n" +
                "4\n" +
                "2 3 6 1"
        SA().run()
    }
}