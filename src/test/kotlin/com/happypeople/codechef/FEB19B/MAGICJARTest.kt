package com.happypeople.codechef.FEB19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class MAGICJARTest {

    @Before
    fun setUp() {
        MAGICJAR.printLog=true
    }

    @Test
    fun run() {
        MAGICJAR.inputStr="2   \n" +
                "4   \n" +
                "1 1 1 1   \n" +
                "2   \n" +
                "1 4   "
        MAGICJAR().run()
    }
}