package com.happypeople.codechef.FEB19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class ARTBALANTest {

    @Before
    fun setUp() {
        ARTBALAN.printLog=true
    }

    @Test
    fun run() {
        ARTBALAN.inputStr="8\n" +
                "ABCB\n" +
                "BBC\n" +
                "ABCCD\n" +
                "BBCD\n" +
                "XXXXABCDE\n" +
                "ABCDEXXXX\n" +
                "ABXCXDXE\n" +
                "ABBCDEFGHIBAJKLMNOPQRSTUVWXYZA\n"
        ARTBALAN().run()
    }

    @Test
    fun run2() {
        ARTBALAN.inputStr="1\n" +
                str(25, 'A')+
                str(26, 'B')+
                str(17, 'C')+
                str(1, 'D')
        ARTBALAN().run()
    }

    @Test
    fun run3() {
        ARTBALAN.inputStr="2\n" +
                str(1000, 'A')+"BCDDDDEFGHIJKLMNOPPPPPQRSTUVWXYZ\n"+
                str(234, 'X')+str(237, 'B')+"DD\n"
        ARTBALAN().run()
    }

    private fun  str(i:Int, c:Char):String {
        var ret=StringBuffer("")
        while(ret.length<i)
            ret.append(c)
        return ret.toString()
    }
}