package com.happypeople.codechef.FEB19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class DEPCHEFTest {

    @Before
    fun setUp() {
        DEPCHEF.printLog=true
    }

    @Test
    fun run() {
        DEPCHEF.inputStr="2\n" +
                "4\n" +
                "1 1 4 1\n" +
                "3 4 2 1\n" +
                "7\n" +
                "5 4 5 4 5 4 5\n" +
                "3 2 4 7 2 5 9"
        DEPCHEF().run()
    }
}