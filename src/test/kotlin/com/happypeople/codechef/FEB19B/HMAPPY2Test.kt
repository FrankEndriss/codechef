package com.happypeople.codechef.FEB19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class HMAPPY2Test {

    @Before
    fun setUp() {
        HMAPPY2.printLog=true
    }

    @Test
    fun run() {
        HMAPPY2.inputStr="1\n" +
                "6 2 3 3"
        HMAPPY2().run()
    }
}