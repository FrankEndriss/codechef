package com.happypeople.codechef.FEB19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CHEFINGTest {

    @Before
    fun setUp() {
        CHEFING.printLog=true
    }

    @Test
    fun run() {
        CHEFING.inputStr="2\n" +
                "3\n" +
                "abcaa\n" +
                "bcbd\n" +
                "bgc\n" +
                "3\n" +
                "quick\n" +
                "brown\n" +
                "fox"
        CHEFING().run()
    }
}