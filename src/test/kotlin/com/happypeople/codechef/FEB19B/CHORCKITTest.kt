package com.happypeople.codechef.FEB19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CHORCKITTest {

    @Before
    fun setUp() {
        CHORCKIT.printLog=true
    }

    @Test
    fun run() {
        CHORCKIT.inputStr="6 3 2 13 3\n" +
                "AFBBet\n" +
                "BBc 6\n" +
                "cp 4\n" +
                "A 3\n" +
                "EE 3\n" +
                "Bc 1"
        CHORCKIT().run()
    }
}