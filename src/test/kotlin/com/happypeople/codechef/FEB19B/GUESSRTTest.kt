package com.happypeople.codechef.FEB19B

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class GUESSRTTest {

    @Before
    fun setUp() {
        GUESSRT.printLog = true
    }

    @Test
    fun run() {
        GUESSRT.inputStr = "3\n" +
                "5 9 1\n" +
                "7 9 2\n" +
                "3 20 3"
        GUESSRT().run()
        // results:
        // 400000003
        // 196428573
        // 555555560
    }

    @Test
    fun run2() {
        val t=123   // 100000
        GUESSRT.inputStr = "${t+3}\n" +
                "5 9 10\n" +
                "7 7 20\n" +
                "40 20 100\n"
        for (i in 1..t) {
            GUESSRT.inputStr += "3 ${i/4+100} ${i/4 + 1123}\n"
        }
        GUESSRT().run()
    }

    @Test
    fun run3() {
        GUESSRT.inputStr = "3\n" +
                "4 3 2\n"+
                "4 3 3\n"+
                "1 1 1\n"
        GUESSRT().run()

    }

    @Test
    fun inv1() {
        val expected1 = GUESSRT.Inv.fraction(22, 112)
        val expected2 = GUESSRT.Inv.fraction(11, 56)

        assertEquals("should be same", expected1, expected2)

        val f1 = GUESSRT.Inv.fraction(1, 7)
        val f2 = GUESSRT.Inv.fraction(6, 7)
        val f3 = GUESSRT.Inv.fraction(1, 16)
        val fResult = GUESSRT.Inv.plus(f1, GUESSRT.Inv.mul(f2, f3))
        assertEquals("11/56", fResult, expected1)
    }

    @Test
    fun inv2() {
        val f1 = GUESSRT.Inv.fraction(1, 7)
        val f6 = GUESSRT.Inv.fraction(6, 7)
        val f7 = GUESSRT.Inv.fraction(7, 7)
        val one = GUESSRT.Inv.fraction(1, 1)
        assertEquals("7/7 == 1/1", f7, one)
        val fResult = GUESSRT.Inv.plus(f1, f6)
        assertEquals("7/7 == 1/7 + 6/7", f7, fResult)

        val expected = GUESSRT.Inv.fraction(6, 7)
        val fResultMinus = GUESSRT.Inv.plus(f7, -f1)
        assertEquals("7/7 - 1/7 == 6/7", expected, fResultMinus)
    }

    @Test
    fun inv3() {
        val f1 = GUESSRT.Inv.fraction(1, 6)
        val f1_p2 = GUESSRT.Inv.mul(f1, f1)
        var res1 = 1
        for (i in 1..100) {
            res1 = GUESSRT.Inv.mul(res1, f1)
        }

        var res2 = 1
        for (i in 1..50) {
            res2 = GUESSRT.Inv.mul(res2, f1_p2)
        }

        assertEquals("1/6 pow 100", res1, res2)

        var res3 = GUESSRT.Inv.toPower(f1, 100).toInt()
        assertEquals("1/6 pow 100", res1, res3)


    }

}