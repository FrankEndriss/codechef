package com.happypeople.codechef.FEB19B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class MANRECTTest {

    @Before
    fun setUp() {
        MANRECT.printLog=true
    }

    @Test
    fun run() {
        MANRECT.fakeServer=true
        MANRECT.targetX1X=1
        MANRECT.targetX1Y=2
        MANRECT.targetX2X=3
        MANRECT.targetX2Y=4
        MANRECT().run()
    }

    @Test
    fun run2() {
        MANRECT.fakeServer=true
        MANRECT.targetX1X=1
        MANRECT.targetX1Y=1
        MANRECT.targetX2X=2
        MANRECT.targetX2Y=3
        MANRECT().run()
    }

    @Test
    fun run3() {
        MANRECT.fakeServer=true
        MANRECT.targetX1X=0
        MANRECT.targetX1Y=0
        MANRECT.targetX2X=30
        MANRECT.targetX2Y=10
        MANRECT().run()
    }

    @Test
    fun run4() {
        MANRECT.fakeServer=true
        MANRECT.targetX1X=0
        MANRECT.targetX1Y=800000000
        MANRECT.targetX2X=0
        MANRECT.targetX2Y=900000000
        MANRECT().run()
    }
}