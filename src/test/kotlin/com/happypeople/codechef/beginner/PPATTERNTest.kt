package com.happypeople.codechef.beginner

import com.happypeople.codechef.LTIME67B.PPATTERN
import org.junit.Before
import org.junit.Test

class PPATTERNTest {

    @Before
    fun setUp() {
        PPATTERN.printLog=true
    }

    @Test
    fun run() {
        PPATTERN.inputStr="2\n4\n5\n"
        PPATTERN().run()
    }
}