package com.happypeople.codechef.COOK104A

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class MARCAPSTest {

    @Before
    fun setUp() {
        MARCAPS.printLog=true
    }

    @Test
    fun run() {
        MARCAPS.inputStr="2\n" +
                "9\n" +
                "1 1 1 2 2 2 3 3 3\n" +
                "2\n" +
                "1 1"
        MARCAPS().run()
    }
}