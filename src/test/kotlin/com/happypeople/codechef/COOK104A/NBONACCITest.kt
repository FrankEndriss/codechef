package com.happypeople.codechef.COOK104A

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class NBONACCITest {

    @Before
    fun setUp() {
        NBONACCI.printLog=true
    }

    @Test
    fun run() {
        NBONACCI.inputStr="3 4\n" +
                "0 1 2\n" +
                "7\n" +
                "2\n" +
                "5\n" +
                "1000000000"
        NBONACCI().run()
    }
}