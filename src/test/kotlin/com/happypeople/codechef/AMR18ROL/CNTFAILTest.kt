package com.happypeople.codechef.AMR18ROL

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*

class CNTFAILTest {

    @Before
    fun setUp() {
        CNTFAIL.printLog=true
    }

    @Test
    fun run() {
        CNTFAIL.inputStr="2\n" +
                "4\n" +
                "3 2 2 2\n"+
                "3\n"+
                "1 2 1"

        CNTFAIL().run()
    }
}