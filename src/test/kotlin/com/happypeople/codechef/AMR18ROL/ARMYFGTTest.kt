package com.happypeople.codechef.AMR18ROL

import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import java.io.PrintWriter
import java.io.StringWriter

class ARMYFGTTest {

    var grabbedOutput: StringWriter?=null

    @Before
    fun setUp() {
        ARMYFGT.printLog=true
        grabbedOutput= StringWriter()
        ARMYFGT.grabResult=PrintWriter(grabbedOutput)
    }

    @Test
    fun run1() {
        ARMYFGT.inputStr="1\n" +
                "2\n" +
                "2 4\n" +
                "4 8"
        ARMYFGT().run()
        assertEquals("5,6,7", "3\r\n", grabbedOutput?.toString())
    }

    @Test
    fun run2() {
        ARMYFGT.inputStr="1\n" +
                "3\n" +
                "2 3 4\n" +
                "10 13"
        ARMYFGT().run()
        assertEquals("10,11,13", "3\r\n", grabbedOutput?.toString())
    }

    @Test
    fun run3() {
        ARMYFGT.inputStr="1\n" +
                "3\n" +
                "2 6 8\n" +
                "20 29"
        ARMYFGT().run()
        assertEquals("20,21,22,23,25,26,27,28,29", "9\r\n", grabbedOutput?.toString())
    }

    @Test
    fun run4() {
        ARMYFGT.inputStr="1\n" +
                "2\n" +
                "2 3\n" +
                "6 12"
        ARMYFGT().run()
        assertEquals("7,8,9,10,11", "5\r\n", grabbedOutput?.toString())
    }

    @Test
    fun run5() {
        ARMYFGT.inputStr="1\n" +
                "2\n" +
                "2 3\n" +
                "6 11"
        ARMYFGT().run()
        assertEquals("7,8,9,10,11", "5\r\n", grabbedOutput?.toString())
    }

    @Test
    fun run6() {
        ARMYFGT.inputStr="1\n" +
                "2\n" +
                "2 3\n" +
                "6 13"
        ARMYFGT().run()
        assertEquals("7,8,9,10,11,13", "6\r\n", grabbedOutput?.toString())
    }

    @Test
    fun run7() {
        ARMYFGT.inputStr="1\n" +
                "2\n" +
                "2 3\n" +
                "7 11"
        ARMYFGT().run()
        assertEquals("7,8,9,10,11", "5\r\n", grabbedOutput?.toString())
    }

    @Test
    fun run8() {
        ARMYFGT.inputStr="1\n" +
                "1\n" +
                "1\n" +
                "7 11"
        ARMYFGT().run()
        assertEquals("", "0\r\n", grabbedOutput?.toString())
    }

    @Test
    fun run9() {
        ARMYFGT.inputStr="1\n" +
                "2\n" +
                "4 4\n" +
                "4 8"
        ARMYFGT().run()
        assertEquals("5, 6, 7", "3\r\n", grabbedOutput?.toString())
    }

}