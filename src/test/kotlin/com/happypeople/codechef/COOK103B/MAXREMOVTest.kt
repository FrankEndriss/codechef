package com.happypeople.codechef.COOK103B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class MAXREMOVTest {

    @Before
    fun setUp() {
        MAXREMOV.printLog=true
    }

    @Test
    fun run() {
        MAXREMOV.inputStr="1\n" +
                "3 2\n" +
                "2 6\n" +
                "4 9\n" +
                "1 4"
        MAXREMOV().run()
    }

    @Test
    fun binsearch() {
        val list=listOf(1, 2, 3, 4, 7)
        val result=(0..10).map { list.binarySearch(it) }
        println("result: $result")
    }

}