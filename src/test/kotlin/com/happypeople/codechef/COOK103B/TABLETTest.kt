package com.happypeople.codechef.COOK103B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class TABLETTest {

    @Before
    fun setUp() {
        TABLET.printLog=true
    }

    @Test
    fun run() {
        TABLET.inputStr="3\n" +
                "3 6\n" +
                "3 4 4\n" +
                "5 5 7\n" +
                "5 2 5\n" +
                "2 6\n" +
                "3 6 8\n" +
                "5 4 9\n" +
                "1 10\n" +
                "5 5 10"
        TABLET().run()
    }
}