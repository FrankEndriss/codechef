package com.happypeople.codechef.COOK103B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class SECPASSTest {

    @Before
    fun setUp() {
        SECPASS.printLog=true
    }

    @Test
    fun run() {
        SECPASS.inputStr="3\n" +
                "3\n" +
                "abc\n" +
                "5\n" +
                "thyth\n" +
                "5\n" +
                "abcbc"
        SECPASS().run()
    }
}