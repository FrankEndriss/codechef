package com.happypeople.codechef.COOK103B

import org.junit.Assert.*

import org.junit.Before
import org.junit.Test

class CHFPARTYTest {

    @Before
    fun setUp() {
        CHFPARTY.printLog=true
    }

    @Test
    fun run() {
        CHFPARTY.inputStr="3\n" +
                "2\n" +
                "0 0\n" +
                "6\n" +
                "3 1 0 0 5 5\n" +
                "3\n" +
                "1 2 3"
        CHFPARTY().run()
    }
}