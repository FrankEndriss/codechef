package com.happypeople.codechef.LTIME69A

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        TRICKYDL().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class TRICKYDL {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for (i in (1..T)) {
            val A=sc.nextLong()
            val d=mutableListOf<Int>()

            var sum1=0L
            var sum2=0L
            var maxSum=-1L
            var maxAt=-1
            for(i in 1..40) {
                sum1+=A
                sum2+=(1L shl (i-1))
                //log("$sum2")
                if(sum2 < sum1)
                    d.add(i)
                if(maxSum<sum1-sum2) {
                    maxSum=sum1-sum2
                    maxAt=i
                }
            }
            println("${d.max()} $maxAt")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
