package com.happypeople.codechef.LTIME69A

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        ROLLBAR().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

inline fun <reified E> arrOf(size:Int, init:(Int)-> E ):Array<E> {
    val a= arrayOfNulls<E>(size)
    for(i in a.indices)
        a[i]=init(i)
    return a as Array<E>
}

var grid = listOf<List<Boolean>>()
var N = 0
var M = 0

class ROLLBAR {
    enum class Dir(val x:Int, val y:Int, val idx:Int) {
        VERT(0, 0, 0),
        N(0, 1, 1),
        S(0, -1, 2),
        E(-1, 0, 3),
        W(1, 0, 4)
    }

    data class Cube(val posX:Int, val posY:Int, val dir:Dir) {
        fun ok(posX:Int, posY:Int, dir:Dir): Boolean {
            return min(posX+dir.x, posX)>= 0 && min(posY+dir.y, posY) >= 0 && max(posX, posX+dir.x) < N && max(posY, posY+dir.y) < M && grid[posX+dir.x][posY+dir.y] && grid[posX][posY]
        }

        fun calcMove(pDir:Dir): Cube? {
            var nDir=when(dir) {
                Dir.VERT -> pDir
                Dir.N, Dir.S -> if(pDir==Dir.E||pDir==Dir.W) dir else Dir.VERT
                Dir.E, Dir.W -> if(pDir==Dir.N||pDir==Dir.S) dir else Dir.VERT
            }
            val nX=if(dir.x==pDir.x) posX+dir.x+pDir.x else posX+pDir.x
            val nY=if(dir.y==pDir.y) posY+dir.y+pDir.y else posY+pDir.y
            if(ok(nX, nY, nDir))
                return Cube(nX, nY, nDir)
            else
                return null
        }
    }

    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        for (i in (1..T)) {
            N = sc.nextInt()
            M = sc.nextInt()
            val x = sc.nextInt() - 1
            val y = sc.nextInt() - 1
            grid = (1..N).map { sc.next().trim().map { it == '1' } }

            val steps = grid.map { it.map { -1 }.toMutableList() }
            //val checkedCubes = mutableSetOf(Cube(x, y, Dir.VERT))
            val checkedCubes = arrOf(N) { arrOf(M) { BooleanArray(5) }}
            steps[x][y] = 0
            val dirs = setOf(Dir.N, Dir.S, Dir.E, Dir.W)
            var next = setOf(Cube(x, y, Dir.VERT))

            var step = 1
            while (next.size > 0) {
                val nextnext = mutableSetOf<Cube>()
                for (cube in next) {
                    dirs.map { cube.calcMove(it) }.filterNotNull().forEach {
                        if (!checkedCubes[it.posX][it.posY][it.dir.ordinal]) {
                            nextnext.add(it)
                            checkedCubes[it.posX][it.posY][it.dir.ordinal]=true
                        }
                    }
                }
                for (cube in nextnext) {
                    if (cube.dir == Dir.VERT && steps[cube.posX][cube.posY] == -1)
                        steps[cube.posX][cube.posY] = step
                }
                //log("nextnexst: $nextnext")
                //log("steps: $steps")
                step++
                next = nextnext
            }
            val sb=StringBuffer()
            for (i in (0..N - 1)) {
                for (j in (0..M - 1)) {
                    sb.append("${steps[i][j]} ")
                }
                sb.append("\n")
            }
            print(sb.toString())
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
