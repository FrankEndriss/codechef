package com.happypeople.codechef.LTIME69A

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        CHFCH().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class CHFCH {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for (i in (1..T)) {
            val N=sc.nextInt()
            val K=sc.nextInt()
            val a=(1..N).map { sc.nextInt() }
            val A=Array(300001) { mutableListOf<Int>()}
            for(i in a.indices)
                A[a[i]].add(i)
            var minSum=Int.MAX_VALUE
            for(i in A) {
                if(i.size>=K) {
                    var sum=0
                    // i.sort()

                    //log("indexes: $i")
                    for(idx in 0..i.size-K)
                        sum=calcMinSteps(i, idx, K)
                    //log("sum: $sum")

                    minSum=min(minSum, sum)
                }
            }
            if(minSum==Int.MAX_VALUE)
                minSum=-1
            println("$minSum")
        }
    }

    // k positions in array i, starting at idx.
    // Calc how much moves are necessary to move these positions to be adjacent.
    // Use average-method to do so. (move all positions to average position)
    private fun calcMinSteps(list: MutableList<Int>, idx: Int, k: Int): Int {
        var ret=Int.MAX_VALUE
        for(iIdx in idx..idx+k-1) {
            var i=iIdx

            //log("idx of biggest right to left=$i")

            var steps = 0L
            var j = i + 1 // remind for later
            //log("idx of smallest left to right=$j")
            var ii = list[i]
            var iii=ii+1
            while (i >= 0) {
                //log("move ${list[i]} to $ii")
                steps += ii - list[i]
                ii--
                i--
            }

            while (j < idx + k) {
                //log("move ${list[j]} to $iii")
                steps += list[j] - iii
                iii++
                j++
            }

            ret=min(ret, steps.toInt())
        }
        return ret
    }


    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
