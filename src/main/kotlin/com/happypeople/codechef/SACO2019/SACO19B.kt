package com.happypeople.codechef.SACO2019

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        SACO19B().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class SACO19B {
    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        for (i in 1..T) {
            val N = sc.nextInt() // pocket count
            val K = sc.nextInt() // min halluci
            val h = (1..N).map { sc.nextInt() }

            var sSize = 0
            var sStart= -1
            var gCount=0
            val kPos = mutableListOf<Int>()
            for (i in h.indices) {
                if(sStart<0 && h[i]>=K)
                    sStart=i

                if (h[i] == K) {
                    kPos.add(i - sStart)
                    sSize++
                }

                if (h[i] < K) {
                    var lastK=0
                    for (j in kPos) {
                        gCount += ((j + 1-lastK) * (sSize - j))
                        //log("1 gCount $gCount")
                        lastK=j+1
                    }
                    kPos.clear()
                    sSize = 0
                    sStart = -1
                }
                if (h[i]>K) {
                    sSize++
                }
                //log("$kPos")
            }
            var lastK=0
            for(j in kPos) {
                gCount += ((j + 1-lastK) * (sSize - j))
                //log("2 gCount $gCount j=$j lastK=$lastK sSize=$sSize")
                lastK=j+1
            }
            // calc gCount
            println("$gCount")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
