package com.happypeople.codechef.SACO2019

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        SACO19A().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class SACO19A {
    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        val N = sc.nextInt()
        val Q = sc.nextInt()
        val s = arrayOfNulls<MutableList<String>>(N + 1)
        for (i in s.indices)
            s[i] = mutableListOf<String>()
        for (i in 0..T - 1) {
            for (j in 1..(N * 3)) {
                val x = sc.nextInt()
                val name = sc.next().trim()
                val leader = sc.next().trim()
                if (leader == "N")
                    s[x]!!.add(name)
                else {
                    s[x]!!.add(0, name)
                }
                if (s[x]!!.size == 3 && s[x]!![1] > s[x]!![2]) {
                    val tmp = s[x]!![1]
                    s[x]!![1] = s[x]!![2]
                    s[x]!![2] = tmp
                }
            }

            for (i in 0..Q - 1) {
                val x = sc.nextInt()
                val y = sc.nextInt()  // seat number 1, 2, 3
                println("${s[x]!![y - 1]}")
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
