package com.happypeople.codechef.COOK103B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        SECPASS().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class SECPASS {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for (i in (1..T)) {
            val N=sc.nextInt()
            val s=sc.next().trim()

            var idx=0
            var firstIdx=s.mapIndexed { index, c -> Pair(index, c)  }
                    .filter{ it.second==s[0]}.map { it.first }
            val count=firstIdx.size
            while(firstIdx.size==count && count>1) {
                idx++
                firstIdx=firstIdx.filter{ it+idx<s.length && s[it+idx]==s[idx] }
            }
            if(count==1)
                println("$s")
            else
                println("${s.substring(0, idx)}")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
