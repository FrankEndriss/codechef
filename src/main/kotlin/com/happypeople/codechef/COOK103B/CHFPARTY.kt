package com.happypeople.codechef.COOK103B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        CHFPARTY().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class CHFPARTY {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for (i in 1..T) {
            val N=sc.nextInt()
            val a=(1..N).map { sc.nextInt() }.sorted()
            //log("a=$a")

            var friends=0
            for(i in a.indices) {
                if (friends >= a[i])
                    friends++
            }

            println("$friends")
        }


    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
