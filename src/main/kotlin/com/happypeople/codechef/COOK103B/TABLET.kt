package com.happypeople.codechef.COOK103B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        TABLET().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class TABLET {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        (1..T).forEach {
            val N=sc.nextInt()
            val B=sc.nextInt()
            var maxArea=0
            (1..N).forEach {
                val w=sc.nextInt()
                val h=sc.nextInt()
                val p=sc.nextInt()
                if(p<=B)
                    maxArea=max(maxArea, w*h)
            }
            if(maxArea==0)
                println("no tablet")
            else
                println("$maxArea")

        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
