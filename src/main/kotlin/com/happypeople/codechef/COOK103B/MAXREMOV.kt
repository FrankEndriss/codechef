package com.happypeople.codechef.COOK103B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        MAXREMOV().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class MAXREMOV {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for (i in (1..T)) {
            val N=sc.nextInt()
            val K=sc.nextInt()
            val c=IntArray(100001)
            val rangesL=IntArray(N+1)
            val rangesR=IntArray(N+1)
            for (i in (1..N)) {
                val l=sc.nextInt()
                val r=sc.nextInt()
                c[l]++
                c[r+1]--
                rangesL[i]=l
                rangesR[i]=r
            }
            val prefixSum=IntArray(100001)
            for(i in 1..100000) {
                prefixSum[i]=prefixSum[i-1]+c[i]
            }
            //log("prefixSum=${prefixSum.toList()}")

            val indexes1=prefixSum.mapIndexed { index, i -> Pair(index, i) }.filter { it.second==K+1 }.map { it.first }
            //log("indexes1=$indexes1")
            val indexes0=prefixSum.mapIndexed { index, i -> Pair(index, i) }.filter { it.second==K }.map { it.first }
            //log("indexes0=$indexes0")

            // find range(l, r) which includes most of indexes1 minus indexes0
            var maxCount=Int.MIN_VALUE
            for(i in 1..rangesL.size-1) {
                //log("range: ${rangesL[i]} ${rangesR[i]}")

                // lowest i so that indexes1[i] is included in range(l, r), or indexes.size if none
                var lowerBound1=indexes1.binarySearch(rangesL[i])
                if(lowerBound1<0)
                    lowerBound1=abs(lowerBound1+1)
                //log("lowerBound1=$lowerBound1")

                // highes i so that indexes1[i] is included in range(l, r), or -1 if none
                var upperBound1=indexes1.binarySearch(rangesR[i])
                if(upperBound1<0)
                    upperBound1=abs(upperBound1+1)
                //log("upperBound1=$upperBound1")

                var lowerBound0=indexes0.binarySearch(rangesL[i])
                if(lowerBound0<0)
                    lowerBound0=abs(lowerBound0+1)
                //log("lowerBound0=$lowerBound0")

                var upperBound0=indexes0.binarySearch(rangesR[i])
                if(upperBound0<0)
                    upperBound0=abs(upperBound0+1)
                //log("upperBound0=$upperBound0")

                val diff=max(0, (upperBound1-lowerBound1))-max(0, (upperBound0-lowerBound0))    // count increases by diff if remove range[i]
                //log("diff=$diff")

                if(diff>maxCount)
                    maxCount=diff
            }

            val ans=indexes0.size+maxCount
            println("$ans")
        }
    }

    fun bins(list:List<Int>, e:Int, correction:Int):Int {
        val ret=list.binarySearch(e)
        if(ret<-1) return abs(ret+1) else return ret
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
