package com.happypeople.codechef.COOK103B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        EVILAND().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class EVILAND {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for (i in (1..T)) {
            val N=sc.nextInt()
            val M=sc.nextInt()
            val F=sc.nextInt()
            val P=(1..N).map { sc.nextInt() }

            // find minimum number of P[i] to remove so that
            // there is no multiplication of any P[i] possible
            // so that we reach F+(n*M) for any n including 0.

        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
