package com.happypeople.codechef.FEB19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        ARTBALAN().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class ARTBALAN {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        val C='Z'-'A'+1     // number of chars
        for(i in 1..T) {
            val s=sc.next().trim()

            val f=IntArray(C)
            for(c in s)
                f['Z'-c]++

            // possible numbers of different chars
            val differentChars= (1..C).filter { s.length % it ==0 }
            var ans=Int.MAX_VALUE
            //log("s.len=${s.length} $possibleChars")
            for(k in differentChars) {
                // We need to find the chars in s with freq nearest to occPerChar, and sum the diff to occPerChar
                val occPerChar=s.length/k
                // log("k=$k occPerChar=$occPerChar")
                var occ=f.map { it-occPerChar }.sortedBy { -it }.take(k)
                //var occ=f.filter { it!=0 }.sortedWith( compareBy({ abs(occPerChar-it) }, { -it } ) ).take(k).toMutableList()
                //while(occ.size<k)
                //    occ.add(0)
                //val changesNeeded=occ.map { it-occPerChar }
                val cPos=occ.filter { it>0 }.sum()
                val cNeg=occ.filter { it<0 }.sum()
                val changes=max(abs(cPos), abs(cNeg))
                //log("s=$s k=$k cPos=$cPos cNeg=$cNeg occ=$occ changes=$changes")

                ans=min(ans, changes)
                //log("k=$k changes=$changes minChanges=$ans")
            }
            println("$ans")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
