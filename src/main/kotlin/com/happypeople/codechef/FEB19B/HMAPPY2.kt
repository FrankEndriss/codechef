package com.happypeople.codechef.FEB19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        HMAPPY2().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class HMAPPY2 {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for(i in 1..T) {
            val N=sc.nextLong()
            val A=sc.nextLong()
            val B=sc.nextLong()
            val K=sc.nextLong()

            val m=Gcd.lcm(A.toLong(), B.toLong())

            val both=N/m
            val appy=N/A-both
            val chef=N/B-both
            if(appy+chef>=K)
                println("Win")
            else
                println("Lose")
        }
    }
    class Gcd {
        companion object {

            fun lcm(a: Long, b: Long): Long {
                return a * (b / gcd(a, b))
            }

            fun lcm(input: List<Long>): Long {
                var result = input[0]
                for (i in input.indices)
                    result = lcm(result, input[i])
                return result
            }

            fun gcd(pA: Long, pB: Long): Long {
                var b = pB
                var a = pA
                while (b > 0) {
                    val temp = b
                    b = a % b
                    a = temp
                }
                return a
            }

            fun gcd(input: List<Long>): Long {
                var result = input[0]
                for (i in input.indices)
                    result = gcd(result, input[i])
                return result
            }
        }
    }
    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
