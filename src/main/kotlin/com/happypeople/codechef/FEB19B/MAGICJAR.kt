package com.happypeople.codechef.FEB19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        MAGICJAR().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class MAGICJAR {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for(i in 1..T) {
            val N = sc.nextInt()
            var ans = 0L
            for (i in 1..N) {
                val a = sc.nextInt()
                if (a > 0)
                    ans += a - 1
            }
            ans++
            println("$ans")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
