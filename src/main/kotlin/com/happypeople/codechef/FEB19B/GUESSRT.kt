package com.happypeople.codechef.FEB19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        GUESSRT().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class GUESSRT {
    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        val nFrac=IntArray(50001)
        nFrac[1]=Inv.fraction(1, 1)
        val sAns=StringBuffer("")
        for (i in 1..T) {
            var N = sc.nextInt()
            val K = sc.nextInt()
            val M = sc.nextInt()

            var negProb = nFrac[1]  // 1/1
            for (j in 1..M) {
                // log("j=$j N=$N K=$K negProb=$negProb")
                if (j == M || N <= K) { // guess/calc
                    if(nFrac[N]==0)
                        nFrac[N]=Inv.fraction(1, N)

                    val prob = Inv.mul(negProb, nFrac[N])
                    negProb = Inv.plus(negProb, -prob)

                    N += K
                } else {
                    N %= K
                    if (N == 0)
                        N = K
                }
            }
            var ans = Inv.plus(nFrac[1], -negProb)
            sAns.append("$ans\n")
        }
        println("$sAns")
    }

    val MOD = 1000000007

    class Inv {
        companion object {
            val defaultMod = 1000000007
            var MOD = defaultMod

            fun toPower(a: Int, p: Int, mod: Int=MOD): Int {
                var a = a
                var p = p
                var res = 1
                while (p != 0) {
                    if (p and 1 == 1)
                        res = mul(res, a)
                    p = p shr 1
                    a = mul(a, a)
                }
                return res
            }

            fun inv(x: Int, mod: Int = MOD): Int {
                return toPower(x, mod - 2)
            }

            fun fraction(zaehler: Int, nenner: Int): Int {
                return mul(zaehler, inv(nenner))
            }

            fun mul(v1: Int, v2: Int): Int {
                return ((1L * v1 * v2) % Inv.MOD).toInt()
            }

            fun plus(v1: Int, v2: Int): Int {
                var res = v1 + v2

                if (res < 0)
                    res += Inv.MOD

                if(res>=Inv.MOD)
                    res-=Inv.MOD

                return res
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
