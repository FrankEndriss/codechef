package com.happypeople.codechef.FEB19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        DEPCHEF().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class DEPCHEF {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for(i in 1..T) {
            val N=sc.nextInt()
            val a=(1..N).map { sc.nextInt() }
            val d=(1..N).map { sc.nextInt() }
            val maxAttack=a.mapIndexed { it, ign ->
                if(it==0) {
                    a[N-1]+a[1]
                } else if(it==N-1) {
                    a[N-2]+a[0]
                } else
                    a[it-1]+a[it+1]
            }
            val ans=d.filterIndexed { idx, it->
                d[idx]-maxAttack[idx]>0
            }.sortedDescending().firstOrNull()

            if(ans==null)
                println("-1")
            else
                println("${ans!!}")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
