package com.happypeople.codechef.FEB19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs

fun main(args: Array<String>) {
    try {
        MANRECT().run()
    } catch (e: Throwable) {
        println("$e")
        e.printStackTrace()
    }
}

class MANRECT {
    val MAX = 1000000000
    fun run() {
        val sc = Scanner(systemIn())
        val T=if(fakeServer) 5 else sc.nextInt()
        //val T = sc.nextInt()
        for (i in 1..T) {
            val ld = q(0, 0, sc)
            //log("ld=$ld")
            val lu = q(0, MAX, sc)
            //log("lu=$lu")

            val sectY1 = inter(ld, lu)
            //log("sectY1=$sectY1")
            val x1X = q(0, sectY1, sc)
            //log("x1X=$x1X")
            val x1Y = ld - x1X

            val x2X = MAX - q(MAX, sectY1, sc)
            //log("x2X=$x2X")
            val x2Y = MAX - lu + x1X
            println("A $x1X $x1Y $x2X $x2Y")
            System.out.flush()
            if (!fakeServer) {
                val fini = sc.nextInt()
                if (fini < 0)
                    break
            }
        }
    }

    /** Calcs the coordinate of the intersection between d1 and d2 */
    fun inter(d: Int, u: Int): Int {
        return (d - u + MAX) / 2
    }

    fun q(x: Int, y: Int, sc: Scanner): Int {
        if (fakeServer) {
            var ret = -1
            if (x == 0) {
                if (y == 0)
                    ret = targetX1X + targetX1Y
                else if (y == MAX)
                    ret = targetX1X + (MAX - targetX2Y)
                else if (y in targetX1Y..targetX2Y)
                    ret = targetX1X
                else
                    throw IllegalArgumentException("bad query: x=$x y=$y")
            } else if (x == MAX) {
                if (y == 0)
                    ret = MAX - targetX2X + targetX1Y
                else if (y == MAX)
                    ret = (MAX - targetX2X) + (MAX - targetX2Y)
                else if (y in targetX1Y..targetX2Y)
                    ret = MAX - targetX2X
                else
                    throw IllegalArgumentException("bad query: x=$x y=$y")
            } else if (x in targetX1X..targetX2X)
                throw RuntimeException("not implemented, not needed")

            return ret

        } else {
            println("Q $x $y")
            System.out.flush()
            val ret = sc.nextInt()
            if (ret < 0)
                throw RuntimeException("something wrong :/")

            return ret
        }
    }

    companion object {
        var targetX1X = 0
        var targetX1Y = 0
        var targetX2X = 0
        var targetX2Y = 0

        var fakeServer = false

        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
