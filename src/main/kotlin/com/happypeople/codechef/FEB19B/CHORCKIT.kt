package com.happypeople.codechef.FEB19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs

fun main(args: Array<String>) {
    try {
        CHORCKIT().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class CHORCKIT {
    val MAX_OP=100000
    val MAX_LEN=1000000

    data class Pretty(val melody:String, val C:Int, val occ:MutableList<Int>)

    fun run() {
        val sc = Scanner(systemIn())
        val N=sc.nextInt()
        val M=sc.nextInt()
        val A=sc.nextInt()
        val X=sc.nextInt()
        val R=sc.nextInt()
        val store=sc.next().trim()

        val pretty=(1..M).map {
            val melody=sc.next().trim()
            val C=sc.nextInt()
            val r=Regex(melody)
            val occ=r.findAll(store).map { it.range.first }.toMutableList()
            Pretty(melody, C, occ)
        }.filter { it.C>0 }
        .sortedByDescending { it.C }

        val prettyByLen=pretty.sortedByDescending { (1.0 * it.C) / it.melody.length }
        log("$prettyByLen")

        val good=(1..A).map { Pair(sc.next().trim(), sc.nextInt()) }    // Pi / Qi

        var x=0 // costs
        var op=0
        val prettyVal=pretty.map { it.C * it.occ.size }.sum()       // current pretty value
        log ("prettyVal before: $prettyVal")

        // simple strategy
        // Build the most valueable melody as much times as possible
        val ans=mutableListOf<String>()
        val mel=prettyByLen[0]
        var melIdx=0
        var storeIdx=0
        val storeA=store.toCharArray()
        while(x<X && op<MAX_OP) {
            op++
            if(storeIdx<storeA.size) {
                if(storeA[storeIdx]!=mel.melody[melIdx]) {
                    x += abs(mapc(storeA[storeIdx]) - mapc(mel.melody[melIdx]))
                    if (x > X)
                        break
                    ans.add("1 ${storeIdx+1} ${mel.melody[melIdx]}")
                }
                storeIdx++
                melIdx++
                melIdx %= mel.melody.length
            }
        }

        println("${ans.size}")
        for(i in ans.indices)
            println("${ans[i]}")


    }

    fun mapc(c:Char):Int {
        return if(c>='a' && c<='z')
            c-'a'+1
        else
            c-'A'+27
    }
    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
