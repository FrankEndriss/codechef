package com.happypeople.codechef.FEB19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        CHEFING().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class CHEFING {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for(i in 1..T) {
            val N=sc.nextInt()
            val counters=('a'..'z').map { 0 }.toMutableList()
            for(i in 1..N) {
                val s=sc.next().trim().toSet()
                for(c in s)
                    counters[c-'a']++
            }
            val ans=counters.filter { it==N }.count()
            println("$ans")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
