package com.happypeople.codechef.CONE2018

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        AE04().run()
    } catch (e: Throwable) {
        AE04.log("" + e)
    }
}

class AE04 {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val k = sc.nextInt()
        val j = sc.nextInt()
        val a = IntArray(n)
        for (i in 0 until n)
            a[i] = sc.nextInt()

        for (i in (1..k)) {
            val t = sc.nextInt()
            val arg1 = sc.nextInt()
            val arg2 = sc.nextInt()
            if (t == 1) {
                a[arg1-1]=a[arg1-1] xor arg2
            } else if (t == 2) {

                var sum=0
                for(idx in arg1-1..arg2-1) {
                    a[idx] = jVal(a, idx, j)
                    // log("jVal at $idx: ${a[idx]}")
                    sum=sum+a[idx]
                }
                println("$sum")
            }
        }
    }

    fun jVal(a:IntArray, idx:Int, j:Int):Int {
        if(idx+j-1>=a.size)
            return 0

        var ret=a[idx]
        for(i in 1..j-1)
            ret=ret xor a[idx+i]
        return ret
    }

    // Nice try, but completely irrelevant for the problem :/
    /** Sum of Int */
    class SegmentTree(val a: IntArray) {
        private val tree = IntArray(calcTreeSize(a.size))
        private val treeDataOffset = tree.size / 2 - 1

        init {
            //log("arr: ${a.toList()}")
            // copy raw data to tree
            for (i in a.indices)
                tree[treeDataOffset + i] = a[i]
            //log("tree after copy: ${tree.toList()}")

            construct(treeDataOffset, treeDataOffset*2, 0)
            //log("tree: ${tree.toList()}")
        }

        /** dataStart, dataEnd including, stores and returns sum at nodeIndex. */
        private fun construct(dataStart: Int, dataEnd: Int, nodeIndex: Int): Int {
            //log("construct start=$dataStart end=$dataEnd, nodeIndex=$nodeIndex")
            if (dataStart == dataEnd) {
                // if (nodeIndex != dataStart)
                //    throw RuntimeException("something wrong nodeIndex=$nodeIndex dataStart=$dataStart")
                return tree[nodeIndex]
            }

            // If there are more than one elements, then recur for left and
            // right subtrees and store the cummulative value of two nodes in treeIndex
            val mid = getMid(dataStart, dataEnd)
            tree[nodeIndex] = minVal(
                    construct(dataStart, mid, leftChild(nodeIndex)),
                    construct(mid + 1, dataEnd, rightChild(nodeIndex)))
            return tree[nodeIndex]
        }

        private fun getMid(s: Int, e: Int): Int {
            return s + (e - s) / 2
        }

        // TODO rename
        private fun minVal(a1: Int, a2: Int): Int {
            return a1 xor a2
        }

        /** TODO rename to updateCummulative or the like. */
        fun update(dataIdx: Int, value: Int) {
            //log("update dataIdx=$dataIdx value="+value)
            //log("bef update, tree=${tree.toList()}")
            val treeIdx = treeDataOffset + dataIdx
            tree[treeIdx] = minVal(tree[treeIdx], value)
            updateNode(parent(treeIdx))
            // log("aft update, tree=${tree.toList()}")
        }

        private fun parent(treeIdx: Int) = (treeIdx - 1) / 2
        private fun leftChild(treeIdx: Int) = treeIdx * 2 + 1
        private fun rightChild(treeIdx: Int) = treeIdx * 2 + 2

        private fun updateNode(nodeIdx: Int) {
            tree[nodeIdx] = minVal(tree[leftChild((nodeIdx))], tree[rightChild(nodeIdx)])
            if(nodeIdx!=0)
                updateNode(parent(nodeIdx))
        }

        fun get(idxL: Int, idxR: Int): Int {
            return getUtil(0, tree.size / 2, idxL, idxR, 0)
        }

        private fun getUtil(ss: Int, se: Int, qs: Int, qe: Int, index: Int): Int {
            // If segment of this node is a part of given range, then
            // return the value of the segment
            if (qs <= ss && qe >= se)
                return tree[index]

            // If segment of this node is outside the given range
            if (se < qs || ss > qe)
                return 0

            // If a part of this segment overlaps with the given range
            val mid = getMid(ss, se)
            return minVal(
                    getUtil(ss, mid, qs, qe, 2 * index + 1),
                    getUtil(mid + 1, se, qs, qe, 2 * index + 2))
        }

        companion object {
            fun calcTreeSize(arrSize: Int): Int {
                var s = 2
                while (s < arrSize * 2)
                    s *= 2
                return s
            }
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }

}
