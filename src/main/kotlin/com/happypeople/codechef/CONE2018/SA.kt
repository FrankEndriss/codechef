package com.happypeople.codechef.CONE2018

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        SA().run()
    } catch (e: Throwable) {
        SA.log("" + e)
    }
}

class SA {
    fun run() {
        val sc = Scanner(systemIn())
        val t = sc.nextInt()
        log("testcases: $t")

        for (i in (1..t)) {

            val n = sc.nextInt()
            val A = (1..n).map { sc.nextInt() }.sortedDescending()

            // When is gcd(P1, P2)==1 ???
            // 1. If one of the Ps eq 1, so if only {1} is in it.
            // 2. If both of the Ps are only primes.
            // When is gcd (of products) !=1 ???
            // 1. If at least one member of P1 is a divisor of at least one member of P2

            // BUCKETS LOOP DOES NOT WORK :/
            // ... create buckets, sort a desc,
            // loop A a
            //   loop buckets B
            //     loop B b
            //       if(a is divisor of b or b is divisor of a)
            //       then B+=a ; next a ;
            //     endloop
            //   endloop
            //   new bucket of a
            // endloop
            // ans=2^buckets.size
            val buckets = mutableListOf<MutableList<Int>>()
            for (a in A) {
                var bucketFound = false
                for (bucket in buckets) {
                    for (b in bucket) {
                        if (b % a == 0) {
                            bucket.add(a)
                            bucketFound = true
                        }
                        break
                    }
                    if (bucketFound)
                        break
                }
                if (!bucketFound)
                    buckets.add(mutableListOf(a))
            }

            val MOD = (10e9 + 7).toLong()
            var ans = 1L
            buckets.forEach {
                ans *= 2
                ans %= MOD
            }
            println("$ans")
        }
    }

    class Gcd {
        companion object {

            fun lcm(a: Long, b: Long): Long {
                return a * (b / gcd(a, b))
            }

            fun lcm(input: List<Long>): Long {
                var result = input[0]
                for (i in input.indices)
                    result = lcm(result, input[i])
                return result
            }

            fun gcd(pA: Long, pB: Long): Long {
                var b = pB
                var a = pA
                while (b > 0) {
                    val temp = b
                    b = a % b
                    a = temp
                }
                return a
            }

            fun gcd(input: List<Long>): Long {
                var result = input[0]
                for (i in input.indices)
                    result = gcd(result, input[i])
                return result
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}