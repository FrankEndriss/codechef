package com.happypeople.codechef.CONE2018

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        COS().run()
    } catch (e: Throwable) {
        COS.log("" + e)
    }
}

class COS {
    var minCosts = Int.MAX_VALUE
    var c1 = 0
    var c2 = 0
    var n = 0L
    var counter = 0
    var queue = PriorityQueue<S>()

    val OP_NONE=0
    val OP1=1
    val OP2=2

    data class S(val sum: Long, val curr: Long, val costs: Int, val lastOp:Int) : Comparable<S> {
        override fun compareTo(other: S): Int {
            return costs - other.costs
        }
    }

    fun run() {
        val sc = Scanner(systemIn())
        n = sc.nextLong()
        c1 = sc.nextInt()
        c2 = sc.nextInt()

        queue.add(S(1L, 0L, 0, OP_NONE))

        while (!queue.isEmpty() && !(queue.peek().costs > minCosts)) {
            cheapestPaths(queue.poll())
        }
        println("$minCosts $counter")

    }

    /** TODO to speed things up, we can add more komplex operations, build by allready known operations.
     * This makes things faster since we do not have to do all steps at once.
     * Especially double, quadruple, 2^n-times... to reach huge N fast.
     * We would have to combine these komplex operations in a way that multiple of them can be put into the searchtree
     * at once.
     **/
    /** @return costs of cheapest path in this branch. */
    fun cheapestPaths(s: S) {
        //log("cheapestPaths, s=$s")
        if (s.sum == n) { // found a path
            if (s.costs <= minCosts) {
                if (s.costs < minCosts) // new min found, restart counting
                    counter = 0
                minCosts = s.costs
                counter++
            }
        } else if (s.sum > n) {    // cant reach n
            ;
        } else if (s.costs > minCosts) {     // stop searching, wont be counted
            ;
        } else { // recurse deeper
            if(s.lastOp!=OP1)   // does not make sense to do it twice
                queue.add(S(s.sum, s.sum, s.costs + c1, OP1))        // 0P1, curr=sum
            if(s.curr!=0L)      // does not make sense to add 0
                queue.add(S(s.sum + s.curr, s.curr, s.costs + c2, OP2)) // O2, sum=sum+curr
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}