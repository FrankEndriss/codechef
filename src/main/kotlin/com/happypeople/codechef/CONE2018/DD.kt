package com.happypeople.codechef.CONE2018

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        DD().run()
    } catch (e: Throwable) {
        DD.log("" + e)
    }
}

class DD {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()

        val postfixMap=mutableMapOf<Int, Set<String>>()
        for(k in listOf(2, 4, 8)) {
            val postfixes = mutableSetOf<String>()
            val bitmask=k-1
            for (d1 in 0..9) {
                for (d2 in 0..9) {
                    val str="$d1$d2".removePrefix("0")
                    val strInt=str.toInt()
                    if (strInt and bitmask == 0)
                        postfixes.add(str)
                }
            }
            postfixMap[k]=postfixes
        }

        for(i in 0 until t) {
            val n = sc.nextInt()
            val k = sc.nextInt()  // 1, 2, 4, 8
            val str = sc.next()!!.trim()

            // try to make postfix of str fit bitmask k
            if(k==1) {
                println("0")
                continue
            }

            // k can be 2, 4, 8
            //if 2 last bit must be 0
            //if 4 last two bits must be 0
            //if 8 last three bits must be 0
            // We have to care for the last two digits only, since these determine (more than) the last 3 bits
            // find all combinations of the last two digits which fit the bitmask

            val postfixes=postfixMap[k]!!
            // now make str to have postfix as one of postfixes
            // 1. make last digit fit
            // 2. make pre-last digit fit

            // while loop
            // if last digits do not fit with any prefix, remove last digit from str, counter++
            // if last digit fits
            //   if postfix.length==1 fini
            //   else search for second digit, counter+=searchLen
            // Use min-Cost-Search

        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}