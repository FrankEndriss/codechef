package com.happypeople.codechef.LTIME67B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        PPATTERN().run()
    } catch (e: Throwable) {
        PPATTERN.log("" + e)
    }
}

class PPATTERN {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        (1..t).forEach {
            val n=sc.nextInt()

            // naive, create all lines, then output
            val a=(1..n).map { (1..n).map { 0 }.toMutableList() }
            var row=0
            var col=0
            (1..(n*n)).forEach {
                // dump(a)
                a[row][col]=it
                if(col>0 && row<n-1) {
                    col--
                    row++
                } else {
                    if(col==0) {
                        col = row + 1
                        row = 0
                        if(col==n) { // corner left down
                            col--
                            row++
                        }
                    } else { // row==n-1
                        row=col+1
                        col=n-1
                    }
                }
            }
            dump(a)
        }
    }

    private fun dump(a: List<MutableList<Int>>) {
        a.forEach { println(it.joinToString(" ")) }
    }


    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}