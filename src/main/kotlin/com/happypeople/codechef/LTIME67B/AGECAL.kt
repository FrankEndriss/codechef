package com.happypeople.codechef.LTIME67B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        AGECAL().run()
    } catch (e: Throwable) {
        AGECAL.log("" + e)
    }
}

class AGECAL {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        (1..t).forEach {
            val numMonths=sc.nextInt()
            val monthLens=(1..numMonths).map { sc.nextInt() }
            val y0=sc.nextInt()
            val m0=sc.nextInt()
            val d0=sc.nextInt()
            val y1=sc.nextInt()
            val m1=sc.nextInt()
            val d1=sc.nextInt()

        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}