package com.happypeople.codechef.MARCH19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.math.BigDecimal
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        PTREE().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class PTREE {
    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        (1..T).forEach {
            val N = sc.nextInt()
            val Q = sc.nextInt()

            /** Maps vertices to its children. Root=0 */
            val nTree = arrOf(200001) { mutableListOf<Int>() }
            for (i in 1..N - 1) {
                val u=sc.nextInt()
                val v=sc.nextInt()
                nTree[min(u, v) - 1].add(max(u, v) - 1)
            }

            val nTreeDists = arrOf(200001) { mutableListOf<Int>() }
            precalcDistances(nTree, nTreeDists)

            var ans=0
            (1..Q).forEach {
                val v = (ans xor sc.nextInt())-1
                val y = (ans xor sc.nextInt())%Inv.MOD
                log("query, v=$v y=$y")

                val dCounts = nTreeDists[v]
                //log("dCounts=$dCounts")

                // Use geometric series:
                // https://math.stackexchange.com/questions/982970/how-to-find-sum-of-powers-from-1-to-r
                // need to calc sum( y pow dCounts[i] .. y pow dCount[i+1])
                // sum ( y pow 0 .. y pow x) == (((y pow x+1)) -1 / (y-1)) -1

                ans = 0
                var oldCount=0
                for (dist in dCounts.indices) {
                    val x=xpow(y, oldCount, oldCount+dCounts[dist])
                    //log("oldCount=$oldCount dCounts[dist]=${dCounts[dist]} y=$y => x=$x")
                    oldCount+=dCounts[dist]
                    ans=Inv.plus(ans, Inv.mul(x, dist+1))
                }
                println("$ans")
            }
        }
    }

    fun xpow(y:Int, p1:Int, p2:Int):Int {
        if(y==1) {
            return p2-p1
        }

        var pp2=Inv.toLongPower(y.toLong(), p2+1, longMOD)
        val pp1=Inv.toLongPower(y.toLong(), p1+1, longMOD)
        //log("y=$y p1=$p1 -> pm=$pm")
        val p=Inv.longPlus(pp2, -pp1, longMOD)

        return ((p/(y-1))%Inv.MOD).toInt()
    }

    val longMOD=1L*Inv.MOD*Inv.MOD

    // After this method the nTreeDists is filled.
    // for any one node this array has a List of number of nodes in that distance.
    // ie [3, 7] means 3 childs(dist==1) and 7 grandchilds(dist==2)
    fun precalcDistances(nTree: Array<MutableList<Int>>, nTreeDists: Array<MutableList<Int>>) {
        val stack=LinkedList<Int>()
        var next = listOf(0)
        while(!next.isEmpty()) {
            for(i in next) {
                if (nTree[i].size > 0) {
                    //log("precalc, push: $i")
                    stack.push(i)
                }
            }
            if(nTree[next.first()].size>0)
                next = next.flatMap { nTree[it] }
            else
                next=listOf<Int>()
        }

        while(!stack.isEmpty()) {
            val node=stack.pop()
            // log("precalc, pop: $node")
            // nTreeDists[node].add(nTree[node].size)
            nTreeDists[node].add(nTree[node].size)
            for(i in 0..nTreeDists[nTree[node].first()].size-1) {
                val sum=nTree[node].map { nTreeDists[it][i] }.sum()
                nTreeDists[node].add(sum)
            }
        }
    }

    inline fun <reified E> arrOf(size: Int, init: (Int) -> E): Array<E> {
        val a = arrayOfNulls<E>(size)
        for (i in a.indices)
            a[i] = init(i)
        return a as Array<E>
    }

    class Inv {
        companion object {
            val defaultMod = 1000000007
            var MOD = defaultMod

            fun toPower(a: Int, p: Int, mod: Int = MOD): Int {
                var a = a
                var p = p
                var res = 1
                while (p != 0) {
                    if (p and 1 == 1)
                        res = mul(res, a)
                    p = p shr 1
                    a = mul(a, a)
                }
                return res
            }

            fun toLongPower(a:Long, p:Int, mod: Long):Long {
                var a = a
                var p = p
                var res = 1L
                while (p != 0) {
                    if (p and 1 == 1)
                        res = longMul(res, a, mod)
                    p = p shr 1
                    a = longMul(a, a, mod)
                }
                return res
            }

            fun inv(x: Int, mod: Int = MOD): Int {
                return toPower(x, mod - 2)
            }

            fun fraction(zaehler: Int, nenner: Int): Int {
                return mul(zaehler, inv(nenner))
            }

            fun longMul(v1: Long, v2: Long, mod:Long):Long {
                return BigDecimal(v1).multiply(BigDecimal(v2)).remainder(BigDecimal(mod)).longValueExact()
            }

            fun mul(v1: Int, v2: Int): Int {
                return ((1L * v1 * v2) % Inv.MOD).toInt()
            }

            fun longPlus(v1:Long, v2:Long, mod:Long):Long {
                var res = v1 + v2

                if (res < 0)
                    res += mod

                if (res >= mod)
                    res -= mod

                return res
            }

            fun plus(v1: Int, v2: Int): Int {
                var res = v1 + v2

                if (res < 0)
                    res += Inv.MOD

                if (res >= Inv.MOD)
                    res -= Inv.MOD

                return res
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
