package com.happypeople.codechef.MARCH19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        CHNUM().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class CHNUM {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        (1..T).forEach {
            val n=sc.nextInt()
            val a=(1..n).map { sc.nextInt() }
            val pos=a.filter { it>0 }.size
            val neg=a.filter { it<0 }.size

            val largest=max(pos, neg)
            val smallest=if(min(pos, neg)==0) largest else min(pos, neg)
            println("$largest $smallest")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
