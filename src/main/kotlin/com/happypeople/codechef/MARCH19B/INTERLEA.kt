package com.happypeople.codechef.MARCH19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        INTERLEA().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class INTERLEA {
    fun run() {
        val sc = Scanner(systemIn())
        val n = sc.nextInt()
        val s = (1..n).map { sc.next().trim().map { it - '0' }.toIntArray() }.toTypedArray()
        val pos = IntArray(n) // pos[i] == current position in s[i]
        val list = (0..9).map { LinkedList<Int>() }   // Lists of index to s, which are string with i at position list[i][....]

        var count = 0
        for (i in s.indices) {
            list[s[i][0]].push(i)
            count += s[i].size
        }

        val sb = StringBuffer()
        val idx = listOf(0..9, 8 downTo 1).flatMap { it }
        //val tsz = listOf(0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0)
        var i = 0
        var sz = 0;
        list.forEachIndexed { idx, it ->
            if (it.size > sz) {
                i = idx
                sz = it.size
            }
        }
        if(i>4)
            i=idx.size-i+1
        while (count > 0) {
            // find i as the next bigger slot next to the current slot

            //val ts=if(list[idx[i]].size>1) tsz[i] else 0
            while (list[idx[i]].size > 0) {
                val next = list[idx[i]].pop()!!
                sb.append("${next + 1} ")
                //print("${s[next][pos[next]]}")
                count--
                if (pos[next] < s[next].size - 1) {
                    pos[next]++
                    list[s[next][pos[next]]].push(next)
                }
            }
            i = (i + 1) % idx.size
        }
        log("")
        println(sb)
    }

    inline fun <reified E> arrOf(size: Int, init: (Int) -> E): Array<E> {
        val a = arrayOfNulls<E>(size)
        for (i in a.indices)
            a[i] = init(i)
        return a as Array<E>
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
