package com.happypeople.codechef.MARCH19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.math.BigDecimal
import java.util.*
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        PTREE().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class PTREE {
    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        (1..T).forEach {
            val N = sc.nextInt()
            val Q = sc.nextInt()

            /** Maps vertices to its children. Root=0 */
            val nTree = arrOf(200001) { mutableListOf<Int>() }
            for (i in 1..N - 1) {
                val u = sc.nextInt()
                val v = sc.nextInt()
                nTree[min(u, v) - 1].add(max(u, v) - 1)
            }

            val nTreeDists = arrOf(200001) { mutableListOf<Int>() }
            precalcDistances(nTree, nTreeDists)

            var ans = 0
            (1..Q).forEach {
                val v = (ans xor sc.nextInt()) - 1
                val y = (ans xor sc.nextInt()) % Inv.MOD
                //log("query, v=$v y=$y")

                val dCounts = nTreeDists[v]
                //log("dCounts=$dCounts")

                // Use geometric series:
                // https://math.stackexchange.com/questions/982970/how-to-find-sum-of-powers-from-1-to-r
                // need to calc sum( y pow dCounts[i] .. y pow dCount[i+1])
                // sum ( y pow 0 .. y pow x) == (((y pow x+1)) -1 / (y-1)) -1

                ans = 0
                var oldCount = 0
                var oldGeomSum = geomSum(y, 1, Inv.MOD)
                for (dist in dCounts.indices) {
                    // return Inv.plus(geomSum(y, p2 + 1, Inv.MOD), -geomSum(y, p1 + 1, Inv.MOD))

                    val newGeomSum = geomSum(y, oldCount + dCounts[dist] + 1, Inv.MOD)
                    //log("newGeomSum=$newGeomSum")
                    val x = Inv.plus(newGeomSum, -oldGeomSum)
                    /*
                            if (oldCount + dCounts[dist] > 32)
                                xpow_geom(y, oldCount, oldCount + dCounts[dist])
                            else
                                xpow_old(y, oldCount, oldCount + dCounts[dist])
                                */

                    //log("x=$x x_old=$x_old")
                    //log("oldCount=$oldCount dCounts[dist]=${dCounts[dist]} y=$y => x=$x")
                    oldCount += dCounts[dist]
                    oldGeomSum = newGeomSum
                    ans = Inv.plus(ans, Inv.mul(x, dist + 1))
                }
                println("$ans")
            }
        }
    }

    var lastB=Int.MIN_VALUE
    var lastN=Int.MIN_VALUE
    var lastX=Int.MIN_VALUE
    var lastSum=Int.MIN_VALUE
    fun geomSum_simple(b:Int, pN: Int): Int {
        var x1 = 1
        var sum1 = 1

        if(b!=lastB || pN<lastN) {
            for (i in 1..pN - 1) {
                x1 = Inv.mul(x1, b)
                sum1 = Inv.plus(sum1, x1);
            }
        } else {
            x1=lastX
            sum1=lastSum
            for (i in lastN..pN - 1) {
                x1 = Inv.mul(x1, b)
                sum1 = Inv.plus(sum1, x1);
            }
        }
        lastB=b
        lastN=pN
        lastX=x1
        lastSum=sum1
        return sum1;
    }

    // https://stackoverflow.com/questions/1522825/calculating-sum-of-geometric-series-mod-m
    /**
     * @param y the value
     * @param p the exponent
     * @param m the modulus
     */
    fun geomSum(b: Int, pN: Int, m: Int): Int {
        var n = pN
        var T = 1L
        //var e=(1L*b)%m
        var e = (1L * b) % m
        var total = 0L
        while (n > 0) {
            if ((n and 1) !=0)
                total = (e * total + T) % m
            T = ((e + 1) * T) % m
            e = (e * e) % m
            n = n / 2
        }
        val ti = total.toInt()
        //log("geomSum b=$b n=$pN -> $ti")
        return ti
    }

    /*
    def geometric(n,b,m):
    T=1
    e=b%m
    total = 0
    while n>0:
    if n&1==1:
    total = (e*total + T)%m
    T = ((e+1)*T)%m
    e = (e*e)%m
    n = n/2
    //print '{} {} {}'.format(total,T,e)
    return total
    */

    //val longMOD=1L*Inv.MOD*Inv.MOD
    val longMOD = (1L * Inv.MOD) shl 31

    // After this method the nTreeDists is filled.
    // for any one node this array has a List of number of nodes in that distance.
    // ie [3, 7] means 3 childs(dist==1) and 7 grandchilds(dist==2)
    fun precalcDistances(nTree: Array<MutableList<Int>>, nTreeDists: Array<MutableList<Int>>) {
        val stack = LinkedList<Int>()
        var next = listOf(0)
        while (!next.isEmpty()) {
            for (i in next) {
                if (nTree[i].size > 0) {
                    //log("precalc, push: $i")
                    stack.push(i)
                }
            }
            if (nTree[next.first()].size > 0)
                next = next.flatMap { nTree[it] }
            else
                next = listOf<Int>()
        }

        while (!stack.isEmpty()) {
            val node = stack.pop()
            // log("precalc, pop: $node")
            // nTreeDists[node].add(nTree[node].size)
            nTreeDists[node].add(nTree[node].size)
            for (i in 0..nTreeDists[nTree[node].first()].size - 1) {
                val sum = nTree[node].map { nTreeDists[it][i] }.sum()
                nTreeDists[node].add(sum)
                //log("nTreeDists add, node=$node sum=$sum")
            }
        }
    }

    inline fun <reified E> arrOf(size: Int, init: (Int) -> E): Array<E> {
        val a = arrayOfNulls<E>(size)
        for (i in a.indices)
            a[i] = init(i)
        return a as Array<E>
    }

    class Inv {
        companion object {
            val defaultMod = 1000000007
            var MOD = defaultMod

            fun toPower(a: Int, p: Int, mod: Int = MOD): Int {
                var a = a
                var p = p
                var res = 1
                while (p != 0) {
                    if (p and 1 == 1)
                        res = mul(res, a)
                    p = p shr 1
                    a = mul(a, a)
                }
                return res
            }

            fun toLongPower(a: Long, p: Int, mod: Long): Long {
                var a = a
                var p = p
                var res = 1L
                while (p != 0) {
                    if (p and 1 == 1)
                        res = longMul(res, a, mod)
                    p = p shr 1
                    a = longMul(a, a, mod)
                }
                return res
            }

            fun inv(x: Int, mod: Int = MOD): Int {
                return toPower(x, mod - 2)
            }

            fun fraction(zaehler: Int, nenner: Int): Int {
                return mul(zaehler, inv(nenner))
            }

            fun longMul(v1: Long, v2: Long, mod: Long): Long {
                return BigDecimal(v1).multiply(BigDecimal(v2)).remainder(BigDecimal(mod)).longValueExact()
            }

            fun mul(v1: Int, v2: Int): Int {
                return ((1L * v1 * v2) % Inv.MOD).toInt()
            }

            fun longPlus(v1: Long, v2: Long, mod: Long): Long {
                var res = v1 + v2

                if (res < 0)
                    res += mod

                if (res >= mod)
                    res -= mod

                return res
            }

            fun plus(v1: Int, v2: Int): Int {
                var res = v1 + v2

                if (res < 0)
                    res += Inv.MOD

                if (res >= Inv.MOD)
                    res -= Inv.MOD

                return res
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
