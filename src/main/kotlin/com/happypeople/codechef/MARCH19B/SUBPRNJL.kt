package com.happypeople.codechef.MARCH19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        SUBPRNJL().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class SUBPRNJL {
    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        (1..T).forEach {
            val N = sc.nextInt()
            val K = sc.nextInt()
            val A = (1..N).map { sc.nextInt() }

            val ans = solve3(A, K)
            /*
            val ans3=solve3(A, K)
            if(ans==ans3)
                log("ok, ans=ans2")
            else
                println("failed, ans!=ans2 $ans!=$ans3")
                */
            println("$ans")
        }
    }

    private fun solve2(A: List<Int>, K: Int): Int {
        var ans = 0
        for (i in A.indices) {
            for (j in i + 1..A.size) {
                val S = A.subList(i, j)
                val B=mutableListOf<Int>()
                while(B.size<K)
                    B.addAll(S)
                B.sort()
                val X=B[K-1]
                val c=S.filter { it==X }.count()
                if(S.contains(c)) {
                    //log("$i:$j solve2, beautiful S: $S, count=$c")
                    ans++
                }
                //else
                    //log("$i:$j solve2, not beautiful S: $S, count=$c")
            }
        }

        return ans
    }


    private fun solve3(A: List<Int>, K: Int): Int {
        // loop all subarrays of A
        var ans = 0
        for (i in A.indices) {
            val S=mutableListOf<Int>()
            for (j in i..A.size-1) {
                //S.add(A[j])
                //S.sort()
                // val S = A.subList(i, j).sorted()

                val nIdx=S.binarySearch(A[j])
                if(nIdx>=0)
                    S.add(nIdx, A[j])
                else
                    S.add(-(nIdx+1), A[j])

                //log("\nS=$S")
                val m = calcM(i, j+1, K)
                // define X=B[K], i.e. as a K-th smallest element of B
                val idx = (K-1) / m
                var count = 1
                var ii = idx - 1
                while (ii >= 0 && S[idx] == S[ii]) {
                    ii--
                    count++
                }
                ii = idx + 1
                while (ii < S.size && S[idx] == S[ii]) {
                    ii++
                    count++
                }
                //log("count of ${S[idx]}: $count")

                if (S.binarySearch(count) >= 0) {
                    //log("$i:$j solve1, beautiful S: $S, count=$count")
                    ans++
                }
                //else
                //log("$i:$j solve1, not beautiful S: $S, count=$count")
            }
        }
        return ans
    }

    private fun solve(A: List<Int>, K: Int): Int {
        // loop all subarrays of A
        var ans = 0
        for (i in A.indices) {
            for (j in i + 1..A.size) {
                val S = A.subList(i, j).sorted()
                //log("\nS=$S")
                val m = calcM(i, j, K)
                // define X=B[K], i.e. as a K-th smallest element of B
                val idx = (K-1) / m
                var count = 1
                var ii = idx - 1
                while (ii >= 0 && S[idx] == S[ii]) {
                    ii--
                    count++
                }
                ii = idx + 1
                while (ii < S.size && S[idx] == S[ii]) {
                    ii++
                    count++
                }
                //log("count of ${S[idx]}: $count")

                if (S.binarySearch(count) >= 0) {
                    //log("$i:$j solve1, beautiful S: $S, count=$count")
                    ans++
                }
                //else
                //log("$i:$j solve1, not beautiful S: $S, count=$count")
            }
        }
        return ans
    }

    private fun calcM(l: Int, r: Int, k: Int): Int {
        // m is the smallest integer such that m * (r−l+1) ≥K.
        val x = r - l
        val ret = (k + x - 1) / x
        //log("calcM($l, $r, $k), ret=$ret")
        return ret
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
