package com.happypeople.codechef.MARCH19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        CHEFSOC2().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class CHEFSOC2 {
    // Left are the unused dogs left of pos.
    // Right are the used dogs right of pos.
    // pos is the current position.
    data class State(val left:Set<Int>, val right:Set<Int>, val pos:Int) {
        /** @return the next possible moves */
        fun next(dogs:IntArray):List<State> {
            val ret=mutableListOf<State>()
            if(pos>0 && left.contains(pos-1))
                ret.add(State(left.minus(pos-1), right.plus(pos), pos-1))
            if(pos<dogs.size-1 && !right.contains(pos+1))
                ret.add(State(left, right, pos+1))

            if(dogs[pos]==2) {
                if(pos>1 && left.contains(pos-2)) {
                    // if pos-2 is unused pos-1 is used
                    ret.add(State(left.minus(pos - 2), right.plus(pos).plus(pos-1), pos - 2))
                }
                if(pos<dogs.size-2 && !right.contains(pos+2)) {
                    var l=left
                    if(!right.contains(pos+1))
                        l=l.plus(pos+1)
                    ret.add(State(l, right.minus(pos).minus(pos+1), pos+2))
                }
            }
            //log("from: $this to: $ret")
            return ret
        }
    }

    val sc = Scanner(systemIn())
    var currStates=setOf<State>()     // maps of States at current move

    fun solve(n:Int) {
        val dogs=(1..n).map { sc.nextInt() }.toIntArray()
        currStates=setOf(State(setOf<Int>(), setOf<Int>(), 0))   // pos 0, left of 0 nothing free, right of 0 all free
        var ans=1
        while(!currStates.isEmpty()) {
            val lList=currStates.flatMap { it.next(dogs) }
            ans=Inv.plus(ans, lList.size)
            currStates=lList.toSet()
            //log("ans=$ans")
        }
        println("$ans")
    }

    fun run() {
        val T=sc.nextInt()
        for(t in 1..T) {
            solve(sc.nextInt());
        }
    }

    class Inv {
        companion object {
            val defaultMod = 1000000007
            var MOD = defaultMod

            fun toPower(a: Int, p: Int, mod: Int=MOD): Int {
                var a = a
                var p = p
                var res = 1
                while (p != 0) {
                    if (p and 1 == 1)
                        res = mul(res, a)
                    p = p shr 1
                    a = mul(a, a)
                }
                return res
            }

            fun inv(x: Int, mod: Int = MOD): Int {
                return toPower(x, mod - 2)
            }

            fun fraction(zaehler: Int, nenner: Int): Int {
                return mul(zaehler, inv(nenner))
            }

            fun mul(v1: Int, v2: Int): Int {
                return ((1L * v1 * v2) % Inv.MOD).toInt()
            }

            fun plus(v1: Int, v2: Int): Int {
                var res = v1 + v2

                if (res < 0)
                    res += Inv.MOD

                if(res>=Inv.MOD)
                    res-=Inv.MOD

                return res
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
