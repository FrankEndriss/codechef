package com.happypeople.codechef.MARCH19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        CHEFSOC().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class CHEFSOC {
    val NEG2 = 1
    val NOTNEG2 = 15 xor NEG2
    val NEG1 = 2
    val NOTNEG1 = 15 xor NEG1
    val POS1 = 4
    val NOTPOS1 = 15 xor POS1
    val POS2 = 8
    val NOTPOS2 = 15 xor POS2

    val JUMPS = listOf(NEG2, NEG1, POS1, POS2)
    //val NOTJUMPS = listOf(NOTNEG2, NOTNEG1, NOTPOS1, NOTPOS2)

    val OFFS = listOf(0, -2, -1, 0, 1, 0, 0, 0, 2)

    val ALL = NEG2 + NEG1 + POS1 + POS2

    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        (1..T).forEach {
            val N = sc.nextInt()
            val a = (1..N).map { sc.nextInt() }
            val ans = solve1(a)
            println("$ans")
        }
    }

    inline fun <reified E> arrOf(size: Int, init: (Int) -> E): Array<E> {
        val a = arrayOfNulls<E>(size)
        for (i in a.indices)
            a[i] = init(i)
        return a as Array<E>
    }

    val iNEG2 = 0
    val iNEG1 = 1
    val iPOS1 = 2
    val iPOS2 = 3
    val iBJ=4   // backjumpcount

    fun dpSolve(a: List<Int>): Int {
        val dp = a.map { arrOf(4) { IntArray(5) } }

        // ie dp[iPOS1][iPOS2] is the number of possibilities if the last two steps where POS1 and POS2

        val last = a.size - 1
        dp[last][iPOS1][iPOS1] = 1
        dp[last][iPOS1][iPOS2] = 2
        if (a[last] == 1) {
            dp[last][iPOS2][iPOS1] = 1
        } else {
            dp[last][iPOS2][iPOS1] = Int.MIN_VALUE       // backjump, mark for later calculation
        }

        if (a.size > 1) {
            for (i in 0..3)
                for (j in 0..3)
                    dp[last - 1][i][j] = 2
            dp[last-1][iPOS2][iPOS1]=Int.MIN_VALUE      // backjump, later calculation
            dp[last-1][iPOS2][iPOS2]=Int.MIN_VALUE      // backjump, later calculation
        }

        for (i in a.size - 3 downTo 0) {
            if(a[i]==1) {
                dp[i][iPOS1][iPOS1]=1+dp[i+1][iPOS1][iPOS1]
                dp[i][iPOS2][iPOS1]=1+dp[i+1][iPOS1][iPOS1]
                dp[i][iNEG1][iPOS1]=0   // impossible
                dp[i][iNEG2][iPOS1]=1   // fini

                dp[i][iPOS1][iPOS2]=1+ dp[i-1][iPOS2][iNEG1] + dp[i+1][iPOS2][iPOS1]
                dp[i][iPOS2][iPOS2]=1+ dp[i-1][iPOS2][iNEG1] + dp[i+1][iPOS2][iPOS1]
                dp[i][iNEG1][iPOS2]=1+ dp[i+1][iPOS1][iPOS1]
                dp[i][iNEG2][iPOS2]=0   // impossible

                dp[i][iPOS1][iNEG1]=0 // impossible
                dp[i][iPOS2][iNEG1]=1 // fini
                dp[i][iNEG1][iNEG1]=0 // impossible
                dp[i][iNEG2][iNEG1]=0 // impossible

                dp[i][iPOS1][iNEG2]=1 // TODO backjumps
                dp[i][iPOS2][iNEG2]=0 // impossible
                dp[i][iNEG1][iNEG2]=1 // TODO backumps
                dp[i][iNEG2][iNEG2]=1 // TODO backumps

            } else { // a[i]==2
                dp[i][iPOS1][iPOS1]=1+dp[i+1][iPOS1][iPOS1]+dp[i+2][iPOS1][iPOS2]
                dp[i][iPOS2][iPOS1]=1+dp[i+1][iPOS1][iPOS1]+dp[i+2][iPOS1][iPOS2]
                dp[i][iNEG1][iPOS1]=0 // impossible
                dp[i][iNEG2][iPOS1]=1+dp[i+2][iPOS1][iPOS2]

                dp[i][iPOS1][iPOS2]=1+ dp[i-1][iPOS2][iNEG1] + dp[i+1][iPOS2][iPOS1] + dp[i+2][iPOS2][iPOS2]    // TODO backjump...
                dp[i][iPOS2][iPOS2]=1+ dp[i-1][iPOS2][iNEG1] + dp[i+1][iPOS2][iPOS1] + dp[i+2][iPOS2][iPOS2]    // TODO backjump...
                dp[i][iNEG1][iPOS2]=1+ dp[i+1][iPOS1][iPOS1] + dp[i+2][iPOS1][iPOS2]
                dp[i][iNEG2][iPOS2]=0   // impossible

                dp[i][iPOS1][iNEG1]=0 // impossible
                dp[i][iPOS2][iNEG1]=1 + dp[i+2][iNEG1][iPOS2] + dp[i-2][iNEG1][iNEG2]       // TODO backjump
                dp[i][iNEG1][iNEG1]=0 // impossible
                dp[i][iNEG2][iNEG1]=0 // impossible

                dp[i][iPOS1][iNEG2]=1 // TODO backjumps
                dp[i][iPOS2][iNEG2]=0 // impossible
                dp[i][iNEG1][iNEG2]=1 // TODO backumps
                dp[i][iNEG2][iNEG2]=1 // TODO backumps
            }
        }

        return 42
    }

    /* Calcs the sum of possibilities at pos if jumpedto using jump jump.
    **/

    private fun sumFrom(dp: List<Array<IntArray>>, pos: Int, jump: Int): Int {
        return dp[pos][jump][iPOS2] +
                dp[pos][jump][iPOS1] +
                dp[pos][jump][iNEG1] +
                dp[pos][jump][iNEG2]

    }

    fun solve1(a: List<Int>): Int {
        val J = listOf(0, NEG1 or POS1, ALL)
        val A = a.map {
            J[it]
        }.toMutableList()

        A[0] = A[0] and (NOTNEG2 and NOTNEG1)
        A[A.size - 1] = A[A.size - 1] and (NOTPOS2 and NOTPOS1)
        if (A.size > 1) {
            A[1] = A[1] and NOTNEG2
            A[A.size - 2] = A[A.size - 2] and NOTPOS2
        }

        // it is obvious that we can allways go +1
        // -2 never possible
        // if current dog has skill 2, we can go +2
        // if we did go +2, then
        // -we can go -1 next move,
        // -and propably +2 from there, or finish
        return recSolve(A, 0)
    }

    private fun recSolve(a: List<Int>, pos: Int): Int {
        var ret = 1

        if (a[pos] != 0) {

            val aCopy = a.toMutableList()
            if (pos >= 2)
                aCopy[pos - 2] = aCopy[pos - 2] and NOTPOS2
            if (pos >= 1)
                aCopy[pos - 1] = aCopy[pos - 1] and NOTPOS1
            if (pos < a.size - 1)
                aCopy[pos + 1] = aCopy[pos + 1] and NOTNEG1
            if (pos < a.size - 2)
                aCopy[pos + 2] = aCopy[pos + 2] and NOTNEG2

            // jump the possible jumps
            JUMPS.filter { (it and a[pos]) != 0 }.forEach { jump ->
                ret += recSolve(aCopy, pos + OFFS[jump])
            }
        }

        return ret
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
