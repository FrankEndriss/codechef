package com.happypeople.codechef.MARCH19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        CHDIGER().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class CHDIGER {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for (i in (1..T)) {
            val N=sc.nextLong()
            val d=sc.nextInt()

            val allDigits=(""+N).map { it-'1'+1 }
            val digits=allDigits.filter { it < d}.toMutableList()
            val ans= mutableListOf<Int>()
            while(!digits.isEmpty()) {
                val m=digits.min()!!
                while(!digits.isEmpty() && digits[0]>m)
                    digits.removeAt(0)
                if(!digits.isEmpty()) {
                    ans.add(digits[0])
                    digits.removeAt(0)
                }
            }

            while(ans.size<allDigits.size)
                ans.add(d)

            println("${ans.joinToString("") { ""+it }}")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
