package com.happypeople.codechef.MARCH19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        CHONQ().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class CHONQ {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        (1..T).forEach {
            val N=sc.nextInt()
            val K=sc.nextInt()
            val A=IntArray(N)
            for(i in A.indices)
                A[i]=sc.nextInt()

            val ans=solve2(A, K)
            println("$ans")
        }
    }

    private fun solve2(A: IntArray, K: Int):Int {
        for (p in A.indices) {
            if (isAngerSmallerK2(A, p, K)) {
                return p + 1
            }
        }
        return A.size+1
    }

    /** Simple brute force. */
    private fun isAngerSmallerK2(A: IntArray, p: Int, K:Int): Boolean {
        var sum=0
        var divisor=1
        for(i in p..A.size-1) {
            //log("K2 adding ${A[i]} / ${i-p+1}")
            sum+=A[i]/divisor
            divisor++
            if(sum>K) {
                //log("false")
                return false
            }
        }
        return true
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
