package com.happypeople.codechef.MARCH19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        JAIN().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class JAIN {
    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        val cFlags = IntArray(128)
        cFlags['a'.toInt()] = 1
        cFlags['e'.toInt()] = 2
        cFlags['i'.toInt()] = 4
        cFlags['o'.toInt()] = 8
        cFlags['u'.toInt()] = 16

        val sb = StringBuffer()

        for (t in (1..T)) {
            val n = sc.nextInt()
            val dCount=IntArray(32)

            for (j in 1..n) {
                val s = sc.next()!!
                var x = 0
                for (i in s) {
                    x = x or cFlags[i.toInt()]
                    if (x == 31)
                        break
                }
                dCount[x]++
            }

            var ans = 0L
            for(i in 1..30) {
                for(j in i+1..31) {
                    if((i or j)==31)
                        ans+=(dCount[i]*dCount[j])
                }
            }
            ans+=fak(dCount[31]-1)

            //println("${meals.size}")
            sb.append("$ans\n")
        }
        println(sb.toString())
    }

    fun fak(i:Int):Long {
        var ret=0L+if(i>0) 1 else 0
        for(j in 2..i)
            ret+=j
        return ret
    }

    public companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
