package com.happypeople.codechef.INAL2019

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        IALG2().run()
    } catch (e: Throwable) {
        println("")
    }
}

// ok
class IALG2 {
    fun run() {
        val sc = Scanner(systemIn())
        val N = sc.nextInt()
        val treeMap=TreeMap<String, String>()
        for(i in 1..N) {
            treeMap.put(sc.next().trim(), sc.next().trim())
        }

        val umlaute = setOf('a', 'e', 'i', 'o', 'u')
        val q = sc.nextInt()
        for (i in 1..q) {
            val query = sc.next().trim()
            val hits=treeMap.subMap(query, query+"\uFFFF")

            val sb=StringBuffer("")
            if (hits.size == 0)
                println("-1")
            else {
                val hitList=mutableListOf<Pair<String, String>>()
                for(ent in hits.entries) {
                    hitList.add(Pair(ent.key, ent.value))
                }

                val w = hitList.sortedByDescending { it.second }.first().first
                val c = w.filter { it=='a' || it=='e' || it=='i' || it=='o' || it=='u' }.count()
                println("$c")
            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}