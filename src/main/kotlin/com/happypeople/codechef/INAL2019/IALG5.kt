package com.happypeople.codechef.INAL2019

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        IALG5().run()
    } catch (e: Throwable) {
        println("")
    }
}

// ok
class IALG5 {
    fun run() {
        val sc = Scanner(systemIn())
        var n=sc.nextInt()
        var k=sc.nextInt()
        var r=sc.nextInt()

        var sum=0
        while(n>0 && k>0) {
            sum+=r
            k -= sum
            if(k>=0)
                n--
        }
        println("$n")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}