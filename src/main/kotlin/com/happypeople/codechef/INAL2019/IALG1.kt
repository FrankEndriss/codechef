package com.happypeople.codechef.INAL2019

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.math.BigDecimal
import java.util.*

fun main(args: Array<String>) {
    try {
        IALG1().run()
    } catch (e: Throwable) {
        println("")
    }
}

// ok
class IALG1 {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        for(i in 1..T) {
            val A = sc.nextInt()
            val B = sc.nextInt()
            val bdA = BigDecimal(A.toLong()).setScale(100)
            val a = bdA.divide(BigDecimal(B), BigDecimal.ROUND_HALF_UP).toPlainString()
            var ans="$a"
            while(ans.endsWith("0"))
                ans=ans.removeSuffix("0")
            if(ans.endsWith("."))
                ans=ans.removeSuffix(".")
            println(ans)
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}