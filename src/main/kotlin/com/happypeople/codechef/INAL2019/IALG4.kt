package com.happypeople.codechef.INAL2019

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        IALG4().run()
    } catch (e: Throwable) {
        println("")
    }
}

// see https://stackoverflow.com/questions/15013800/find-the-maximally-intersecting-subset-of-ranges
class IALG4 {
    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        for (i in 1..T) {
            val N = sc.nextInt()
            val k = sc.nextInt()

            // Find maximum number of intersecting ranges.
            // Note that we need to sort the open/close in opposite
            // of the usual order to get the "less than or equals to" right.
            var count = 0
            var maxCount = 0
            (1..N).flatMap {
                val x = sc.nextInt()
                listOf( // +1 for opening a range, -1 for closing
                        Pair(x / ((x % k) + 2), +1),
                        Pair(x, -1)
                )
            }.sortedWith(compareBy({ it.first }, { it.second }))
                    .forEach {
                        count += it.second
                        maxCount = max(count, maxCount)
                    }
            println("$maxCount")
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}