package com.happypeople.codechef.codechef

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CodechefApplication

fun main(args: Array<String>) {
	runApplication<CodechefApplication>(*args)
}

