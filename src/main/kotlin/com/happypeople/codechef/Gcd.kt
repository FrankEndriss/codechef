package com.happypeople.codechef

/** Greatest common divisor and lowest common multiple. */
class Gcd {
    companion object {

        fun lcm(a: Long, b: Long): Long {
            return a * (b / gcd(a, b))
        }

        fun lcm(input: List<Long>): Long {
            var result = input[0]
            for (i in input.indices)
                result = lcm(result, input[i])
            return result
        }

        fun gcd(pA: Long, pB: Long): Long {
            var b = pB
            var a = pA
            while (b > 0) {
                val temp = b
                b = a % b
                a = temp
            }
            return a
        }

        fun gcd(input: List<Long>): Long {
            var result = input[0]
            for (i in input.indices)
                result = gcd(result, input[i])
            return result
        }
    }
}