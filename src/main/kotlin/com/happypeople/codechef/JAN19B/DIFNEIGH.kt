package com.happypeople.codechef.JAN19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        DIFNEIGH().run()
    } catch (e: Throwable) {
        DIFNEIGH.log("" + e)
    }
}

/** FINI **/
class DIFNEIGH {
    fun run() {
        val sc = Scanner(systemIn())
        val t = sc.nextInt()
        for (i in (1..t)) {
            val n = sc.nextInt()
            val m = sc.nextInt()

            if(m*n<=2)
                prln("1")
            else if (min(m, n) == 1 || m*n==4)
                prln("2")
            else if (min(m, n) == 2)
                prln("3")
            else
                prln("4")

            printPattern(n, m)
        }
    }

    val pattern1 = listOf(
            listOf(1, 1, 2, 2)
    )

    val pattern2 = listOf(
            listOf(1, 2, 3),
            listOf(1, 2, 3)
    )

    val pattern4 = listOf(
            listOf(1, 1, 3, 3),
            listOf(2, 2, 4, 4),
            listOf(3, 3, 1, 1),
            listOf(4, 4, 2, 2)
    )


    private fun printPattern(n: Int, m: Int, pattern: List<List<Int>>, rot: Boolean) {
        if (!rot) {
            for (j in 0..n - 1) {
                for (i in 0..m - 1) {
                    pr("${pattern[j % pattern.size][i % pattern[0].size]} ")
                }
                prln()
            }
        } else {
            for (i in 0..m - 1) {
                for (j in 0..n - 1) {
                    pr("${pattern[j % pattern.size][i % pattern[0].size]} ")
                }
                prln()
            }
        }
    }

    private fun printPattern(pN: Int, pM: Int) {
        var rot = false
        var n = pN
        var m = pM
        if (m < n) {
            rot = true
            val tmp = n
            n = m
            m = tmp
        }

        when (n) {
            1 -> printPattern(n, m, pattern1, rot)
            2 -> printPattern(n, m, pattern2, rot)
            else -> printPattern(n, m, pattern4, rot)
        }
    }

    companion object {
        var inputStr: String? = null
        var outputStr:StringBuffer?=null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        fun pr(str:String) {
            if(outputStr!=null) {
                outputStr!!.append(str)
            }
            print(str)
        }
        fun prln(str:String="") {
            pr(str)
            pr("\n")
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}