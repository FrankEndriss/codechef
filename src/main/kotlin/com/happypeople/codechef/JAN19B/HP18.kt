package com.happypeople.codechef.JAN19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        HP18().run()
    } catch (e: Throwable) {
        HP18.log("" + e)
    }
}

/**  FINI
 * Need to consider that Vs are _NOT_ distinct. */
class HP18 {
    val ALICE="ALICE"
    val BOB="BOB"

    fun run() {
        val sc = Scanner(systemIn())
        val t = sc.nextInt()
        for (i in (1..t)) {
            val N = sc.nextInt()
            val bob = sc.nextInt()
            val alice = sc.nextInt()

            var bobCount = 0
            var aliceCount = 0
            var bothCount = 0
            for (i in (1..N)) {
                val v = sc.nextInt()
                if (v % bob == 0 && v % alice == 0)
                    bothCount++
                else {
                    if (v % bob == 0)
                        bobCount++
                    else if (v % alice == 0)
                        aliceCount++
                }
            }
            val bobMoves = bobCount + if (bothCount >0) 1 else 0
            val aliceMoves = aliceCount

            // Since bob has to start, he wins if he has more available moves than alice.
            if (bobMoves > aliceMoves)
                println(BOB)
            else
                println(ALICE)
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}