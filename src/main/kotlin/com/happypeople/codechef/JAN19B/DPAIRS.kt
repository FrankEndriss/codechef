package com.happypeople.codechef.JAN19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        DPAIRS().run()
    } catch (e: Throwable) {
        DPAIRS.log("" + e)
    }
}

/** FINI **/
class DPAIRS {
    val out=StringBuffer("")
    fun run() {
        val sc = Scanner(systemIn())
        val N=sc.nextInt()
        val M=sc.nextInt()

        val a=(1..N).map { sc.nextInt() }
        val aI=(0..N-1).sortedBy { a[it] }
        val b=(1..M).map { sc.nextInt() }
        val bI=(0..M-1).sortedBy { b[it] }

        for(x in 0..M-1)
            out.append("${aI[0]} ${bI[x]}\n")
        for(y in 1..N-1)
            out.append("${aI[y]} ${bI[M-1]}\n")
        println(out.toString())
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}