package com.happypeople.codechef.JAN19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        MGAME().run()
    } catch (e: Throwable) {
        MGAME.log("" + e)
    }
}

class MGAME {
    fun run() {
        val sc = Scanner(systemIn())
        val t = sc.nextInt()

        for (i in 1..t) {
            val N = sc.nextLong()
            val P = sc.nextLong()

            val sb = StringBuffer("")
            if (N > 2) {
                val x = (N + 1) / 2 - 1
                val ans = (P - x) * (P - x) + ((P - N) * ((P - x) + (P - N)))
                sb.append("$ans\n")
            } else {
                val ans = P.toLong() * P * P
                sb.append("$ans\n")
            }
            println(sb)
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }

        var outBuffer: StringBuffer? = null
        fun prln(s: String) {
            if (outBuffer != null)
                outBuffer!!.append(s)
            println(s)
        }
    }
}