package com.happypeople.codechef.JAN19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        MATCHIT().run()
    } catch (e: Throwable) {
        MATCHIT.log("" + e)
    }
}

data class P(val x: Int, val y: Int) {
    fun freePointsArround(): Sequence<P> {
        var idx = 0
        return generateSequence {
            if (idx == 0) {
                idx = 1
                if (x > 0 && (!unfreeP.isSet(x - 1, y)))
                    return@generateSequence P(x - 1, y)
            }

            if (idx == 1) {
                idx = 2
                if (x < N - 1 && (!unfreeP.isSet(x + 1, y)))
                    return@generateSequence P(x + 1, y)
            }

            if (idx == 2) {
                idx = 3
                if (y > 0 && (!unfreeP.isSet(x, y - 1)))
                    return@generateSequence P(x, y - 1)
            }

            if (idx == 3) {
                idx = 4
                if (y < N - 1 && (!unfreeP.isSet(x, y + 1)))
                    return@generateSequence P(x, y + 1)

            }

            null
        }
    }

    fun freePointsArround2(): List<P> {
        return listOf(
                P(x - 1, y),
                P(x + 1, y),
                P(x, y - 1),
                P(x, y + 1)
        ).filter { it.x >= 0 && it.y >= 0 && it.x < N && it.y < N && (!unfreeP.isSet(it)) }
    }

    fun weigth(): Long = vN[y][x].toLong()

    fun borderDist(): Int {
        return min(x, N - x) * min(y, N - y)
    }
}

data class PP(val p1: P, val p2: P) {
    fun manhattan(): Int {
        return abs(p1.x - p2.x) + abs(p1.y - p2.y)
    }

    fun borderDist(): Int {
        return p1.borderDist() + p2.borderDist()
    }
}

data class PMapChar(val n: Int) {
    val v: Array<CharArray>

    init {
        v = Array(n) { CharArray(n) { it -> '.' } }
    }

    fun mark(plist: List<P>, c: Char) {
        for (p in plist)
            v[p.y][p.x] = c
    }

    override fun toString(): String {
        val sb = StringBuffer("")
        for (y in v) {
            sb.append(y.joinToString(""))
            sb.append("\n")
        }
        return sb.toString()
    }
}

data class PMapI(val n: Int) {
    val v: Array<IntArray>

    init {
        v = Array(n) { IntArray(n) }
    }

    fun contains(p: P): Boolean {
        return v[p.y][p.x] > 0
    }

    fun set(p: P, value: Int) {
        v[p.y][p.x] = value
    }

    fun get(p: P): Int {
        return v[p.y][p.x]
    }
}

data class PMapB(val n: Int) {
    val v: Array<BooleanArray>

    init {
        v = Array(n) { BooleanArray(n) }
    }

    override fun toString(): String {
        val sb = StringBuffer("")
        for (y in v) {
            sb.append(y.map { if (it) "X" else "-" }.joinToString(""))
            sb.append("\n")
        }
        return sb.toString()
    }

    fun isSet(x: Int, y: Int) = v[y][x]
    fun isSet(p: P): Boolean = v[p.y][p.x]

    fun set(p: P) {
        v[p.y][p.x] = true
    }

    fun unset(p: P) {
        v[p.y][p.x] = false
    }

    fun setAll(plist: List<P>) {
        for (p in plist)
            v[p.y][p.x] = true
    }

    fun unsetAll(plist: List<P>) {
        for (p in plist)
            v[p.y][p.x] = false
    }
}

var N = 0
var M = 0
var vN = listOf(listOf(0))
var unfreeP = PMapB(1)

class MATCHIT {
    // data class Pathweigth(val steps: Int, val weigth: Long)

    fun run() {
        val sc = Scanner(systemIn())
        N = sc.nextInt()
        M = sc.nextInt()
        val m1 = (1..M).map { P(sc.nextInt() - 1, sc.nextInt() - 1) }
        val m2 = (1..M).map { P(sc.nextInt() - 1, sc.nextInt() - 1) }

        vN = (1..N).map { (1..N).map { sc.nextInt() } }

        val mP = Array(M) { PP(m1[it], m2[it]) }

        /* sorted by manhattan distance from p1 to p2
                */
        val mIdx = (0..M - 1).toList()
                .sortedWith(Comparator<Int> { idx1, idx2 -> mP[idx1].manhattan() - mP[idx2].manhattan() })

        /* sorted by manhattan distance from center DESC
        val center = P((N - 1) / 2, (N) / 2)
        val mIdx = (0..M - 1).toList()
                .sortedWith(Comparator<Int> { idx1, idx2 ->
                    val dist1 =
                            max(PP(mP[idx1].p1, center).manhattan(),
                                    PP(mP[idx1].p2, center).manhattan())
                    val dist2 =
                            max(PP(mP[idx2].p1, center).manhattan(),
                                    PP(mP[idx2].p2, center).manhattan())
                    dist1 - dist2
                })
         *  */
        /*
        val mIdx=(0..M-1).toList()
                .sortedWith(Comparator<Int> { idx1, idx2 ->  mP[idx1].borderDist() - mP[idx2].borderDist()})
                */

        unfreeP = PMapB(N)
        unfreeP.setAll(m1)
        unfreeP.setAll(m2)
        //log("unfreeP at start: ")
        //log("$unfreeP")

        // find min 500 Pathes from m1[0] to m2[0]... trie to collect most valuable vN on the way, avoid negative vN

        // simple brute force
        // Find one path after another
        // Each one by search some possible path
        val paths = mutableListOf<List<P>>()
        // var pathMarker='a'
        // var pathDisplay=PMapChar(N)

        for (i in mIdx.indices) {
            val pp = mP[mIdx[i]]
            // log("searching from ${pp.p1} to ${pp.p2} len=${pp.manhattan()}")
            unfreeP.unset(pp.p1)
            unfreeP.unset(pp.p2)

            var step = 0
            var nextPoints = setOf(pp.p1)
            var countedPoints2 = PMapI(N)
            countedPoints2.set(pp.p1, 1)
            // var countedPoints = mutableMapOf(pp.p1 to 0)       // count lengthOfPath
            while (!countedPoints2.contains(pp.p2)) {
                step++
                val nextNextPoints = mutableSetOf<P>()
                for (p in nextPoints) {
                    // log("nextPoint: $p")
                    for (freeP in p.freePointsArround()) {
                        // log("freePoint arround: $freeP")
                        val cP = countedPoints2.get(freeP)
                        // log("cP for freePoint arround: $cP")
                        if (cP == 0) {
                            countedPoints2.set(freeP, step)
                            nextNextPoints.add(freeP)
                        }
                    }
                }
                if (nextNextPoints.isEmpty()) {
                    println("no path from ${pp.p1} to ${pp.p2} idx=$i, abort")
                    //println("$countedPoints")
                    return
                }
                nextPoints = nextNextPoints
            }
            // countedPoints now contains m2, and all points on a Path to m1.
            // If more than one path is possible, they can be choosen using the weigth.
            val path = mutableListOf<P>()
            path.add(pp.p2)
            var currStep = countedPoints2.get(pp.p2)
            //log("extracting path, starting at: ${pp.p2}")
            while (!path.contains(pp.p1)) {
                val head = path.last()
                //log("extracting path, head=$head")
                currStep--
                val nextPoint = head.freePointsArround()
                        .map { it to countedPoints2.get(it) }
                        .filter { it.first == pp.p1 || (it.second != null && it.second == currStep) }
                        .first()
                path.add(nextPoint.first)
            }
            unfreeP.setAll(path)
            paths.add(path)
            //pathDisplay.mark(path, pathMarker)
            //pathMarker++
            //log("$pathDisplay")
        }

        // val ans=paths
        for (path in paths) {
            print("${path.size} ")
            for (p in path)
                print("${p.x} ${p.y} ")
            println()
            // println("${path.size} ${path.joinToString(" ")}")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
