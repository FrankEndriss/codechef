package com.happypeople.codechef.JAN19B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        FANCY().run()
    } catch (e: Throwable) {
        FANCY.log("" + e)
    }
}

/** FINI **/
class FANCY {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        sc.nextLine()       // skip EOL of first line
        for (i in (1..t)) {
            val str=sc.nextLine()!!.trim()
            log(str)
            if(str.equals("not") || str.startsWith("not ") || str.contains(" not ") || str.endsWith(" not"))
                println("Real Fancy")
            else
                println("regularly fancy")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}