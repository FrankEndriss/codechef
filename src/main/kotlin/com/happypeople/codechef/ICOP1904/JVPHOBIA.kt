package com.happypeople.codechef.ICOP1904

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        JVPHOBIA().run()
    } catch (e: Throwable) {
        println("")
    }
}

class JVPHOBIA {
    fun run() {
        val sc = Scanner(systemIn())
        val nodes = sc.nextInt()
        val edges = sc.nextInt()
        val graph = mutableMapOf<Int, MutableSet<Int>>()
        val edgelist = mutableListOf<Pair<Pair<Int, Int>, Int>>()
        for (i in 1..edges) {
            edgelist.add(Pair(Pair(sc.nextInt(), sc.nextInt()), sc.nextInt()))
        }
        val K = sc.nextInt()
        val S = sc.nextInt()
        val friends = (1..K).map { sc.nextInt() }

        // build the graph
        edgelist.filter { it.second <= S }.map { it.first }.forEach {
            graph.getOrPut(it.first) { mutableSetOf<Int>() }.add(it.second)
            graph.getOrPut(it.second) { mutableSetOf<Int>() }.add(it.first)
        }
        // traverse the graph
        val nodesVisited = mutableSetOf<Int>()
        var currNodes = mutableSetOf<Int>()
        currNodes.addAll(friends)
        while(!currNodes.isEmpty()) {
            nodesVisited.addAll(currNodes)
            currNodes = currNodes.flatMap {
                graph.get(it)!!
            }.filter { !nodesVisited.contains(it) }.toMutableSet()
        }
        val ans=nodesVisited.size
        println("$ans")
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}