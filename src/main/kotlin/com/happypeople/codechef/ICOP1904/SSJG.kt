package com.happypeople.codechef.ICOP1904

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

fun main(args: Array<String>) {
    try {
        SSJG().run()
    } catch (e: Throwable) {
        println("")
    }
}

fun lsb(i: Int) = i and -i

/* Better solution would be to create prefix sum array
    and bruteforce all possibilities.
    This Fenwick solution might  need to check the same number of
    possibilities, but is much slower caused by tree vs array handling.
    Its kind of tricky to build the 2D-Prifix array.
 */
class SSJG {
    fun run() {
        val sc = Scanner(systemIn())
        val T = sc.nextInt()
        (1..T).forEach {

            val N = sc.nextInt()
            val M = sc.nextInt()
            val fenwick = Fenwick2D<Int>(N, N, 0, false) { x, y -> x + y }
            for (i in (1..M)) {
                val x = sc.nextInt()
                val y = sc.nextInt()
                fenwick.updateCumul(x, y, 1)
            }

            var maxC = 0
            val visited=mutableSetOf<Pair<Int, Int>>()
            var next=mutableSetOf(Pair(N/2, N/2))
            while (!next.isEmpty()) {
                //println("$next")
                visited.addAll(next)
                next=next.flatMap {
                    listOf(Pair(it.first+1, it.second),
                           Pair(it.first-1, it.second),
                           Pair(it.first, it.second+1),
                           Pair(it.first, it.second-1)
                    )
                }.filter { it.first>=1 && it.first<=N && it.second>=1 && it.second<=N
                }.filter { !visited.contains(it)
                }.map {
                    val ld = fenwick.readCumul(it.first, it.second)
                    val lu = fenwick.readCumul(it.first, N) - ld
                    val rd = fenwick.readCumul(N, it.second) - ld
                    val ru = M - (rd+lu+ld)
                    Pair(it, listOf(ld, lu, rd, ru).min()!!)
                }.filter { it.second>=maxC
                }.map { maxC=max(maxC, it.second) ; it.first
                }.toMutableSet()
            }
            println("$maxC")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }


/* BinaryIndexTree see
https://www.topcoder.com/community/competitive-programming/tutorials/binary-indexed-trees/
https://en.wikipedia.org/wiki/Fenwick_tree
https://www.youtube.com/watch?v=v_wj_mOAlig
https://github.com/jakobkogler/Algorithm-DataStructures/blob/master/RangeQuery/BinaryIndexedTree.cpp
https://codeforces.com/problemset/problem/1093/E
 */
    /** Two dimensional Fenwick, uses less memory if sparse==true. */
    class Fenwick2D<E>(val sizeX: Int, val sizeY: Int, val neutral: E, val sparse: Boolean = false, val cumul: (E, E) -> E) {
        val tree = arrayOfNulls<FenwickIf<E>>(sizeX + 1)

        fun treeget(x: Int): FenwickIf<E> {
            if (tree[x] == null) {
                tree[x] = if (sparse)
                    FenwickSparse<E>(sizeY, neutral, cumul)
                else
                    Fenwick<E>(sizeY, neutral, cumul)
            }
            return tree[x]!!
        }

        /** Updates the value at pIx/pIy cumulative, i.e. "adds" value to the value stored at pIx/pIy. */
        fun updateCumul(pIx: Int, pIy: Int, value: E) {
            var x = pIx
            while (x < tree.size) {
                treeget(x).updateCumul(pIy, value)
                x += lsb(x)
            }
        }

        /** @return the cumul of range (0,0):(pIx, pIy) inclusive. */
        fun readCumul(pIx: Int, pIy: Int): E {
            var x = pIx
            var sum = neutral
            while (x > 0) {
                sum = cumul(sum, treeget(x).readCumul(pIy))
                x -= lsb(x)
            }
            return sum
        }
    }

    /** Sparse Fenwick tree implementation, uses a map instead of an array.
     * Usefull if less nodes are used.
     * @param E Type of elements
     * @param neutral The neutral element according to cumul.
     * @param cumul The cumulative function, ie sum() or the like.
     **/
    class FenwickSparse<E>(val sizeofTree: Int, val neutral: E, val cumul: (E, E) -> E) : FenwickIf<E> {
        val tree = mutableMapOf<Int, E>()
        /** TODO find a way to dynamically grow the tree to new sizes. */

        /** Updates the value at pIdx cumulative, i.e. "adds" value to the value stored at pIdx. */
        override fun updateCumul(pIdx: Int, value: E) {
            var idx = pIdx
            while (idx < sizeofTree) {
                tree[idx] = cumul(tree.getOrDefault(idx, neutral), value)
                idx += lsb(idx)
            }
        }

        /** @return the cumul of range 0:pIdx inclusive. */
        override fun readCumul(pIdx: Int): E {
            var idx = pIdx
            var sum = neutral
            while (idx > 0) {
                sum = cumul(sum, tree.getOrDefault(idx, neutral))
                idx -= lsb(idx)
            }
            return sum
        }

    }

// TODO FenwickInt, based on IntArray
// TODO Fenwick2DInt, based on single IntArray
// TODO Fenwick2DSparse, both dimension based on Map

    /** All purpose Fenwick tree implementation. Note that Idx is 1 based, idx=0 must not be used.
     * @param E Type of elements
     * @param size Max index into tree.
     * @param neutral The neutral element according to cumul.
     * @param cumul The cumulative function, ie sum() or the like.
     **/
    class Fenwick<E>(size: Int, val neutral: E, val cumul: (E, E) -> E) : FenwickIf<E> {
        val tree = (0..size).map { neutral }.toMutableList()

        override fun updateCumul(pIdx: Int, value: E) {
            if (pIdx < 1)
                throw IllegalArgumentException("lowest possible index is 1, you sent: $pIdx")
            var idx = pIdx
            while (idx < tree.size) {
                tree[idx] = cumul(tree[idx], value)
                idx += lsb(idx)
            }
        }

        /** Read a single value at  pIdx, needs an uncumul function, ie minus if used plus for cumul. */
        fun readSingle(pIdx: Int, uncumul: (E, E) -> E): E {
            var idx = pIdx
            var sum = tree[idx]
            if (idx > 0) {
                val z = idx - lsb(idx)
                idx--
                while (idx != z) {
                    sum = uncumul(sum, tree[idx])
                    idx -= lsb(idx)
                }
            }
            return sum
        }

        override fun readCumul(pIdx: Int): E {
            var idx = pIdx
            var sum = neutral
            while (idx > 0) {
                sum = cumul(sum, tree[idx])
                idx -= lsb(idx)
            }
            return sum
        }
    }

    interface FenwickIf<E> {
        /** Updates the value at pIdx cumulative, i.e. "adds" value to the value stored at pIdx.
         * @param pIdx 1 based index into structure.
         * @param value the delta for the Update. */
        fun updateCumul(pIdx: Int, value: E)

        /** @return the cumul of range 0:pIdx inclusive. */
        fun readCumul(pIdx: Int): E
    }

}