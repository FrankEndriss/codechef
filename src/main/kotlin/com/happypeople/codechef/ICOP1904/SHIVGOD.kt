package com.happypeople.codechef.ICOP1904

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.lang.Long.max
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        SHIVGOD().run()
    } catch (e: Throwable) {
        println("")
    }
}

class SHIVGOD {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        for(i in 1..t) {
            val N=sc.nextInt()
            val maxPages=sc.nextInt()
            val minPages=sc.nextInt()
            val pages=(1..N).map { sc.nextInt() }

            var sums=(minPages..maxPages).map{ 0L }.toMutableList()
            var avgs=(minPages..maxPages).map { 0.0 }.toMutableList()
            var ps=(minPages..maxPages).toList()

            for(i in 0..N-1) {
                for(j in sums.indices) {
                    sums[j] = sums[j] + pages[i]
                    if(i==ps[j]-1) {
                        avgs[j] = sums[j] / ps[j].toDouble()
                    }

                    if (i >= ps[j]) {
                        sums[j] = sums[j] - pages[i - ps[j]]
                        avgs[j] = max(avgs[j], sums[j] / ps[j].toDouble())
                    }
                }
            }
            println("${avgs.max()}")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}