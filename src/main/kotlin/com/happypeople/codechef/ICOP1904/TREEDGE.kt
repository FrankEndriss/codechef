package com.happypeople.codechef.ICOP1904

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.lang.Long.max
import java.util.*
import kotlin.math.max

fun main(args: Array<String>) {
    try {
        TREEDGE().run()
    } catch (e: Throwable) {
        println("")
    }
}

class TREEDGE {
    fun run() {
        val sc = Scanner(systemIn())
        val T=sc.nextInt()
        val tree=mutableMapOf<Int, MutableList<Pair<Int, Int>>>()
        (1..T).forEach {
            val nodes=sc.nextInt()
            val queries=sc.nextInt()
            (1..nodes-1).forEach {
                val n1=sc.nextInt()
                val n2=sc.nextInt()
                val w=sc.nextInt()
                // assume n1 is allways parent of n2
                // not sure about that, problem setters didn't care
                val parentNode=n1
                val list=tree.getOrPut(parentNode) { mutableListOf<Pair<Int, Int>>() }
                list.add(Pair(n2, w))
            }

            (1..queries).forEach {
                val u=sc.nextInt()
                val v=sc.nextInt()
                val w=sc.nextInt()
                // TODO find max path (which ends in node maxNode) in subtree u and v,
                // add the pathweights add w
                val ans=maxpath(tree, u) + maxpath(tree, v) + w
                println("$ans")
            }
        }
    }

    /* Finds the maximum Path in subtree root. */
    fun maxpath(tree:MutableMap<Int, MutableList<Pair<Int, Int>>>, root:Int):Int {
        // in fact, we need to traverse the whole subtree, counting
        // the path with every step.
        val emptyList=listOf<Pair<Int, Int>>()
        var maxW=Int.MIN_VALUE
        val stack=mutableListOf(Pair(root, 0)) // start at root with weight 0
        while(!stack.isEmpty()) {
            val nextNode=stack.last()
            stack.removeAt(stack.size-1)
            maxW=max(maxW, nextNode.second)
            val childs=tree.getOrDefault(nextNode.first, emptyList).map { it.first to it.second+nextNode.second }
            stack.addAll(childs)
        }
        return maxW
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}