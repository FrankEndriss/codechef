package com.happypeople.codechef.COOK102B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        ADAPWNS().run()
    } catch (e: Throwable) {
        println("")
    }
}

class ADAPWNS {
    fun run() {
        val sc = Scanner(systemIn())
        val t = sc.nextInt()
        for (i in 1..t) {
            val s = sc.next().trim().map { it == 'P' }

            // count number of min/max moves
            val pCount=0
            val maxMoves=0
            for (i in s.indices) {

            }
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}