package com.happypeople.codechef.COOK102B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        ADAKNG().run()
    } catch (e: Throwable) {
        println("")
    }
}

class ADAKNG {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        for(i in 1..t) {
            val x = sc.nextInt() - 1
            val y = sc.nextInt() - 1
            val k = sc.nextInt()

            // brute force
            var next = mutableListOf(Pair(x, y))
            val map = mutableSetOf<Pair<Int, Int>>()
            map.add(next[0])
            var moves = 0
            while (!next.isEmpty() && moves < k) {
                moves++
                next = next.flatMap {
                    listOf(
                            Pair(it.first - 1, it.second - 1),
                            Pair(it.first - 1, it.second),
                            Pair(it.first - 1, it.second + 1),
                            Pair(it.first + 1, it.second - 1),
                            Pair(it.first + 1, it.second),
                            Pair(it.first + 1, it.second + 1),
                            Pair(it.first, it.second - 1),
                            Pair(it.first, it.second + 1)
                    )
                }.filter { it.first >= 0 && it.second >= 0 && it.first <= 7 && it.second <= 7 }
                        .filter { !map.contains(it) }
                        .toMutableList()
                map.addAll(next);
            }
            val ans = map.size
            println("$ans")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}