package com.happypeople.codechef.COOK102B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        ADASCOOL().run()
    } catch (e: Throwable) {
        println("")
    }
}

class ADASCOOL {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        for(i in 1..t) {
            val n=sc.nextInt()
            val m=sc.nextInt()
            val even=(n*m)%2==0
            val ans=if(even) "YES" else "NO"
            println("$ans")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}