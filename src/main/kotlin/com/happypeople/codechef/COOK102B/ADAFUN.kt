package com.happypeople.codechef.COOK102B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        ADAFUN().run()
    } catch (e: Throwable) {
        println("")
    }
}

class ADAFUN {
    fun run() {
        val sc = Scanner(systemIn())
        val t = sc.nextInt()
        for (i in 1..t) {
            val n = sc.nextInt()
            val x = (1..n).map { sc.nextLong() }.toSet()
            for (i in x) {
                val ans = w(x.minus(setOf(i)))
                print("$ans ")
            }
            println()
        }
    }

    fun w(x: Set<Long>): Long {
        val list=x.toList()
        if (x.size == 1)
            return list[0]
        else if (x.size == 2) {
            return (list[0] xor list[1]) - 1
        } else {
            var lastPair = Pair(0L, 0L)
            var mask = 0L
            for (i in 1..63) {
                val nextPair = findPair(list, mask)
                if (nextPair != null)
                    lastPair = nextPair
                mask = (mask shl 1) or 1
            }
            val remPair = setOf(lastPair.first, lastPair.second)
            return w(remPair) xor w(x.minus(remPair))
        }
    }

    private fun findPair(list: List<Long>, mask: Long): Pair<Long, Long>? {
        for (j in list.indices) {
            for (k in j + 1..list.size - 1) {
                if (list[j] and mask == list[k] and mask)
                    return Pair(list[j], list[k])
            }
        }
        return null
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}