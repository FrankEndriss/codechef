package com.happypeople.codechef.COOK102B

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        ADAMTR().run()
    } catch (e: Throwable) {
        println("")
    }
}

class ADAMTR {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        for(i in 1..t) {
            solveOne(sc)
        }
    }

    private fun solveOne(sc: Scanner) {
        val n=sc.nextInt()
        val A=IntArray(n*n)
        val B=IntArray(n*n)
        for(i in 0..n*n-1) { A[i]=sc.nextInt() }
        for(i in 0..n*n-1) { B[i]=sc.nextInt() }

        for(i in 0..n-1)
            if(A[i*n+i]!=B[i*n+i]) {
                println("No")
                return
            }

        // TODO 1. printing to StringBuffer should make it fast enough 2. Comparing is not enough, we need to execute the swap.
        for(i in 0..n-1) {
            for (j in i+1..n-1) {
                if((A[i*n+j]==B[i*n+j] && A[j*n+i]==B[j*n+i]) || (A[i*n+j]==B[j*n+i] && A[j*n+i]==B[i*n+j])) {
                    ; // ok
                } else {
                    println("No")
                    return
                }
            }
        }
        println("Yes")
    }


    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}