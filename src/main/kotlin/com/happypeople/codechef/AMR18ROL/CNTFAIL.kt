package com.happypeople.codechef.AMR18ROL

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        CNTFAIL().run()
    } catch (e: Throwable) {
        CNTFAIL.log("" + e)
    }
}

class CNTFAIL {
    fun run() {
        val sc = Scanner(systemIn())
        val t = sc.nextInt()
        (1..t).forEach {
            val n = sc.nextInt()
            val passCount = (1..n).map { sc.nextInt() }
            val min = passCount.min()!! // passCount can contain only two numbers
            val minC=passCount.filter { it==min }.count()
            val max = passCount.max()!! // and these must differ by one
            val maxC=passCount.filter { it==max }.count()
            if (max - min > 1 || max>n-1) {
                println("-1")
            } else if(min==max) { // all passed/failed
                if(min==n-1)
                    println("0")
                else if(min==0)
                    println("$n")
                else
                    println("-1")
            } else {
                // check if consistent
                val failed=n-max
                if(maxC!=failed)
                    println("-1")
                else
                    println("${failed}")
            }
        }

    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}