package com.happypeople.codechef.AMR18ROL

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        ISITCAKE().run()
    } catch (e: Throwable) {
        ISITCAKE.log("" + e)
    }
}

class ISITCAKE {
    fun run() {
        val sc = Scanner(systemIn())
        val t=sc.nextInt()
        (1..t).forEach {
            var c=0
            (1..100).forEach {
                if(sc.nextInt()<=30)
                    c++
            }
            if(c>=60)
                println("yes")
            else
                println("no")
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}