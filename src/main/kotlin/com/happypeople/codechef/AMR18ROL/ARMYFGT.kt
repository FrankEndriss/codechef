package com.happypeople.codechef.AMR18ROL

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.io.PrintWriter
import java.util.*

fun main(args: Array<String>) {
    try {
        ARMYFGT().run()
    } catch (e: Throwable) {
        ARMYFGT.log("" + e)
    }
}

class ARMYFGT {
    fun run() {
        val sc = Scanner(systemIn())
        val t = sc.nextInt()
        (1..t).forEach {
            val n = sc.nextInt()
            val rebels = (1..n).map { sc.nextLong() }
            val kingLo = sc.nextLong()
            val kingUp = sc.nextLong()

            var product = lcm(rebels)
            log("product: $product")

            // now find how often multiples of product are in range kLo to kUp
            // 1. find where the first occurence is:
            val firstOcc = if (kingLo % product == 0L) kingLo else kingLo + product - kingLo % product
            log("firstOcc=$firstOcc")
            var c = kingUp - kingLo + 1
            if (firstOcc > kingUp) { // no occ in range
                c = c
            } else {
                c -= (kingUp - firstOcc) / product
                c -= 1
            }

            prntln("$c")
        }
    }

    private fun lcm(a: Long, b: Long): Long {
        return a * (b / gcd(a, b))
    }

    private fun lcm(input: List<Long>): Long {
        var result = input[0]
        for (i in input.indices)
            result = lcm(result, input[i])
        return result
    }

    fun gcd(pA: Long, pB: Long): Long {
        var b = pB
        var a = pA
        while (b > 0) {
            val temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    fun gcd(input: List<Long>): Long {
        var result = input[0];
        for (i in input.indices)
            result = gcd(result, input[i]);
        return result;
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }

        var grabResult: PrintWriter? = null
        fun prntln(str: String) {
            grabResult?.println(str)
            println(str)
        }
    }
}