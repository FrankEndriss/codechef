package com.happypeople.codechef.COOK104A

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        MARCAPS().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class MARCAPS {
    fun run() {
        val sc = Scanner(systemIn())
        val t = sc.nextInt()
        for(T in 1..t) {
            val n = sc.nextInt()
            val a = (1..n).map { sc.nextInt() - 1 }
            val freqC = IntArray(n)
            var m = 0
            for (c in a) {
                freqC[c]++
                if (freqC[c] > m)
                    m = freqC[c]
            }
            if (m > n / 2) {
                println("NO")
                continue
            } else {
                println("YES")
            }

            // Find distribution by sorting of frequencies.
            // We need to find the color with lowest frequency, and round change that cap
            // with all other colors. That decreases to number of caps of every color by one.
            // Repeat until finished.
            // BUT: We need to output that distribution in the right permutation, which is the
            // one given in a[]. -> sic :/

            /*
            val freqIdx=(0..freqC.size-1).sortedBy { freqC[it] }.filter { freqC[it]>0 }
            var first=0
            while(first<freqIdx.size) {
                // find next non empty color
                while(freqC[freqIdx[first]]==0)
                    first++
                for(i in first..freqIdx.size-1) {
                    print("${freqIdx[i]+1} ")
                    c[sortedC[i]]--
                }
            }
            */
            println()
        }
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
