package com.happypeople.codechef.COOK104A

import java.io.ByteArrayInputStream
import java.io.InputStream
import java.util.*

fun main(args: Array<String>) {
    try {
        NBONACCI().run()
    } catch (e: Throwable) {
        println("")
        e.printStackTrace()
    }
}

class NBONACCI {
    fun run() {
        val sc = Scanner(systemIn())
        val N = sc.nextInt()
        val Q = sc.nextInt()
        val F = IntArray(N * 2)
        var Fx = 0
        for (i in 0..N - 1) {
            F[i] = sc.nextInt()
            Fx = Fx xor F[i]
        }
        for (i in N..N * 2 - 1)
            F[i] = Fx xor F[i - N]

        val S = IntArray(N * 2)
        S[0] = F[0]
        for (i in 1..N * 2 - 1)
            S[i] = S[i - 1] xor F[i]

        // S[N] = S[N-1] xor F[N-N]
        // S[N+1] = S[N] xor F[N+1-N]
        // S[N+2] = S[N+1] xor F[N+2-N]

        (1..Q).forEach {
            val q = sc.nextInt()
            println("${S[q % S.size]}")
        }

        // scheiss gefiddel...
    }

    companion object {
        var inputStr: String? = null

        fun systemIn(): InputStream {
            if (inputStr != null)
                return ByteArrayInputStream(inputStr!!.toByteArray())
            else
                return System.`in`
        }

        var printLog = false
        fun log(str: String) {
            if (printLog)
                println(str)
        }
    }
}
