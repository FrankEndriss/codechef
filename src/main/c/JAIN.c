
/* This was to slow in kotlin :/
 */

#include <stdio.h>

int cFlags[256];
char str[100001];
int dCount[32];

long long fak(int i) {
long long ret=0;
int j;
	if(i>0)
		ret=1;

	for(j=2; j<=i; j++)
		ret*=j;
	return ret;
}

void solveOne() {
int n;
	scanf("%d", &n);

int i=0;
	for(i=0; i<32; i++)
		dCount[i]=0;

	for(i=0; i<n; i++) {
		scanf("%s", str);
// printf("String: %s\n", str);
int x=0;
int sIdx=0;
int c;
		while(x!=31 && (c=str[sIdx++])>0)
			x|=cFlags[c];

		dCount[x]++;
	}
long long ans=0;
int j=0;

	for(i=1; i<31; i++) {
		for(j=i+1; j<=31; j++) {
			if((i|j)==31)
				ans+=(dCount[i]*dCount[j]);
		}
	}
	ans+=fak(dCount[31]-1);

	printf("%lld\n", ans);
}

int main(int argc, char **argv) {
int t;
int i;
	cFlags['a']=1;
	cFlags['e']=2;
	cFlags['i']=4;
	cFlags['o']=8;
	cFlags['u']=16;

	scanf("%d", &t);
	for(i=0; i<t; i++) {
		solveOne();
	}
	return 0;
}

