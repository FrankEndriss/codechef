
/* This was to slow in kotlin :/
 */

#include <stdio.h>

void solveOne() {
int n;
		scanf("%d", &n);
int A[n][n];
int B[n][n];
int c;
	for(int i=0; i<n; i++)
		for(int j=0; j<n; j++) {
			scanf("%d", &c);
			A[i][j]=c;
		}
	for(int i=0; i<n; i++)
		for(int j=0; j<n; j++) {
			scanf("%d", &c);
			B[i][j]=c;
		}

    // TODO we need to execute tha swap of col/rows if A[i][j]!=B[i][j]
    // Simply comparing the two elements leads to wrong answer.
	for(int i=0; i<n; i++) {
		for(int j=0; j<n; j++) {
			if(((A[i][j]!=B[i][j]) && (A[i][j]!=B[j][i]))) {
                    		printf("No\n");
                    		return;
			}
		}
	}
	printf("Yes\n");
}

int main(int argc, char **argv) {
int t;
	scanf("%d", &t);
	for(int i=0; i<t; i++) {
		solveOne();
	}
	return 0;
}

