
#include <stdio.h>

/* This was to slow in kotlin :/
 */
int main(int argc, char **argv) {
long t, N, P, x, ans;
long long lAns;
	scanf("%ld", &t);
	for(int i=0; i<t; i++) {
		scanf("%ld %ld", &N, &P);
		if(N>2) {
                	x = (N + 1) / 2 - 1;
                	ans = (P - x) * (P - x) + ((P - N) * ((P - x) + (P - N)));
			printf("%ld\n", ans);
		} else {
			lAns=P;
			lAns=lAns*lAns*lAns;
			printf("%lld\n", lAns);
		}
	}
	return 0;
}
