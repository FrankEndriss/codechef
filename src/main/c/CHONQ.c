
/* This was to slow in kotlin :/
 */

#include <stdio.h>

int A[100001];

int solve(int n, int k) {
int i;
int j;
int sum;
int divisor;
int d;
	for(i=0; i<n; i++) {
		sum=0;
		divisor=1;
		for(j=i; j<n; j++) {
			sum+=(d=A[j]/divisor);
			divisor++;
			if(sum>k) {
				if(d>k)	// jump if possible
					i=j;				
				break;
			}	
		}
		if(sum<=k)
			return i+1;
	}
	return n+1;
}

void solveOne() {
int n;
int k;
	scanf("%d", &n);
	scanf("%d", &k);

	int i=0;
	for(i=0; i<n; i++) {
		scanf("%d", &A[i]);
	}

	int ans=solve(n, k);
	printf("%d\n", ans);
}

int main(int argc, char **argv) {
int t;
int i;

	scanf("%d", &t);
	for(i=0; i<t; i++) {
		solveOne();
	}
	return 0;
}

