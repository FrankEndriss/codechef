
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LONG long
int min(int a, int b) {
	if(a<b)
		return a;
	return b;
}

int max(int a, int b) {
	if(a>b)
		return a;
	return b;
}

struct vector {
	int size;
	int used;
	int *data;
};

void vector_construct(struct vector *this) {
	this->size=0;
	this->used=0;
	this->data=0;
}

void vector_destruct(struct vector *this) {
	if(this->data)
		free(this->data);
}

#define VEC_INITSZ 16
void vector_add(struct vector *this, int value) {
//printf("vector_add, this=%p, value=%d\n", this, value);
	if(this->used==this->size) {
		if(this->data==0) {
			this->data=(int*)malloc(sizeof(int)*VEC_INITSZ);
			this->size=VEC_INITSZ;
			this->used=0;
		} else {
			this->data=(int*)realloc(this->data, this->size*2*sizeof(int));
			this->size*=2;
		}
	}
	this->data[this->used++]=value;
//printf("vector_add fini\n");
}

int vector_get(struct vector *this, int idx) {
	return this->data[idx];
}

void vector_clear(struct vector *this) {
	this->used=0;
}

void stack_push(struct vector *this, int val) {
	vector_add(this, val);
}

int stack_pop(struct vector *this) {
	this->used--;
	return this->data[this->used];
}

#define TREESZ 200001
struct vector nTree[TREESZ];
struct vector nTreeDists[TREESZ];

void precalcDistances() {
//printf("precalcDistances()\n");
struct vector stack; vector_construct(&stack);
struct vector next;  vector_construct(&next);
struct vector next2;  vector_construct(&next2);
struct vector *pNext, *pNext2, *pTmp;
int i, j;

	pNext=&next;
	pNext2=&next2;
	vector_add(pNext, 0);	// root
	while(pNext->used) {
		for(i=0; i<pNext->used; i++) {
			if(nTree[pNext->data[i]].used>0 ) {
//printf("precalcDistances(), stack_push(%d)\n", next.data[i]);
				stack_push(&stack, pNext->data[i]);
			}
		}

		if(nTree[pNext->data[0]].used>0) {
			for(i=0; i<pNext->used; i++) {
				for(j=0; j<nTree[pNext->data[i]].used; j++) {
//printf("precalcDistances(), i=%d j=%d\n", i, j);
//printf("precalcDistances(), vector_add(next2, %d)\n", nTree[next.data[i]].data[j]);
					vector_add(pNext2, nTree[pNext->data[i]].data[j]);
				}
			}
//printf("precalcDistances(), bev swap next2.used=%d\n", next2.used);
			vector_destruct(pNext);
			vector_construct(pNext);
			pTmp=pNext;
			pNext=pNext2;
			pNext2=pTmp;
			
		} else
			vector_clear(pNext);
	}
	vector_destruct(pNext);
	vector_destruct(pNext2);
	
	while(stack.used) {
		int node=stack_pop(&stack);
//printf("stack_pop()=%d\n", node);
		vector_add(&nTreeDists[node], nTree[node].used);
		for(i=0; i<nTreeDists[nTree[node].data[0]].used; i++) {
			int sum=0;
			for(j=0; j<nTree[node].used; j++) {
				sum+=nTreeDists[ nTree[node].data[j] ].data[i];
			}
//printf("nTreeDists add, node=%d sum=%d\n", node, sum);
			vector_add(&nTreeDists[node], sum);
		}
	}
	vector_destruct(&stack);
}

#define MOD 1000000007

int plus(int v1, int v2) {
	int res = v1 + v2;

	if (res < 0)
		res += MOD;
	else if (res >= MOD)
		res -= MOD;

	return res;
}

int mul(int v1, int v2) {
	return (int)((1L * v1 * v2) % MOD);
}

int lastB=-424242;
int lastN=-424242;
int lastX=-424242;
int lastSum=-424242;

int geomSum_simple(int b, int pN) {
int i;
        int x1 = 1;
        int sum1 = 1;

        if(b!=lastB || pN<lastN) {
            for (i=1; i<=pN - 1; i++) {
                x1 = mul(x1, b);
                sum1 = plus(sum1, x1);
            }
        } else {
            x1=lastX;
            sum1=lastSum;
            for (i=lastN; i<=pN - 1; i++) {
                x1 = mul(x1, b);
                sum1 = plus(sum1, x1);
            }
        }
        lastB=b;
        lastN=pN;
        lastX=x1;
        lastSum=sum1;
        return sum1;
}

int geomSum(int b, int pN, int m) {
	int n=pN;
	LONG T = 1L;
	LONG e = (1L * b) % m;
	LONG total = 0L;
	while (n > 0) {
		if (n & 1)
			total = (e * total + T) % m;
		T = ((e + 1) * T) % m;
		e = (e * e) % m;
		n /= 2;
	}
//printf("geomSum b=%d n=%d -> %d\n", b, pN, (int)total);
	return (int)total;
}

// edges, unknown which one is parent, which one is child
struct vector graph[TREESZ];
int parent[TREESZ];

void solve() {
int N, Q, i;
	memset(nTree, 0, sizeof(nTree));
	memset(nTreeDists, 0, sizeof(nTreeDists));
	memset(graph, 0, sizeof(graph));
	memset(parent, 0, sizeof(parent));
	scanf("%d", &N);
	scanf("%d", &Q);

//printf("N=%d Q=%d\n", N, Q);

int u, v, y;
	for(i=0; i<N-1; i++) {
		scanf("%d", &u);
		scanf("%d", &v);
		vector_add(&graph[u-1], v-1);
		vector_add(&graph[v-1], u-1);

		//vector_add(&nTree[min(u, v)-1], max(u, v)-1);
	}

struct vector stack; stack.data=0; stack.used=stack.size=0;

	stack_push(&stack, 0);	// root
	while(stack.used) {
		int node=stack_pop(&stack);
		for(i=0; i<graph[node].used; i++) {
			int childI=graph[node].data[i];
			if(childI!=parent[node]) {
				parent[childI]=node;
				stack_push(&stack, childI);
				vector_add(&nTree[node], childI);
			}
		}
	}

	precalcDistances();

	int ans=0;
	while(Q--) {
		scanf("%d", &v);
		scanf("%d", &y);
		v= (ans ^ v) -1;
		y= (ans ^ y) % MOD;
//printf("ans=%d v=%d y=%d\n", ans, v, y);
		if((v>=TREESZ) || (v<0)) {
			printf("root node out of scope: %d\n", v);
			exit(1);
		}
		
		ans=0;
		int oldCount=0;
		int oldGeomSum=geomSum_simple(y, 1);
		int dist;
		for(dist=0; dist<nTreeDists[v].used; dist++) {
			int distcount=nTreeDists[v].data[dist];
			int newGeomSum;
			if(distcount<8)
				newGeomSum=geomSum_simple(y, oldCount+distcount+1);
			else
				newGeomSum=geomSum(y, oldCount+distcount+1, MOD);

//printf("newGeomSum=%d\n", newGeomSum);
			int x=plus(newGeomSum, -oldGeomSum);
			oldCount+=distcount;
			oldGeomSum=newGeomSum;
			ans=plus(ans, mul(x, dist+1));
		}
		printf("%d\n", ans);
	}
}

int main() {
int t, i;
	scanf("%d", &t);
	for(i=0; i<t; i++)
		solve();
}
