
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int INF=1e18;
void solve1(vi &a) {
    int sum=0;
    int ans=-INF;
    for(size_t i=0; i<a.size(); i++) {
        sum+=a[i];
        ans=max(ans, sum);
        if(sum<0)
            sum=0;
    }
    cout<<ans<<endl;
}

void solve2(vi &a) {
    vi l(a.size());
    vi r(a.size());

    int sum=0;
    for(size_t i=0; i<a.size(); i++) {
        sum+=a[i];
        l[i]=sum;
        if(sum<0)
            sum=0;
    }
    for(size_t i=1; i<l.size(); i++)
        l[i]=max(l[i], l[i-1]);

    sum=0;
    for(int i=a.size()-1; i>=0; i--) {
        sum+=a[i];
        r[i]=sum;
        if(sum<0)
            sum=0;
    }
    for(int i=((int)a.size())-2; i>=0; i--) {
        r[i]=max(r[i], r[i+1]);
    }


    int ans=-INF;
    for(int i=0; i+1<a.size(); i++) {
        ans=max(ans, l[i]+r[i+1]*2);
    }
    cout<<ans<<endl;
}

/*
 * We want to choose some big-sum array as rightmost one.
 * K max 100
 * Cosider small K, 1 or 2
 *
 * Foreach position, we need to find the biggest possible
 * postfix, and the biggest possible prefix.
 *
 * ...
 * Just solve for K=1 :/
 *
 * How to do for bigger ones?
 *
 * Obviously it is some kind of dp. let
 * dp[i][j]=max val we can get if i subarrays...blah.
 * Adding an element can
 * -add it to rightmost array
 * -add to new array, arraycnt++
 * -ignore
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    if(k==1) {
        solve1(a);
        return;
    } else if(k==2)  {
        solve2(a);
        return;
    }

    vvi dp(2, vi(k+1, -INF)); /* dp[x][i]=cur max score we can get with i subarrays 
                                 x=0 -> dp[0][i] can be extended 
                                 x=1 -> dp[1][i] we ignored an a[i] hence cannot extend ith array
    */
    dp[1][0]=0;
    for(int i=0; i<n; i++) {
        vvi dp0=vvi(2, vi(k+1, -INF));
        for(int j=0; j<=k; j++) {
            dp0[1][j]=max(dp0[1][j], dp[0][j]);        /* ignore a[i] */
            dp0[1][j]=max(dp0[1][j], dp[1][j]);        /* ignore a[i] */

            if(j>0) {
                dp0[0][j]=max(dp0[0][j], dp[0][j]  +a[i]*j); /* add to last subarray */
                dp0[0][j]=max(dp0[0][j], dp[0][j-1]+a[i]*j); /* create new subarray */
                dp0[0][j]=max(dp0[0][j], dp[1][j-1]+a[i]*j); /* create new subarray */
            }
        }
        dp0.swap(dp);
    }
    cout<<max(dp[0][k],dp[1][k])<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}