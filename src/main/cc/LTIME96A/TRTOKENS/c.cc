
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * dfs, result foreach vertex is, 
 * number of token in longest chain in that subtree,
 * and number of moves allready done in that subtree.
 *
 * These numbers are calculated from the children by 
 * taking the max of them, see code.
 *
 * ...problem.
 * If we have an vertex without token, and each child
 * has a chain of token, then we can move the longest
 * chain one up with x moves, or move the second longest
 * chain one up with y moves, but creating a chain one longer.
 * So we get two results.
 *
 * So, each vertex does not return a pair, but a list of pair
 * ...complecated :/
 */
void solve() {
    cini(n);
    cins(s);
    vvi adj(n);

    for(int i=2; i<=n; i++) {
        cini(p);
        adj[p-1].push_back(i-1);
        //adj[i-1].push_back(p-1);
    }

    vi ch(n);   /* longest chain */
    vi mo(n);   /* moves */
    function<void(int)> dfs=[&](int v) {

        bool hasTok=(s[v]=='1');
        int cnt=0;
        for(int chl : adj[v]) {
            dfs(chl);
            mo[v]+=mo[chl];
            ch[v]=max(ch[v], ch[chl]);
            if(ch[chl]>0)
                cnt++;
        }

        if(!hasTok)
            mo[v]+=ch[v];

        if(hasTok || cnt>1)
            ch[v]++;
    };
    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
