
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/dsu>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1000000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* n<=1000, brute force */
void solve1(int n) {
    dsu d(n+1);

    vb vis(n+1);
    for(int i=2; i<=n; i++) {
        if(vis[i])
            continue;
        vis[i]=true;

        for(int j=i+i; j<=n; j+=i) {
            d.merge(i,j);
            vis[j]=true;
        }
    }
    vector<vector<signed>> g=d.groups();
    size_t ans=g.size()-2;  /* remove 0 and 1  */
    cout<<ans<<endl;
}

/*
 * How to find the groups in time?
 * do the sieve of erast and use dsu.
 * ...no, there must be some quick solution.
 *
 * primes>n/2 build an own group, else
 * they can be combined with 2.
 * So 
 * ans=number of primes > n/2 +1
 *
 * ...WA :/ idk
 * What can be wrong here?
 * Is there any not single element, not in group with the 2?
 */
void solve() {
    cini(n);
    if(n<=1000) {
        solve1(n);
        return;
    }

    auto it1=upper_bound(all(pr), n);
    assert(it1!=pr.end());
    auto it2=upper_bound(all(pr), n/2);
    int ans=distance(it2,it1)+1;
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
