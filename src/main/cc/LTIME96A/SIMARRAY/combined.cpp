
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* consider n=2
 * We want to find the min possible
 * (a[0]-r0*b[0])^2 + (a[1]-r1*b[1])^2
 * with r0>=r1
 *
 * Consider r0=r1==0
 * ans=(a[0]-b[0])^2 + (a[1]-b[1])^2
 *
 * Note that a[i],b[i] are positive and <=1000
 *
 * to make first term=0 we need to choose 
 * r0=a[0]/b[0]
 * r1=a[1]/b[1]
 * if(r0>=r1) then ans=0.0, else
 * best r1 we can choose is r1=r0
 * so we increase r0 and r1 until
 * both terms are equal.
 *
 * a[0]-r*b[0] = a[1]-r*b[1]
 * -r*b[0] = a[1]-a[0]-r*b[1]
 * r*b[1]-r*b[0]=a[1]-a[0]
 * r(b[1]-b[0])=a[1]-a[0]
 * r=(b[1]-b[0])/(a[1]-a[0])
 */
const ld EPS=0.00000000001;
void solve1(vi &a, vi &b) {
    ld r0=a[0]*1.0/b[0];
    ld r1=a[1]*1.0/b[1];

    if(r0+EPS>=r1) {
        cout<<"0.0"<<endl;
        return;
    }

    if(a[1]==a[0]) {
        ld t0=a[0]-r0*b[0];
        ld t1=a[1]-r0*b[1];
        ld ans=t0*t0+t1*t1;
        t0=a[0]-r1*b[0];
        t1=a[1]-r1*b[1];
        ans=min(ans, t0*t0+t1*t1);
        cout<<ans<<endl;
        return;
    }

    ld r=1.0*(b[1]-b[0])/(a[1]-a[0]);

    ld t0=a[0]-r*b[0];
    ld t1=a[1]-r*b[1];
    ld ans=t0*t0+t1*t1;
    cout<<ans<<endl;
}

void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);
    if(n==2) {
        solve1(a,b);
    } else
        cout<<"-1.0"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}