/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    string s;
    cin>>s;

/* Find number of s.substr() where number of '0'== number of '1' zum quadrat.
 * Lens are 1+1, 2+4, 3+9, 4+16, ...
 */
    
/* lets try brute force...*/
    int i=1;
    while(i*i+i<s.size())
        i++;

    vi dp(i+1); // number of 1 in substring of len i*i+i ending at current char.
    vi p(i+1);
    for(int j=1; j<=i; j++)
        p[j]=j*j+j;

// that should work, but is expected to be to slow.
    ll ans=0;
    for(int i=0; i<s.size(); i++) {
        int inc=s[i]-'0';
        for(int j=1; j<dp.size(); j++) {
            dp[j]+=inc;
            if(i-p[j]>=0 && s[i-p[j]]=='1')
                dp[j]--;

            if(p[j]<=i+1) {
                if(dp[j]==j)
                    ans++;
            }
        }
    }
    
    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

