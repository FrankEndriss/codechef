
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Consider candies sorted by price, smallest first.
 * Foreach candy we search the most sweet candy with
 * p[j] <= d-p[i]
 *
 * So sort by price smallest first, and maintain sweet
 * values in a multiset.
 * Then foreach price remove the according sweet-value from
 * the set, and also remove all sweet-values of prices
 * to expensive.
 * Then the biggest sweet-value is the one.
 *
 * How to correctly remove sweets from the sweets list?
 * -> Use two pointer.
 *
 * Uhmm... did read input wrong :/
 */
void solve() {
    cini(n);
    cini(d);

    //cerr<<"N="<<n<<" D="<<d<<endl;

    int ans=0;
    vector<pii> p(n);
    multiset<int> sweet;
    for(int i=0; i<n; i++)  {
        cin>>p[i].first;
    }
    for(int i=0; i<n; i++) {
        cin>>p[i].second;
        sweet.insert(p[i].second);

        if(p[i].first<=d)
            ans=max(ans, p[i].second);
    }

    sort(all(p));

    int r=n-1; /* idx of most expensive candy */

    for(size_t i=0; i<p.size(); i++) {
        /* remove the current one */
        auto it=sweet.find(p[i].second);
        assert(it!=sweet.end());
        sweet.erase(it);

        /* remove all to expensive ones */
        //cerr<<"p[i].first+p[r].first="<<p[i].first+p[r].first<<" d="<<d<<endl;
        while(r>i && p[i].first+p[r].first>d) {
            //cerr<<"i="<<i<<" r="<<r<<" p[i].second="<<p[i].second<<" p[r].second="<<p[r].second<<endl;
            it=sweet.find(p[r].second);
            assert(p[r].second==(*it));
            //cerr<<"erase sweet="<<(*it)<<endl;
            assert(it!=sweet.end());
            sweet.erase(it);
            r--;
        }

        if(sweet.size()==0 || r<=i)
            break;

        it=sweet.end();
        it--;
        //cerr<<"pair "<<p[i].second<<" + "<<(*it)<<endl;
        ans=max(ans, p[i].second+(*it));
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}