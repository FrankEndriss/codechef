
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We need to find foreach city the time it is first reached
 * by a train.
 * That is, the distance to the nearest 1 left of it, or the
 * nearest 2 rigth of it. Binary search.
 *
 * Stil WA :/
 * Could be that there is a really stupid implementation bug.
 * But with same prob the statement text includes some
 * hidden super stupid edgecase.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    vi l,r;
    for(int i=0; i<n; i++) {
        cini(a);
        if(a==1)
            r.push_back(n-1-i);
        else if(a==2)
            l.push_back(i);
    }
    reverse(all(r));

    for(int i=0; i<m; i++) {
        cini(b);
        b--;

        int ans=INF;

        auto it=lower_bound(all(l), b);
        if(it!=l.end())
            ans=(*it)-b;

        b=n-1-b;
        it=lower_bound(all(r), b);
        if(it!=r.end())
            ans=min(ans, (*it)-b);

        if(ans==INF)
            ans=-1;
        cout<<ans<<" ";
    }
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
