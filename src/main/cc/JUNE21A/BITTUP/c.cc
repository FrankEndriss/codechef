
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using mint=modint1000000007;

const int N=1e6+7;
vector<mint> fac(N);

const bool initFac=[]() {
    fac[1]=1;
    for(int i=2; i<N; i++)
        fac[i]=fac[i-1]*i;
    return true;
}();

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
mint nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return fac[n]*fac[k].inv()*fac[n-k].inv();
}

/* some more math...inclusion/exclusion
 *
 * Each number has m bits, so the number of all tuples is
 * 2^(n*m)
 * The number of all tuples having the first (or any other bit)
 * set in all n numbers is
 * 2^(n*(m-1))
 * So, we need to subtract the overcount ... somehow.
 *
 * ans= 2^(n*m) - 2^(n*(m-1))*n +- overcount
 *
 * The overcount is, that for the second bit
 * 2^(n*(m-2)), so we need to add this
 * and subtract the third..
 * 2^(n*(m-3)), and so on.
 * So, if m is even, we need to add all even indexes and subtract the odds,
 * if its odd vice versa.
 *
 * ...somehow, still WA, need to get the inclusion/exclusion right :/
 *
 * ***
 * Ok, lets formulate it otherwise
 * all tuples = 2^(n*m)
 *
 * subtract the ones having the first bit set in all n numbers:
 * - 2^(n*(m-1))
 *
 * subtract the ones having the second bit set in all n numbers, but not
 * having the first bit set in all numbers.
 * - 2^(n*(m-1)) + 1 * 2^(n*(m-2))
 *
 * subtract the ones having the third bit set in all n numbers, but not
 * having the first bit set or the second bit set in all numbers.
 * - 2^(n*(m-1)) + 2 * 2^(n*(m-2)) - 1 * 2^(n*(m-3))
 *
 * subtract the ones having the 4th bit set in all n numbers, but not
 * having the first bit set or the second bit set or the third bit set in all numbers.
 * - 2^(n*(m-1)) + 3 * 2^(n*(m-2)) - 2 * 2^(n*(m-3)) + 1 * 2^(n*(m-4))
 *
 * ...
 * let g(n)=n*(n+1)/2
 * - m*2^(n*(m-1)) + g(m-1)*2^(n*(m-2)) - g(m-2)*2^(n*(m-3)) + ... - ... + ....
 *
 * Now we need some prefix sums to not have to loop m times foreach testcase.
 * ...
 * ***
 * N numbers with each having M bits.
 * Lets formulate it by pairs, triples, 4-tuples, etc
 * all tuples = 2^(n*m)
 * sub singles= nCr(m,1) * 2^(n*(m-1))
 * add pairs  = nCr(m,2) * 2^(n*(m-2))
 * sub triples= nCr(m,3) * 2^(n*(m-3))
 * ...
 * Still we need to somehow sum that up.
 * ***
 * brute force, most likely TLE 
 * How can we put these formulars above into a closed form, or sum kind
 * of prefix sum? 
 **/
void solve() {
    cini(n);
    cini(m);

    mint two=2;
    mint ans=two.pow(n*m);
    //cerr<<"first two.pow(n*m)="<<ans.val()<<endl;
    for(int i=1; i<=m; i++) {
        mint sum=nCr(m,i)*two.pow(n*(m-i));
        //cerr<<"i="<<i<<" sum="<<sum.val()<<endl;
        if(i&1)
            ans-=sum;
        else
            ans+=sum;
    }
    //cerr<<"ans="<<ans.val()<<endl;

    cout<<ans.val()<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
