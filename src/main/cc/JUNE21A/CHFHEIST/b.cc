
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Ans has three parts:
 * The base, that is 
 * 1. number of days times initial prod rate.
 * 2. Full periods of d days with each having an increment
 * 3. last period of D%d days
 * Sum of that is ans.
 */
void solve() {
    cini(D);
    cini(d);
    cini(P);
    cini(Q);

    //cerr<<"D="<<D<<" d="<<d<<" P="<<P<<" Q="<<Q<<endl;
    int ans=D*P;
    //cerr<<"base="<<ans;
    int cyc=D/d;    /* full cycles */
    if(cyc>0)
        cyc--;
    //cerr<<" cyc="<<cyc;

    int ans2=((cyc*(cyc+1))/2) * Q * d;
    //cerr<<" inc="<<ans2;

    int ans3=(cyc+1)*Q*(D%d);
    //cerr<<" last="<<ans3<<endl;

    cout<<ans+ans2+ans3<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
