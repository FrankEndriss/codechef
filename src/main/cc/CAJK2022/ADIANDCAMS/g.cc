/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Again stack work.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);

    vi l(n,-1);
    stack<pii> st;
    st.emplace(INF,-1);
    for(int i=0; i<n; i++) {
        while(st.top().first<=a[i])
            st.pop();
        l[i]=st.top().second;
        st.emplace(a[i],i);
    }

    st.emplace(INF, n);
    vi r(n,n);
    for(int i=n-1; i>=0; i--) {
        while(st.top().first<=a[i])
            st.pop();
        r[i]=st.top().second;
        st.emplace(a[i],i);
    }

    for(int i=0; i<q; i++) {
        cini(x);
        x--;
        //cerr<<"x="<<x<<" l[x]="<<l[x]<<" r[x]="<<r[x]<<endl;
        if(l[x]==-1)
            cout<<-1;
        else
            cout<<a[l[x]];
        cout<<" ";

        if(r[x]==n)
            cout<<-1;
        else
            cout<<a[r[x]];
        cout<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
