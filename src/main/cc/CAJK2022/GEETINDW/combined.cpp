
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * A,B,C should be at some most sign bit swap border,
 * so that the msb(A&B) != msb(C)
 * These borders are at multiples of 2.
 *
 * Note that if most sign bit of l^3 and r^3 is same, 
 * we need to remove it first.
 * ...to much edgecases :/
 */
void solve2(int l, int r, int y) {

    //cerr<<"l="<<l<<" r="<<r<<endl;
    //
    if(l+2>r) {
        cout<<"NO"<<endl;
        return;
    }


    /* find most sign bit differs in r and l 
     * Note that it is set in r, since r>l.
     **/
    int b=(1LL<<32);
    int bb=0;
    while((l&b)==(r&b)) {
        if(l&b)
            bb+=b;
        b/=2;
    }
    assert(bb+b<=r);
    //cerr<<"bb="<<bb<<" b="<<b<<endl;

    if(bb+b-2>=l) {
        //int val=((bb+b-2)&(bb+b-1))^b;
        int val=((bb+b-2)+(bb+b-1))+b;
        //cerr<<"1 val="<<val<<" y="<<y<<endl;
        if(val==y) {
            cout<<b-2+bb<<" "<<b-1+bb<<" "<<b+bb<<endl;
            cout<<"YES"<<endl;
            return;
        }
    }  else {
        solve2(l+1, r, y);
        return;
    }

    cout<<"NO"<<endl;

}

void solve() {
    cini(l);
    cini(r);
    cini(y);
    l=l*l*l;
    r=r*r*r;
    solve2(l,r,y);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}