/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;

#include <atcoder/lazysegtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



using S=int; /* type of values */
using F=int; /* type of upates */

S st_op(S a, S b) {
    return a+b;
}

/* This is the neutral element */
S st_e() {
    return 0;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f+x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using streeLazy=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * Note that not the coins value is XORed, but
 * the coins freq!
 *
 * So use try brute force.
 */
const int N=1e3+3;
const int M=2e5+7;
void solve() {
    cini(n);
    cini(q);

    streeLazy segLazy(M);
    for(int i=0; i<n; i++) {
        cini(l); l--;
        cini(r);
        segLazy.apply(l,r,1);
    }

    vi val(M);
    for(int i=0; i<M; i++) 
        val[i]=segLazy.get(i);

    /*
    cerr<<"val=";
    for(int i=0; i<20; i++) 
        cerr<<val[i]<<" ";
    cerr<<endl;
    */

    for(int i=0; i<q; i++) {
        cini(l); l--;
        cini(r); 
        vb cnt(N);
        for(int j=l; j<r; j++) 
            cnt[val[j]]=true;

        int ans=0;
        for(int j=1; j<N; j++)  {
            if(cnt[j]==0)
                continue;
            for(int k=j+1; k<N; k++) 
                if(cnt[k])
                    ans=max(ans, j^k);
        }

        cout<<ans<<endl;
    }

}

signed main() {
    solve();
}
