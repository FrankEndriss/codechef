
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* LCA based on euler path with segment tree and tin/tout. */
struct LCA {
    vector<int> height, euler, first, segtree;
    vector<bool> visited;
    int n;
    int time;
    vector<int> tin;
    vector<int> tout;

    LCA(vector<vector<int>> &adj, int root = 0) {
        n = (int)adj.size();
        height.resize(n);
        first.resize(n);
        euler.reserve(n * 2);
        visited.assign(n, false);
        tin.resize(n);
        tout.resize(n);
        time=1;
        dfs(adj, root);
        int m = (int)euler.size();
        segtree.resize(m * 4);
        build(1, 0, m - 1);
    }

    void dfs(vector<vector<int>> &adj, int node, int h = 0) {
        tin[node]=time++;
        visited[node] = true;
        height[node] = h;
        first[node] = (int)euler.size();
        euler.push_back(node);
        for (auto to : adj[node]) {
            if (!visited[to]) {
                dfs(adj, to, h + 1);
                euler.push_back(node);
            }
        }
        tout[node]=time++;
    }

    void build(int node, int b, int e) {
        if (b == e) {
            segtree[node] = euler[b];
        } else {
            int mid = (b + e) / 2;
            build(node << 1, b, mid);
            build(node << 1 | 1, mid + 1, e);
            int l = segtree[node << 1], r = segtree[node << 1 | 1];
            segtree[node] = (height[l] < height[r]) ? l : r;
        }
    }

    int query(int node, int b, int e, int L, int R) {
        if (b > R || e < L)
            return -1;
        if (b >= L && e <= R)
            return segtree[node];
        int mid = (b + e) >> 1;

        int left = query(node << 1, b, mid, L, R);
        int right = query(node << 1 | 1, mid + 1, e, L, R);
        if (left == -1) return right;
        if (right == -1) return left;
        return height[left] < height[right] ? left : right;
    }

    /* @return true if parent is ancestor of node in O(1) */
    bool isAncestor(int node, int parent) {
        return tin[node]>tin[parent] && tin[node]<tout[parent];
    }

    /* @return the lca of u and v */
    int lca(int u, int v) {
        int left = first[u], right = first[v];
        if (left > right)
            swap(left, right);
        return query(1, 0, (int)euler.size() - 1, left, right);
    }
};
/**
 * Tree of n vertices, need to compress them.
 * ...no, constraints are wrong, a[i]<=N.
 *
 * Then, on query, we need to find if C is anc of A and of B.
 */
void solve() {
    cini(n);
    map<int,int> id;

    function<int(int)> toId=[&](int i) {
        auto it=id.find(i);
        if(it==id.end()) {
            int ans=id.size();
            id[i]=ans;
            return ans;
        } else
            return it->second;
    };

    int root=-1;

    vvi adj(n);
    for(int i=1; i<=n; i++) {
        cini(p);
        if(p!=i) {
            adj[p-1].push_back(i-1);
            adj[i-1].push_back(p-1);
        } else {
            assert(root<0);
            root=p-1;
        }
    }
    assert(root>=0);

    LCA lca(adj, root);
    cini(q);
    for(int i=0; i<q; i++) {
        cini(A); A--;
        cini(B); B--;
        cini(C); C--;

        if(lca.isAncestor(A,C) && lca.isAncestor(B,C)) 
            cout<<"YES"<<endl;
        else
            cout<<"NO"<<endl;
    }
}

signed main() {
    solve();
}