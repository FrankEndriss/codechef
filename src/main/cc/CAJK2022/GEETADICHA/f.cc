
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Simulate.
 * Assume: If a number is smallest, but not distinct,
 * all occ of that nubmer are replaced by 0 in one step.
 *
 * Since "all at once" does not work, try replace only one occ.
 * Replace odd or even indices first?
 */
const int N=1e5+7;
void solve() {
    vvi pos(N);

    cini(n);
    vi sum(2);
    set<int> a;
    for(int i=0; i<n; i++) {
        cini(aux);
        pos[aux].push_back(i);
        if(aux>0)
            a.insert(aux);

        sum[i&1]+=aux;
    }

    for(int i=0; i<N; i++)
        if(pos[i].size()>1)
            reverse(all(pos[i]));

    while(sum[1]<=sum[0] && a.size()) {
        auto it=a.begin();
        int mi=*it;

        /* remove leftmost occ of mi */
        int p=pos[mi].back();
        pos[mi].pop_back();
        sum[p&1]-=mi;
        if(pos[mi].size()==0)
            a.erase(it);

        /* remove all occ of mi
        a.erase(it);
        for(int idx : pos[mi])
            sum[idx&1]-=mi;
            */
    }

    if(sum[1]>sum[0])
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
