
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;


const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=100000;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* adjust as needed */
using S=int; /* type of values */

S st_op(S a, S b) {
    return a+b;
}

S st_e() {
    return 0LL;
}

using stree=segtree<S, st_op, st_e>;

/**
 * Use segment trees, prefix sumx.
 * At each position find sum and count to L and R,
 * add/subtract primes.
 *
 * Note that 
 *
 * Is 1 counted as a prime?
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);

    notpr[0]=true;
    notpr[1]=true;

    stree segPSum(n);   /* prefix sums of primenumbers */
    stree segPCnt(n);   /* prefix count of primenumbers */
    for(int i=0; i<n; i++) {
        if(!notpr[a[i]]) {
            segPSum.set(i,a[i]);
            segPCnt.set(i,1);
        }
    }

    vi l(n, -1);
    stack<pii> st;
    st.emplace(INF,-1);
    for(int i=0; i<n; i++) {
        while(st.top().first<=a[i])
            st.pop();
        l[i]=st.top().second;
        if(notpr[a[i]])
            st.emplace(a[i],i);
    }
    while(st.size())
        st.pop();
    st.emplace(INF,n);
    vi r(n, n);
    for(int i=n-1; i>=0; i--) {
        while(st.top().first<=a[i])
            st.pop();
        r[i]=st.top().second;
        if(notpr[a[i]])
            st.emplace(a[i],i);
    }

    vi ans(n);
    stree seg(a);
    for(int idx=0; idx<n; idx++) {
        if(!notpr[a[idx]]) {
            ans[idx]=0;
            continue;
        }

        //cerr<<"work on idx="<<idx<<" a[idx]="<<a[idx]<<endl;
        int sumL=seg.prod(l[idx]+1,idx);
        int sumLSum=segPSum.prod(l[idx]+1,idx);
        int sumLCnt=segPCnt.prod(l[idx]+1,idx);
        int sumR=seg.prod(idx+1,r[idx]);
        int sumRSum=segPSum.prod(idx+1,r[idx]);
        int sumRCnt=segPCnt.prod(idx+1,r[idx]);

        //cerr<<"sumL="<<sumL<<" sumLSum="<<sumLSum<<" sumLCnt="<<sumLCnt<<" sumR="<<sumR<<" sumRSum="<<sumRSum<<" sumRCnt="<<sumRCnt<<endl;

        int lans= (idx-(l[idx]+1))*a[idx] - sumL 
                + (r[idx]-(idx+1))*a[idx] - sumR
                - sumLCnt*a[idx]+sumLSum 
                - sumRCnt*a[idx]+sumRSum;
        ans[idx]=lans;
        //seg.set(idx,a[idx]);
    }

    for(int i=0; i<n; i++)
        cout<<ans[i]<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--) 
        solve();
}
