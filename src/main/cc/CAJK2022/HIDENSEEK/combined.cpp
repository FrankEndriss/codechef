
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vvb= vector<vb>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * simple dfs and count
 */
const int INF=1e9;
void solve() {
    cini(M);
    cini(n);    /* number of steps */
    cini(k);
    cini(sx);
    cini(sy);

    vvb obs(M, vb(M));
    for(int i=0; i<k; i++) {
        cini(x);
        cini(y);
        obs[x][y]=true;
    }

    const vi dx={ -1, 1, 0, 0 };
    const vi dy={ 0, 0, -1, 1 };
    queue<pii> q;
    vvi dp(M, vi(M,INF));
    dp[sx][sy]=0;
    q.emplace(sx,sy);
    int ans=0;
    while(q.size()) {
        auto [x,y]=q.front();
        q.pop();
        if(dp[x][y]==n)
            continue;

        for(int kk=0; kk<4; kk++) {
            int xx=x+dx[kk];
            int yy=y+dy[kk];
            if(xx>=0 && xx<M && yy>=0 && yy<M && !obs[xx][yy] && dp[xx][yy]==INF) {
                dp[xx][yy]=dp[x][y]+1;
                ans++;
                q.emplace(xx,yy);
            }
        }
    }
    cout<<ans+1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}