/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Both allways choose the biggest block.
 * Note that N is most likely not 1e9, consider 1e5 or 1e6
 */
void solve() {
    cini(n);
    map<int,int> f;
    for(int i=0; i<n; i++) {
        cini(aux);
        f[-aux]++;
    }

    int pl=1;    /* flag if it is first players move */
    vi cnt(2); /* points of players */
    while(f.size()) {
        auto it=f.begin();
        cnt[pl]+=it->first*((it->second+1)/2);
        cnt[!pl]+=it->first*(it->second/2);
        if(it->second%2)
            pl=!pl;
        int nval=it->first/2;
        int ncnt=it->second;
        f.erase(it);
        if(nval!=0)
            f[nval]+=ncnt;
    }
    cout<<abs(cnt[0]-cnt[1])<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}