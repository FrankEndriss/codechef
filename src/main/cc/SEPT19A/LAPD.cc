/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const ll INF=1e8+7;
const int MOD=1e9+7;

const ll n[]={ 1, 2, INF };
const int C=2;

bool ok(const int a, const int b, const int c) {
    for(int x=0; x<C; x++) {
        for(int y=0; y<C; y++) {
            if((a-1)*n[x]*n[x] + (c-1)*n[y]*n[y] <= 2*b*n[x]*n[y] )
                return false;
        }
    }
    return true;
}

void solve() {
    cinll(a);   
    cinll(b);
    cinll(c);

    ll ans=0;
    for(int ib=1; ib<=b; ib++) {
        for(int ia=1; ia<=a; ia++) {
            for(int ic=1; ic<=c; ic++) {
                if(ok(ia, ib, ic))
                    ans++;
            }
        }
    }
    cout<<ans%MOD<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

