/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cinll(m);

    if(m<n-1) {
        cout<<-1<<endl;
        return;
    }

    if(n==1) {
        if(m<=1)
            cout<<m<<endl;
        else
            cout<<-1<<endl;

        return;
    }

    if(n==2) {
        if(m==1)
            cout<<1<<endl;
        else if(m<=3)
            cout<<2<<endl;
        else
            cout<<-1<<endl;

        return;
    }

    m-=(n-1);   // now all nodes have 2 connections, exept first and last which have only 1
    m-=2;       // first and last self-connected, now all have 2 connections
    if(m<=0) {
        cout<<2<<endl;
        return;
    }
    
    m-=(n-2);   // all self-connected
    m--;        // first and last connected
    if(m<=0) {
        cout<<3<<endl;
        return;
    }

/* all comps are self-connected, and connected to exact two other comps. 
 * So, we still can connect any comp to at most n-3 other comps. */
    ll sockets=1LL*n*(n-3);
    if(m*2>sockets) {   // every cable needs two sockets
        cout<<-1<<endl;
        return;
    }

    sockets-=(m*2);
    cout<<n-(sockets/n)<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

