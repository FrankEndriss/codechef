/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

typedef unsigned long long ull;
vector<tuple<ull,ull,ull>> target;

#define RAX 0
#define RBX 1
#define RCX 2
#define RDX 3
const string reg[]={ "rax", "rbx", "rcx", "rdx" };
ull r[4]={ 0, 0, 0, 0 };

ostringstream cans;
int ans;


/* set reg reg to val val */
void set_reg(int ridx, ull val) {
    if(r[RDX]!=1) {
        cans<<"xor "<<reg[RDX]<<" "<<reg[RDX]<<endl;
        ans++;
        cans<<"inc "<<reg[RDX]<<endl;
        ans++;
        r[RDX]=1;
    }

    cans<<"xor "<<reg[ridx]<<" "<<reg[ridx]<<endl;
    ans++;
    r[ridx]=0;
    if(val==0)
        return;

    stack<int> st;
    while(val) {
        st.push((int)(val&1));
        val>>=1;
    }
    bool first=true;
    while(st.size()) { 
        if(!first) {
            cans<<"shl "<<reg[ridx]<<" "<<reg[RDX]<<endl;
            ans++;
            r[ridx]<<=r[RDX];
        }
        first=false;
        int tmp=st.top();
        st.pop();
        if(tmp) {
            r[ridx]++;
            cans<<"inc "<<reg[ridx]<<endl;
            ans++;
        }
    }
    
}

void create(int i) {
    set_reg(RAX, get<0>(target[i]));
    set_reg(RBX, get<1>(target[i]));
    set_reg(RCX, get<2>(target[i]));
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    for(int i=0; i<n; i++) {
        ull a,b,c;
        cin>>a>>b>>c;
        target.push_back({ a, b, c });
    }
    sort(target.begin(), target.end());

    for(int i=0; i<n; i++) {
        create(i);
    }

    cout<<ans<<endl;
    cout<<cans.str();
}

