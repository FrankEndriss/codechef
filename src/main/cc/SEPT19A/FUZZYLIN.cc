/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<ll, ll> pllll;
typedef vector<pllll> vpllll;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    cinai(a, n);
    cini(q);
    vpllll k;
    for(ll i=0; i<q; i++) {
        cinll(tmp);
        k.push_back({ tmp, i });
    }
    sort(k.begin(), k.end());

    /* We need to find for any k.first the smallest l,r so that multiples
     * of a[l]..a[r] sum up to k.
     * Then we can calculate the number of possibilities.
     *
     * To do so, in fact, for any l, we have to find the smallest r, and
     * then for that r, the biggest l, and then
     * sum up ans+=(l+1)*(n-r+1)    // for l,r zero based indices
     * Then go on at r+1.
     *
     * Note: for any pair l,r there could be another pair l+x,r-y pair including
     * all l,r possibilities... :/
     *
     * -> Linear Dophantine Equation
     *  if k is divisable by gcd(a,b) then there is a solution.
     *  So we need to find all pairs k%gcd(a[l], a[r]) == 0
     *  Maybe it works: k%gcd(a[l],...,a[r]) == 0
     */

    
    vb kb(q);

    for(int i=0; i<q; i++) {
        if(kb[i])
            continue;

        int prevl=0;
        int lidx=0;
        int ridx=0;
        ll ans=0;

        while(lidx<n) {
            ridx=lidx;

            /* find biggest r to given l */
            int g=a[ridx];
            while(k[i].first%g!=0) {
                ridx++;
                if(ridx==n)
                    break;
                g=__gcd(g, a[ridx]);
            }
            if(ridx==n)
                break;

            /* find biggest l to given r */
            prevl=lidx;
            lidx=ridx;
            g=a[lidx];
            while(k[i].first%g!=0) {
                lidx--;
                assert(lidx>=0);
                g=__gcd(g, a[lidx]);
            }

            ans+=(lidx-prevl+1) * (n-ridx);
            lidx=ridx+1;
        }

        k[i].first=ans;
    }
    
    sort(k.begin(), k.end(), [&](pllll p1, pllll p2) {
        return p1.second<p2.second;
    });

    for(int i=0; i<q; i++)
        cout<<k[i].first<<endl;
}

