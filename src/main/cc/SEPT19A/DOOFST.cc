/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(m);

    if(m==0) {
        cout<<0<<endl;
        return 0;
    }

/* find sets of persons not hating each other.
 * repeat.
 *
 * Any of these sets must be hatered once against the others.
 * Except the last set, since that one was hated by all other sets before.
 */

    vector<unordered_set<int>> tree(n);
    for(int i=0; i<m; i++) {
        cini(a);
        cini(b);
        if(a>b)
            swap(a,b);
        tree[a-1].insert(b-1);
    }

    vb used(n);
    vvi sets;
    bool impossible=false;
    for(int i=0; i<n; i++) {
        if(used[i])
            continue;
        used[i]=true;
        sets.push_back(vi());
        sets.back().push_back(i);
        for(int j=i+1; j<n; j++) {
            if(tree[i].count(j)==0) {
                if(used[j]) {
                    impossible=true;
                    break;
                }
                sets.back().push_back(j);
                used[j]=true;
            }
        }
        if(impossible)
            break;
    }

    if(impossible) {
        cout<<-1<<endl;
        return 0;
    }

    const size_t one=1;
    int ans=0;
    while((one<<ans) < sets.size())
        ans++;

    const int MAXK=1e6;
    if(ans*n>MAXK) {
        cout<<-1<<endl;
        return 0;
    }

    cout<<ans<<endl;
    size_t ssize=2;
    for(int j=0; j<ans; j++) {
        string sans(n, '0');
            
        for(size_t k=0; k<sets.size(); k+=ssize)  {
            for(size_t i=k; i<k+ssize/2 && i<sets.size(); i++) {
                for(int idx : sets[i]) {
                    sans[idx]='1';
                }
            }
        }

        cout<<sans<<endl;
        ssize*=2;
    }

}

