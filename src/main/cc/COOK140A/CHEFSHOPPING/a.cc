
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Use cheapest left and remove all left from it.
 * ...
 * Ok, it is other problem.
 * We can do on each ingredient only one operation.
 * At least it seems so.
 *
 * Poor problem statement.
 */
void solve() {
    cini(n);
    cinai(l,n);
    cinai(r,n);

    cerr<<"n="<<n<<endl;

    using t3=tuple<int,int,int>;
    vector<t3> v;
    for(int i=0; i<n; i++) {
        v.emplace_back(l[i],i,0);
        v.emplace_back(r[i],i,1);
    }
    sort(all(v));

    int ans=0;
    int L=0;
    int R=n-1;
    for(size_t i=0; i<v.size(); i++) {
        auto [val,j,d]=v[i];
        //cerr<<"val="<<val<<" j="<<j<<" d="<<d<<endl;
        if(d==0) {
            //cerr<<"j="<<j<<" L="<<L<<" R="<<R<<endl;
            if(j>L && j<=R) {
                cerr<<"cut L="<<L<<" j="<<j<<endl;
                ans+=val*(j-L);
                L=j;
            }
        } else if(d==1) {
            if(j<R && j>=L) {
                cerr<<"cut R="<<R<<" j="<<j<<endl;
                ans+=val*(R-j);
                R=j;
            }
        } else
            assert(false);
    }

    assert(L==R);
    cout<<ans<<endl;
}

/*
 * How does this work at all?
 * Consider leftmost position.
 * It can be removed only be the operation L right of it.
 * ...
 * We want to use n-1 operations, so choose the cheapest n-1 ones.
 * How to see if they are sufficient, and in what order?
 */
void solve2() {
}

signed main() {
    cini(t);
    while(t--)
        solve2();
}
