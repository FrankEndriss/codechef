
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider arrays a[] with all elements of even freq, they all contribute.
 * Then consider a[n-1] is odd.
 * We need 
 * 2*a[n-2] to fix it, or
 * 4*a[n-3] to fix it, or
 * 1*a[n-2]+2*a[n-3] to fix it, or
 * ...all combinations.
 *
 * Also a[n-1] could be 3 instead of 1, and we fix it with more smaller elements...
 * But since limited to 5e3, we need to consider at most n-13
 *
 * So it seems to be a dp, where state is the number of elements with
 * the up to 12/13 higher bits.
 * Then foreach such state calc the next 12/13 bits
 * ...
 * Consider going from biggest to smallest, maintaining the list of possible leftovers:
 * a leftover is a vi(13) with the number of left over elements at each position.
 * From this, we again create a list of leftovers for the next position, by
 * removing, adding all...
 *
 * after n-1th stage leftover is all %2 numbers.
 * then in n-2th stage we can remove up to b[n-2]/2 units from the leftover
 * and add every remaining %2-number.
 * Problem is, the state is vi(13), which is a hube number of states, even if each 
 * position is limited to (1<<13)/pos.
 * 5000*2500*1250*625*...
 *
 * idk :/
 */
const int N=14;
void solve() {
    cini(n);
    cinai(a,n);

    vi dp(N);
    for(int i=0; i<n; i++) {

        for(int j=N-1; j>=0; j--) {

        }
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
