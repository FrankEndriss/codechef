
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Actually all subarray (except the whole array) must
 * differ after sorting.
 * So, each single position must differ
 * each pos and pos+1 must differ...
 * Sort b[], and cycle shift in max(freq) positions.
 * This works if max(freq)<=n/2
 */
void solve() {
    cini(n);
    cinai(a,n);

    map<int,int> f;
    for(int i=0; i<n; i++)
        f[a[i]]++;
    int ma=0;
    for(auto ent : f)
        ma=max(ma, ent.second);

    if(ma>n/2) {
        cout<<"NO"<<endl;
        return;
    }

    vi b=a;
    sort(all(b));
    vi c=b;
    rotate(c.begin(), c.begin()+ma, c.end());
    cout<<"YES"<<endl;
    for(int i : b)
        cout<<i<<" ";
    cout<<endl;
    for(int i : c) 
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}