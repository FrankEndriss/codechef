
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * consider two consecutive values in a[]
 * 0,0 0,+,+,+
 * 0,1 0,+,++,-
 * 0,2 0,++,+,--
 * 0,3 impossible
 * 1,0 0,-,++,+
 * 1,1 0,-,+++,--
 * 1,2 0,+++,--,+
 * 1,3 0,+++,-,-
 * 2,0 0,--,+,++
 * 2,1 0,--,++,-
 * 2,2 0,--,--,+
 * 2,3 0,+,--,-
 * 3,0 impossible
 * 3,1 0,-,-,++
 * 3,2 0,-,--,+
 * 3,3 0,-,-,-
 *
 * Example 2, 
 * 0 1 2 3
 *
 * +,+,-,+,
 * ...
 * We can replace a triple inversion by two double inversions, 
 * so
 * 0 -> +,+
 * 1 -> +,- | -,+
 * 2 -> --,+
 * 3 -> -,-
 *
 * ->
 *
 * And then???
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    int cur;

    if(a[0]==0)
        cur=1;
    else if(a[0]==1)
        cur=0;
    else if(a[0]==2)
        cur=1;
    else if(a[0]==3)
        cur=-1;
    else
        assert(false);

    bool ok=true;
    for(int i=1; i<n; i++)  {
        if(cur==-1) {
            if(a[i]==2)
                cur=1;
            else if(a[i]==3)
                cur=-1;
            else
                ok=false;
        } else if(cur==0) {
            if(a[i]==0)
                cur=1;
            else if(a[i]==1)
                cur=0;
            else if(a[i]==2)
                cur=1;
            else if(a[i]==3)
                cur=-1;
            else
                assert(false);
        } else if(cur==1) {
            if(a[i]==0)
                cur=1;
            else if(a[i]==1) 
                cur=-1;
            else 
                ok=false;
        }

        if(!ok) {
            cout<<"NO"<<endl;
            return;
        }
    }

    cout<<"YES"<<endl;
                
}

signed main() {
    cini(t);
    while(t--)
        solve();
}