/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

template <typename E>
class SegmentTree {
  private:
    vector<E> tree;
    E neutral;
    function<E (E, E)> cumul;

    int treeDataOffset;
    const int ROOT = 1;

    inline int leftChild(const int treeIdx) {
        return  treeIdx * 2;
    }
    inline int rightChild(const int treeIdx) {
        return treeIdx * 2 + 1;
    }
    inline int parent(const int treeIdx) {
        return treeIdx / 2;
    }
    inline bool isOdd(const int idx) {
        return (idx & 0x1);
    }

  public:
    /** SegmentTree. Note that you can hold your data in your own storage and give
     * an Array of indices to a SegmentTree.
     * @param beg, end The initial data.
     * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
     * @param pCumul The cumulative function to create the "sum" of two nodes.
    **/
    template<typename Iterator>
    SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E (E, E)> pCumul) {
        neutral=pNeutral;
        cumul=pCumul;
        treeDataOffset=(int)distance(beg, end);
        tree=vector<E>(treeDataOffset*2, pNeutral);

        int i=treeDataOffset;
        for (auto it=beg; it!=end; it++)
            tree[i++] = *it;

        for (int j=treeDataOffset - 1; j>=1; j--)
            tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
    }

    /** Updates all elements in interval(idxL, idxR].
    * iE add 42 to all elements from 4 to inclusive 7:
    * update(4, 8, [](int i1) { return i1+42; });
    */
    void updateRange(int pIdxL, int pIdxR, function<E (E)> apply) {
        pIdxL+=treeDataOffset;
        pIdxR+=treeDataOffset;
        for(int i=pIdxL; i<pIdxR; i++)
            tree[i]=apply(tree[i]);

        while (pIdxL != ROOT) {
            pIdxL=parent(pIdxL);
            pIdxR=max(pIdxL, parent(pIdxR-1));
            for(int i=pIdxL; i<=pIdxR; i++)
                tree[i]=cumul(tree[leftChild(i)], tree[rightChild(i)]);
        }
    }

    /** Updates the data at dataIdx to value returned by apply. */
    void updateApply(int dataIdx, function<E(E)> apply) {
        updateSet(dataIdx, apply(get(dataIdx)));
    }

    /** Updates the data at dataIdx by setting it to value. */
    void updateSet(int dataIdx, E value) {
        int treeIdx = treeDataOffset + dataIdx;
        tree[treeIdx] = value;

        while (treeIdx != ROOT) {
            treeIdx = parent(treeIdx);
            tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
        }
    }

    /** @return value at single position, O(1) */
    E get(int idx) {
        return tree[idx+treeDataOffset];
    }

    /** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
    E get(int pIdxL, int pIdxR) {
        int idxL = pIdxL + treeDataOffset;
        int idxR = pIdxR + treeDataOffset;
        E cum = neutral;
        while (idxL < idxR) {
            if (isOdd(idxL)) { // left is odd
                cum = cumul(cum, tree[idxL]);
                idxL++;
            }
            if (isOdd(idxR)) {
                idxR--;
                cum = cumul(cum, tree[idxR]);
            }
            idxL = parent(idxL);
            idxR = parent(idxR);
        }
        return cum;
    }

};

/* we need to find the subset of k elements in a,
 * such that the sum of the maximum element of all
 * subarrays is minimized.
 *
 * There are two properties:
 * How often contributes an element.
 * How much does it contribute.
 *
 * Consider k smallest elements of a[]. Can we get better 
 * result? No.
 *
 * Then, how to find ans for given kk[]?
 * Brute force.
 *
 * But, did get it wrong:
 * We need to sum _all_ elements of a, not only the k ones.
 * But it is not that bad, since then kk[] must be a subarray
 * of a[].
 * Which one?
 * We need to find for every element the number of times it
 * is the max element.
 *
 * Consider the leftmost elem, kk[0]
 * it is max elem the number of times untill right of it
 * there is a bigger one.
 * Consider elem kk[i]
 * it is max elem in all subseqs starting at the one right
 * of the first bigger one left of it to itself, times
 * the number of elems right of it smaller than itself.
 *
 *
 * Consider all subarrays of size k in a[].
 *
 * ... to complecated :/
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vi data(k);
    int ans=0;
    for(int i=0; i+k<n; i++) {

        SegmentTree<int> posL(data.begin(), data.end(), 0, [](int i1, int i2) {
            return max(i1, i2);
        });

        map<int,int> posL;   /* posL[i]=max position of all kk[i] left of current */
        map<int,vi> posR;   /* posR[i]=positions of i sorted desc right of current pos. */
        for(int idx=i+k-1; idx>=i; i--)
            posR[a[idx]].push_back(i);

        int lans=0;
        for(int j=0; j<k; j++) {
            auto it=posL.lower_bound(a[i+j]);
            int cntL=1;
            if(it!=posL.end()) 
                cntL=j-it->second+1;

            int cntR=1;
            auto it=posR.find(a[i+j]);
            assert(it!=posR.end());
            assert(it->second.back()==j);
            it->second.pop_back();
            if(it->second.size()==0)
                posR.erase(it);

            auto it=posR.lower_bound(a[i+j]);
            if(it!=posR.end())
                cntR=it->second.back()-j+1;

            lans+=cntL*cntR*a[i+j];

            posL[a[i+j]]=j;
        }
        ans=max(ans, lans);
    }


    cout<<ans<<endl;

    
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
