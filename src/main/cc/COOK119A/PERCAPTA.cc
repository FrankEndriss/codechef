/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we need to choose the biggest component
 * consisting of provices with max b[i]/a[i]
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);
    cinai(b,n);

    int maa=-1;
    int mab=-1;
    for(int i=0; i<n; i++) {
        int g=__gcd(a[i], b[i]);
        if(g>1) {
            a[i]/=g;
            b[i]/=g;
        }
        if(maa==-1 || maa*b[i]<a[i]*mab) {
            maa=a[i];
            mab=b[i];
        }
    }

    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vb vis(n);
    vi vans;
    function<int(int,int)> dfs=[&](int v, int p) {
        vans.push_back(v);
        vis[v]=true;
        int ans=1;
        for(int c : adj[v]) {
            if(!vis[c] && a[c]==a[v] && b[c]==b[v])
                ans+=dfs(c,v);
        }
        return ans;
    };

    int ans=0;
    vi vvans;
    for(int i=0; i<n; i++) {
        if(vis[i])
            continue;
        if(a[i]!=maa || b[i]!=mab) 
            continue;

        vans.clear();
        int cnt=dfs(i, -1);
        if(cnt>ans) {
            ans=cnt;
            vvans=vans;
        }
    }

    cout<<vvans.size()<<endl;
    for(int v : vvans)
        cout<<v+1<<" ";
    cout<<endl;
}

signed main() {
    cini(t)
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
