/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* Move all point in first quadrant.
 *
 * We need to group the points by 
 * smallest possible x, and fitting y
 *
 * Then for every group find median
 * of x and calc the number of operations
 * for every element.
 */
void solve() {
    cini(n);
    cini(c);

    vi x(n);
    vi y(n);

    int mix=0;
    int miy=0;
    for(int i=0; i<n; i++) {
        cin>>x[i]>>y[i];
        mix=min(mix, x[i]);
        miy=min(miy, y[i]);
    }

    map<pii,vi> group;

    for(int i=0; i<n; i++) {
        x[i]-=mix;
        y[i]-=miy;

        int smx=x[i]%c;
        int smy=y[i]-(x[i]/c)*c;

        group[{smx,smy}].push_back(i);
    }

    int ans1=group.size();
    int ans2=0;
    for(auto ent : group) {
        sort(all(ent.second), [&](int i1, int i2) {
            return x[i1]<x[i2];
        });
        int med=ent.second.size()/2;
        
        for(size_t i=0; i<ent.second.size(); i++) 
            ans2+=abs(x[ent.second[med]]-x[ent.second[i]])/c;
    }
    cout<<ans1<<" "<<ans2<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
