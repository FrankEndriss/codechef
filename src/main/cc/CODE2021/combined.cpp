
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



/* simple fraction implementation based on long long */
struct fract {
    ll f, s;

    fract(ll counter, ll denominator) :
        f(counter), s(denominator) {
        norm();
    }

    bool operator<(const fract &rhs) {
        return f * rhs.s < s * rhs.f;
    }

    fract& operator+=(const fract &rhs) {
        ll g = __gcd(this->s, rhs.s);
        ll lcm = this->s * (rhs.s / g);
        this->f = this->f * (lcm / this->s) + rhs.f * (lcm / rhs.s);
        this->s = lcm;
        norm();
        return *this;
    }

    friend fract operator+(fract lhs, const fract &rhs) {
        lhs += rhs;
        return lhs;
    }

    friend fract operator-(fract lhs, const fract &rhs) {
        lhs += fract(-rhs.f, rhs.s);
        return lhs;
    }

    fract& operator*=(const fract &rhs) {
        this->f *= rhs.f;
        this->s *= rhs.s;
        norm();
        return *this;
    }

    friend fract operator*(fract lhs, const fract &rhs) {
        lhs *= rhs;
        return lhs;
    }

    friend fract operator/(fract lhs, const fract &rhs) {
        return lhs * fract(rhs.s, rhs.f);
    }

    void norm() {
        int sign = ((this->f < 0) + (this->s < 0)) % 2;
        sign = -sign;
        if (sign == 0)
            sign = 1;
        ll g = __gcd(abs(this->f), abs(this->s));
        if (g != 0) {
            this->f = (abs(this->f) * sign) / g;
            this->s = abs(this->s) / g;
        }
    }
};

/* Use 2D-Fenwick to check if
 * there are some other competitors with
 * both values bg the current one.
 *
 * Note that it is enough that one of the values is same,
 * the other bigger.
 * ...
 * No, we cannot assume that both counts, bigger swim and bigger run
 * speed. So this is more like some convex hull.
 * If there is any x,y where the function of a competitor yields
 * max possible value, then that competitor can win.
 * How to find if such x,y exists?
 * If this competitor is fastest runner or fastest swimmer then
 * he might win.
 *
 * Compare current comp to all other, and the ratio of s/r.
 * if cur is faster/slower in both then he is allways faster/slower.
 * Else, there is a min/max ratio of s/r for which he will be faster.
 * Check these upper/lower bounds for all pairs.
 *
 * if there  is some j so that ss[i]<ss[j], and rr[i]>rr[j]
 * let ds,dr the distances, so 
 * ti=ds/ss[i] + dr/rr[i]
 * tj=ds/ss[j] + dr/rr[j]
 * tj=ti - ds/ss[i] + ds/ss[j] - dr/rr[i] + dr/rr[j]
 * tj-ti = ds/ss[j]-ds/ss[i] + dr/rr[j]-dr/rr[i]
 *
 * Let ss[i] and rr[i] not be speed, but pace. Pace is time per distance. Then
 * ti=ds*ss[i] + dr * rr[i]
 * tj=ds*ss[j] + dr * rr[j]
 * ti-tj= ds*(ss[i]-ss[j]) + dr*(rr[i]-rr[j])
 * So, if the diff of the swim paces multiplicated by swim distance is 
 * bg the diff of the run paces multiplicated by the run distance, then
 * i is faster.
 * So we need to consider the ratio of the diff of the swim paces to 
 * the difference of the run paces.
 */
const int N=1e4+7;
const int INF=1e9;

void solve() {
    cini(n);

    vi ans(n, -1);  /* -1==unkown */

    vi ss(n);
    vi rr(n);
    for(int i=0; i<n; i++) {
        cini(s);
        cini(r);
        ss[i]=s;
        rr[i]=r;
    }

    for(int i=0; i<n; i++) {
        if(ans[i]>=0)
            continue;

        fract minRat(1, INF);   /* s/r */
        fract maxRat(INF, 1);

        for(int j=i+1; ans[i]<0 && j<n; j++) {
            if((ss[j]<ss[i] && (rr[j]<rr[i] || rr[j]==rr[i])) || ((ss[j]<ss[i] || ss[j]==ss[i]) && rr[j]<rr[i])) {
                ans[j]=0;
                continue;
            }
            if((ss[i]<ss[j] && rr[i]<=rr[j]) || (ss[i]<=ss[j] && rr[i]<rr[j])) {
                ans[i]=0;
                break;
            } else {
                fract siPace(1,ss[i]);
                fract sjPace(1,ss[j]);
                fract riPace(1,rr[i]);
                fract rjPace(1,rr[j]);
                fract sdPace(siPace-sjPace);
                fract rdPace(riPace-rjPace);
                if(sjPace<siPace) {
                    minRat=max(minRat, sdPace/rdPace);
                } else if(rjPace<riPace) { 
                    maxRat=max(maxRat, sdPace/rdPace);
                } else
                    assert(false);  /* one of them must be bigger */
            }
        }
        if(ans[i]<0) {
            if(minRat<=maxRat)
                ans[i]=1;
            else
                ans[i]=0;
        }
    }

    for(int i=0;i <n; i++) {
        assert(ans[i]>=0);
        if(ans[i]>0)
            cout<<i+1<<" ";
    }

    cout<<endl;
}

signed main() {
    solve();
}
