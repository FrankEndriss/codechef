
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define N 256
int idx[N];

int main() {
string s;
	cin>>s;
	for(int i=0; i<s.length(); i++)
		idx[s[i]]++;

	int ans=0;
	for(int i=0; i<N; i++) 
		if(idx[i])
			ans++;
	cout<<ans<<endl;


}

