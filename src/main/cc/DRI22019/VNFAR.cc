/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef unsigned long long ll;


ll gcd(ll a, ll b) {
    while (b > 0) {
        ll temp = b;
        b = a % b;
        a = temp;
    }
    return a;
}

void solve() {
    ll a1, l1, l2;
    cin>>a1>>l1>>l2;
    ll price;
    cin>>price;

	ll ax=a1;

    ll g=gcd(l1, l2);
    l1/=g;
    l2/=g;

    g=gcd(a1, l2);
    a1/=g;
    l2/=g;



    ll a=ax+(a1*l1/l2);

    cout<<a*price<<endl;

}

int main() {
    int t;
    cin>>t;
    for(int i=0; i<t; i++)
        solve();

}

