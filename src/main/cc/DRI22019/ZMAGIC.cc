/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define N 22;
int a[N+1];
int n;
void solve() {
	cin>>n;
	for(int i=0; i<n; i++)
		cin>>a[i];

	sort(&a, &a+n);
/* find all nonempty subseqs... 
 * where a subsubseq exists where sum(subsubseq)==sum(subseq)/2.
 * knapsack?
 *
 * How many subseqs exist?
 * |N|-> 1
 * |N-1| -> N
 * |N-2| -> N * N-1
 * ...
 * |2| -> N * N-1
 * |1| -> N
 *
 * Sorted subseqs:
 * ZERO of len=2-> all same pairs
 *
 **/


}

int main() {
int t;
	cin>>t;
	for(int i=0; i<t; i++)  {
		solve();
	}
}

