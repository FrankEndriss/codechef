/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s) c = c == ','?  ' ': c;
    stringstream ss; ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) { cerr << endl; }
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0) cerr << ", ";
    stringstream ss; ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

const int N=2e6;

class fenwick {
public:
    map<int, int> fenw;

    void modify(int x, int v) {
        while (x < N) {
            fenw[x] += v;
            x |= (x + 1);
        }
    }

    /* get sum of range (0,x), including x */
    int get(int x) {
        int v{};
        while (x >= 0) {
            v += fenw[x];
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

class fenwick2d {
public:
    map<int,fenwick> fenw2d;

    void modify(int x, int y, int v) {
        while (x < N) {
            fenw2d[x].modify(y, v);
            x |= (x + 1);
        }
    }

    int get(int x, int y) {   // range 0..x/y, including x/y
        x=min(x, N-1);
        y=min(y, N-1);
        int v{};
        while (x >= 0) {
            v += fenw2d[x].get(y);
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

pii rot45(int x, int y) {
        x-=1e6;
        int y1=y - x;
        int x1=y + x;
        x1+=1e6;
    return { x1, y1 };
}

/* we would rotate the whole thing by 45 degree in
 * clockwise dir around the point (1e6,0)
 * Then use 2D-Fenwick to find the number of points
 * in rect.
 * What about roundings, after rotation a point might be
 * on real coordinates.
 * If we multiply everything by two we can place these points
 * on odd coordinates, but how to check if the values are int?
 */
void solve() {
    cini(n);
    cini(q);

    const double a=45*PI/180;
    const double cosa=cos(a);
    const double sina=sin(a);

    fenwick2d fen;
    
    for(int i=0; i<n; i++) {
        cini(x);
        cini(y);
        /* rotate x,y, then insert 1 into fenwick at pos x,y */
        auto p=rot45(x,y);
        auto x1=p.first;
        auto y1=p.second;
        fen.modify(x1, y1, 1);
    }

    for(int i=0; i<q; i++) {
        cini(l);
        cini(r);

        auto p=rot45(l,0);    /* left upper corner */
        auto x1=p.first;
        auto y2=p.second;
        p=rot45(r,0);    /* right bottom corner */
        auto x2=p.first;
        auto y1=p.second;

        int ans=fen.get(x2, y2)+fen.get(x1,y1)-fen.get(x1,y2)-fen.get(x2,y1);
        cout<<ans<<" ";
    }
    cout<<endl;
}

void test() {
    const double a=45*PI/180;
    const double cosa=cos(a);
    const double sina=sin(a);

cout<<setprecision(12)<<fixed;
cout<<"cosa="<<cosa<<" sina="<<sina<<endl;

}
signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
       solve();
}

