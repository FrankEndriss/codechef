/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

vector<string> vec_splitter(string s) {
    for(char& c: s)
        c = c == ','?  ' ': c;
    stringstream ss;
    ss << s;
    vector<string> res;
    for(string z; ss >> z; res.push_back(z));
    return res;
}

void debug_out(vector<string> __attribute__ ((unused)) args, __attribute__ ((unused)) int idx) {
    cerr << endl;
}
template <typename Head, typename... Tail>
void debug_out(vector<string> args, int idx, Head H, Tail... T) {
    if(idx > 0)
        cerr << ", ";
    stringstream ss;
    ss << H;
    cerr << args[idx] << " = " << ss.str();
    debug_out(args, idx + 1, T...);
}

#ifdef XOX
#define debug(...) debug_out(vec_splitter(#__VA_ARGS__), 0, __VA_ARGS__)
#else
#define debug(...) 42
#endif

const double PI = acos(-1);
typedef long long ll;
#define int ll
#define fori(n) for(int i=0; i<int(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

typedef pair<int, int> pii;
typedef pair<double, double> pdd;
typedef vector<double> vd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

/*
 * a2[0]=a1[l[0]] ^ a1[l[0]+1] ^ ... ^ a1[r[0]]
 * a2[1]=a1[l[1]] ^ a1[l[1]+1] ^ ... ^ a1[r[1]]
 * ...
 * a3[0]=a2[l[0]] ^ a2[l[0]+1] ^ ... ^ a2[r[0]]
 *
 * brute force?
 * There is some periodic system in it.
 */
void solve() {
    cini(n);
    cini(k);
    k--;
    cinai(a,n);
    vi l(n);
    vi r(n);
    for(int i=0; i<n; i++) {
        cin>>l[i]>>r[i];
        l[i]--;
        r[i]--;
    }

    map<vi,int> vis;
    vis[a]=0;

    int ans=-1;
    for(int kk=1; kk<=k; kk++) {
        vi ax(n);
        for(int i=0; i<n; i++) {
            for(int j=l[i]; j<=r[i]; j++)
                ax[i]^=a[j];
        }
        a=ax;
        auto it=vis.find(a);
        if(it!=vis.end()) {
            int seen=it->second;
            int p=kk-seen;
            ans=(k-kk)%p+seen;
//cerr<<"seen="<<seen<<" p="<<p<<" kk="<<kk<<" ans="<<ans<<endl;
            break;
        }

        vis[a]=kk;
    }

    for(auto ent : vis) {
        if(ent.second==ans) {
            a=ent.first;
            break;
        }
    }
    for(int i=0; i<n; i++)
        cout<<a[i]<<" ";
    cout<<endl;

}

signed main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

