
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* if(y%x==0) there is a solution, else not.
 * we can reduce y=y/x, and x=1;
 * Then it is allways optimal to use the longest possible strike,
 * since if a strike is ended before the next strike gets the longer.
 * Note that ans is fairly small number, not much more than ~60
 *
 * Still WA, what is wrong?
 * It is trivial algorythm.
 * -> all a[i] pairwise distinct!
 */
void solve() {
    cini(x);
    cini(y);

    if(y%x!=0) {
        cout<<-1<<endl;
        return;
    }
    y/=x;
    x=1;

    set<pii> s; /* <burgers,minutes> */
    int a=1;
    int aa=1;
    int cnt=1;
    while(aa<=y) {
        s.emplace(aa,cnt);
        a*=2;
        aa+=a;
        cnt++;
    }

    int ans=0;
    while(y>0) {
        auto it=s.lower_bound({y+1,0});
        if(it!=s.begin()) {
            it--;
            y-=it->first;
            ans+=it->second;
            s.erase(it);
        } else {
            cout<<-1<<endl;
            return;
        }

        if(y>0)
            ans++;
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
