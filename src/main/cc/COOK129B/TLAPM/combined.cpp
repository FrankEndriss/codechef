
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

const int N=1003;
vvi a(N, vi(N));
vvi preNS(N, vi(N));
vvi preWE(N, vi(N));

void init() {

    a[0][0]=1;
    for(int i=0; i<N-1; i++) {
        a[i+1][0]=a[i][0]+i+2;

        for(int j=1; j<N-1; j++) {
            a[i][j]=a[i][j-1]+j+i;
        }
    }

    for(int i=0; i<N; i++) {
        for(int j=0; j<N; j++) {
            if(i>0)
                preNS[i][j]=preNS[i-1][j]+a[i][j];
            else
                preNS[i][j]=a[i][j];

            if(j>0)
                preWE[i][j]=preWE[i][j-1]+a[i][j];
            else
                preWE[i][j]=a[i][j];
        }
    }
}

/* How in less than O(n^3)?
 * We cannot simply use the diff of both dp cells.
 * There must be some greedy.
 * ...-> first go down, then go right
 *
 * What an uncomfortable problem :/
 * So much possible error sources.
 */
void solve() {

    cini(x1);
    x1--;
    cini(y1);
    y1--;
    cini(x2);
    x2--;
    cini(y2);
    y2--;

    int ans1=preNS[x2][y1];
    if(x1>0)
        ans1-=preNS[x1-1][y1];

    int ans2=preWE[x2][y2]-preWE[x2][y1];
    //cerr<<"ans1="<<ans1<<" ans2="<<ans2<<endl;
    int ans=ans1+ans2;
    cout<<ans<<endl;

}

signed main() {
    init();
    cini(t);
    while(t--)
        solve();
}