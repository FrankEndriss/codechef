
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


inline void fs(int &number) {
#define gc() getchar_unlocked()

    int c = gc();
    while (c == ' ' || c == 13 || c == 10)
        c = gc();

    number = 0;
    while (c >= '0' && c <= '9') {
        number = number * 10 + c - '0';
        c = gc();
    }
}

/*
 * Op 1, permute, can be done n times in logn
 *
 * How comes op 2 into play, the swap?
 * We need to calc which indexes that where before all the permut operations.
 * So, we need to find the "opposite" permute operation, and do that 
 * one in logn on the given indexes. Then swap the orig array.
 *
 * On op 3 just find the index in opposite permute operation and output
 * its value.
 */
void solve() {
    int n;
    fs(n);
    vi a(n);
    for(int i=0; i<n; i++)
        fs(a[i]);
    vi p(n);
    for(int i=0; i<n; i++) 
        fs(p[i]);

    vvi pp(20, vi(n));   /* reverse p[] */
    for(int i=0; i<n; i++) {
        p[i]--;
        pp[0][p[i]]=i;
    }

    for(int j=1; j<20; j++) 
        for(int i=0; i<n; i++)
            pp[j][i]=pp[j-1][pp[j-1][i]];

    function<int(int,int)> rev=[&](int idx, int k) {
        for(int i=0; k>0;i++) {
            if(k&(1<<i)) {
                idx=pp[i][idx];
                k-=(1<<i);
            }
        }
        return idx;
    };

    int q;
    fs(q);
    int cnt=0;  /* number of permute operations */
    for(int i=0; i<q; i++) {
        int t,x,y;
        fs(t);
        if(t==1) {
            cnt++;
        } else if(t==2) {
            fs(x); x--;
            fs(y); y--;
            x=rev(x, cnt);
            y=rev(y, cnt);
            swap(a[x], a[y]);
        } else if(t==3) {
            fs(x);
            x--;
            x=rev(x,cnt);
            cout<<a[x]<<endl;
        } else
            assert(false);
    }

}

signed main() {
    int t;
    fs(t);
    while(t--) 
        solve();
}
