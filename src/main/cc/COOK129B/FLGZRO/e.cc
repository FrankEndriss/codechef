
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Hah???
 * There are that much find trees possible as there are leafs in the tree.
 * ...
 * Ok, no. Each leaf node has a number of swap operations until reached,
 * and each seq of swaps constructs another tree.
 * So, we need to collect the depth of all leafs, then foreach
 * depth calc the number of possible paths and mult by the number
 * of leafs of that depth.
 * How to find the number of paths per depth?
 * -> it is simply 2^(len-1)
 * ...no
 * since a[i] is not distinct, it is 2^(distinct vals in a[] - 1)
 *
 * We need to multiply by the freqs of distinct values...somehow
 *
 * Nah, that "distinct values" is not correct.
 * It is number of "compressed distinct values".
 * ...no :/
 *
 * We have a list of values, and choose some indexes of it.
 * We want to find the number of such lists that differ from each
 * other after compressing adj same values.
 * How to to that?
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    vvi adj(n);

    for(int i=0; i<n-1; i++) {
        cini(u);
        u--;
        cini(v);
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    cinai(a,n);
    if(n==1) {
        cout<<1<<endl;
        return;
    }

    map<int,int> dp;    /* dp[i]=freq of i in current path */
    vi cnt(n);
    vector<mint> f(n);
    mint freqm=1;
    vb leaf(n, true);
    function<void(int,int)> dfs=[&](int v, int p) {
        if(p<0 || a[p]!=a[v]) {
            dp[a[v]]++;
            if(dp[a[v]]>1) {
                freqm*=dp[a[v]];
                mint mi(dp[a[v]]-1);
                freqm*=mi.inv();
            }
        }

        cnt[v]=dp.size();
        f[v]=freqm;

        for(int c : adj[v]) {
            if(c!=p) {
                leaf[v]=false;
                dfs(c,v);
            }
        }
        if(p<0 || a[p]!=a[v]) {
            auto it=dp.find(a[v]);
            it->second--;
            if(it->second==0)
                dp.erase(it);
            else {
                freqm*=it->second;
                mint ma(it->second+1);
                freqm*=ma.inv();
            }
        }
    };

    dfs(0,-1);

    mint ans=0;
    for(int i=0; i<n; i++) {
        if(leaf[i]) {
            mint two(2);
            two=two.pow(cnt[i]-1);
            ans+=two;
        }
    }

    cout<<ans.val()<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
