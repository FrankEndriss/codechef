
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/lazysegtree>
using namespace atcoder;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

using S=ll; /* type of values */
using F=ll; /* type of upates */

S st_op(S a, S b) {
    return a+b;
}

/* This is the neutral element */
S st_e() {
    return 0;
}

/* This applies an update to some value. */
S st_mapping(F f, S x) {
    return f+x;
}

/* This combines two updates.  */
F st_composition(F f, F g) {
    return f+g;
}

/* This is the neutral update. 
 * It is similar to st_e() in some sense.  */
F st_id() {
    return 0;
}

using stree=lazy_segtree<S, st_op, st_e, F, st_mapping, st_composition, st_id>;

/**
 * Consider 
 * i=1, each element that is there at least twice contributes
 * at each position.
 * i=k, foreach k, if there is an "more leftmost" occ of the prefix up to k,
 *      or a "more rightmost" occ of the postfix after k, then the subarray
 *      contributes.
 * But that O(n^2) at least again. We cannot iterate all subarrays.
 * But, actually if a[i] is not the first occ of a[i], then we know
 * all subarrays starting at i contribute, we can do a range update on ans.
 *
 * We need to check only the the first element of the subarray,
 * if that is moveable then all subarrays starting at that position contribute,
 * else none of them.
 *
 * But, we also have to consider the other side, which is the same prob mirrored.
 * So, from all positions that do not contribute in first direction, we need to 
 * find the ones that do from other direction.
 * How to do that?
 * Or can we somehow find the double count?
 */
void solve() {
    cini(n);
    cinai(a,n);

    vb lmove(n);    /* left moveable positions */
    vb vis(n+1);    /* seen numbers */

    stree ans(n);

    cerr<<"left to right"<<endl;
    for(int i=0; i<n; i++) {
        if(vis[a[i]]) {
            ans.apply(0,n-i,1);
            lmove[i]=true;
            cerr<<"lmove["<<i<<"]=true ans.get(0)="<<ans.get(0)<<endl;
        }
        vis[a[i]]=true;
    }

    stree ansR(n);
    vb visR(n+1);
    for(int i=n-1; i>=0; i--) {
        if(visR[a[i]]) {
            ansR.apply(0,i+1,1);
            cerr<<"ansR.apply at pos="<<i<<endl;
        }
        visR[a[i]]=true;
    }

    for(int i=0; i<n; i++) {
        if(!lmove[i]) {
            cerr<<"!lmove[i], i="<<i<<" ansR.get(i)="<<ansR.get(i)<<endl;
            ans.apply(i,i+1,ansR.get(i));
        }
    }

    for(int i=0; i<n; i++)
        cout<<ans.get(i)<<" ";
    cout<<endl;
}

signed main() {
   cini(t);
   for(int i=1; i<=t; i++) {
       solve();
   }
}
