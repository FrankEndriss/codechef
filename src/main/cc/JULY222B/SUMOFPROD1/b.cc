
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to find the number of continous subarrays
 * with all elements eq 1.
 * Find length of seqs of 1, each contributes
 * l*(l+1)/2
 */
void solve() {
    cini(n);
    cinai(a,n);
    a.push_back(0);

    int ans=0;
    int cnt=0;
    for(int i=0; i<=n; i++) {
        if(a[i]==0) {
            ans+=cnt*(cnt+1)/2;
            cnt=0;
        } else {
            cnt++;
        }
    }
    cout<<ans<<endl;
}

signed main() {
   cini(t);
   for(int i=1; i<=t; i++) {
       solve();
   }
}
