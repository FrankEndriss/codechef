
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * WLOG assume a<=b
 * Consider ans=
 * {N, y*N}, {N, (y-1)*n},... y-2???
 * {N+1, y*(N+1)}, {(N+1, y*(N+1-2)}
 * ...
 * Note that ans={N,N} eq 0.
 */
void solve() {
    cini(n);
    cini(m);

    int ans=0;
    int ans1=n;
    int ans2=n;
    for(int i=n; i<=min(2*n,m); i++) {
        int a=i;
        int b=(m/a)*a;
        if(b+a<=m)
            b+=a;
        while(b>a && gcd(a,b)<n)
            b-=a;
        if(b>a) {
            if(b-a>ans) {
                ans=b-a;
                ans1=a;
                ans2=b;
            }
        }
    }
    cout<<ans1<<" "<<ans2<<endl;
}

signed main() {
   cini(t);
   for(int i=1; i<=t; i++) {
       solve();
   }
}
