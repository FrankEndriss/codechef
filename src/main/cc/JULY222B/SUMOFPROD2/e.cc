
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * So, we would like to summarize the sums of all 2^N possible distributions.
 *
 * Consider {N,0} (ie N times 1, 0 times 0), there is obviously only one
 * possible disttibution, it contributes fac[N]/2.
 * {N-1,1}, there are 2 distributions, contributing fac[N-1]/2 each,
 *          also there are n-2 distributions contributing fac[n-2-j]*fac[j]
 *          for j in 1..n-3. (ie the 0 is moved over all positions.
 *
 * {N-2,2}, ...and so on
 * {N-i,i}  Consider the biggest block of 1s, it is on left end or right end,
 *          or surrounded by two 0s, and the remaining 1s go to the left or 
 *          rigth space.
 *
 * ...much to complected, also there is _no_ way to do this in less 
 * than O(n^2). Just quit.
 *
 */
void solve() {
    cini(n);
    cinai(a,n);
    vi f(2);
    for(int i=0; i<n; i++)
        f[a[i]]++;

}

signed main() {
   cini(t);
   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
   }
}
