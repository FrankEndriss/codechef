
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to construct the longest increasing subseq
 * with all elements of the sorted seq, then the same
 * again.
 * If we can mark all elements in two passes, ans=YES.
 */
void solve() {
    cini(n);
    cinai(a,n);
    vi b=a;
    sort(all(b));

    int idx=0;
    vb vis(n);
    for(int i=0; i<n; i++) {
        if(b[idx]==a[i]) {
            idx++;
            vis[i]=true;
        }
    }
    for(int i=0; idx<n && i<n; i++) {
        if(!vis[i] && idx<n && b[idx]==a[i]) {
            idx++;
        }
    }

    if(idx==n)
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
   cini(t);
   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
   }
}