
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

void solve() {
    ll m;
    cin>>m;

    vector<ll> ans;
    /* assume A is between m+1 and 2*m inclusive */
    for(ll A=m; A<=3*m; A++) {
cout<<"check A="<<A<<endl;
        /*  check if i is interesting, binsearch. */
        ll lower=A;
        ll upper=A;
        while((A*upper)/m <A+upper)
            upper*=2;

        while(lower<upper) {
            ll B=(upper+lower)/2;
cout<<"B="<<B<<" lo="<<lower<<" up="<<upper<<endl;
            if(B==lower) {
                B=lower+1;
                lower=B;
            }
            ll r=(A*B)/m - (A+B);
            if(r==0 && (A*B)%m==0) {
                ans.push_back(A);
                break;
            } else if(r<0)
                lower=B;
            else
                upper=B;
        }
    }
    cout<<ans.size()<<endl;
    for(auto l : ans)
        cout<<l<<endl;
}
int main() {
int t;
    cin>>t;
    fori(t)
        solve();

}

