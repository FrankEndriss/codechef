/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

void solve() {
    int n;
    cin>>n;
    vector<ll> a(n);
    fori(n)
    cin>>a[i];

    const int BITS=30;
    int cnt[BITS][2]= {0};
    for(auto k : a) {
        fori(BITS)
            cnt[i][(k>>i)&1]++;
    }
    int x=0;
    fori(BITS)
        if(cnt[i][1]>cnt[i][0])
            x+=1<<i;

    ll s=0;
    for(auto k : a)
        s+=(k^x);
    cout<<s<<endl;
}

int main() {
    int t;
    cin>>t;
    fori(t)
    solve();

}

