/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

void solve() {
    ll n, x;
    cin>>n>>x;
    vector<ll> a(n);
    vector<int> id(n);      /* sorting of a */
    vector<bool> cooked(n, false); /* cooked stacks */
    set<int> unknown;       /* unknown stacks, need to cleanup. */
    set<int> vis;           /* known removed stacks */

    fori(n) {
        cin>>a[i];
        id[i]=i;
    }

    /** 1. A section should be moved not more than x/a[i] places.
     *  Its allways better to move the smaller ones.
     *  So, start with biggest, move all arround of biggest to biggest, and remove that stack.
     *  Repeat.
     *  Finding the smallers arround: Its hard, because one cannot tell if that
     *  smaller one belongs to this bigger one, or another bigger one.
     *  So, we need to keep track of all cooked sections, and check the unknown stacks
     *  to which cooked section it belongs, if it does.
     *  We need to find the first stack (in both directions) which is to far to be moved to
     *  the current biggest one. Then, all stacks nearer to the current biggest belong to the
     *  biggest. All nearer to the other one are unknown.
     *  If the other one is moved (obviously in the other direction), they could belong to
     *  the current biggest one, to. If not, then to the other one.
     *
    **/

    sort(id.begin(), id.end(), [&a](const int i1, const int i2) {
        return a[i1]<a[i2];
    });

    ll ans=0;

    int bigIdx=n-1; /** index of current biggest stack. */
    while(vis.size()<n) {
        while(vis.count(id[bigIdx]))
            bigIdx--;

        /* find nearest cooked, cook with that if possible, else standard handling. */
        int maxDist=a[id[bigIdx]]==0?n:x/a[id[bigIdx]];
        bool fini=false;
        for(int i=1; i<=maxDist; i++) {
            if((id[bigIdx]+i<n && cooked[id[bigIdx]+i]) || (id[bigIdx]-i>=0 && cooked[id[bigIdx]-i])) {
                ans+=i*a[id[bigIdx]];
                vis.insert(id[bigIdx]);
                fini=true;
                break;
            }
        }
        if(fini)
            continue;

        vis.insert(id[bigIdx]); /** biggest get cooked */
        cooked[id[bigIdx]]=true;
        ans+=x;
//cout<<"cooked="<<id[bigIdx]<<" ans="<<ans<<endl;

        /* find first unmoveable left and right of biggest. */
        int left=-1;
        for(int i=id[bigIdx]-1; i>=0; i--) {
            if(!vis.count(i)) {
                if(a[i]*(id[bigIdx]-i)>x) {
                    left=i;
                    break;
                }
            }
        }
        int right=-1;
        for(int i=id[bigIdx]+1; i<n; i++) {
            if(!vis.count(i)) {
                if(a[i]*(i-id[bigIdx])>x) {
                    right=i;
                    break;
                }
            }
        }
        /* Remove all which are nearer to biggest than to left or right.
           Also remove all unknowns in that area.  */
        int bigAreaLeft;
        if(left<0)
            bigAreaLeft=0;
        else
            bigAreaLeft=id[bigIdx]-((id[bigIdx]-left)/2);
        int bigAreaRight;
        if(right<0)
            bigAreaRight=n-1;
        else
            bigAreaRight=id[bigIdx]+((right-id[bigIdx])/2);

//cout<<"bigArea left="<<bigAreaLeft<<" bigArea right="<<bigAreaRight<<endl;
        for(int i=bigAreaLeft; i<=bigAreaRight; i++) {
            if(!vis.count(i)) { /* move i to cooked stack */
                vis.insert(i);
                unknown.erase(i);
                ans+=a[i]*abs(id[bigIdx]-i);
//cout<<"moved="<<i<<" ans="<<ans<<endl;
            }
        }
    }
    cout<<ans<<endl;
}

int main() {
    int t;
    cin>>t;
    fori(t)
    solve();

}

