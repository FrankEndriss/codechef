/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)
#define fauto(c, vec) for(auto c : (vec))

#define N 200001;

int n, q;
vector<ll> v(N);
vector<int> vIdx(N);

map<pair<int, int>, ll> memo;

#define cost(x, y) (abs(v[x]-v[y]) + y - x)

/* cheapest path from x to y, stop search if more than maxCost. */
ll chPath(int x, int y, ll preCosts, unordered_set<int> prePath) {
    if(memo.count(make_pair(x, y)))
        return memo[make_pair(x, y)];


    priority_queue<pair<int, ll>> paths;
    ll mi=cost(x, y);

    for(int i=0; i<n; i++) {
        if(i!=x && i!=y) {
            ll c=cost(x, i);
            if(c<mi) {
                paths.insert({ i, c });
                //.. TODO
            }
        }
    }
    while(paths.size()) {
    }
}
}

void solve() {
    cin>>n>>q;
    fori(n) {
        ll tmp;
        cin>>tmp;
        v.push_back(tmp);
        vIdx.push_back(i);
    }
    sort(vIdx.begin(), vIdx.end(), [](const int i1, const int i2) {
        return v[i1]<v[i2];
    });


    /* Find min cost path */
    fori(q) {
        int x, y;
        cin>>x>>y;
        x--;
        y--;
        ll mCost=chPath(x, y);
    }
}
int main() {
    int t;
    cin>>t;
    fori(t)
    solve();
}

