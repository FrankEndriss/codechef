/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/** stolen from tourist, Problem D, https://codeforces.com/contest/1208/standings/page/1 */
template <typename T>
class fenwick {
public:
    vector<T> fenw;
    int n;

    fenwick(int _n) : n(_n) {
        fenw.resize(n);
    }

    void modify(int x, T v) {
        while (x < n) {
            fenw[x] += v;
            x |= (x + 1);
        }
    }

    T get(int x) {
        T v{};
        while (x >= 0) {
            v += fenw[x];
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

template <typename T>
class fenwick2d {
public:
    vector<fenwick<T>> fenw2d;
    int n, m;

    fenwick2d(int x, int y) : n(x), m(y) {
        fenw2d.resize(n, fenwick<int>(m));
    }

    void modify(int x, int y, T v) {
        while (x < n) {
            fenw2d[x].modify(y, v);
            x |= (x + 1);
        }
    }

    T get(int x, int y) {
        x=min(x, n-1);
        y=min(y, m-1);
        T v{};
        while (x >= 0) {
            v += fenw2d[x].get(y);
            x = (x & (x + 1)) - 1;
        }
        return v;
    }
};

void solve() {
    cini(n);
    cini(q);
    cinai(a,n);

/* We need to find the numer of inversion and not-inversion in a,
 * and group that numbers by the bit at which the a[i]>a[j] differs.
 *
 * So, how to find the number of inversions in a ?
 * Create id sorting and count the number of position-moves, div by 2.
 *
 * How to find the inversions grouped by bit?
 * And the not-inversions, ie the ones which would be inversions if the 
 * bits where flipped?
 */

    /* example brute force */
    vvi invs(2, vi(31));    // 0==not-inversions, 1==inversions
    for(int i=0; i<n; i++) {
        for(int j=i+1; j<n; j++) {
            for(int bit=30; bit>=0; bit--) {
                if(a[i]&(1<<bit) > a[j]&(1<<bit))
                    invs[1][bit]++;
                else
                    invs[0][bit]++;
            }
        }
    }

    vi aid(n);

// WRONG !!!
    for(int b=0; b<31; b++) {
        iota(aid.begin(), aid.end(), 0);
        int bit=1<<b;
        sort(aid.begin(), aid.end(), [&](int i1, int i2) {
            return (a[i1]&bit)<(a[i2]&bit);
        });
        ll linvs=0;
        for(int i=0; i<n; i++)
            linvs+=abs(aid[i]-i);

        invs[b]=linvs/2;
    }

    for(int i=0; i<n; i++) {
        
    }
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

