
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* Note N,K and a[i] are limited to 1000
 *
 * We want the longest subseq with
 * at most k number changes.
 *
 * Consiser foreach number the indices.
 * Choose k subseqs from these lists to build longest possible list.
 *
 * Having a dp is dp[i][j]=length of longest such list at position i if it ends in j
 * ...no, we need to consider k somehow :/
 *
 *
 * ...idk :/
 */
const int N=1003;
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vvi  dp(N, vi(N));
    vi dp1(N);
    dp[0][a[0]]=1;
    dp1[a[0]]=1;
    for(int i=1; i<n; i++) {

        int ma=0;
        for(int j=1; j<N; j++) {
            dp[i][j]=dp[i-1][j];
            ma=max(ma, dp[i-1][j]);
        }

        dp[i][a[i]]=ma+1;
    }

    int ans=0;
    for(int i=1; i<N ; i++) 
        ans=max(ans, dp[n-1][i]);

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
