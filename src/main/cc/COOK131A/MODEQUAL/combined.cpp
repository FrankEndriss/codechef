
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We can make numbers only smaller, never bigger.
 * We cannot make a 1 from a 2.
 * We cannot make a to a/x, i.e we cannot subtract a divisor.
 * All numbers can be made 0.
 * ...
 * Which numbers can we make it?
 *
 * We need to check if we can make all other numbers eq the smallest number,
 * else ans=n.
 * How to check?
 *
 */
void solve() {
    cini(n);
    cinai(a,n);

    int mi=*min_element(all(a));

    bool ok=true;
    for(int i=0; i<n; i++)  {
        if(a[i]==mi)
            continue;

        /* check if we can make a[i]%m==mi */
        int d=a[i]-mi;
        if(gcd(d,mi)!=1 || (a[i]==2 && mi==1))
            ok=false;
    }

    if(!ok)
        cout<<n<<endl;
    else {
        int ans=0;
        for(int i=0; i<n; i++) 
            if(a[i]!=mi)
                ans++;
        cout<<ans<<endl;
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}