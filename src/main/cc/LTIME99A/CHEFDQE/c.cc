
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We want to remove a prefix of size k and optionally a postfix of size k
 * Removing a prefix or postfix on len i costs numBits(i)
 *
 * So find foreach prefix the cheapest postfix so that the
 * sum is m0, and no bit is set twice.
 * ...No
 * We remove a postfix of any size, and remove a submask of that size
 * as prefix.
 * Size postfix + size prefix must not bg n.
 * Do this by iterating submasks.
 */
void solve() {
    cini(n);
    cini(m);
    cinai(a,n);

        // TODO

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
