
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* shitty complecated problem statement :/
 *
 * What does it to subtract the xor from the or of two values?
 * -> It does leave us with the and of both values.
 * So since we do this with all pairs of values, b[i] is
 * the and of all remaining elements in a.
 */
void solve() {
    cini(n);
    cinai(a,n);

    vi b(n);
    b[n-1]=a[n-1];
    for(int i=n-2; i>=0; i--) 
        b[i]=b[i+1]&a[i];

    for(int i : b)
        cout<<i<<" ";
    cout<<endl;
}

signed main() {
    solve();
}
