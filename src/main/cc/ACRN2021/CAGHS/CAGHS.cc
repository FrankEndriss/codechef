
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* First bfs all reacheable (and hence unreacheable) vertex.
 * Then find min cost with MST like Kruskal.
 * Consider here all edges from and to unreacheable landmarks,
 * and consider all other vertex connected.
 * ...
 * No need for Kruskal, since the cost for the new roads are
 * determined by the landmarks. So it is just the sum
 * of the unreacheable landmarks.
 * ...
 * It is not that simple, because there are some roads between
 * unreacheable vertex, so we do not need to connect all of them.
 * How to find the minimum subset?
 *
 * Each vertex in the set of unreacheable vertex connects a subset
 * of all unreacheable vertex.
 * So, we need to connect the cheapest subset of these vertex covering
 * all of them.
 *
 * How to find foreach vertex the set of reacheable vertex from that one?
 * Since N=M=1e5 we cannot bfs each one.
 * Then do knapsack on bitsets? ...This is all to slow.
 *
 * ...idk :/
 */
void solve() {
    cini(n);
    cini(m);

    queue<int> q;
    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        adj[u].push_back(v);
    }

    vi c(n);
    vb vis(n);
    for(int i=0; i<n; i++) {
        cin>>c[i];
        if(c[i]==0) {
            cerr<<"hospital: "<<i<<endl;
            q.push(i);  /* hospital */
            vis[i]=true;
        }
    }

    while(q.size()) {
        int v=q.front();
        q.pop();
        for(int chl : adj[v]) {
            if(!vis[chl]) {
                vis[chl]=true;
                q.push(chl);
            }
        }
    }
    int ans=0;
    for(int i=0; i<n; i++) 
        if(!vis[i]) {
            cerr<<"i="<<i<<endl;
            ans+=c[i];
        }

    cout<<ans<<endl;
}

signed main() {
    solve();
}
