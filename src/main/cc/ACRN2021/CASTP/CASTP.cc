
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * Take a subset of k different chars.
 * Then find min cost to convert each freq of chars
 * in a to one of taken chars.
 *
 * But that to slow, since nCr(26,k) is fairly huge number.
 *
 * Consider l be number of distinct chars in S.
 * if l<=k then ans=0
 * So if l>k there is a number of chars we need to change to some
 * additional char, we must change l-k distinct chars in S.
 * And we need to change them to some other char.
 * So we can dp this, where
 * dp[i][k]==costs to change first i distinct chars to k distinct ones.
 *
 * We need to find k medians.
 * It is obvious that we want to convert letters to adj letters.
 * Foreach k groups of alphabetic consecutive letters we need
 * to calc the costs.
 * So we have the costs 
 * dp[i][j]=cost of group starting at letter i until letter j.
 * This works, since there are only 26 letters, hence ~600 states.
 */
const int INF=1e9;
const int N='z'-'a'+1;
void solve() {
    cini(n);
    cini(k);
    cins(s);

    vi f(N);
    for(char c : s)
        f[c-'a']++;

    sort(all(s));

    /* dp[i][j]=cost of group starting at letter i until letter j */
    vvi dp(N, vi(N));

    int idx=0;  /* index of first instance of current letter i */
    for(int i=0; i<N; i++) {
        //cerr<<"cur letter="<<i<<" idx="<<idx<<endl;
        /* number of letters in group */
        int cnt=f[i];
        for(int j=i+1; j<N; j++) {
            cnt+=f[j];
            /* calc cost for group [i,j] */
            char med=s[idx+cnt/2]-'a';
            //assert(med>=i && med<=j);
            int cost=0;
            for(int k=i; k<=j; k++)
                cost+=abs(med-k)*f[k];

            dp[i][j]=cost;
        }
        idx+=f[i];
    }

    vvi dp1(N, vi(N, -1));
    /* @return cost of creating kk groups starting at index ii */
    function<int(int,int)> go=[&](int ii, int kk) {
        //cerr<<"ii="<<ii<<" kk="<<kk<<endl;
        if(ii>=N)
            return 0LL;

        if(kk==1) 
            return dp[ii][N-1];

        if(dp1[ii][kk]>=0)
            return dp1[ii][kk];

        int ans=INF;
        for(int jj=ii; jj<N; jj++) 
            ans=min(ans, dp[ii][jj]+go(jj+1, kk-1));

        return dp1[ii][kk]=ans;
    };

    int ans=go(0, k);
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
