
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * There are n*(n+1) subarrays.
 * n   have len==1
 * n-1 have len==2
 * n-2 have len==3
 * ...
 * 1 has len==n
 *
 * Consider position x. From all subarrays on len<=min(x, n-x) the change in value at position x
 * does nothing, since symetric.
 * The other changes makes it bigger or smaller.
 * -> fairly complecated formular :/
 *
 * But we cannot consider all subarrays K times, since N^3.
 * Can we calc the probability foreach element to end in each position?
 * Then from that props find the expected values.
 * But, how to find the props for each position?
 * Consider the number of subarrays an element is part of, subarrays of certain length.
 * ...very complecated again :/
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);
}

signed main() {
    solve();
}
