
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * ex
 *    69   100
 * 10+10 10*10
 * -----------
 *  ->
 *  10*0, 10*X, 49x*X+1
 *
 */
void solve() {
    cini(n);    /* n numbers in seq */
    cini(k);    /* k pairs a[i]^a[j]==x */
    cini(x);

    while(k>0 && n>=0) {
        if(k>=n) {
            int i=1;
            while((i+1)*(i+1)<=k) {
                i++;
            }
            n-=i+i;
            k-=i*i;
            while(k>=i) { /* make 10*11, 10*12, 10*13... */
                n--;
                k-=i;
            }
        } else {    /* if k<n we can add some number m and k-1 times m^x */
            k=0;
            n=0;
        }
    }
    if(n>=0 && k==0) 
        cout<<"YES"<<endl;
    else
        cout<<"NO"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
