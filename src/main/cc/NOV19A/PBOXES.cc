/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int INF=1e9;

/* Something wrong with this, getting WA.
 * Dont get it, algo should work. 
 *
 * Example 4:
 * Basically it says we need to rearange the pairs of the Kth solution.
 * In Ex4 for K=1 two nodes become a pair, and for K=2 this pair
 * has to be split, and two new pairs have to be created.
 * This leads to the question: How to find the optimal pairing of a 
 * set of boxes?
 * Brute-Force is not possible since there are way to much possibilities.
 * (fac(n)/fac(n/2) ???).
 *
 * If it helps we can assume we know which n/2 nodes are the inboxes,
 * and which are the outboxes.
 *
 * We can sort the boxes by pretty.
 * The theoretical optimal solution is to use the n/2 prettiest boxes as outboxes, and the 
 * other half as inboxes.
 * So we need to find for every pretty-half-box a less sized ugly-half-box.
 * The check if this is possible is O(log n), we can simply search for 
 * every pretty-half-box the biggest possible ugly-half-box.
 * So, what in case notfound/notpossible?
 * First of all it means that the optimal solution is not possible, ie we need
 * to move at least one of the pretty boxes to the inboxes, and one vice verca.
 * We could do/check this for every possible pair :/, sorting the pairs by pretty...
 * which is way to slow O(n^2)
 *
 * Graph theory says we need to create a bipartite graph, the both partitions are
 * the inboxes and the outboxes.
 * see https://theory.stanford.edu/~tim/w16/l/l5.pdf
 */

/*
 * Another one, greedy:
 * Think about 'legal' orderings, and swaps which optimize it.
 * a.) A 'legal' ordering is one where all outbox sizes are >= the
 * corresponding inbox sizes.
 * b.) Inboxes and Outboxes are at positions, ordered by their sizes.
 * c.) A swap in<->out is possible if afterwards the ordering is 'legal'.
 * d.) A swap should be done if it improves the cost.
 **/

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    vector<pii> sp(n);
    for(int i=0; i<n; i++) {
        cin>>sp[i].first>>sp[i].second;
    }

    /*
Algo:
We have optimal (at start empty) set of k boxpairs.
for all remaining boxes:
    while(!pairBuild)
        MaxPairOf
            Choose an inbox.
            find prettiest one with bgeq size as the outbox for it.
    endwhile
repeat
    */

    /* compares box sizes, smallest box first. */
    auto scmp =[&] (const int i1, const int i2) {
            if(sp[i1].first==sp[i2].first) {
                if(sp[i1].second==sp[i2].second)
                    return i1<i2;
                else
                    return sp[i1].second<sp[i2].second;
            } else
                return sp[i1].first<sp[i2].first;
        };

    /* compares box prettyness, ugliest box first. */
    auto pcmp =[&] (const int i1, const int i2) {
            if(sp[i1].second==sp[i2].second) {
                if(sp[i1].first==sp[i2].first)
                    return i1<i2;
                else
                    return sp[i1].first<sp[i2].first;
            } else
                return sp[i1].second<sp[i2].second;
        };


/* remaining boxes */
    typedef set<int,decltype(scmp)> boxsetByS;
    typedef set<int,decltype(pcmp)> boxsetByP;

    boxsetByS Sboxes(scmp);
    boxsetByP Pboxes(pcmp);

    for(int i=0; i<n; i++) {
        Sboxes.insert(i);
        Pboxes.insert(i);
    }

    ll ans=0;
    for(size_t k=1; k<=(size_t)n/2; k++) {
#ifdef DEBUG
cout<<"k="<<k<<endl;
#endif
/* brute force try all pairs */
        pii bestPair={-1,-1};
        ll bestAns=-INF;

        for(auto itP=Pboxes.begin(); itP!=Pboxes.end(); itP++) {
            /* find best outbox for that inbox, if one exists. */
            int pretty=-INF;
            int prettyIdx=-1;
            for(auto itS=Sboxes.upper_bound(*itP); itS!=Sboxes.end(); itS++) {
                if(sp[*itS].second>pretty) {
                    pretty=sp[*itS].second;
                    prettyIdx=*itS;
                }
            }
            if(prettyIdx>=0) {
                ll lans=sp[prettyIdx].second-sp[*itP].second;
                if(lans>bestAns) {
                    bestAns=lans;
                    bestPair=make_pair(*itP,prettyIdx);
                }
            }
        }

#ifdef DEBUG
cout<<"bestp, f="<<bestPair.first<<" s="<<bestPair.second<<endl;
#endif
        Sboxes.erase(bestPair.first);
        Sboxes.erase(bestPair.second);
        Pboxes.erase(bestPair.first);
        Pboxes.erase(bestPair.second);
        ans+=bestAns;

        cout<<ans<<endl;
    }

}

