/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void next(vi &num) {
    for(int i=num.size()-1; ; i--) {
        if(num[i]<9) {
            num[i]++;
            for(int j=i+1; j<num.size(); j++)
                num[j]=num[i];
            return;
        }
    }
}

bool check(vi &num) {
    int sum=0;
    int cnt=0;
    for(int i=num.size()-1; i>=0 && num[i]>1; i--) {
        sum+=num[i]*num[i];
        cnt++;
    }
    sum+=num.size()-cnt;

    int ssum=sqrt(sum);
    return (ssum*ssum==sum);
}

string to_string(vi &num) {
    ostringstream oss;
    for(int i : num)
        oss<<i;
    return oss.str();
}

string stupid_bf(int n) {
    vi num;
    for(int i=0; i<n; i++) 
        num.push_back(1);

    while(true) {
        if(check(num))
            return to_string(num);
        next(num);
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--) {
        cini(n);
        string s2=stupid_bf(n);
        cout<<s2<<endl;
    }
}

