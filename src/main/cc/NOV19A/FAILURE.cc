/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cini(m);
    vvi adj(n);
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    /* 1. detect if tree has cycle, if not then -1;
     * 2. Find cycles as list of nodes of that cycle.
     * 3. Find nodes part of all cycles. Such one is
     * point of failure.
     *
     * for some suggestions see
     * https://cp-algorithms.com/graph/finding-cycle.html
     * https://stackoverflow.com/questions/546655/finding-all-cycles-in-a-directed-graph
     */

    vector<int> p(n);
    set<vi> cycles; // set of all cycles

    vb analyzed(n);

    /* Insert the last found cycle into cycles. */
    function<void(int)> trackback=[&](int node) {
#ifdef DEBUG
        cout<<"trackback node="<<node<<endl;
#endif
        int lnode=node;
        vi cycle;
        vb trcvis(n);
        do {
            cycle.push_back(lnode);
            trcvis[lnode]=true;
            lnode=p[lnode];
            if(lnode==-1)
                return;
        } while(!trcvis[lnode]);

        if(lnode!=node)
            return;

        sort(cycle.begin(), cycle.end());
#ifdef DEBUG
        cout<<"cycle= ";
        for(auto c : cycle)
            cout<<c+1<<" ";
        cout<<endl;
#endif
        cycles.insert(cycle);
    };

    vb vis(n);
    int start;

    /* Find all cycles with node start.
    * TODO modify so that all cycles in component
    * are detected. */
    function<void(int,int)> dfs=[&](int node, int parent) {
#ifdef DEBUG
        cout<<"dfs, node="<<node+1<<" parent="<<parent+1<<endl;
#endif
        int oldp=p[node];
        p[node]=parent;

        if(vis[node]) {
            p[node]=parent;
            trackback(node);
        } else {
            vis[node]=true;
            for(int ch : adj[node])
                if(ch!=parent)
                    dfs(ch, node);
            vis[node]=false;
        }

        analyzed[node]=true;
        p[node]=oldp;
    };


    /* Try all nodes, which creates basically all cycles
     * in the graph.
     * Skip all nodes which where part of previous cycles.
     */
    vis.assign(n, false);
    p.assign(n, -1);

    for(int i=0; i<n; i++) {
        //start=i;
        if(!analyzed[i])
            dfs(i,-1);
    }

    vi intersec;    // intersection of nodes in cycles. ie nodes which are in all cycles so far.
#ifdef DEBUG
    cout<<"analyze cycles..."<<endl;
#endif
    bool first=true;
    /* TODO create intersection after each dfs run */
    for(const vi v : cycles) {
        if(first) {
            for(int n : v)
                intersec.push_back(n);
            first=false;
        } else {
            vi res(intersec.size());
            auto it=set_intersection(intersec.begin(), intersec.end(),
                                     v.begin(), v.end(), res.begin());
            res.resize(it-res.begin());
            swap(intersec, res);
        }
    }

    if(intersec.size()>0)
        cout<<intersec[0]+1<<endl;
    else
        cout<<-1<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

