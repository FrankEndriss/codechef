/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    pii start;
    cin>>start.first>>start.second;
    int n,m,k;
    cin>>n>>m>>k;
    vector<vector<pii>> p(3);
    p[0].resize(n);
    p[1].resize(m);
    p[2].resize(k);
    
    for(int i=0; i<n; i++)
        cin>>p[0][i].first>>p[0][i].second;

    for(int i=0; i<m; i++)
        cin>>p[1][i].first>>p[1][i].second;

    for(int i=0; i<k; i++)
        cin>>p[2][i].first>>p[2][i].second;

    vector<pii> nmidx(n+m);
    int nidx=0;
    int midx=0;
    int i=0;
    while(nidx<n || midx<m) {
        if(nidx<n) {
            nmidx[i].first=0;
            nmidx[i].second=nidx++;
            i++;
        }
        if(midx<m) {
            nmidx[i].first=1;
            nmidx[i].second=midx++;
            i++;
        }
    }

    double ans=1e10;

    for(size_t i=0; i<nmidx.size(); i++) {
        double d1=hypot(start.first-p[nmidx[i].first][nmidx[i].second].first,
                        start.second-p[nmidx[i].first][nmidx[i].second].second);

        for(size_t j=0; j<p[!nmidx[i].first].size(); j++) {

            double d2=hypot(p[nmidx[i].first][nmidx[i].second].first- p[!nmidx[i].first][j].first,
                            p[nmidx[i].first][nmidx[i].second].second- p[!nmidx[i].first][j].second);
            if(d1+d2<=ans) {
                for(int l=0; l<k; l++) {
                    double d3=hypot(p[!nmidx[i].first][j].first-p[2][l].first,
                                p[!nmidx[i].first][j].second - p[2][l].second);
                    ans=min(ans, d1+d2+d3);
                }
            }
        }
    }
    cout<<setprecision(12)<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

