/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/** return number of divisors of x */
int cntdiv(int x) {
    int ans=0;
    for(int i=1; i*i<=x; i++) {
        if(x%i==0) {
            ans++;
            int i2=x/i;
            if(i!=i2)
                ans++;
        }
    }
    return ans;
}

/* How to find if a board is winning or loosing position?
 * Def: 
 * freeMove: a move which does not close a stack.
 * freeMoves: number of freeMove in all stacks avail
 * closeMove: a move closing a stack.
 *
 * Number of stacks in (1,2) -> win, since we can close both
 * Number of stacks==3 && freeMoves==0 -> loose, since player must close a stack
 * NoS==3 && freeMoves>0 win, since can reach 3,0
 * ...
 * 1,0; 2,0; 1,1; 0,2 -> win, since we can close all
 * 3,0 -> loose, since only win state reachable
 * 2,1 -> win, since loose reachable
 * 2,2 -> win, since 3,0 reachable
 * 3,1 -> win, since 3,0 reachable
 * 3,2 -> win, since 3,0 reachable
 * 4,0 -> win, since 3,0 reachable
 * 4,1 -> win, ...3.0
 * 4,2 -> loose if not odd
 * 5,0 -> win, since 3,0 reachable
 * 6,0 -> loose, since 5,0;4,0 are win
 * 5,1 -> win, since 6,0 reachable
 * 5,2 -> win, since 6,0 reachable
 * 5,3 -> 
 * ...
 *
 * 4,0-> win
 * 4,x>0 -> 
 *
 */
struct board {
    int n0; // number of stacks which can only be closed
    int n1; // number of stacks with one move before close
    int nX; // number of stacks with more than one move before close
};
// 3,0,0 is the first and ultimate loosing position
// and 6,0,0, and 9,0,0...


/* A board has a min number of moves to be finshed, and a max number.
 * min number is number of stacks, max number is sum of all piles.
 * Case minNumber <=2 is win
 * Case minNumber>2 && maxNumber<=4 is loose
 * ... complicated shit :/
 */

/* A (1) move can:
 * 1: -1,  0,  0 
 * 2:  0, -1,  0
 * 3: +1, -1,  0
 * 4:  0,  0, -1 
 * 5:  0, +1, -1 
 * 6: +1,  0, -1 
 * A (2) move can all combinations of above.
 * 1+1: -2,  0,  0
 * 1+2: -1, -1,  0
 * 1+3:  0, -1,  0
 * 1+4: -1,  0, -1
 * 1+5: -1, +1, -1
 * 1+6:  0,  0, -1
 * 2+2:  0, -2,  0
 * 2+3: +1, -2,  0
 * 2+4:  0, -1, -1
 * 2+5:  0,  0, -1
 * 2+6: +1, -1, -1
 * 3+3: +2, -2,  0
 * 3+4: +1, -1, -1
 * 3+5: +1,  0, -1
 * 3+6: +2, -1, -1
 * 4+4:  0,  0, -2
 * 4+5:  0, +1, -2
 * 4+6: +1,  0, -2
 * 5+5:  0, +2, -2
 * 5+6: +1, +1, -2
 * 6+6: +2,  0, -2
 */

void solve() {
    cini(n);
    cini(k);
    vi h(n);
    for(int i=0; i<n; i++) {
        cini(x);
        h[i]=cntdiv(x);
    }

/* k people can set thresholds in pow(n,k) ways. */
}

prints a win/loos table
void printwl(int n) {
    vvi m(n, vi(n));

    m[0][0]=0   // lost per definition
    m[1][0]=1;  // win
    m[0][1]=1;
    m[1][1]=1;
    m[2][0]=1;
    m[0][2]=1;

/* if at least one loosing pos is reachable, its a win-position. Else a loose pos. */
    for(int i=3; i<n; i++) {
        for(int j=0; j<=i; j++) {
            m[i][j]=...

            if(i!=j) {
                m[j][i]=...
            }
        }
    } 
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

