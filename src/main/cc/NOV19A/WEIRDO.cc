/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

bool vowels[256];
void init() {
    for(int i=0; i<256; i++)
        vowels[i]=false;
    vowels['a']=true;
    vowels['e']=true;
    vowels['i']=true;
    vowels['o']=true;
    vowels['u']=true;
}

#define isVowel(c) (vowels[(int)(c)])

void solve() {
    cini(l);

    vb risAli(l); // risAli[i]==true i is Alice receipe

    vvi xAB(2, vi(256));    // xAB[i][j] == number of receipes of person i containing letter j
    vvi fxAB(2, vi(256));   // fxAB[i][j] == number of occurences of letter j in receipes of person i
    vi k(2); // k[i] == number of receipes of person i

    for(int i=0; i<l; i++) {
        vi f(256);  // f[c] == freq of letter c
        cins(s);

        int c1=0, c2=0, c3=0;

        bool ownerBob=false;
        for(char c : s) {
            c3=c2;
            c2=c1;
            c1=!isVowel(c);
            if(c1+c2+c3>1)
                ownerBob=true;

            f[c]++;
        }

        k[ownerBob]++;
#ifdef DEBUG
        cout<<"s="<<s<<" owner="<<ownerBob<<endl;
#endif

        for(int c='a'; c<='z'; c++) {
            if(f[c]) {
                xAB[ownerBob][c]++;
                fxAB[ownerBob][c]+=f[c];
            }
        }
    }

    vector<ll> mult;
    vector<ll> divs;

    for(int c='a'; c<='z'; c++) {
        for(int i=0; i<2; i++) {
            if(xAB[i][c]>0) {
#ifdef DEBUG
                cout<<"i="<<i<<" c="<<c<<" k[i]="<<k[i]<<" xAB[i][c]="<<xAB[i][c]<<" fxAB[i][c]="<<fxAB[i][c]<<endl;
#endif
                if(i==0) {
                    mult.push_back(xAB[i][c]);
                    for(int j=0; j<k[i]; j++)
                        divs.push_back(fxAB[i][c]);
                } else {
                    for(int j=0; j<k[i]; j++)
                        mult.push_back(fxAB[i][c]);
                    divs.push_back(xAB[i][c]);
                }
            }
        }
    }

    sort(mult.begin(), mult.end());
    sort(divs.begin(), divs.end());

    double ans=1.0;
    int midx=0;
    int didx=0;
    while(midx<mult.size() || didx<divs.size()) {
        if(didx==divs.size())
            ans*=mult[midx++];
        else if(midx==mult.size())
            ans/=divs[didx++];
        else if(ans<=1.0)
            ans*=mult[midx++];
        else
            ans/=divs[didx++];
    }

    if(abs(ans)>=10000000.0)
        cout<<"Infinity"<<endl;
    else
        cout<<setprecision(12)<<ans<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    init();
    cini(t);
    while(t--)
        solve();
}

