/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* @return the color c1 and c2 not have. */
char notC(char c1, char c2) {
    for(char c : { 'R', 'G', 'B' } )
        if(c1!=c && c2!=c)
            return c;
    assert(false);
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);    // number of points (2^9)
    cini(m);    // number of triangles (2^10)
    cini(colorCost);    // cost of color change
    cini(flipCost);    // cost of flip
    cini(p);    // most color changes available
    cini(q);    // most flips available
    cins(colors);   // initial colors of points
    colors=" "+colors;

    vi x(n), y(n);      // coordinates of points
    for(int i=0; i<n; i++) 
        cin>>x[i]>>y[i];

    vvi abc(m, vi(3));
    vi pfreq(n);
    for(int i=0; i<m; i++)  {
        for(int j=0; j<3; j++) {
            cin>>abc[i][j];
            abc[i][j]--;
            pfreq[abc[i][j]]++;
        }
        sort(abc[i].begin(), abc[i].end());
    }
    sort(abc.begin(), abc.end());

/* Strategy1
 * Calc number of ugly triangels.
 * for all utri try to optimize by color change.
 * for all utri try to optimize by flip.
 * for all utri try to optimize by 2x/3x color change.
 * for all utri try to optimize by flip and 2x/3x color change.
 */

    function<bool(int,int,int,int)> canFlip=[&](int p1, int p2, int p3, int p4) {
        vi lp={ p1, p2, p3, p4, p1, p2, p3, p4 };
        int zcp=0;
        for(int i=0; i<4; i++) {
            int dx1 = x[lp[i+1]]-x[lp[i]];
            int dy1 = y[lp[i+1]]-y[lp[i]];
            int dx2 = x[lp[i+2]]-x[lp[i+1]];
            int dy2 = y[lp[i+2]]-y[lp[i+1]];
            
            int zcrossproduct = dx1*dy2 - dy1*dx2;
            if(zcp==0)
                zcp=zcrossproduct>0;
            else if(zcp*zcrossproduct<0)
                    return false;
        }
        return true;
    };

/* dont know in which case a flip makes sense :*/
/* ok, one case it this:
 * There exist 3 triangels which have a common point in the center of themselfes.
 * This is, if there exists a point as a member of 3 triangles which consist
 * of only 4 points at all.
 * Such a construct should be broken up by flip, because it allways contains at 
 * least one ugly triangle.
 */

    /* collects the points which are connected to p. returns in ret */
    function<void(int, set<int>&)> findConnectedPoints=[&](int pp, set<int> &ret) {
        for(int i=0; i<m; i++) {
            for(int j=0; j<3; j++) {
                if(abc[i][j]==pp) {
                    ret.insert(abc[i][(j+1)%3]);
                    ret.insert(abc[i][(j+2)%3]);
                }
            }
        }
    };

    /* @return true if there exist a triangle with edge p1,p2. */
    function<bool(int,int)> isConnected=[&](int p1, int p2) {
        if(p1>p2)   
            swap(p1,p2);
        for(int i=0; i<m; i++) {
            if(abc[i][0]==p1) {
                if(abc[i][1]==p2 || abc[i][2]==p2)
                    return true;
            } else if(abc[i][1]==p1 && abc[i][2]==p2)
                return true;
        }
        return false;
    };

/* returns the point connected to p1 and p2, which is not notP.
 * if notfound return -1 */
    function<int(int,int,int)> findFourthPoint=[&](int p1, int p2, int notP) {
        for(int i=0; i<m; i++) {
            int cnt=0;
            for(int j=0; j<3; j++) {
                if(abc[i][j]==p1 || abc[i][j]==p2)
                    cnt++;
            }
            if(cnt==2) {
                for(int j=0; j<3; j++) {
                    if(abc[i][j]!=p1 && abc[i][j]!=p2 && abc[i][j]!=notP)
                        return abc[i][j];
                }
            }
        }
        return -1;
    };

/* Flips the edge p1,p2 to be i,fp1.
 * Updates arrays. */
    function<void(int,int,int,int)> doFlip=[&](int i, int p1, int fp1, int p2) {
        vi tri1={ i, p1, p2 };
        sort(tri1.begin(), tri1.end());
        vi tri2={ p1, p2, fp1 };
        sort(tri2.begin(), tri2.end());
#ifdef DEBUG
cout<<"doFlip, tri1="<<tri1[0]<<" "<<tri1[1]<<" "<<tri1[2]<<endl;
cout<<"doFlip, tri2="<<tri2[0]<<" "<<tri2[1]<<" "<<tri2[2]<<endl;
#endif

        int cnt=0;
        for(int j=0; j<m && cnt<2; j++) {
            if(abc[j]==tri1) {
#ifdef DEBUG
cout<<"doFlip, found tri1, j="<<j<<endl;
#endif
                for(int k=0; k<3; k++)
                    pfreq[abc[j][k]]--;
                abc[j][0]=i;
                abc[j][1]=p1;
                abc[j][2]=fp1;
                for(int k=0; k<3; k++)
                    pfreq[abc[j][k]]++;
                sort(abc[j].begin(), abc[j].end());
                cnt++;
            } else if(abc[j]==tri2) {
#ifdef DEBUG
cout<<"doFlip, found tri2, j="<<j<<endl;
#endif
                for(int k=0; k<3; k++)
                    pfreq[abc[j][k]]--;
                abc[j][0]=i;
                abc[j][1]=p2;
                abc[j][2]=fp1;
                for(int k=0; k<3; k++)
                    pfreq[abc[j][k]]++;
                sort(abc[j].begin(), abc[j].end());
                cnt++;
            }
        }
        assert(cnt==2);
    };

    int i=0;
    vector<pii> flips;
    int loopc=0;
/* DEBUG */
    q=min(q,4);
/* END DEBUG */
    while(loopc<10000 && i<n && (int)flips.size()<q) {
#ifdef DEBUG
cout<<"flipcheck i="<<i<<endl;
#endif
        /* check only the points with exactly 3 connections */
        if(pfreq[i]!=3) {
#ifdef DEBUG
cout<<"flipcheck pfreq[i]="<<pfreq[i]<<endl;
#endif
            i++;
            continue;
        }
        loopc++;

        /* find the three connected points to point i */
        set<int> conpoints;
        findConnectedPoints(i, conpoints);
        if(conpoints.size()!=3) {
            i++;
#ifdef DEBUG
cout<<"flipcheck conpoints.size()="<<conpoints.size()<<endl;
#endif
            continue;
        }

        /* check if these tree points are connected in a circle. */
        auto it=conpoints.begin();
        int p1=*it;
        it++;
        int p2=*it;
        it++;
        int p3=*it;
        // if yes, we want to flip one of these circle connections.
        if(isConnected(p1, p2) && isConnected(p1,p3) && isConnected(p2,p3)) {
            /* that for, we need to find and check the triangle. */
            int fp1=findFourthPoint(p1, p2, i);
            if(fp1<0) {
                fp1=findFourthPoint(p1, p3, i);
                if(fp1>=0)
                    p2=p3;
            }
            if(fp1<0) {
                fp1=findFourthPoint(p2, p3, i);
                if(fp1>=0) {
                    p1=p2;
                    p2=p3;
                }
            }
#ifdef DEBUG
if(fp1<0)
    cout<<"no fourth point"<<endl;
else
    cout<<"fourth point="<<fp1<<endl;
#endif
            if(fp1>=0 && canFlip(i, p1, fp1, p2)) {
#ifdef DEBUG
cout<<"can flip, so will doFlip"<<endl;
#endif
                doFlip(i, p1, fp1, p2);
                flips.push_back({p1,p2});
                i=min(i+1, min(p1, min(fp1, p2)));
            } else {
#ifdef DEBUG
cout<<"cant flip"<<endl;
#endif
                i++;
            }
        } else {
#ifdef DEBUG
cout<<"flipcheck no circle connection"<<endl;
#endif
            i++;
        }
    }
    

    /* Check triangles for color change */
    int cdone=0;
    for(i=0; i<m && cdone<p; i++) {
        if(colors[abc[i][0]]==colors[abc[i][1]]) {
            colors[abc[i][1]]=notC(colors[abc[i][0]], colors[abc[i][2]]);
            cdone++;
        }
        if(cdone<p && colors[abc[i][0]]==colors[abc[i][2]]) {
            colors[abc[i][2]]=notC(colors[abc[i][1]], colors[abc[i][0]]);
            cdone++;
        }
    }

    cout<<cdone<<" "<<flips.size()<<endl;
    cout<<colors.substr(1)<<endl;
    for(auto fp : flips) 
        cout<<fp.first+1<<" "<<fp.second+1<<endl;
    

}

