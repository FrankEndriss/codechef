/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    vvi q(n);
    priority_queue<pii> lq;
    priority_queue<pii> rq;
    for(int i=0; i<n; i++) {
        cini(k);
        for(int j=0; j<k; j++) {
            cini(a);
            q[i].push_back(a);
        }
        lq.push({q[i].front(), i});
        rq.push({q[i].back(), i});
    }

/* greedy
 * Simply take the biggest coin ever. */
    
    int ans=0;
    bool chefIs=true;
    while(true) {
        if(chefIs) {
            int next=-1;
            while(lq.size()>0 && next<0) {
                next=lq.top().second;
                lq.pop();
                if(q[next].size()==0)
                    next=-1;
            }

            if(next==-1)
                break;

            ans+=q[next][0];
            q[next].erase(q[next].begin());
            if(q[next].size()>0)
                lq.push({q[next].front(), next});
            chefIs=false;
        } else {
            int next=-1;
            while(rq.size()>0 && next<0) {
                next=rq.top().second;
                rq.pop();
                if(q[next].size()==0)
                    next=-1;
            }

            if(next==-1)
                break;

            q[next].pop_back();
            if(q[next].size()>0)
                rq.push({q[next].back(), next});
            chefIs=true;
        }
    }
    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

