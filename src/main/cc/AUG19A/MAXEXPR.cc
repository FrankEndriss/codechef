/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>ll;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinad(a, n) vd a(n); fori(n) { cin>>a[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;
typedef vector<double> vd;

void examples() {
/* assume sum=-10, so we can increase by 10.
 * k[0]=1, k[1]=4
 * Result: d[0]=8; d[1]=0.5;
 * Das bedeutet:
 * d[0]/d[1] == (k[1]*k[1]) / (k[0]*k[0])
 * d[0]*k[0] + d[1]*k[1] == 10
 *
 * k[0]=2, k[1]=4
 * Result: 4; 0.5;
 *
 * k[0]=3, k[1]=4
 * Result: ~1,9; ~1.075;
 *
 * So, x[0] between 0 and 10, x[1] between 0 and 2.5
 */
    for(double x0=1.5; x0<2.3; x0+=.01) {
        double x1=(10-(x0*3))/4;
        double f=sqrt(x0)+sqrt(x1);
        cout<<"f="<<f<<" x0="<<x0<<" x1="<<x1<<endl;
    }
    exit(0);
}

void solve() {
    cini(n);
    cinad(k, n);    
    cinad(c, n);
    vd x(n);
    double sum=0;
    double sumk2=0;
    vd k2(n);
    double maxk2=0;
    for(int i=0; i<n; i++)  {
        k2[i]=k[i]*k[i];
        maxk2=max(maxk2, k2[i]);
        sumk2+=k2[i];

        x[i]=-c[i];     // min possible value for every x[i]
        sum+=x[i]*k[i]; // sum in equation 1
    }
    /* if sum<0 we can increase every x[i] by some value. Else we cannot. */
    if(sum>0) {
        cout<<"-1"<<endl;
        return;
    }

    vd d(n);
    double sumd=0;
    for(int i=0; i<n; i++) {
        d[i]=maxk2/k2[i];
        sumd+=d[i]*k[i];
    }
    for(int i=0; i<n; i++) {
        d[i]*=sum/sumd;
    }

    double f=0;
    double ctrl=0;
    for(int i=0; i<n; i++) {
        //x[i]-=sum*(maxk2/k2[i])/sumk2;
        x[i]-=d[i];
        //x[i]-=sum/n;
        f+=sqrt(x[i]+c[i]);
        ctrl+=x[i]*k[i];
    }
    cout<<f;
    fori(n)
        cout<<" "<<x[i];
//cout<<" dbg:"<<ctrl;
    cout<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
//examples();
    while(t--)
        solve();
}

