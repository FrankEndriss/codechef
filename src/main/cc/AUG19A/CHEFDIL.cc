/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>ll;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }

void solve() {
    cins(s);    // up=='1'

    /* Start with 000..
     * We need to flip them, so we need a 1 somewhere.
     * If num 1s is odd, all following 0s are flipped one after another, so no then followin 1 is needed.
     * If num 1s is even, then one 0  will be left, so after the next 0s a one is needed.
     */
    int idx=0;
    while(true) {
        bool needOne=false;
        while(idx<s.size() && s[idx]=='0') {
            idx++;
            needOne=true;
        }

        if(idx==s.size()) {
            if(needOne)
                cout<<"LOSE"<<endl;
            else
                cout<<"WIN"<<endl;
            return;
        }

        idx++;
        if(idx==s.size()) {
            cout<<"WIN"<<endl;
            break;
        }

        if(s[idx]=='1')
            s[idx]='0';
        else
            s[idx]='1';
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

