/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>ll;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int pl(const int v1, const int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

const int N=100007;
vi dsub10(N);
vi pow10(N);    // pow10[i]= pow(10, i)

void init() {
    pow10[0]=1;
    pow10[1]=10;
    pow10[2]=100;

    dsub10[2]=45;   // 1+2+3+...+9 for 11,22,33,...,99

    for(int i=3; i<N; i++) {
        pow10[i]=mul(pow10[i-1], 10);
        dsub10[i]=mul(110, dsub10[i-1]);
    }
}

/* @return sum of numbers 1 to x*pow(10, p) */
int gaussp(int x, int p) {
    if(p==1)
        return (x*(x+1))/2;

    return pl(mul(pow10[p-1], mul(pow10[p], mul(5, mul(x, x)))),
            mul(x, mul(5, pow10[p-1])));
}

/* @return sum of subtractions in numbers 0 to x*pow(10, p)
 */
int dsub(int x, int p) {
// TODO
 return 42;
}

/* @return f(10^^p) */
void fp(int p) {
}

void solve() {
    cini(nl);
    cins(sl);
    cini(nr);
    cins(sr);

    // TODO
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    init();
    cini(t);
    while(t--)
        solve();
}

