/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>ll;
#define cinas(a, n) vs a(n); fori(n) { cin>>a[i]; }
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cinai(a, n);
    vi dp1(n+1);
    fori(n) {
        dp1[i+1]=dp1[i]^a[i];           /* xor from left */
    }

    /* Brute force */
    int ans=0;
    for(int i=0; i<n-1; i++) {
        for(int j=i+1; j<n; j++) {
            for(int k=j; k<n; k++) {
#ifdef DEBUG
                cout<<"dbg dp1[i]^dp1[j]="<<(dp1[i]^dp1[j])<<endl;
#endif
                if((dp1[i]^dp1[j]) == (dp1[j]^dp1[k+1])) {
#ifdef DEBUG
                    cout<<"dbg i="<<i<<" j="<<j<<" k="<<k<<endl;
#endif
                    ans++;
                }
            }
        }
    }

/* For better solution we somehow need to count the xor-sums of some
 * ranges, and from there find the matching values.
 *
 * We need to somehow find the number of ranges for a given xor-sum.
 * The number of possible xor-sums is limited to 2^^20. (log2(1e6))
 */

    cout<<ans<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}














