/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* greedy, simulate
 * both always work in the mine they can get gold fastest.
 * have a sorted list for each one of both.
 * two pointer, jump from end to end of mining a mine.
 * use priority_queue for events.
 * -> no use for priority_queue, since max two mines in work.
 *
 *  How can we implement this without getting complte bored 
 *  by stupid casework?
 *
 *  Work with events.
 *  There is one event: A mine is finished, then
 *  following can happen:
 *  both where in that mine, the get some gold, and start
 *  working in the next mine.
 *  One is in a mine, gets gold and starts working in 
 *  next mine. Next mine can be the same as others mine, 
 *  or a new one.
 */
void solve() {
    cini(n);
    vd g(n);
    vd a(n);
    vd b(n);
    for(int i=0; i<n; i++)
        cin>>g[i]>>a[i]>>b[i];

    vi aa(n);
    iota(all(aa), 0);
    sort(all(aa), [](int i1, int i2) {
        return g[i1]/a[i1]<g[i2]/a[i2];
    });

    vi bb(n);
    iota(all(bb), 0);
    sort(all(bb), [](int i1, int i2) {
        return g[i1]/b[i1]<g[i2]/b[i2];
    });

    vb vis(n);  /* vis[i]==true -> mine i is finished */
    int idxA=0; /* current mine of a is aa[aidx] */
    int idxB=0;

    double tA=0;    /* time index a started work in current mine */
    double tB=0;

    double sumA=0;  /* gold earned */
    double sumB=0;

    /* three cases
     * both in same mine, both fini same time
     * a fini first
     * b fini first
     */
    int fini=0;  /* number of finished mines */
    
    while(fini<n) {
            int mineA=aa[idxA];
            int mineB=bb[idxB];

            if(mineA==mineB) {
                double g1=0;
                if(tA<tB) {
                    g1=(g[mineA]*(tB-tA))/a[mineA];
                    sumA+=g1;
                } else if(tB<tA) {
                    g1=(g[mineB]*(tA-tB))/b[mineA];
                    sumB+=g1;
                }
                double g2=(g[mineA]-g1)*a[mineA]/b[mineB];
                double g3=(g[mineA]-g1)-g2;
                sumA+=g2;
                sumB+=g3;
                fini++;
                vis[mineA]=true;
                tA=tB=0;
            } else if(tA+a[mineA]<tB+b[mineB]) {
                sumA+=g[mineA];
                tA+=a[mineA];
                fini++;
                vis[mineA]=true;
                idxA++;
                if(idxA==n)
                    continue;
                mineA=aa[idxA];
                if(mineA==mineB) {
                    sumB+=g[mineB]*
                }
            }

    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
