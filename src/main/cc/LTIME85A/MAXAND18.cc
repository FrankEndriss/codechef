/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* we need to count the number of numbers having 
 * bit i set.
 * We calc possible contribution of each set bit.
 * choose the k max ones.
 */
void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    vi cnt(30);
    for(int i=0; i<n; i++) {
        for(int j=0; j<30; j++) 
            if(a[i]&(1LL<<j))
                cnt[j]++;
    }

    vector<pii> con;
    for(int i=0; i<30; i++) {
        int b=1LL<<i;
        con.push_back({cnt[i]*b, -i});
    }

    sort(all(con), greater<pii>());
    int ans=0;
    for(int i=0; i<k; i++)
        ans|=(1LL<<-con[i].second);
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
