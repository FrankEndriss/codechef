/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

/* find submat with sum X
 * of any y*y
 *
 * search per size n.
 * properties of submat:
 * asum(x1,x2)*n + asum(y1,y2)*n
 * asum(x1,x2) + asum(y1,y2) == x/n
 * But we need to check only for those n 
 * which are divisors of x.
 */
const int N=1e6+7;
void solve() {
    cini(n);
    cini(x);
    cinai(a,n);

    vi d;
    for(int i=1; i*i<=x; i++) {
        if(x%i==0) {
            d.push_back(i);
            if(i*i!=x)
                d.push_back(x/i);
        }
    }

    int ans=0;
    for(size_t i=0; i<d.size(); i++) {
        if(d[i]>n)
            continue;

        int z=x/d[i];

        vi f(N);
        vi ff;
        int sum=0;
        for(int j=0; j<d[i]; j++) 
            sum+=a[j];

        if(sum<=z) {
            f[sum]++;
            ff.push_back(sum);
        }

        for(int j=d[i]; j<n; j++) {
            sum+=a[j];
            sum-=a[j-d[i]];
            if(sum<=z) {
                f[sum]++;
                ff.push_back(sum);
            }
        }

        int cnt=0;
        for(int val : ff) {
            int search=x/d[i]-val;
            if(search>=0)
                cnt+=f[search];
        }
        ans+=cnt;
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
