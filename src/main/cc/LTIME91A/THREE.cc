
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to choose 
 * - 1 and 2,
 * - or 3
 * Greedy
 * first choose the 1 and one from the biggest.
 */
void solve() {
    cins(s);
    
    map<char,int> m;
    for(char c : s)
        m[c]++;

    set<pair<int,char>> a;
    for(auto ent : m)
        a.insert({ent.second, ent.first});

    int ans=0;
    while(a.size()) {
        pii p1=*a.begin();
        //cerr<<"p1="<<p1.first<<" "<<(char)p1.second<<endl;
        a.erase(a.begin());
        if(a.size()) {
            auto it=a.end();
            it--;
            pii p2=*it;
            //cerr<<"p2="<<p2.first<<" "<<(char)p2.second<<endl;
            a.erase(it);
            if(p2.first<2)
                break;

            p2.first-=2;
            p1.first--;
            if(p2.first)
                a.insert(p2);
            if(p1.first)
                a.insert(p1);
            ans++;

        } else {
            if(p1.first>=3) {
                ans++;
                p1.first-=3;
                if(p1.first)
                    a.insert({p1.first, p1.second});
            }
        }
    }

    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
