
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We can only swap if left of a pair is '1'
 * So we can move 1s only from left to right,
 * and 0 from right to left.
 *
 * Greedy.
 * So we go from left to right.
 * If s[i]==1 and t[i]==0 we need to move the 1,
 * and we move the smallest possible dist.
 */
void solve() {
    cini(n);
    cins(s);
    cins(t);

    int idx=0;
    for(int i=0; i<n; i++) {
        if(s[i]!=t[i]) {    // need to swap 
            if(s[i]=='0') {
                break;
            }

            /* now s[i]=='1' but needs to be '0', 
             * and we need to find the next '0' to to the right to swap it. */
            idx=max(idx, i);

            while(idx<n && s[idx]=='1')
                idx++;

            if(idx==n) {
                break;
            }
            swap(s[i], s[idx]);
        }
    }

    if(s==t)
        cout<<"Yes"<<endl;
    else
        cout<<"No"<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
