
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * even subseq: longest cont len is even
 * maximal ss : the max len such subseq in a string
 *
 * So we need to make s containing the max seq of eq
 * chars beeing of even len. Ths is 0 or 1 operation.
 * ...No, see examples.
 *
 * Assume we need to make the string containing _only_
 * groups of same chars of even size.
 *
 * dp somehow?
 * dp[i][j][k]=min operations if starting at i with having symbol j for k times just 
 * before it. (k bool)
 * So we can remove each symbol or make the number even.
 */
void solve() {
    cini(n);
    cinai(a,n);

    map<pair<pii,int>,int> memo;

    function<int(int,int,int)> go=[&](int i, int j, int k) {
        if(i==n)
            return k;   /* we need to remove/add one in the end if k==odd */

        auto it=memo.find({{i,j},k});
        if(it!=memo.end())
            return it->second;

        // Cases a[i]==j or a[i]!=j

        int ans;
        if(a[i]==j) { /* do nothing and go on or insert/remove 1 */
            ans=min(go(i+1, j, !k), 1+go(i+1, j, k));
        } else { /* go on or remove current char */
            ans=min(k+go(i+1, a[i], 1), 1+go(i+1, j, k));
        }
        return memo[{{i,j},k}]=ans;
    };


    int ans=go(0, -1, 0);
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
