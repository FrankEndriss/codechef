
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/*
 * We go groups of connected vertex
 * with no restaurant between them.
 * Foreach such group only one friend
 * can get house because else those two friends could not meet.
 *
 * After having the groups, how to find a matching with the frieds?
 * There must be one group foreach value 1..k
 * Greedyly choose the values with smallest freq?
 * Or Edmonds-Carp maxflow network?
 * No, N=1e5
 *
 * But K is small. How to utilize this?
 */
const int MOD=998244353;
const int N=1e5+7;
void solve() {
    cini(n);
    cini(k);
    cinai(c,n);

    vvi adj(n);
    for(int i=0; i<n-1; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    vb vis(n);
    set<int> lans;

    function<void(int)> dfs=[&](int v) {
        vis[v]=true;
        lans.insert(c[v]);
        for(int chl : adj[v]) {
            if(!vis[chl] && c[chl]!=0)
                dfs(chl);
        }
    };

    vector<set<int>> grp;
    for(int i=0; i<n; i++) {
        if(c[i]!=0 && !vis[i]) {
            lans.clear();
            dfs(i);
            grp.push_back(lans);
        }
    }

    /* Now we need to find the number of matchings. 
     * dp somehow
     *
     *
     * Brute force:
     * put friends in all possible groups, calc for remaining friends
     * and remaining groups...
     */

    vb used(N);
    function<int(int)> go=[&](int col) {
        if(col==k+1)
            return 1LL;

        int ans=0;
        for(int i=0; i<grp.size(); i++) {
            if(used[i])
                continue;

            if(grp[i].count(col)) {
                used[i]=1;
                ans=(ans+go(col+1))%MOD;
                used[i]=0;
            }
        }
        return ans;
    };

    int ans=go(1);
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
