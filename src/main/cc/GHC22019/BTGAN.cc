/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int sym[256];

void solve() {
    string s;
    ll M;
    cin>>s>>M;
    reverse(s.begin(), s.end());

    for(int i='0'; i<='9'; i++)
        sym[i]=i-'0';
    for(int i='A'; i<='Z'; i++)
        sym[i]=i-'A'+10;

    int ms=-1;
    for(int c : s)
        ms=max(ms, sym[c]);
    ms++;

    int ans=0;
    // calc even bases and odd bases
    const ll bevencount=(M+(M%2==0?1:0)-ms+1)/2;
    const ll bodd_count=(M+(M%2==1?1:0)-ms+1)/2;
//cout<<"evencount="<<bevencount<<" oddcount="<<bodd_count<<endl;

    // if base is even, only first digit may be odd. And that matters only if there is 
    // an odd number of even bases.
    if(bevencount%2==1)
        ans^=sym[(int)(s[0])]%2;
    if(bodd_count%2==1)
        for(uint i=0; i<s.length(); i++)
            ans^=sym[(int)(s[i])]%2;

    cout<<ans;

}
int main() {
    int t;
    cin>>t;
    for(int i=0; i<t; i++)
        solve();
    cout<<endl;
}

