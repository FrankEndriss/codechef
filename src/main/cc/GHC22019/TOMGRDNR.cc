
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;


int main() {
int n;
int a[100001];
    cin>>n;
    set<int> common;
    for(int i=0; i<n; i++) {
        cin>>a[i];
        common.insert(a[i]);
    }

    vector<int> ans;
    int mgap=1000007;
    for(int c : common) {
        // find biggest gap between two c
        int gap=1;
        int lgap=1;
        for(int i=0; i<n; i++) {
            if(c==a[i])
                lgap=1;
            else
                lgap++;
            gap=max(gap, lgap);
        }
        if(gap<mgap) {
            ans.clear();
            ans.push_back(c);
            mgap=gap;
        } else if(gap==mgap) {
            ans.push_back(c);
        }
    }

    cout<<mgap<<" "<<*min_element(ans.begin(), ans.end())<<endl;
    return 0;
}

