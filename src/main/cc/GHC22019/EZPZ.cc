/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;


int dist(int n, int a, int b) {
    return min(abs(a-b), n-max(a, b)+min(a, b));
}

void solve() {
int n, a, b;
    cin>>n>>a>>b;

    int ans=-1;
    int mDist=10000007;
    

    for(int i=1; i<=n; i++) {
        if(i!=a && i!=b) {
            int ldist=dist(n, a, i)+dist(n, b, i);
            if(ldist<mDist) {
                ans=i;
                mDist=ldist;
            }
        }
    }

    int jA=dist(n, a, ans);
    int jB=dist(n, b, ans);
    cout<<ans<<" "<<jA+jB<<endl;

}

int main() {
int t;
    cin>>t;
    for(int i=0; i<t; i++)
        solve();
}

