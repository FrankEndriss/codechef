
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

int a[1001];
int b[2001];

//#define DEBUG

int main() {
    uint n;
    cin>>n;
    for(uint i=0; i<n; i++) {
        cin>>a[i];
        b[i]=b[i+n]=a[i];
    }

    vector<int> v(b, b+n+n-1);
    unordered_set<ll> subs;

    for(uint i=0; i<v.size(); i++) {
        ll h1=13;
        for(uint j=i; j<v.size(); j++) {
            if(j-i<n) {
                h1+=v[j];
                h1*=13;
                subs.insert(h1);
            } else 
                break;
        }
    }
    reverse(v.begin(), v.end());
    for(uint i=0; i<v.size(); i++) {
        ll h1=13;
        for(uint j=i; j<v.size(); j++) {
            if(j-i<n) {
                h1+=v[j];
                h1*=13;
                subs.insert(h1);
            } else 
                break;
        }
    }

#ifdef DEBUG
    for(auto v : subs) {
        cout<<"(";
        for(auto i : v)
            cout<<i<<" ";
        cout<<") ";
    }
    cout<<endl;
#endif

    cout<<subs.size()<<endl;

}

