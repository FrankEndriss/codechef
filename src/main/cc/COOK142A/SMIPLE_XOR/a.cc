
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * 0100
 * 0101
 * 0110
 * 0111
 * 1000
 * 1001
 *
 * 1010
 * 1011
 *
 * Consider MSB, if it must be in even number
 * of the four numbers.
 *
 * 001
 * 010
 * 011
 * 100
 *
 * But there are some edgecases... :/
 */
void solve() {
    cini(l);
    cini(r);

    int msb=1;
    while((msb*2)<=r)
        msb*=2;

    if(msb+3<=r) {
        for(int i=msb; i<msb+4; i++) 
            cout<<i<<" ";
        cout<<endl;
        return;
    }

    if(l+3<msb) {
        for(int i=l; i<l+4; i++) 
            cout<<i<<" ";
        cout<<endl;
        return;
    }

    if(msb-2>=l && msb+1<=r) {
        for(int i=msb-2; i<=msb+1; i++) 
            cout<<i<<" ";
        cout<<endl;
        return;
    }

    cout<<-1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
