
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider any bit existing in more than one number,
 * split between these two positions.
 *
 * ...no, is the _sum_ of both sides, so we need to have
 * an odd number...no
 *
 * Check the first bit. If the freq can be split into two
 * odd numbers, choose that.
 * So, consider the sum of sum number of low bits.
 *
 * But, what for example
 * 3
 * 1 1 1
 * We need to split in three arrays here.
 * Consider the prefix sums
 * 01 10 11, bitwise it is
 * 101
 * 011
 *
 * We consider the number of changes.
 * -if the freq of changes is >0 and even, 
 *  we can split 1 on the left, and two odd numbers on
 *  the right.
 * -if the freq of changes is odd
 *  we can split to two odd numbers
 */
const int N=60;
void solve() {
    cini(n);
    cinai(a,n);

    for(int i=1; i<n; i++) 
        a[i]+=a[i-1];   /* prefix sum */
        
    vvi b(n, vi(N));   /* set bits in prefix sum */
    for(int i=0; i<n; i++)
        for(int j=0; j<N; j++) 
            b[i][j]=(a[i]>>j)&1;

    vb first(N);
    vi cnt(N);
    for(int i=0; i<n; i++) {
        for(int j=0; j<N; j++)  {
            if(i>0 && first[j])
                cnt[j]+=(b[i][j]!=b[i-1][j]);

            if(b[i][j])
                first[j]=true;
        }
    }

    for(int j=0; j<N; j++) {
        if(cnt[j]>=1) {
            cout<<"YES"<<endl;
            if(cnt[j]%2==0) {
                cout<<3<<endl;
                int i=1;
                for(; i<n; i++) {
                    if(b[i][j]!=b[i-1][j]) {
                        cout<<"1 "<<i<<endl;
                        cout<<i+1<<" ";
                        i++;
                        break;
                    }
                }
                for(; i<n; i++) {
                    if(b[i][j]!=b[i-1][j]) {
                        cout<<i<<endl;
                        cout<<i+1<<" "<<n<<endl;
                        return;
                    }
                }
            } else {
                cout<<2<<endl;
                for(int i=1; i<n; i++) {
                    if(b[i][j]!=b[i-1][j]) {
                        cout<<"1 "<<i<<endl;
                        cout<<i+1<<" "<<n<<endl;
                        return;
                    }
                }
            }
        }
    }
    cout<<"NO"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}