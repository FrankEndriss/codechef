
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;



typedef struct node {
	int parent;
	vector<int> childs;
	long long capa;

	int flowFromParent; 		// amount of water received by parent per second
	int leaveCountPerLevel[TREESZ];	// number of leaves in this subtree per level
} node;

#define TREESZ 1001
node tree[TREESZ];
int maxRecv[TREESZ];	// leaves at level i can receive max X
int maxDepth;
int leaveCountLevel[TREESZ];	// count of child leaves per abs level of that leave

// TODO combine maxRecv with max parents parents capa...
/**
 * Consider:
 * 1. We need to sort the leaves per level of that leave.
 * 2. Then, any one leave should be considered to be filled with water, in the order of level.
 * 3. Any one leave can get as most water as parent provides, so parent ditributes its water to ist childs in order of levels.
 * 4. But one child can have many childs/leaves of many levels.
 * 5. The distribution of the parents water has to be in order of all those childs.
 * ->
 *  1. Calc a list of childs count per levels for every node.
 *  2. Start at top level, distribute water to every child, BFS
 *  3. Continue at every childs childs, ditribute the distributed water.
 *  4. Store the distributed water in a list by level.
 *  5. Add sums of levels up to maxDepth.
 *  6. At maxDepth the max distribution per second is reached, calc total time.
 *  TODO
 */
long long dfsCapa(int np, int second) {
	maxDepth=max(maxDepth, second);
	long long csum=0;
	for(uint i=0; i<tree[np].childs.size(); i++) {
		csum+=dfsCapa(tree[np].childs[i], second+1);
		tree[np].leaveCountLevel[second]+=tree[tree[np].childs[i].leaveCountLevel[second+1];
	}
	if(tree[np].childs.size()>0) {
		int ret=min(csum, tree[np].capa);
		tree[np]=ret;
		return ret;
	} else {
		tree[np].leaveCountLevel[second]=1;	// this node is the leave at level second
		return tree[np].capa;
	}
}

void solve() {
	for(int i=0; i<TREESZ; i++) {
		maxRecv[i]=0;
		tree[i].childs.clear();
	}
	maxDepth=0;
int N;
long long X;
	cin>>N>>X;
	for(int i=2; i<=N; i++) {
		cin>>tree[i].parent;
		tree[tree[i].parent].childs.push_back(i);
	}
	for(int i=2; i<=N; i++) {
		cin>>tree[i].capa;
	}
	tree[1].capa=1000000001;	// root capacity inf

	long long capa=dfsCapa(1, 1);

	long long ans=1;
	long long sumRecv=0;
	for(int i=1; i<=maxDepth; i++) {
		sumRecv+=maxRecv[i];
		ans++;
		X-=sumRecv;
		if(X<=0) {
			cout<<ans<<endl;
			return;
		}
	}

	if(X%capa==0)
		ans+=X/capa;
	else
		ans+=X/capa+1;
	cout<<ans<<endl;
}

int main() {
int T;
	cin>>T;
	while(T--)
		solve();

}

