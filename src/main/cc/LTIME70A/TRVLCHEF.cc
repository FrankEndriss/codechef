
#include <math.h> 
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

int C[100001];

void solve() {
int N, D;
	cin>>N>>D;
	for(int i=0; i<N; i++)
		cin>>C[i];

	sort(C+1, C+N);
	for(int i=2; i<N; i++) {
		if(abs(C[i]-C[i-1])>D) {
			cout<<"NO"<<endl;
			return;
		}
	}
	if(abs(C[0]-C[1])>D && abs(C[N-1]-C[0])>D) {
		cout<<"NO"<<endl;
		return;
	}
	cout<<"YES"<<endl;
}
int main() {
int T;
	cin>>T;
	for(int i=0; i<T; i++) 
		solve();
}

