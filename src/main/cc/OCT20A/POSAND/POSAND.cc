/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We need to start at N and pair it with the next lower
 * 2^x.
 * Since the Number with only a single bit set are critical,
 * they need to be paired with a higher number.
 *
 * 10000, 1xxxx, 10001, 1001, 1000, 1xxx, 1010, 110, ...
 *
 * So we start with 2^x, and end that block with 2^x+1, then start next block with
 * 2^(x-1)+1, and end that block with 2^(x-1)+2^(x-2) and repeat until zero.
 *
 * example n=10100
 * 10000    (first loop)
 * 10100
 * 10011
 * 10010
 * 10001    (ends with nn+1)
 * 01001    (second loop)
 * 01000    (first and second reversed)
 * 01010
 * 01011
 * 01100
 * 01101
 * 01110
 * 01111
 * 00101    (third loop)
 * 00100    (first and second reversed)
 * 00110
 * 00111
 * 00011
 * 00010
 * 00001    (only first)
 *
 * example 1001
 */
void solve() {
    cini(n);
    if(n==1) {
        cout<<1<<endl;
        return;
    } else if(n==2) {
        cout<<-1<<endl;
        return;
    }


    int nn=1;
    while(nn*2<=n)
        nn*=2;

    if(nn==n) {
        cout<<-1<<endl;
        return;
    }

    /* first loop */
    cout<<nn<<" ";
    for(int i=n; i>nn; i--) 
        cout<<i<<" ";
    cout<<1<<" ";

    /* second loop */
    nn>>=1;
    while(nn>1) {
        cout<<nn+1<<" ";
        cout<<nn<<" ";
        for(int i=nn+2; i<(nn<<1); i++) 
            cout<<i<<" ";
        nn>>=1;
    }
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
