/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* 
 * operation:
 * Update the kth smallest element to some value.
 * ie -1, x or INF
 *
 * Note that 
 * elements left of a[k] cannot be made bigger.
 * elements right of a[k] cannot be made smaller.
 *
 * An operation is to rotate a subarray starting or ending in k by 1 position, 
 * updating the outermost (most left or right) value.
 * If it is right the value is incremented, if it is left the value is 
 * decremented.
 */
void solve() {
    cini(n);
    cini(x);
    cini(p); p--;
    cini(k); k--;
    cinai(a,n);
    sort(all(a));

    if((p<k && a[p]<x) || (p>k && a[p]>x)) {
        cout<<-1<<endl;
        return;
    }

    if(p<k) {
        /* number of elements bigger than x up to a[k]. */
        int idx=upper_bound(all(a), x)-a.begin();
        idx=min(idx,p);
        int ans=p-idx+1;
        cout<<ans<<endl;
    } else {
        /* number of elements smaller than x starting at a[k] */
        int idx=lower_bound(all(a), x)-a.begin();
        idx=max(idx, p);
        int ans=idx-p;
        cout<<ans<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
