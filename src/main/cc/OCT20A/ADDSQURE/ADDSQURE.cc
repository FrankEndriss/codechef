/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* We would like to check all possible k.
 * How to implement that check efficiently?
 *
 * How to find number of distinct areas even without the
 * added line?
 * There are (m-1)*(n-1) rectangles.
 *
 * Note that we search for _squares_, there can be
 * at most min(H,W) different ones.
 *
 * There is a diagonal with slope 1,1 from each point, cutting the vertical
 * lines. If the cutting point is on hight of a horizontal line, that builds
 * a square.
 * So, a horz line at y[k] builds squares with all the lines below y[k]
 * if there is a vertical line at the crosspoint.
 *
 *
 */
void solve() {
    cini(w);
    cini(h);
    cini(n);
    cini(m);

    cinai(a,n);
    cinai(b,m);
}

signed main() {
    solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
