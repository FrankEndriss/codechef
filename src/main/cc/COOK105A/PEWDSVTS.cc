
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

const int N=100001;
int c[N];

void solve() {
ll n, a, b, x, y, z;
    cin>>n>>a>>b>>x>>y>>z;
    for(int i=0; i<n; i++)
        cin>>c[i];

    /* calc when will holi take the net. */
    ll days=(z-b)/y;
    if((z-b)%y==0)
        days--;
    /* after days days holi will be one day before taking the net. */
    ll valPip=a+days*x;
    ll needed=z-valPip;

//cout<<"days="<<days<<" needed="<<needed<<endl;

    vector<int> heap(c, c+n);
    make_heap(heap.begin(), heap.end());

    int ans=0;
    while(needed>0 && heap.size()>0) {
//cout<<"needed="<<needed<<endl;
        const int ma=heap.front();
//cout<<"needed="<<needed<<" contrib="<<ma<<endl;
        pop_heap(heap.begin(), heap.end());
        heap.pop_back();

        needed-=ma;
        if(ma>0)
            ans++;
        else
            break;
        if(ma/2>0) {
            heap.push_back(ma/2);
            push_heap(heap.begin(), heap.end());
        }
    }

    if(needed>0)
        cout<<"RIP"<<endl;
    else
        cout<<ans<<endl;
}

int main() {
int t;
    cin>>t;
    for(int i=0; i<t; i++)
        solve();

}

