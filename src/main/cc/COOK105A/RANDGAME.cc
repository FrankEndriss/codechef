/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

bool isWin(ll n) {
    if(n==1 || n==3 || n==4 || n==5)
        return true;
    else if(n==2 || n==6 || n==8)
        return false;

    if(n%2==0)
        return !isWin(n/2);
    else
        return !isWin(n+1) || !isWin(n-1);
}

string move(ll &n) {
    if(n%2==0) {
        n/=2;
        return "/2";
    } else if(isWin(n+1)) {
        n++;
        return "+1";
    } else {
        n--;
        return "-1";
    }
}

void solve() {
ll n;
    cin>>n;

    if(!isWin(n)) {
        cout<<"Lose"<<endl;
        string resp;
        cin>>resp;
        if(resp=="WA")
            exit(0);
        return;
    }
    /* play */
    while(true) {
        cout<<move(n);
        string resp;
        cin>>resp;
        if(resp=="GG")
            return;
        if(resp=="+1")
            n++;
        else if(resp=="-1")
            n--;
        else if(resp=="/2)")
            n/=2;
        else
            exit(0);
    }
}

int main() {
int t;
    cin>>t;
    for(int i=0; i<t; i++)
        solve();

}

