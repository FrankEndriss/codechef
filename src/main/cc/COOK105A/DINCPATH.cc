
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

const int N=100001;

vector<ll> a(N);
vector<vector<int>> g(N);
vector<map<vector<int>, int>> memo(N);

int dfs(int node, ll prevVal) {
//cout<<"dfs, node="<<node<<" prevVal="<<prevVal<<endl;

    ll diff=a[node]-prevVal;
    vector<int> v;
    for(auto c : g[node])
        if(a[c]>a[node]+diff)
            v.push_back(c);

    auto it=memo[node].find(v);
    if(it!=memo[node].end())
        return it->second;

    int m=0;
    for(auto c : v)
        m=max(m, dfs(c, a[node]));

    memo[node][v]=m+1;
    return m+1;
}

void solve() {
    int n, m;
    cin>>n>>m;
    for(int i=0; i<n; i++)
        cin>>a[i];

    for(int i=0; i<m; i++) {
        int u, v;
        cin>>u>>v;
        u--; v--;
        g[u].push_back(v);
        g[v].push_back(u);
    }


    int ans=0;
    for(int i=0; i<n ; i++)
        ans=max(ans, dfs(i, a[i]));

    cout<<ans<<endl;

    for(int i=0; i<n; i++) {
        g[i].clear();
        memo[i].clear();
    }
}

int main() {
int t;
    cin>>t;
    for(int i=0; i<t; i++)
        solve();

}

