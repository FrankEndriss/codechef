
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* note that all subarrays of size 2 (i, i+1) are good.
 * Then we can extend them.
 * We can extend 
 * 2 left
 * 1 left, one right
 * 2 right
 *
 * let mi=min(a[i],a[i+1]), ma=max(a[i],a[i+1])
 * 2 left is good if both are <mi or both are >ma
 * or smaller on is on smaller end and bigger one on bigger end.
 * 2 right same.
 *
 * 1l,1r is good if ...
 *
 * consider a good subarray, it has two starting positions, the one of the 
 * smaller half, and the one of the bigger half.
 * The bigger half has a min value, smaller half a max value.
 * Then, the two new values must seamlessly integrate into
 * these borders.
 * There are several cases.
 * ... How to not TLE?
 */
void solve() {
    cini(n);
    cinai(a,n);


}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
