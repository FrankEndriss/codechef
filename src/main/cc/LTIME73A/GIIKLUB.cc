/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

void solve() {
    int n, x;
    cin>>n>>x;
    vector<vector<ll>> a(n+1, vector<ll>(n+1));
    for(int row=0; row<n; row++)
        for(int col=0; col<n; col++)
            cin>>a[row][col];


    // dp[i][j]= x to number of ways x can be reached at position [i][j]
    vector<vector<map<ll, ll>>> dp(n+1, vector<map<ll, ll>>(n+1));
    dp[0][0][a[0][0]]=1;

    ll sumX=a[0][0];
    ll sumY=a[0][0];
    for(int i=1; i<n; i++) {
        sumX+=a[0][i];
        sumY+=a[i][0];
        if(sumX<=x)
            dp[0][i][sumX]=1;
        if(sumY<=x)
            dp[i][0][sumY]=1;
    }

    for(int i=1; i<n; i++) {
        for(int col=i; col<n; col++) { // left to right in row i
            for(auto top : dp[i-1][col]) {
                ll lx=top.first+a[i][col];
                if(lx<=x) {
                    dp[i][col][lx]+=top.second;
//cout<<" l2r top, i="<<i<<" col="<<col<<" lx="<<lx<<endl;
                }
            }
            for(auto left : dp[i][col-1]) {
                ll lx=left.first+a[i][col];
                if(lx<=x) {
                    dp[i][col][lx]+=left.second;
//cout<<" l2r lef, i="<<i<<" col="<<col<<" lx="<<lx<<endl;
                }
            }
        }
        for(int row=i+1; row<n; row++) { // top to bottom in col i
            for(auto top : dp[row-1][i]) {
                ll lx=top.first+a[row][i];
                if(lx<=x) {
                    dp[row][i][lx]+=top.second;
                }
            }
            for(auto left : dp[row][i-1]) {
                ll lx=left.first+a[row][i];
                if(lx<=x) {
                    dp[row][i][lx]+=left.second;
                }
            }
        }
    }

//cout<<"dp[1][1]="<<endl;
//for(auto ent : dp[1][1]) {
//    cout<<"first="<<ent.first<<" second="<<ent.second<<endl;
//}

    ll ans=0;
    for(auto ent : dp[n-1][n-1]) {
//cout<<"ans.first="<<ent.first<<" ans.second="<<ent.second<<endl;
        ans+=ent.second;
    }

    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();

}

