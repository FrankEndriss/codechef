/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Considert b[] to be a permutation, just sort.
 * Consider checkerboard,
 * A fields: (i+j) even
 * B fields: (i+j) odd
 *
 * Consider path N-1 times down, then M-1 times right,
 * so len=N-1+M-1== N+M-1;
 * 1 0
 * 2 n*m-1
 * 3 -1
 * 4 n*m-2
 * 5 -2
 * 6 7 8 9 10 11 12 ... a[n-1][m-1]
 *
 * But note that we can do this on arbitrary segment in b[],
 * so we need to find best segment.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);
    cinai(b,n*m);
    sort(all(b));

    bool sw=false;
    if(n>m) {
        sw=true;
        swap(n,m);
    }

    vvi ans(n, vi(m, -INF));

    int idx=0;
    for(int i=(n%2==0?n-2: n-3); i>=0; i-=2)
        ans[i][0]=idx++;

    for(int i=1; i<n; i++)
        ans[i][0]=idx++;
    for(int j=1; j<m; j++)
        ans[n-1][j]=idx++;

    const int mi=ans[0][0];
    const int ma=ans[n-1][m-1];

    vb vis(n*m);

    int optIdx=mi;
    int optSum=b[ma]-b[mi];
    for(int i=0; ma+i<n*m; i++) {
        if(b[ma+i]-b[idxL+i] < optSum) {
            optIdx=i;
            optSum=b[ma+i]-b[idxL+i];
        }
    }
    //cerr<<"after opt sum"<<endl;


    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)
            if(ans[i][j]>=0) {
                if(ans[i][j]<=idxR)
                    ans[i][j]+=idxL;
                vis[ans[i][j]]=true;
            }

    //cerr<<"after opt sum2"<<endl;

    int nidx=0;
    for(int i=0; i<n; i++)
        for(int j=0; j<m; j++)  {
            if(ans[i][j]<0) {
                while(nidx<n*m && vis[nidx])
                    nidx++;

                assert(nidx<n*m);

                vis[nidx]=true;
                ans[i][j]=b[nidx];
            } else {
                assert(vis[ans[i][j]]);
                ans[i][j]=b[ans[i][j]];
            }
        }


    if(sw) {
        vvi ans2(m, vi(n));
        for(int i=0; i<n; i++)
            for(int j=0; j<m; j++)
                ans2[j][i]=ans[i][j];
        ans=ans2;
        swap(n,m);
    }
    //cerr<<"after swap"<<endl;

    for(int i=0; i<n; i++)  {
        for(int j=0; j<m; j++)
            cout<<ans[i][j]<<" ";
        cout<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
// marker
