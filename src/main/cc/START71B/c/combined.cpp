/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
//#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/* Longest common subseq */

int N=0;
void solve() {
    cini(n);
    N+=n;
    assert(N<=5000);

    cins(str1);
    string str2=str1;
    reverse(all(str2));

    vvi dp(n, vi(n, -1));

    function<int(int,int)> lcs=[&](int i, int j) {

        // base condition
        if(i>=n-1-j)
            return dp[i][j]=0;

        // if lcs has been computed
        if (dp[i][j] != -1)
            return dp[i][j];

        // if characters are same return previous + 1 else
        // max of two sequences after removing i'th and j'th
        // char one by one
        if (str1[i] == str2[j])
            dp[i][j] = 1 + lcs(i + 1, j + 1);
        else
            dp[i][j] = max(lcs(i + 1, j), lcs(i, j + 1));

        return dp[i][j];
    };


    int ans=lcs(0,0);
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}