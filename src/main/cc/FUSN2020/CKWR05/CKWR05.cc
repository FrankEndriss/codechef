
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

int nextOdd(set<int> &s) {
    auto it=s.begin();
    while(it!=s.end()) {
        if(*it%2==1) {
            int ans=*it;
            s.erase(it);
            return ans;
        }
        it++;
    }
    return -1;
}
int nextEven(set<int> &s) {
    auto it=s.begin();
    while(it!=s.end()) {
        if(*it%2==0) {
            int ans=*it;
            s.erase(it);
            return ans;
        }
        it++;
    }
    return -1;
}

/*
 * There must be at least one vertex with two
 * leaf childs. If sum of childs is odd, vertex must be even,
 * and vice versa.
 *
 * So there are two even and one odd or 3 odd.
 *
 * Since we must start with 1, lets start with
 * So lets start with two odd, then alternate.
 */
void solve() {
    cini(n);

    set<int> s;
    for(int i=1; i<=n; i++)
        s.insert(i);


    /* we can start with 1,3,5 or with 1,2,4 */
    vvi st= { {1,3,5}, {1,2,4}};

    vector<pii> ans;
    bool ok=true;
    set<int> ss=s;
    int root=1;
    ss.erase(1);

    while(ss.size()>=2) {
        int i1, i2;
        if(root%2==0) {
            i1=nextOdd(ss);
            i2=nextOdd(ss);
            if(i1<0 || i2<0) {
                ok=false;
                break;
            }
        } else {
            i1=nextEven(ss);
            i2=nextEven(ss);
            if(i1<0 || i2<0) {
                ok=false;
                break;
            }
        }
        ans.push_back({root, i1});
        ans.push_back({i2, i1});
        root=i1;
    }
    if(ss.size()==0 && ok) {
        cout<<root<<endl;
        for(size_t i=0; i<ans.size(); i++)
            cout<<ans[i].first<<" "<<ans[i].second<<endl;
        return;
    }
    cout<<-1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
