
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"



template<typename E>
struct SegmentTree {
    vector<E> data;

    E neutral;
    int n;

    function<E(E,E)> plus;

    SegmentTree(int _n, E _neutral) {
        n=1;
        while(n<_n)
            n*=2;

        data.resize(n*2, _neutral);
        neutral=_neutral;
    }

    /* bulk update in O(n) */
    template<typename Iterator>
    void init(Iterator beg, Iterator end) {
        for(int idx=n; beg!=end; beg++, idx++)
            data[idx]=*beg;

        for(int idx=n-1; idx>=1; idx--)
            data[idx]=plus(data[idx*2], data[idx*2+1]);
    }

    /* @return accumulative (l,r), both inclusive, top down */
    E query(int node, int sL, int sR, int l, int r) {
        if (r < sL || l > sR)
            return neutral;
        //cerr<<"query, node="<<node<<" sL="<<sL<<" sR="<<sR<<" l="<<l<<" r="<<r<<" data[node]="<<data[node]<<endl;

        if (l<=sL && r>=sR)
            return data[node];

        int mid = (sL+sR)/2;
        return plus(query(node*2, sL, mid, l, r), query(node*2+1, mid+1, sR, l, r));
    }
    E query(int l, int r) {
        return query(1, 0, n-1, l, r);
    }

    /* set val at position */
    void set(int idx, E val) {
        idx+=n;
        data[idx]=val;
        while(idx>1) {
            idx/=2;
            data[idx]=plus(data[idx*2], data[idx*2+1]);
        }
    }
};


/* a segment is bitonic if it has exactly one local maximum 
 * if we combine a segment we need to distinguish four types:
 * increasing, decreasing, bitonic and otherwise.
 */

struct mytype {
    int le, ri;
    int type;
    mytype():le(-1),ri(-1),type(0) {
    }
    void dump(string prefix) {
        cerr<<prefix<<" le="<<le<<" ri="<<ri<<" type="<<type<<endl;
    }
};

const int N=2e5+7;
void solve() {
    cini(n);

    vector<mytype> data(n);

    mytype neutral;
    neutral.type=0;

    SegmentTree<mytype> seg(n, neutral);

    seg.plus=[](mytype e1, mytype e2) {
            //e1.dump("e1");
            //e2.dump("e2");
            if(e1.type==0) {
                return e2;
            } else if(e2.type==0)
                return e1;

            mytype ans;
            if((e1.type&16) || (e2.type&16)) {
                ans.type=16;
                return ans;
            }

            ans.le=e1.le;
            ans.ri=e2.ri;
            ans.type=0;

            if((e1.type&1) && (e2.type&1) && e1.ri<=e2.le) { /* all increasing */
                ans.type|=1;
            }

            if((e1.type&2) && (e2.type&2) && e1.ri>=e2.le) { /* all decreasing */
                ans.type|=2;
            } 

            if((e1.type&1) && (e2.type&4) && e1.ri<=e2.le) { /* increasing + bitonic */
                ans.type=4;
            } else if((e1.type&4) && (e2.type&2) && e1.ri>=e2.le) { /* bitonic + decreasing */
                ans.type=4;
            } else if(ans.type==0 && (e1.type&1) && (e2.type&2)) { /* increasing + decreasing */
                ans.type=4;
            } else  if(ans.type==0)
                ans.type=16;

            //ans.dump("ans");
            return ans;
    };

    for(int i=0; i<n; i++) {
        cin>>data[i].le;
        data[i].ri=data[i].le;
        data[i].type=3; /* increasing and decreasing */
    }
    seg.init(all(data));


    cini(q);
    for(int i=0; i<q; i++) {
        cini(t);
        if(t==1) {
            cini(l); l--;
            cini(r); r--;
            mytype ans=seg.query(l, r);
            if(ans.type&16)
                cout<<"NO"<<endl;
            else
                cout<<"YES"<<endl;
        } else if(t==2) {
            cini(pos);
            cini(val);
            mytype v;
            v.type=3;
            v.le=v.ri=val;
            seg.set(pos-1, v);
        } else  {
            cerr<<"t="<<t<<endl;
            assert(false);
        }
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
