
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note all ques with k==1 would be true.
 * Also all ques with k==2 would be true, since there is a simple
 * path between each pair of vertex.
 * So ques with k==3
 * If we find a pair of vertex with only exaktly one other vertex
 * in path, then we know the both are siblings, and the one is the parent
 * of both of them.
 * Unfortunatly that is O(n^3) :/
 * Or, is it less?
 *
 * ****
 * Cosinder pair of vertex u and v.
 * Try all other vertex with them, what tells us the answer?
 * -no matches -> u or v is leaf, other is parent
 * -one match -> match is parent of u and parent of v
 * -two matches x,y ->  ???
 *
 */
void solve() {
    cini(n);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
