
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to flip the cols so that
 * in first row max one 1 remains
 * in second row max one 1 remains
 * ....
 * O(n^2)
 * Consider dependent cols, that are cols
 * with overlapping positions.
 * These need to be alternating.
 * ...no
 * Consider the rows.
 * Each row defines a group of cols, from which 
 * at most 1 can be "used", that is fliped
 * the wrong (ie. 1) way.
 *
 * How to organize a max-flow?
 *
 */
void solve() {
    cini(n);
    cini(m);

    cinas(s,n);

    vector<pii> adj(m);

    for(int i=0; i+1<m; i++) 
        for(int j=i+1; j<m; j++) {
            for(int k=0; k<n; k++) {
                int val=0;
                if(s[i][k]!='x' && s[j][k]!='x') {
                    /* dependend cols */
                    if(s[i][k]==s[i][
                }
            }
        }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
