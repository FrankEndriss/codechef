
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Foreach distinct number in p[], we need to
 * find the max number of bags of that kind takeable.
 * Then take them, and continue with smaller bags.
 *
 * ...But note that taking bags of one kind also makes
 * bags of other sizes unuseable.
 *
 * But we need to traverse the bags only once.
 * So simply maintain set of used bags, binary search
 * each bag before using it.
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    vvi idx(n+2);
    for(int i=0; i<n; i++) {
        cini(aux);
        idx[aux].push_back(i);
    }

    // remove this
    for(int i=0; i<=n; i++) {
        int cnt=0;
        int prev=-2;
        for(size_t j=0; j<idx[i].size(); j++) {
            if(prev<idx[i][j]-1) {
                cnt++;
                prev=idx[i][j];
            }
        }
        f[i]=cnt;
    }
    // end remove this

    mint ans=0;
    int idxi=n+1;
    int idxj=0;
    set<int> vis;
    for(int k=1; k<=(n+1)/2; k++) {
        while(idxj>=idx[idxi].size()) {
            idxi--;
            idxj=0;
        }

        if(idxj==0) {
            /* now here we want to use the max possible number of bags of size idxi,
             * but we want to choose them so that the number of remaining bags
             * of next smaller size is maximum.
             * How?
             * Consider blocks of adjacent bags of size idxi.
             * If blocksize is odd, we must choose 1,3,5...
             * But if the blocksize is even, we have two possibilities.
             * Just try both, see which removes less of next size. But...
             * if is same, we need to consider next-next size...what is complecated,
             * since those could be removed by next-sized bags also...does not work :/
             *
             * How can we arrange the used bags, most likely greedily?
             * -> Note that each block of bags has only to neigbours.
             *  Choose the most valueable neigbours.
             *  But again problem: If blocks have exaktly one neigbour between them,
             *  how to process?
             *  Consider groups of blocks, and do dp on the in between neigbours?
             *  If killing first and third, we can use second.
             *  We can use each even block as L or R, then use the elements between L and R
             *  blocks.
             *  What about odd sized blocks? -> These are both/LR at once.
             *  ...All to comlecated :/
             */
        }

        ans+=mint(2).pow(idxi);
        cout<<ans.val()<<" ";
        f[idxi]--;
        //idxj++;
    }
    cout<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
