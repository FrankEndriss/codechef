
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * zero based or one based?
 * -> one based
 *
 * Ignore the primes
 *
 * 1, p[0], 2, x, 3
 *
 * How?
 * Some greedy....
 * choose the smallest available number on even indexs,
 * use biggest available number on odd indexes.
 * Whe would that work?
 *
 * No, use biggest even number, which is 2*n, and pair with
 * the halfe of it.
 * go backwards.
 */
void solve() {
    cini(n);

    vi a(n);
    int num=2*n;
    for(int i=0; i<n; i+=2) {
        if(i+1<n)
            a[i+1]=num;
        a[i]=num/2;
        num-=2;
    }

    for(int i=0; i<n; i++) 
        cout<<a[i]<<" ";
    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}