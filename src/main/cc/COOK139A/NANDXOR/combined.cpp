
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


/**
 * ...it other problem
 * popcount(a[i1] ^ a[i2])
 * not popcount(a[i1]) ^ popcount(a[i2])
 */
const int N=32;
void solve() {
    cini(n);

    vb s(N);
    vvi idx(N);
    for(int i=0; i<n; i++) {
        signed aux;
        cin>>aux;
        int cnt=0;
        while(aux) {
            cnt+=(aux&1);
            aux/=2;
        }
        s[cnt]=true;
        idx[cnt].push_back(i+1);
    }


    for(int i1=0; i1<N; i1++)  {
        for(int i2=0; i2<N; i2++)  {
            for(int i3=0; i3<N; i3++)  {
                for(int i4=0; i4<N; i4++) {
                    if((i1^i2)!=(i3^i4))
                        continue;

                    vi cnt(N);
                    cnt[i1]++;
                    cnt[i2]++;
                    cnt[i3]++;
                    cnt[i4]++;

                    if(cnt[i1]>(int)idx[i1].size())
                        continue;
                    if(cnt[i2]>(int)idx[i2].size())
                        continue;
                    if(cnt[i3]>(int)idx[i3].size())
                        continue;
                    if(cnt[i4]>(int)idx[i4].size())
                        continue;

                    for(int i=0; i<N; i++) {
                        while(cnt[i]) {
                            cout<<idx[i].back()<<" ";
                            idx[i].pop_back();
                            cnt[i]--;
                        }
                    }
                    cout<<endl;
                    return;
                }
            }
        }
    }
    cout<<-1<<endl;
}


/*
 * How?
 * Consider the number of set bits in a[i1] and a[i2]
 * Each bit in a[i2] increments or decrements popcount()
 * ???
 *
 * Note that there are at most 30 distinct popcounts, so after checking 30*30
 * pairs there must be a hit by pidgeon principle.
 */
void solve2() {
    cini(n);
    cinai(a,n);

    for(int i1=0; i1<n; i1++)  {
        for(int i2=0; i2<n; i2++)  {
            if(i1==i2)
                continue;
            for(int i3=0; i3<n; i3++)  {
                if(i3==i1 || i3==i2)
                    continue;
                for(int i4=0; i4<n; i4++) {
                    if(i4==i1 || i4==i2 || i4==i3)
                        continue;

                    if(__builtin_popcount(a[i1]^a[i2])==__builtin_popcount(a[i3]^a[i4])) {
                        cout<<i1+1<<" "<<i2+1<<" "<<i3+1<<" "<<i4+1<<endl;
                        return;
                    }
                }
            }
        }
    }

    cout<<-1<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve2();
}