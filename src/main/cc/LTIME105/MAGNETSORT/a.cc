
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider [0,S] and [N,n-1]
 *
 * This creates 3 sets of numbers, a[0..S], [N..S], [S..n-1]
 * Note that these overlap by at least one position.
 * Let num overlap be ov.
 *
 * In two operations we can move ov elements from set3 to set1,
 * and also from set1 to set3
 *
 * Count numbers of set1 the needs to go to set3 and vice versa.
 * Then start transfering at bigger set.
 * Note that once all numbers are transferred, the middle section 
 * is sorted anyway.
 * Min ans=1
 *
 * How to implement?
 * Simulation is a lot of cases...
 * idk :/
 */
void solve() {
    cini(n);
    cinai(a,n);
    cins(s);

    int l=0;
    while(l<n && s[l]==s.back())
        l++;

    int r=n-1;
    while(r>=0 && s[r]==s[0]) 
        r--;

    if(is_sorted(all(a))) {
        cout<<0<<endl;
        return;
    } else if(l==n || r<0) {
        cout<<-1<<endl;
        return;
    }

    vi b=a;
    sort(all(b));

    set<int> s1,s3;

    for(int 


}

signed main() {
    cini(t);
    while(t--)
        solve();
}
