
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

/*
ll gcd(ll a, ll b) {
    while (b > 0) {
        ll temp = b;
        b = a % b;
        a = temp;
    }
    return a;
}
*/

ll lcm(ll x, ll y) {
    return (x/__gcd(x, y))*y;
}

void solve() {
    ll x, y;
    cin>>x>>y;

    if(y==0) {
        cout<<"Alice";
        return;
    }

    ll dist=lcm(x, y);
    if((dist/x)%2==0) {
        if((dist/y)%2==0)
            cout<<"Camila";
        else
            cout<<"Alice";
    } else {
        if((dist/y)%2==0)
            cout<<"Camila";
        else
            cout<<"Bob";
    }
    cout<<endl;
}

int main() {
    int t;
    cin>>t;
    for(int i=0; i<t; i++)
        solve();
}

