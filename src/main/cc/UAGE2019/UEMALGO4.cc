
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

const int mod=1000000007*2;
void solve() {
int n, m, k;
    cin>>n>>m>>k;

    int fixedBalls=n*k;
    int freeBalls=m-fixedBalls;

// now we can distribute 0 to freeBalls balls freely between the boxes. 
    ll ans=0;
    for(int i=0; i<freeBalls; i++) {
        ll tmp=1;
        for(int j=0; j<i; j++) {
            tmp*=n;
            tmp%=mod;
        }
        ans+=tmp;
        ans%=mod;
    }
    cout<<ans/2<<endl;
}
int main() {
int t;
    cin>>t;
    for(int i=0; i<t; i++)
        solve();

}

