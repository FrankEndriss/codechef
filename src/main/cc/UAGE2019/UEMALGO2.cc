/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

void solve() {
    int n, k;
    cin>>n>>k;
    vector<ll> a(n+1);
    vector<ll> dp(n+1);
    multiset<ll> ms;

    for(int i=0; i<n; i++) {
        cin>>a[i];
        dp[i]=a[i];
    }
    dp[n]=0;
    for(int i=0; i<k; i++)
        ms.insert(dp[i]);

    for(int i=k; i<=n; i++) {
        dp[i]+=*ms.begin();
        ms.erase(ms.find(dp[i-k]));
        ms.insert(dp[i]);
    }

    cout<<dp[n]<<endl;

}

int main() {
    int t;
    cin>>t;
    for(int i=0; i<t; i++)
        solve();
}

