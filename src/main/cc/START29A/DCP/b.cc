
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    cini(m);

    vector<mint> a(n);
    for(int i=0; i<n; i++) {
        cini(aux);
        a[i]=aux;
    }

    vi incnt(n);
    vector<vector<pii>> adj(n);
    for(int i=0; i<m; i++) {
        cini(v);
        v--;
        cini(cnt);
        for(int j=0; j<cnt; j++) {
            cini(w);
            cini(c);
            c--;
            adj[v].emplace_back(c,w);
            incnt[c]++;
        }
    }

    queue<int> q;
    for(int i=0; i<n; i++) {
        if(incnt[i]==0)
            q.push(i);
    }

    while(q.size()) {
        int v=q.front();
        q.pop();

        if(adj[v].size()>0) {
            for(pii chl : adj[v]) {
                a[chl.first]+=a[v]*chl.second;
                incnt[chl.first]--;
                if(incnt[chl.first]==0)
                    q.push(chl.first);
            }
            a[v]=0;
        }
    }

    for(int i=0; i<n; i++)
        cout<<a[i].val()<<endl;

}

signed main() {
    solve();
}
