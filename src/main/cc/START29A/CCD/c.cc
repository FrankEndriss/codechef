
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to convert first position, which also changes second, 
 * then convert second position, which also changes third...
 * Converstion A to B is possible, if changing the prelast char
 * results in the last char to be correct.
 *
 * Each position need a given number of operations to be correct.
 * ...brainbraker :/
 */
void solve() {
    cini(n);
    cini(q);
    cins(a);
    cins(b);

    assert(a.size()==n);
    assert(b.size()==n);

    vi data(a.size());
    vi pre(a.size()+1);
    for(size_t i=0; i<a.size(); i++) {
        data[i]=(b[i]-a[i]+26)%26;
        pre[i+1]=pre[i]+data[i];
    }

    for(int i=0; i<q; i++) {
        cini(l); l--;
        cini(r);

        cerr<<"l="<<l<<" r="<<r<<endl;
        cerr<<"pre[r-1]-pre[l]="<<pre[r-1]-pre[l]<<" data[r-1]="<<data[r-1]<<endl;
        if((pre[r-1]-pre[l])%26==data[r-1]%26)
            cout<<"Yes"<<endl;
        else
            cout<<"No"<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
