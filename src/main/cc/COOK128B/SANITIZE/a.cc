
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

const int N=1e5+7;
vi sq(N);
void init() {
    for(int i=1; i<N; i++) 
        sq[i]=i*i;
}

using t3=tuple<int,int,int>;

/* construct ax+by+c=0 from two points */
t3 line(pii p1, pii p2) {
    const int a=p1.second-p2.second;
    const int b=p2.first-p1.first;
    return { a, b, -a*p1.first -b*p1.second};
}

pii fire(int a, int b, int c) {
    cout<<1<<" "<<a<<" "<<b<<" "<<c<<endl;
    cini(id);
    cini(d);
    return { id, d };
}
/* We can fire the laser twice per germ.
 * So, we need to make sure we do not get the same germs ID more than twice.
 * Consider firing first laster along x-axis.
 * The we know the y-coordinate of germ with ID=42, and we know there
 * is none more near to the x-axis.
 *
 * If we fire twice with different slopes and get the same germs ID twice,
 * then we can calculate the position of that germ.
 * How to fire the laser?
 * a=1, b=1, c=0
 * a=1, b=2, c=0
 * a=1, b=3, c=0
 * ...
 * How to find the coordinates of germ 42 if we got its coordinate twice?
 * ...To much geometry.
 */
void solve() {
    cini(n);
    cini(m);

    vector<pair<t3,int>> x(n+1);

    for(int i=0; i<2*n; i++) {
        auto [a,b,c]=line({0,0}, {i,-1});
        auto [id,d]=fire(a,b,c);
        if(get<0>(x[id])!=0) {
            /* find the germ id */
        } else 
            x[id]={{a,b,c},d};
    }

}

signed main() {
    init();
    solve();
}
