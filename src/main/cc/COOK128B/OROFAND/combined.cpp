
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/* How to find initial ans?
 * Bit by bit.
 * Consider consequtive segments of positions a bit is set.
 * Foreach segment of len i there are
 * ((i-1)*i)/2 pairs.
 *
 * How to maintain the freqs of the arrays, updateable?
 * -> A set<pii> <first,last> of a segment
 *    That is binsearchable on update.
 *
 * Fuck!!!
 * We need the bitwise or of all subarrays, not the sum!!!
 *
 * Since each subarray of len 1 has all bits of that number set,
 * we only search for the bitwise or of all numbers.
 * FUCK APRILS FOOL SHIT!!!!
 */
const int N=32;
void solve() {
    cini(n);
    cini(q);

    vi f(N);
    cinai(a,n);
    for(int i=0; i<n; i++) {
        for(int j=0; j<N; j++) {
            if(a[i]&(1LL<<j))
                f[j]++;
        }
    }

    int ans=0;

    for(int j=0; j<N; j++) {
        if(f[j])
            ans+=(1LL<<j);
    }
    cout<<ans<<endl;

    for(int k=0; k<q; k++) {
        cini(x);
        x--;
        cini(v);

        for(int j=0; j<N; j++)
            if(a[x]&(1LL<<j))
                f[j]--;
        a[x]=v;
        for(int j=0; j<N; j++)
            if(a[x]&(1LL<<j))
                f[j]++;

        ans=0;
        for(int j=0; j<N; j++) {
            if(f[j])
                ans+=(1LL<<j);
        }
        cout<<ans<<endl;
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}