/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define N 200001
#define ROOT 0
#define BITS 20

#define DEBUG

int w[N];
int minNode[N];		// minimum node number of subtree
uint countNodes[N];	// count of nodes in subtree
//int parent[N];		// map to parents
vector<int> tree[N];	// the tree
int n;			// number of nodes

typedef struct trienode {
    int node;		// id of this node if leaf, else -1
    int tchilds[2];		// childs with bit notset/set. Since ROOT==0 (and hence child of no node) tchilds[i]==0 means "no child".
} trienode;

trienode trie[N*2*BITS*BITS];	// the tries
int trieMAX;

#ifdef DEBUG
void dumpTrie() {
    for(int i=0; i<trieMAX; i++)
        cout<<"trie["<<i<<"], node="<<trie[i].node<<" c[0]="<<trie[i].tchilds[0]<<" c[1]="<<trie[i].tchilds[1]<<endl;
}
#endif

/** Build a trie.
 * trie[tnode] is a trienode whichs children are segmented by (w[tnode]>>bit)&1.
 * If bit<0 that means there are nodes with same w[i]. In this case we set the leaf to the node with lower index.
 **/
void addNode(int tnode, const int bit, int node) {
#ifdef DEBUG
    int bitval=(w[node]>>bit)&1;
    cout<<"adding tnode="<<tnode<<" node="<<node<<" w[node]="<<bitset<BITS>(w[node])<<" bit="<<bit<<" bitval="<<bitval<<endl;
#endif
    if(trie[tnode].node>=0) {	// tnode is leaf
        if(bit<0) {
            // If bit==0 then there is more than one node for value w[node].
            // In this case we need to put min(node) as result for query.
            trie[tnode].node=min(trie[tnode].node, node);
#ifdef DEBUG
            cout<<"leaf set since bit<0"<<endl;
            dumpTrie();
#endif
            return;
        }

        /* Move this leafs value to new node, make tnode parent of that new tnode.
        	Note that the other child stays 0, ie not set. */
        trie[trieMAX].node=trie[tnode].node;  // create new node, with no children
        trie[tnode].tchilds[(w[trie[tnode].node] >> bit) & 1]=trieMAX;	// set new node as child of tnode
        trie[tnode].node=-1;	// mark tnode as beeing not leaf
#ifdef DEBUG
        cout<<"this node is leaf, tnode="<<tnode<<", created new tnode="<<trieMAX<<endl;
#endif
        trieMAX++;
    }

    const int bset=(w[node] >> bit) & 1;
    if(trie[tnode].tchilds[bset]==0) {	// child slot empty, put a new leaf node into it
#ifdef DEBUG
        cout<<"child slot empty, putting new node into it, new node="<<trieMAX<<endl;
#endif
        trie[tnode].tchilds[bset]=trieMAX;
        trie[trieMAX++].node=node;  	// set that new node to be leaf
    } else
        addNode(trie[tnode].tchilds[bset], bit-1, node);

#ifdef DEBUG
    if(bit==BITS-1) {
        cout<<"after addNode"<<endl;
        dumpTrie();
    }
#endif

}

/** Simply copie complete structure of src to dst, by coping
 * node by node.
 */
void copyTrie(int dst, int src) {
#ifdef DEBUG
    cout<<"copyTrie, dst="<<dst<<" src="<<src<<endl;
#endif
    trie[dst].node=trie[src].node;
    for(int i=0; i<2; i++) {
        if(trie[src].tchilds[i]==0)
            trie[dst].tchilds[i]=0;
        else {
            trie[dst].tchilds[i]=trieMAX++;
            copyTrie(trie[dst].tchilds[i], trie[src].tchilds[i]);
        }
    }
}

void mergeTrie(int dst, int bit, int src) {
    /* cases:
    * 1. leaf, leaf:    simple add
    * 2. notleaf, leaf: simple add
    * 3. leaf, notleaf: copy and add
    * 4. notleaf, notleaf: merge children
    * 4.1 x, empty: do nothing
    * 4.2 emtpy, notempty: copy
    * 4.3 notemtpy, notempty: merge
    */

#ifdef DEBUG
    cout<<"mergeTrie, dst="<<dst<<" bit="<<bit<<" src="<<src<<endl;
#endif
    if(trie[src].node>=0)   // Case 1+2
        addNode(dst, bit, trie[src].node);
    else {
        if(trie[dst].node>=0) { // Case 3
            int tmp=trie[dst].node;
            copyTrie(dst, src);
            addNode(dst, bit, tmp);
        } else {    // Case 4
            for(int i=0; i<2; i++) {
                if(trie[src].tchilds[i]>0) {
                    if(trie[dst].tchilds[i]==0) { // 4.2
                        trie[dst].tchilds[i]=trieMAX++;
                        copyTrie(trie[dst].tchilds[i], trie[src].tchilds[i]);
                    } else { // 4.3
                        mergeTrie(trie[dst].tchilds[i], bit-1, trie[src].tchilds[i]);
                    }
                } // else 4.1
            }
        }
    }
}

/** Build a trie bottom up for every node. @return number of nodes in subtree. */
void dfs(int node, int p) {
#ifdef DEBUG
    cout<<"dfs node="<<node<<" p="<<p<<endl;
#endif
    //parent[node]=p;
    for(auto c : tree[node])
        if(c!=p) {
            dfs(c, node);
            mergeTrie(node, BITS-1, c);
        }
}


/** query in subtree node for max(w[i] ^ k). */
int query(int node, int k) {
#ifdef DEBUG
    cout<<"query node="<<node<<" k=0b"<<bitset<BITS>(k)<<endl;
    dumpTrie();
#endif

    int round=BITS-1;
    while(true) {
        if(trie[node].node>=0) {	// trie[node] is a leaf
#ifdef DEBUG
            cout<<"query found leave in tnode="<<node<<" whichs node="<<trie[node].node<<endl;
#endif
            return trie[node].node;
        }

        if(trie[node].tchilds[0]==0) // no childs with bit notset
            node=trie[node].tchilds[1];
        else if(trie[node].tchilds[1]==0) // no childs with bit set
            node=trie[node].tchilds[0];
        else {	// else switch to child dependent of bit.
            int bit=((w[trie[node].node] ^ k)>>round)&1;
            bit^=1;
            node=trie[node].tchilds[bit];
        }
        round--;
#ifdef DEBUG
        cout<<"query next round="<<round<<", tnode="<<node<<endl;
#endif
    }

}

void solve() {
    int q;
    cin >> n >> q;

    for (int i = 0; i < n; i++)
        cin >> w[i];

    for (int i = 0; i < n - 1; i++) {
        int x, y;
        cin >> x >> y;
        tree[x - 1].push_back(y - 1);
        tree[y - 1].push_back(x - 1);
    }

    for(int i=0; i<trieMAX; i++)
        trie[i].node=trie[i].tchilds[0]=trie[i].tchilds[1]=0;

    for(int i=0; i<n; i++)	// create a tnode for every node at same index, which is the root of the trie for that node
        trie[i].node=i;

    trieMAX=n;
#ifdef DEBUG
    cout<<"initial trie"<<endl;
    dumpTrie();
#endif

    dfs(ROOT, -1);	// build the tries

    int x = 0, v = 0;
    for (int i = 0; i < q; i++) { // answer the queries
        int a, b;
        cin >> a >> b;
        v ^= a;
        int k = x ^ b;
        int ans = query(v - 1, k);
        v = ans + 1;
        x = k ^ w[ans];
        cout << v << " " << x << endl;
    }
}

int main() {
    int t;
    cin >> t;
    for (int i = 0; i < t; i++)
        solve();
}

