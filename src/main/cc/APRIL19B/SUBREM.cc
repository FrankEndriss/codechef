/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;
#define INF 1000000000

int A[100001];
int parent[100001];
ll sumA[100001];
ll sumAx[100001];
vector<int> tree[100001];
ll profit;
int X;

/** DFS collecting the sum of weigths in the subtree rooted by node. 
 * sumAx[node] is the max profit we can get from removing this node or
 * a subset of its children. 
 * sumA[i] is the raw sum of weight of the children and this node,
 * needed to calculate the sumAx[i]. */
void dfsSumA(int node, int parentOfNode) {
ll childsX=0;	
ll a=A[node];
	parent[node]=parentOfNode;

	for(auto c  : tree[node])  {
		if(c!=parentOfNode) {
			dfsSumA(c, node);
			a+=sumA[c];
			childsX+=sumAx[c];
		}
	}
	sumA[node]=a;

	if(-a-X>childsX)
		sumAx[node]=-a-X;
	else
		sumAx[node]=childsX;
	sumAx[node]=max(sumAx[node], 0LL);	// we can simply not remove this node or any child, too
}

void solve(int N) {

	for(int i=0; i<N; i++) {
		cin>>A[i];
		profit+=A[i];
	}

	for(int i=0; i<N-1; i++) {
int u, v;	cin>>u>>v;
		tree[u-1].push_back(v-1);
		tree[v-1].push_back(u-1);
	}

	dfsSumA(0, -1);
	profit+=sumAx[0];
	cout<<profit<<endl;
}

int main() {
int t; cin>>t;
	for(int i=0; i<t; i++) {
		int N; cin>>N>>X;
		for(int j=0; j<N; j++) {
			sumA[j]=sumAx[j]=A[j]=0;
			parent[j]=-1;
			tree[j].clear();
		}
		profit=0;
		solve(N);
	}
	return 0;
}

