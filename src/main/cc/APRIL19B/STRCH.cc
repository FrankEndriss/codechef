
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;

void dump(vector<int> &v) {
for(uint i=0; i<v.size(); i++)
	cout<<v[i]<<" ";
cout<<endl;
}

void solve() {
int n;
string s;
string c;
vector<int> v;
	cin>>n;
	cin>>s;
	cin>>c;
	for(int i=0; i<n; i++)
		if(s[i]==c[0])
			v.push_back(i);
//cout<<s<<endl;
//dump(v);

	unsigned long ans=0;
	uint prefIdx=0;
	for(auto i : v) {
		uint szL=i-prefIdx+1;
		uint szR=n-i;
//cout<<"szL="<<szL<<" szR="<<szR<<endl;
		ans+=szL*szR;
		prefIdx=i+1;
	}
	cout<<ans<<endl;
}
	

int main() {
int t;
	cin>>t;
	for(int i=0; i<t; i++)
		solve();

}

