
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define N 100001
#define ROOT 0
vector<int> tree[N];
ll v[N];
ll m[N];
ll ans[N];
bool leaf[N];

typedef struct sdfs {
    int node;
    int parent;
    ll gcd;
} sdfs;

vector<sdfs> sdfsSt;

ll mygcd(ll a, ll b) {
    if(a==1)
        return a;
    while (b > 0) {
        auto temp = b;
        b = a % b;
        a = temp;
    }
    return a;
}

void dfs_stacked(int node, int parent, ll gcd) {
    sdfsSt.clear();
    sdfsSt.push_back({ node, parent, gcd });

    while(sdfsSt.size()) {
        sdfs entry=sdfsSt.back();
        sdfsSt.pop_back();

        bool isLeaf=true;
        for(auto c : tree[entry.node]) {
            if(c!=entry.parent) {
                sdfsSt.push_back({ c, entry.node, mygcd(entry.gcd, v[c])});
                isLeaf=false;
            }
        }
        if(isLeaf) {
            ans[entry.node]=m[entry.node]-mygcd(entry.gcd, m[entry.node]);
            leaf[entry.node]=true;
        }
    }
}

void solve() {
    int n;
    cin>>n;
    for(int i=0; i<n; i++) {
        tree[i].clear();
        leaf[i]=false;
    }
    for(int i=0; i<n-1; i++) {
        int u, v;
        cin>>u>>v;
        tree[u-1].push_back(v-1);
        tree[v-1].push_back(u-1);
    }
    for(int i=0; i<n; i++)
        cin>>v[i];
    for(int i=0; i<n; i++)
        cin>>m[i];

    dfs_stacked(ROOT, -1, v[ROOT]);

    ostringstream oss;
    for(int i=0; i<n; i++)
        if(leaf[i])
            oss<<ans[i]<<" ";
    cout<<oss.str()<<endl;
}

int main() {
    int t;
    cin>>t;
    for(int i=0; i<t; i++)
        solve();
}

