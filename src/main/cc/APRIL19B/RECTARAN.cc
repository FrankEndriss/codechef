/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*********************************************
 * Need to rework this in OO style structuring.
 * To complicated to implement quick style.
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define NORTH 0
#define EAST 1
#define SOUTH 2
#define WEST 3

const int stepX[] = { 0, 1, 0, -1 };
const int stepY[] = { 1, 0, -1, 0 };

const int newdirs[][2] = { { EAST, WEST }, { NORTH, SOUTH }, { EAST, WEST }, { NORTH, SOUTH } };
const int revdirs[] = { SOUTH, WEST, NORTH, EAST };

#define REVERSE "REVERSE"
#define ROTATE "ROTATE"

typedef struct rect {
	int vx;
	int vy;
	int dx;
	int dy;
	vector<int> actionS;
	vector<int> actionDir;

	int tx;		// target x with current actions
	int ty;		// target y with current actions
	int currDir;	// current direction
} rect;

/* do a move on rect r in second s so that newdir is dir. */
typedef struct action {
	int r;
	int s;
	int dir;
} action;

typedef struct inters {
	uint r1;
	uint r2;
	int sz;
	vector<action> actions;
} intersection;

vector<intersection> intersections;
vector<intersection> mostValueable;
int unusedRect;		// the rect which is not in use, and can be used as a dummy where needed

#define N 1001
uint n;
rect r[N];
uint inters[N][N];
int minS;
vector<int> rSizes;			// index sorted by size of rects
vector<pair<int, int>> openPairs;	// pairs near enough to each other to be matched
vector<int> open;
vector<int> finished;			// rects which not be operated any more.

bool intersCmp(const intersection &i1, const intersection &i2) {
	return !(i1.sz < i2.sz);
}

/** calc the intersection at end of time. */
int intersect(int _1, int _2) {
	int sectX = 0, sectY = 0;
	if (r[_2].tx > r[_1].tx && r[_2].tx < r[_1].tx + r[_1].dx) {
		if (r[_2].tx + r[_2].dx > r[_1].tx + r[_1].dx)
			sectX = r[_1].tx + r[_1].dx - r[_2].tx;
		else
			sectX = r[_2].dx;
	}
	if (r[_1].tx > r[_2].tx && r[_1].tx < r[_2].tx + r[_2].dx) {
		if (r[_1].tx + r[_1].dx > r[_2].tx + r[_2].dx)
			sectX = r[_2].tx + r[_2].dx - r[_1].tx;
		else
			sectX = r[_1].dx;
	}

	if (r[_2].ty > r[_1].ty && r[_2].ty < r[_1].ty + r[_1].dy) {
		if (r[_2].ty + r[_2].dy > r[_1].ty + r[_1].dy)
			sectY = r[_1].ty + r[_1].dy - r[_2].ty;
		else
			sectY = r[_2].dy;
	}
	if (r[_1].ty > r[_2].ty && r[_1].ty < r[_2].ty + r[_2].dy) {
		if (r[_1].ty + r[_1].dy > r[_2].ty + r[_2].dy)
			sectY = r[_2].ty + r[_2].dy - r[_1].ty;
		else
			sectY = r[_1].dy;
	}

	return sectX * sectY;

}

uint manh_begin(int ect, int other) {
	if (ect == other)
		return 0;
	return abs(r[ect].tx - r[other].tx) + abs(r[ect].ty - r[other].ty);
}

/* Calculates the biggest possible intersection at EOT if both
 * rects moved optimally.
 */
int potentialEotIntersect(const int _1, const int _2) {
	int overlap = min(r[_1].dx, r[_2].dx) * min(r[_1].dy, r[_2].dy);
	uint manh = manh_begin(_1, _2);
	return manh < n ? overlap : 0;
}

/** recalculate the position at end of time. */
void recalcTarget(const int ect) {
	r[ect].tx = r[ect].vx;
	r[ect].ty = r[ect].vy;
	int lastS = 0;
	for (uint i = 0; i < r[ect].actionS.size(); i++) {
		int dur = r[ect].actionS[i] - lastS;
		r[ect].tx += stepX[r[ect].actionDir[i]] * dur;
		r[ect].ty += stepY[r[ect].actionDir[i]] * dur;
		lastS = r[ect].actionS[i];
	}
	int dur = n - lastS;
	r[ect].tx += stepX[r[ect].actionDir.back()] * dur;
	r[ect].ty += stepY[r[ect].actionDir.back()] * dur;
}

int rdir(int ect) {
	int ret = r[ect].actionDir.back();
//cout << "log rdir, ect=" << ect << " actionDir.size()=" << r[ect].actionDir.size() << " ret=" << ret << endl;
	return ret;
}

bool bySecond(const intersection &isect1, const intersection &isect2) {
	int i1 = isect1.actions.size() > 0 ? isect1.actions[0].s : 1000001;
	int i2 = isect2.actions.size() > 0 ? isect2.actions[0].s : 1000001;
	return i1 < i2;
}
void reverse() {
	//cout<<"log doing reverse"<<endl;
	int countS0 = 0;

	for (intersection &isect : mostValueable) {
		if (isect.actions.size() > 0) {
			if (isect.actions[0].s == 0) {
				countS0++;
				if (revdirs[isect.actions[0].r] == isect.actions[0].dir) {
					cout << isect.actions[0].r + 1 << isect.actions[0].s << endl;
					isect.actions.erase(isect.actions.begin());
					return;
				}
			}
		}
	}
	//cout<<"log doing reverse, after first loop, countS0="<<countS0<<endl;
	if (countS0 > 0) {
		cout << unusedRect + 1 << 0 << endl;
		return;
	}

	sort(mostValueable.begin(), mostValueable.end(), bySecond);
	//cout<<"log doing reverse, after sort"<<endl;
	while (mostValueable.size() > 0 && mostValueable.back().actions.size() == 0)
		mostValueable.pop_back();
	//cout<<"log doing reverse, after pop_back"<<endl;

	if (mostValueable.size() > 0 && revdirs[mostValueable[0].actions[0].r] == mostValueable[0].actions[0].dir) {
		cout << mostValueable[0].actions[0].r + 1 << mostValueable[0].actions[0].s << endl;
		minS = mostValueable[0].actions[0].s;
		mostValueable[0].actions.erase(mostValueable[0].actions.begin());
		return;
	} else {
		r[unusedRect].currDir = revdirs[r[unusedRect].currDir];
		cout << unusedRect + 1 << " " << minS << endl;
	}
}

void rotate() {
	//cout<<"log doing rotate"<<endl;
	int countS0 = 0;

	for (intersection &isect : mostValueable) {
		if (isect.actions.size() > 0) {
			if (isect.actions[0].s == 0) {
				countS0++;
				if (newdirs[r[isect.actions[0].r].currDir][0] == isect.actions[0].dir
						|| newdirs[r[isect.actions[0].r].currDir][1] == isect.actions[0].dir) {
					cout << isect.actions[0].r + 1 << " " << isect.actions[0].dir << " " << isect.actions[0].s << endl;
					isect.actions.erase(isect.actions.begin());
					return;
				}
			}
		}
	}
	if (countS0 > 0) {
		r[unusedRect].currDir = newdirs[r[unusedRect].currDir][0];
		cout << unusedRect + 1 << " " << r[unusedRect].currDir << " " << 0 << endl;
		return;
	}

	sort(mostValueable.begin(), mostValueable.end(), bySecond);
	while (mostValueable.size() > 0 && mostValueable.back().actions.size() == 0)
		mostValueable.pop_back();

	if (mostValueable.size() > 0
			&& (newdirs[r[mostValueable[0].actions[0].r].currDir][0] == mostValueable[0].actions[0].dir
					|| newdirs[r[mostValueable[0].actions[0].r].currDir][1] == mostValueable[0].actions[0].dir)) {
		cout << mostValueable[0].actions[0].r + 1 << mostValueable[0].actions[0].s << endl;
		minS = mostValueable[0].actions[0].s;
		mostValueable[0].actions.erase(mostValueable[0].actions.begin());
		return;
	} else {
		r[unusedRect].currDir = newdirs[r[unusedRect].currDir][0];
		cout << unusedRect + 1 << " " << r[unusedRect].currDir << " " << minS << endl;
	}
}

template<typename T> int sign(T val) {
	return (T(0) < val) - (val < T(0));
}

bool actScmp(const action &a1, const action &a2) {
	return a1.s < a2.s;
}

/* There are several possible rendevouz points for the rects.
 * 1. First choice would be that at second 0 the directions are choosen that they meet at
 * (r1.y, r2.x) or (r1.x, r2.y). That for, at some point of time the movement has to
 * be reversed because the rects are in place to early.
 * 2. Second choice is that only one is reversed, and the other one changes direction
 * at some point of time. This is usefull if one rects initial dir is toward
 * one of the rendevouz points, and the other one is away from that rp.
 */
void findActions(intersection &inter) {

	/* Action for r1 moving on x-axis to adjust direction at s==0. */
	action a;
	a.s = 0;
	a.r = inter.r1;
	if (r[inter.r1].vx < r[inter.r2].vx)
		a.dir = EAST;
	else
		a.dir = WEST;
	if (a.dir != r[inter.r1].actionDir[0]) /* push only if dir needs to be changed */
		inter.actions.push_back( { a.r, a.s, a.dir });
	/* Action to reverse the direction because to early at rp. */
	a.s = (n - abs(r[inter.r2].vx - r[inter.r1].vx)) / 2;
	a.dir = revdirs[a.dir];
	inter.actions.push_back( { a.r, a.s, a.dir });

	/* same for r2, moving on y-axis */
	a.r = inter.r2;
	if (r[inter.r2].vy < r[inter.r1].vy)
		a.dir = NORTH;
	else
		a.dir = SOUTH;
	if (a.dir != r[inter.r2].actionDir[0]) /* push only if dir needs to be changed */
		inter.actions.push_back( { a.r, a.s, a.dir });
	a.s = (n - abs(r[inter.r2].vy - r[inter.r1].vy)) / 2;
	a.dir = revdirs[a.dir];
	inter.actions.push_back( { a.r, a.s, a.dir });

	/* sort actions by sec */
	sort(inter.actions.begin(), inter.actions.end(), actScmp);
}

bool sizeCmp(const int _1, const int _2) {
	return r[_1].dx * r[_1].dy < r[_2].dx * r[_2].dy;
}

void solve() {
	cin >> n;
	rSizes.clear();
	intersections.clear();
	mostValueable.clear();
	for (uint i = 0; i < n; i++) {
		rSizes.push_back(i);
		r[i].actionS.clear();
		r[i].actionDir.clear();
		int dir;
		cin >> r[i].vx >> r[i].vy >> r[i].dx >> r[i].dy >> dir;
		r[i].currDir = dir;
		r[i].actionS.push_back(0);
		r[i].actionDir.push_back(dir);
		recalcTarget(i);
	}
	//cout<<"log did read input "<<endl;

	sort(rSizes.begin(), rSizes.end(), sizeCmp);
	/** TODO Find pairs of mostly big rects which can be overlapped at eot */

	for (uint i = 0; i < n; i++)
		for (uint j = i + 1; j < n; j++)
			intersections.push_back( { i, j, potentialEotIntersect(i, j) });
	//cout<<"log did collect potential intersections "<<endl;

	sort(intersections.begin(), intersections.end(), intersCmp);
	//cout<<"log did sort potential intersections "<<endl;

// place most valueable pairs in vector
	vector<bool> used(n, false);
	unusedRect = -1;
	for (intersection isect : intersections) {
		if (mostValueable.size() >= n / 4) {
			if (unusedRect < 0) {
				if (!used[isect.r1]) {
					unusedRect = isect.r1;
					break;
				} else if (!used[isect.r2]) {
					unusedRect = isect.r2;
					break;
				} else
					continue;
			}
		}
		if (!used[isect.r1] && !used[isect.r2]) {
			mostValueable.push_back(isect);
			used[isect.r1] = true;
			used[isect.r2] = true;
		}
		if (isect.sz == 0)
			break;
	}
	//cout<<"log did select mostValueable "<<endl;

// find strategy for most valueable pairs
	for (intersection isect : mostValueable)
		findActions(isect);
	//cout<<"log did calculate actions for mostValueable "<<endl;

	minS = 1;
	for (uint i = 0; i < n; i++) {
		string s;
		cin >> s;
		if (s.compare(REVERSE) == 0)
			reverse();
		else
			rotate();
	}
}

int main() {
	int t;
	cin >> t;
	for (int i = 0; i < t; i++)
		solve();
}

