/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;

int A[100001];

void solve() {
int n, a;
int m1=0, m2=0;
	cin>>n;
	for(int i=0; i<n; i++) {
		cin>>a;
		if(a>m1) {
			m2=m1;
			m1=a;
		} else if(a>m2 && a!=m1) {
			m2=a;
		}
	}
	cout<<m2<<endl;
}

int main() {
	solve();
}

