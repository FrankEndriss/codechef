/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;

void solve() {
int N, M, K;
	cin>>N>>M>>K;
	set<pair<int, int>> plants;

	int r, c;
	for(int i=0; i<K; i++) {
		cin>>r>>c;
		plants.insert(make_pair(r, c));
	}

	int ans=0;
	// check left-right
	for(auto p : plants) {
		if(plants.count(make_pair(p.first+1, p.second))==0)
			ans++;
		if(plants.count(make_pair(p.first-1, p.second))==0)
			ans++;
		if(plants.count(make_pair(p.first, p.second+1))==0)
			ans++;
		if(plants.count(make_pair(p.first, p.second-1))==0)
			ans++;
	}

	cout<<ans<<endl;
}

int main() {
int t;
	cin>>t;
	for(int i=0; i<t; i++)
		solve();

}

