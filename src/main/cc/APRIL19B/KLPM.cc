/**
 ** Dont raise your voice, improve your argument.
 ** --Desmond Tutu
 **/

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

vector<pair<int, int>> pld;	// start and len of all palindromes with len>1  in s
string s;

// maps string of the positions in s where the reverse-string starts.
map<string, vector<int>> revidx;

void indexRev() {
/* Build index of all reversed strings */
	for(uint i=0; i<s.length(); i++) {
		for(uint j=i; j<s.length(); j++) {
			string r=s.substr(i, j-i+1);
			reverse(r.begin(), r.end());
			revidx[r].push_back(i);
		}
	}
}

//#define DEBUG

#ifdef DEBUG
typedef struct palmatch {
    int s1start;
    int s1len;
    int s2start;
    int s2len;
} palmatch;
vector<palmatch> palmatches;
#endif

bool byFirst(const pair<int, int> &p1, const pair<int, int> &p2) {
    return p1.first < p2.first;
}

void indexPalindromes() {
    for (uint i = 0; i < s.length(); i++) { // odd len
        uint len = 1;
        while (len < i && i + len < s.length() - 1 && s[i - len] == s[i + len]) {
            pld.push_back( { i - len, len * 2 + 1 });
#ifdef DEBUG
            cout << "log pushed odd palindrome: " << pld.back().first << " " << pld.back().second << endl;
#endif
            len++;
        }
    }

    for (uint i = 1; i < s.length(); i++) { // even len
        int len = 0;
        while (i - len >= 1 && i + len < s.length() - 1 && s[i - 1 - len] == s[i + len]) {
            pld.push_back( { i - 1 - len, 2 * (len + 1) });
#ifdef DEBUG
            cout << "log pushed even palindrome: " << pld.back().first << " " << pld.back().second << endl;
#endif
            len++;
        }
    }
    if (pld.size() > 0)
        sort(pld.begin(), pld.end(), byFirst);
}

/***
 *  Find positions in s with start of reversed s.substr(start, end-start).
 *  Simple string comparison, no palindrome checks at all.
 *  Calls action for each one found, sums up returns of these action calls.
 *  @returns the sum of the action calls
 */
int findRawMinusX(const int start, const int end, int (*action)(int startS2)) {
    int ret = 0;
    auto v = revidx[s.substr(start, end-start)];
    for (auto it = lower_bound(v.begin(), v.end(), end); it != v.end(); it++) {
        ret += action(*it);
    }
#ifdef DEBUG
    //cout << "log findRawMinusX(), start=" << start << " end=" << end << " ret=" << ret << endl;
#endif
    return ret;
}

#ifdef DEBUG
void dumpmatch(const palmatch &pm) {
    cout << pm.s1start << " " << pm.s1len << " " << pm.s2start << " " << pm.s2len << " " << s.substr(pm.s1start, pm.s1len) << " "
         << s.substr(pm.s2start, pm.s2len) << endl;
}
void logmatch(string prefix, int s1start, int s1len, int s2start, int s2len) {
    palmatches.push_back( { s1start, s1len, s2start, s2len });
    cout << prefix << " ";
    dumpmatch(palmatches.back());
}
#endif

int gstart, gend, gpsecond;
/***
 *  Find strings starting with s[end], check each one if palindrome with (start,end]
 *  1. x  : -x
 *  2. xC : -x
 *  3. x  : C-x
 */
int findMinusX(const int start, const int end) {
#ifdef DEBUG
    //cout << "log findMinuxX(), start=" << start << " end=" << end << endl;
#endif
    gstart = start;
    gend = end;
    int ret = findRawMinusX(start, end, [](int startS2) {
        int innerret=1;	// simple case x : -x
#ifdef DEBUG
        logmatch("log pair1.1= ", gstart, gend-gstart, startS2, gend-gstart);
#endif
        if(startS2>gend) { // cases "xC : -x" and "x : C-x"
            innerret+=2;
#ifdef DEBUG
            logmatch("log pair2.1= ", gstart, gend-gstart, startS2-1, gend-gstart+1);
            logmatch("log pair2.2= ", gstart, gend-gstart+1, startS2, gend-gstart);
#endif
        }
        return innerret;
    });

    return ret;
}

int findPminusX(const int start, const int end) {
#ifdef DEBUG
    cout << "log findPminuxX(), start=" << start << " end=" << end << endl;
#endif
    int ret = 0;
    auto it = lower_bound(pld.begin(), pld.end(), pair<int, int> { end, -1 }, byFirst);
    for (; it != pld.end(); it++) { // check each palindrome right of end if the string after it is -x
        auto p = *it;
#ifdef DEBUG
        cout<<"log matches palindrome 1, start="<<p.first<<" len="<<p.second<<endl;
#endif
        bool match = true;
        int slen = (int) s.length();
        for (int i = 0; i < end - start; i++) {
            if (p.first + p.second + i >= slen || s[end - 1 - i] != s[p.first + p.second + i]) {
                match = false;
                break;
            }
        }
        if (match) {
            ret++;
#ifdef DEBUG
            logmatch("log pair3.1= ", start, end - start, p.first, p.second + end - start);
#endif
        }
    }

    /* check if a postfix of [start, end) is one of the palindromes, and match the non-palindrome-part. */
    it = lower_bound(pld.begin(), pld.end(), pair<int, int> { start+1, -1 }, byFirst);
    for (; it != pld.end(); it++) { // check each palindrome right of start if it is a postfix of [start, end)
        auto p = *it;
#ifdef DEBUG
        cout<<"log matches palindrome 2, start="<<p.first<<" len="<<p.second<<endl;
        gstart=start;
        gend=end;
        gpsecond=p.second;
#endif
        if(p.first>=end)	/* since p is right of end this p and all follwing are not part of [start,end) */
            break;
        if(p.first+p.second!=end)	/* since len does not match p is not postfix */
            continue;

        ret += findRawMinusX(start, end-p.second, [](int startS2) {
            if(startS2>=gend) {
#ifdef DEBUG
                logmatch("log pair3.2= ", gstart, gend - gstart, startS2, gend-gstart-gpsecond);
#endif
                return 1;
            } else {
                return 0;
            }
        });
    }

    return ret;
}

/** Finds the number of s2 fitting to s1. */
int findSecond(const int start, const int end) {
    /** we search all pairs of form (x=any string, p=palindromesubstring, -x=reverseX)
     * where x starts at end or more right.
     * 	 1. x  -> -x; string/reversestring
     * 	 2. x -> C-x; string/anysinglechar/reversestring
     * 	 3. x -> p-x; string/palindrome/reversestring
     * 	 3. xQ -> P-x; where Q is the prefix of palindromesubstring, and P is the palindrome (without the prefix)
     */
#ifdef DEBUG
    //cout << "log findSecond begin" << endl;
#endif
    int ret1 = findMinusX(start, end);
    int ret2 = findPminusX(start, end);
#ifdef DEBUG
    //cout << "log findSecond for start=" << start << " end=" << end << " ret1=" << ret1 << " ret2=" << ret2 << endl;
#endif
    return ret1 + ret2;
}

#ifdef DEBUG
bool byPalmatch(const palmatch &pm1, const palmatch &pm2) {
    if (pm1.s1start != pm2.s1start)
        return pm1.s1start < pm2.s1start;
    else if (pm1.s1len != pm2.s1len)
        return pm1.s1len < pm2.s1len;
    else if (pm1.s2start != pm2.s2start)
        return pm1.s2start < pm2.s2start;
    else
        return pm1.s2len < pm2.s2len;
}

void dumpPalmatches() {
    int i = 1;
    for (auto it = palmatches.begin(); it != palmatches.end(); it++) {
        cout << i++ << ". ";
        dumpmatch(*it);
    }
}
#endif

int main() {
    cin >> s;
#ifdef DEBUG
    cout << "s=" << s << endl;
#endif

    indexPalindromes();
    indexRev();

    ll ans = 0;
    for (uint i = 0; i < s.size() - 1; i++) {
        for (uint j = i + 1; j <= s.length(); j++) { // all substrings [i, j)
            ans += findSecond(i, j);
#ifdef DEBUG
            //cout << "ans=" << ans << " @ i=" << i << " j=" << j << endl;
#endif
        }
    }
#ifdef DEBUG
    sort(palmatches.begin(), palmatches.end(), byPalmatch);
    dumpPalmatches();
#endif
    cout << ans << endl;

    return 0;
}
