/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

const int N=500001;
set<int> tree[N];
vector<int> w(N);

void solve() {
    fori(N)  {
        tree[i].clear();
        w[i]=0;
    }

    int n, m;
    cin>>n>>m;

    fori(n)
        cin>>w[i];

    fori(m) {
        int u, v;
        cin>>u>>v;
        tree[u-1].insert(v-1);
        tree[v-1].insert(u-1);
    }
/* Find an initial set of nodes in the graph, such that there exists a sequence of nodes which,
 * when removed from the graph, do _not_ separate it into two parts.
 * If multiple sets possible, choose the cheapest one.
 *
 * We can go other way: Choose the set of all nodes, remove subsequent unneeded ones.
 * Leaf: Node with only one edge.
 * If there are N leafs in the tree, we need to cure that N-1 leafs on first step.
 * Because if we would not, we would have to cure the vertex connected to the leaf
 * before the leaf, wich would make the leaf disconnected. This is possible only for the last leaf.
 * 50 Point Subtask should be solvable with that: Simply choose all but the most expensive
 * leaf at first, then remove leafs until no more exist.
 * 30 Points: That should work for w[i]=1, too.
 */
    vector<int> leafs;
    int ma=-1;
    int maIdx=-1;
    int lastLeaf;
    fori(n) {
        if(tree[i].size()==1) {
            leafs.push_back(i);
            if(w[i]>ma) {
                ma=w[i];
                maIdx=leafs.size()-1;
                lastLeaf=i;
            }
        }
    }
    if(leafs.size()==0) {
        cout<<-1<<endl;
        exit(0);
    }
    leafs.erase(leafs.begin()+maIdx);
//cout<<"leafs.size()="<<leafs.size()<<" ma="<<ma<<" maIdx="<<maIdx<< " leafs="<<endl;

    // now remove leafs, which produces new leafs...which are removed...until no more leafs exist.
    // How to produce new leafs? Brute force, remove edges from tree.
    vector<int> next;
    cout<<leafs.size()<<endl;
    while(leafs.size()>0) {
//cout<<"leafs.size()="<<leafs.size()<<" ma="<<ma<<" maIdx="<<maIdx<< " leafs="<<endl;
//for(auto l : leafs) cout<<l+1<<" ";
//cout << endl;

        for(auto leaf : leafs) {
//cout<<"log leaf="<<leaf<<endl;
            int parent = *tree[leaf].begin();
            tree[parent].erase(leaf);
            cout<<leaf+1<<" ";
            if(tree[parent].size()==1)
                next.push_back(parent);
        }
        leafs=next;
        next.clear();
    }
    cout<<lastLeaf+1<<endl;
    
}

int main() {
    int t;
    cin>>t;
    while(t--)
        solve();
}

