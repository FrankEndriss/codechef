/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

#define RANDTEST

/** We need a testing tool. TODO
 * 1. A small board, say 8x8
 * 2. random pawns.
 * 3. Create permutations of all possible moves
 * 4. Count remaining pawns for all permutations.
 * 5. Run same board against algo.
 * 6. Report if any permutation creates better result than algo.
 * 7. Adjust algo, run again.
 * -create boards by seed, count seed up from 1 to infinity.
 * -if nessecary, try with bigger board.
 * -permutate number of pawns, too.
 */

/*
 * Consider:
 *
 * ...O...
 * ..O.O..
 * .O.O.O.
 * O.O.O.O
 * How to find that the row with the three has to be removed firstly?
 *
 * We use a pointing system.
 * STATES::
 * Fixed: An unmovable position, allways saved.
 * Unsaveable: A position moving to a Fixed.
 * Saveable: A position which can be saved, depending on the order of removals.
 * POINTS: activeP and passiveP
 * targetP: Number of possitions this pawn can be moved to.
 * activeP: Points a position saves if removed early
 * passivP: Points an active earns when saving this position.
 *
 * Fixed, a=0, p=0;
 * Unsaveable: p=0, a=-NumSaves;
 * Saveable: p=2, a=-NumSaves;
 *
 * Cascade Saves: Remove One, and all connected to saves by that one, and so on...
 *
 *
 */
vector<pair<int,int>> gRandomMoves;

bool hasLeftTarget(vector<vector<bool>> &b, int row, int col) {
    return b[row][col] && ((col>0 && row>0 && b[row-1][col-1]));
}

bool canMove(vector<vector<bool>> &b, int row, int col) {
    return b[row][col] && ((col>0 && row>0 && b[row-1][col-1]) || (col<(int)b.size()-1 && row>0 && b[row-1][col+1]));
}

/** @return the number of consecutive pawns (of distance 2) starting at row/col which, if removed all at once, fix
 * some consecutive pawns of the previous row. The number of fixed pawns is returned  through
 * the "fixes" paremeter.
 */
int removeGroupSize(vector<vector<bool>> &b, int row, int col, int &fixes) {
    const int n=b.size();
    fixes=0;
    int ret=0;
    if(!b[row][col] || !canMove(b, row, col) || row>=n-1)
        return 0;

/* first check if there is a left fixable pawn, and if that one will be fixed
 * by removing row/col. */
    if(col>0 && row<n-1 && b[row+1][col-1]) {   // there is a left fixable pawn
        if(col<2 || !b[row][col-2])     //  which has not other target left of row/col, so it will be fixed
            fixes++;
    }

    int count=0;
/* next check all consecutive pawn right of row/col */
    while(col<n-1 && b[row][col] && canMove(b, row, col) && b[row+1][col+1] && (col>n-3 || !b[row][col+2] || canMove(b, row, col+2))) {
        fixes++;
        col+=2;
        count++;
    }

    return max(fixes, count);
    
}

typedef struct removeGroup {
    int fixes, size, row, col;
} removeGroup;

void findRemoveGroups(vector<vector<bool>> &b, vector<removeGroup> &ret) {
    int n=b.size();
    for(int row=n-2; row>=0; row--) {
        for(int col=0; col<n; col++) {
            int fixes;
            int remSize=removeGroupSize(b, row, col, fixes);
            if(remSize>0) {
                ret.push_back({ fixes, remSize, row, col });
                col+=2*remSize;
            }
        }
    }
}

int solveByGroups(vector<vector<bool>> &b) {
    vector<removeGroup> groups;
    findRemoveGroups(b, groups);
cout<<"found groups, #="<<groups.size()<<endl;
    sort(groups.begin(), groups.end(), [](auto t1, auto t2) {
        return -t1.fixes<-t2.fixes;
    });

    ostringstream oss;
    int moves=0;
    for(auto &p : groups) {
        for(int i=0; i<p.size; i++) {
            if(canMove(b, p.row, p.col+i*2)) {
                b[p.row][p.col+i*2]=false;
                moves++;
                if(hasLeftTarget(b, p.row, p.col+i*2))
                    oss<<p.row+1<<" "<<p.col+i*2+1<<" L"<<endl;
                else
                    oss<<p.row+1<<" "<<p.col+i*2+1<<" R"<<endl;

            }
        }
    }
    cout<<moves<<endl;
    cout<<oss.str()<<endl;
    return moves;
}

/* (re)move one random pawn. ret false if no move possible, else true; */
bool randomMove(vector<vector<bool>> &b) {
    vector<pair<int, int>> moves;
    for(uint row=1; row<b.size(); row++) {
        for(uint col=0; col<b.size(); col++) {
            if(b[row][col]) {
                if((col>0 && b[row-1][col-1]) ||  (col<b.size()-1 && b[row-1][col+1]) )
                    moves.push_back({ row, col });
            }
        }
    }
    if(moves.size()==0)
        return false;

    int idx=rand()%moves.size();
    b[moves[idx].first][moves[idx].second]=false;
    gRandomMoves.push_back(moves[idx]);
    return true;
}

// Odd fields: odd colnums in odd rownums, even colnums in even rownums
/* places count pawns on random odd fields in the field */
void randomPawns(vector<vector<bool>> &b, int count) {
    vector<vector<bool>> f(b.size(), vector<bool>(b.size()/2, false));

    int c=0;
    while(c<count) {
        auto posX=rand()%b.size()/2;
        auto posY=rand()%b.size();
        if(!f[posY][posX]) {
            f[posY][posX]=true;
            c++;
        }
    }
    for(uint row=0; row<b.size(); row++) {
        int offs=row%2;
        for(uint col=0; col<b.size()/2; col++)
            b[row][col*2+offs]=f[row][col];
    }
}

int reallyFixTwo(vector<vector<bool>> &b) {
    const int n=b.size();
    int ans=0;
    for(int row=1; row<n-1; row++) {
        for(int col=1; col<n-2; col++) {
            if(b[row][col] &&
                    (b[row-1][col-1] || b[row-1][col+1]) && // can move
                    (b[row+1][col-1] && (col<2 || !b[row][col-2])) && // really fixes left
                    (b[row+1][col+1] && (col>=n-2 || !b[row][col+2]))) {  // really fixes right
                b[row][col]=false;
                ans++;
            }
        }
    }
    return ans;
}

typedef struct points {
    int targets;
    int act;
    bool unsaveable;
} ppoints;


struct tagpawn { int w, row, col; };
vector<struct tagpawn> pawns;

bool pawnsByW(int i1, int i2) {
    cout<<"step 4.4, in sort,i1="<<i1<<" i2="<<i2<<" pawns.size()="<<pawns.size()<<" i1.w="<<pawns[i1].w<<" i2.w="<<pawns[i2].w<<endl;
        assert(i1>=0);
        assert(i2>=0);
        assert(i1<pawns.size());
        assert(i2<pawns.size());
    return pawns[i2].w<pawns[i1].w;
};

int collectPoints(vector<vector<bool>> &b) {
    const int n=b.size();
    vector<vector<ppoints>> points(n);
    for(int row=0; row<n; row++) {
        for(int col=0; col<n; col++) {
            points[row].push_back({ 0, 0, false});
        }
    }


    /* Step1, targets, count the number of possible move-to positions. */
    for(int row=n-1; row>0; row--) {
        for(int col=0; col<n; col++) {
            if(b[row][col]) {   // pawn exists
                if(row>0) {
                    if(col>0 && b[row-1][col-1])    // can move left
                        points[row][col].targets++;
                    if(col<n-1 && b[row-1][col+1])  // can move right
                        points[row][col].targets++;
                }
            }
        }
    }
//cout<<"after step 1"<<endl;

    /* Step2, passive, points another one earns saveing this one. */
    for(int row=n-1; row>0; row--) {
        for(int col=0; col<n; col++) {
            if(b[row][col]) {   // pawn exists
                if(row>0 && col>0 && b[row-1][col-1] && points[row-1][col-1].targets==0)    // left target fixed
                    points[row][col].unsaveable=true;
                if(row>0 && col<n-1 && b[row-1][col+1] && points[row-1][col+1].targets==0)    // right target fixed
                    points[row][col].unsaveable=true;
            }
        }
    }
//cout<<"after step 2"<<endl;

    /* Step3, active points, mark pawns potentially saving others. */
    for(int row=n-2; row>0; row--) {
        for(int col=0; col<n; col++) {
            if(b[row][col]) {   // pawn exists
                if(row<n-2 && col>0 && b[row+1][col-1])     // save left
                    points[row][col].act+= points[row+1][col-1].unsaveable?0:2/points[row+1][col-1].targets;
                if(row<n-2 && col<n-1 && b[row+1][col+1])   // save right
                    points[row][col].act+= points[row+1][col+1].unsaveable?0:2/points[row+1][col+1].targets;
            }
        }
    }
//cout<<"after step 3"<<endl;

    /* Step 4, sort pawns by points */
    pawns.clear();
    for(int row=0; row<n; row++) {
        for(int col=0; col<n; col++) {
            if(b[row][col]) {   // pawn exists
//cout<<"step 4.1, row="<<row<<" col="<<col<<" points.size()="<<points.size()<<endl;
                pawns.push_back({ points[row][col].act, row, col });
            }
        }
    }
    vector<uint> id(pawns.size());
    for(uint i=0; i<pawns.size(); i++)
        id[i]=i;

//cout<<"step 4.2, will sort"<<endl;
/*
    sort(id.begin(), id.end(), pawnsByW);
*/
    sort(id.begin(), id.end(), [&](int i1, int i2) {
        assert(i1>=0);
        assert(i2>=0);
        assert(i1<pawns.size());
        assert(i2<pawns.size());
        cout<<"step 4.3, in sort,i1="<<i1<<" i2="<<i2<<endl;
        return pawns[i2].w<pawns[i1].w;
        //return i1>=i2;
    });

    cout<<"after step 4"<<endl;
    ostringstream oss;
    int ans=0;
    for(int i : id) {
        //for(pair<int, pair<int, int>> &p : pawns) {
        auto &p = pawns[i];
cout<<"step 5.1, row="<<p.row<<" col="<<p.col<<" targets="<<points[p.row][p.col].targets<<" w="<<p.w<<endl;
        if(points[p.row][p.col].targets>0 && p.col>0 && b[p.row-1][p.col-1]) { // move left
            b[p.row][p.col]=false;
cout<<"rm left"<<endl;
            oss<<p.row+1<<" "<<p.col+1<<" L"<<endl;
            ans++;
        } else if(points[p.row][p.col].targets>0 && p.col<n-1 && b[p.row-1][p.col+1]) { // move right
cout<<"rm right"<<endl;
            b[p.row][p.col]=false;
            oss<<p.row+1<<" "<<p.col+1<<" R"<<endl;
            ans++;
        }
    }

#ifndef RANDTEST
    cout<<ans<<endl;
    cout<<oss.str();
#endif
//cout<<"after step 5"<<endl;
    return ans;
}

int solveB(vector<vector<bool>> &b) {
    /* A fixed pawn is one wich cannot delete any pawn.
     * Create as much of them as possible.
     */
    ostringstream oss;
    int ans=0;
    int n=b.size();

    ans+=reallyFixTwo(b);

    // move all which really fixes a pawn
    for(int row=n-2; row>0; row--) {
        for(int col=1; col<n-2; col++) {
            if(b[row][col]) {  // is pawn
                if(b[row-1][col-1] || b[row-1][col+1]) { // can move
                    if((b[row+1][col-1] && col>=2 && !b[row][col-2]) || (b[row+1][col+1] && col<=n-3 && !b[row][col+2])) { // fixes left or right
                        b[row][col]=false;
                        ans++;
                        if(b[row-1][col+1])
                            oss<<row+1<<" "<<col+1<<" R"<<endl;
                        else
                            oss<<row+1<<" "<<col+1<<" L"<<endl;
                    }
                }
            }
        }
    }

    /*
        // move all which fixes one pawns, starting at _bottom_
        for(int row=n-2; row>0; row--) {
            for(int col=0; col<n; col++) {
                if(b[row][col]) {  // is pawn
                    if((col>0 && b[row-1][col-1]) || (col<n-1 && b[row-1][col+1])) { // can move
                        if((col>0 && b[row+1][col-1]) || (col<n-1 && b[row+1][col+1])) { // fixes one
                            b[row][col]=false;
                            ans++;
                            if(col<n-1 && b[row-1][col+1])
                                oss<<row+1<<" "<<col+1<<" R"<<endl;
                            else
                                oss<<row+1<<" "<<col+1<<" L"<<endl;
                        }
                    }
                }
            }
        }
    */


    // move all moveable, starting at 1
    for(int row=1; row<n; row++) {
        for(int col=0; col<n; col++) {
            if(b[row][col]) {
                if(col>0 && b[row-1][col-1]) {
                    b[row][col]=false;
                    oss<<row+1<<" "<<col+1<<" L"<<endl;
                    ans++;
                } else if(col<n-1 && b[row-1][col+1]) {
                    b[row][col]=false;
                    oss<<row+1<<" "<<col+1<<" R"<<endl;
                    ans++;
                }
            }
        }
    }
#ifndef RANDTEST
    cout<<ans<<endl;
    cout<<oss.str();
#endif

    return ans;
}

void solve() {
    int n;
    cin>>n;
    vector<string> bs(n);
    for(int i=0; i<n; i++)
        cin>>bs[i];

    vector<vector<bool>> b(n, vector<bool>(n, false));
    for(int y=0; y<n; y++)
        for(int x=0; x<n; x++)
            b[y][x]=bs[y][x]!='.';

    solveByGroups(b);
}

int solveRandom(vector<vector<bool>> &b) {
    int c=0;
    gRandomMoves.clear();
    while(randomMove(b))
        c++;
    return c;
}

void dumpB(vector<vector<bool>> &b) {
    for(uint y=0; y<b.size(); y++) {
        for(uint x=0; x<b.size(); x++)
            cout<<(b[y][x]?'O':'.');
        cout<<endl;
    }
}

void dumpGmoves() {
    for(uint i=0; i<gRandomMoves.size(); i++)
        cout<<"("<<gRandomMoves[i].first<<","<<gRandomMoves[i].second<<") ";
    cout<<endl;
}

int main() {
#ifdef RANDTEST
    srand(42);
    const int sz=8;
    for(int i=0; i<10000; i++) {
        //cout<<"check "<<i<<endl;
        vector<vector<bool>> b1(sz, vector<bool>(sz, false));
        randomPawns(b1, 4+rand()%20);
        vector<vector<bool>> b2=b1; // copy
        vector<vector<bool>> b3=b1; // copy again

        //int m1=collectPoints(b1);
        int m1=solveByGroups(b1);
        int m2=solveRandom(b2);
        if(m2<m1) {
            cout<<"found it!!!. Before:"<<endl;
            dumpB(b3);
            cout<<endl<<"After:"<<endl;
            dumpB(b2);
            cout<<endl<<"Algo:"<<endl;
            dumpB(b1);
            dumpGmoves();
            exit(0);
        }
    }
#else
    int t;
    cin>>t;
    while(t--)
        solve();
#endif
}

