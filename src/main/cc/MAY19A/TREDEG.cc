/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

#define MOD 998244353

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
    return mul(zaehler, inv(nenner));
}

int plus(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

void solve() {
    int n, k;
    cin>>n>>k;

/* The sum of degrees in a tree with N nodes is (N-1)*2 since every edge connects two nodes.
 * Every nodes min degree is 1, so we have N-2 "free" edge-ends wich can be distributed to
 * virtually any other node. (Note that we cannot build cylcles with N-1 edges.)
 * So, the number of possible trees is pow(n, n-2)
 *
 * Distribution-Product: The DP of a tree is d1*d2*...*dn
 * We need to count and sum the DPs (powed by k) of all trees.
 * Brute-Force:
 * N time     d1=n-2, dx=1              { n-2, 0, 0, 0, ...}
 * N*N-1 time d1=n-3, d2=2, dX=1        { n-3, 1, 0, 0, ...}
 * N*N-1*N-2  d1=n-4, d2=2, d3=2, dX=1  { n-4, 1, 1, 0, ...}
 *                                      { n-4, 2, 0, 0, ...}
 *
 */

1-2: 2
1-2-3: 6
1-2-3-4: 24   ie n!/2

}

int main() {
    int t;
    cin>>t;
    while(t--)
        solve();
}

