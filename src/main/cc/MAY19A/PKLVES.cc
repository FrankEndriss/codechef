/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;


const int MOD=998244353;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

int fraction(const int zaehler, const int nenner) {
    return mul(zaehler, inv(nenner));
}

int plus(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}


#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

const int N=300003;
vector<int> parent(N);      /* parent of node[i] */
vector<int> tree[N];        /* list of childs of node[i]. */
vector<int> leafs;          /* list of all leafs in tree, in order. */
vector<int> level(N);       /* level of node[i] in tree */

vector<unordered_map<int, int>> distmemo(N);
//vector<vector<int>> distmemo(N, vector<int>(N));
vector<unordered_map<int, pair<ll, ll>>> innermemo(N);
//vector<vector<pair<ll, ll>>> innermemo(N, vector<pair<ll, ll>>(N));
//vector<vector<pair<ll, ll>>> outermemo(N);

int distInTree(int u, int v) {
    int dist=distmemo[u][v];
    if(dist>0)
        return dist;
    int levelU=level[u];
    int levelV=level[v];
    while(levelU>levelV) {
        dist++;
        levelU--;
        u=parent[u];
    }
    while(levelV>levelU) {
        dist++;
        levelV--;
        v=parent[v];
    }
    while(u!=v) {
        dist+=2;
        u=parent[u];
        v=parent[v];
    }
    return distmemo[u][v]=dist;
}

void dfs(int node, int depth) {
    level[node]=depth;
    for(auto c : tree[node])
        if(c!=parent[node])
            dfs(c, depth+1);
}

int main() {
    int n, q;
    cin>>n>>q;
    for(int i=2; i<=n; i++) {
        cin>>parent[i];
        tree[i].push_back(parent[i]);
        tree[parent[i]].push_back(i);
    }
    for(int i=2; i<=n; i++)  {
        if(tree[i].size()==1) {
//cout<<"push leaf="<<i<<endl;
            leafs.push_back(i);
        }
    }

    dfs(1, 1);

    // how to quickly index the distances?

    int ans=0;
    fori(q) {
        int x, y;
        cin>>x>>y;
        x^=ans;
        y^=ans;

        assert(x<=n);
        assert(y<=n);

        auto firstLeaf=lower_bound(leafs.begin(), leafs.end(), x);
        auto lastLeaf =upper_bound(leafs.begin(), leafs.end(), y);

        int lastLeafIdx;
        if(lastLeaf==leafs.end())
            lastLeafIdx=-42;
        else
            lastLeafIdx=*(lastLeaf-1);

        ll dans=0;
        ll cans=0;
        for(; firstLeaf!=lastLeaf; firstLeaf++) {
            ll cInner=0;
            ll dInner=0;
            auto m=innermemo[*firstLeaf][lastLeafIdx];
            if(m.first>0) {
                cInner=m.first;
                dInner=m.second;
            } else {
                for(auto j=firstLeaf+1; j!=lastLeaf; j++) {
                    cInner++;
                    int dist=distInTree(*firstLeaf, *j);
                    dInner+=dist;
//cout<<"distance from "<<*firstLeaf<<" to "<<*j<<" ="<<dist<<endl;
                }
                innermemo[*firstLeaf][lastLeafIdx]={ cInner, dInner };
            }
            cans+=cInner;
            dans+=dInner;
        }

//cout<<"cans="<<cans<<" dans="<<dans<<endl;
        ans=fraction(dans, cans);
        cout<<ans<<endl;
    }

}

