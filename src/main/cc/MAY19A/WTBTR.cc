/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

const double PI4=3.14159265358979323846264338/4;
const double MPI4=-PI4;
const double COS=cos(PI4);
const double MCOS=cos(MPI4);
const double SIN=sin(PI4);
const double MSIN=sin(MPI4);


void solve() {
    int n;
    cin>>n;

    vector<pair<ll, ll>> p;
    vector<int> id1;
    vector<int> id2;

    fori(n) {
        ll x, y;
        cin>>x>>y;
        p.push_back({x, y});
        id1.push_back(i);
        id2.push_back(i);
    }

    sort(id1.begin(), id1.end(), [&](int i1, int i2) {
        return p[i1].first*COS-p[i1].second*SIN<p[i2].first*COS-p[i2].second*SIN;
    });

    sort(id2.begin(), id2.end(), [&](int i1, int i2) {
        return p[i1].first*MCOS-p[i1].second*MSIN<p[i2].first*MCOS-p[i2].second*MSIN;
    });

    vector<ll> d;
    ll mad=1000000007;
    fori(n-1) {
        ll d1=abs(abs(p[id1[i]].first-p[id1[i+1]].first)-abs(p[id1[i]].second-p[id1[i+1]].second));
        ll d2=abs(abs(p[id2[i]].first-p[id2[i+1]].first)-abs(p[id2[i]].second-p[id2[i+1]].second));

        if(d1==0 || d2==0) {
            cout<<"0"<<endl;
            return;
        }
        mad=min(mad, min(d1, d2));

    }

    cout<<fixed<<setprecision(2)<<mad/2.0<<endl;
}

int main() {
    int t;
    cin>>t;
    fori(t)
        solve();
}

