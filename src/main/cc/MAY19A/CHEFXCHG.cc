/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

const int N=103;

vector<ll> B;        // credit balance of people in currency 1
vector<vector<ll>> W;     // wallets[user][currency] of people
vector<ll> xchg;
vector<int> xID;

int people, currencies, requests;

void readResp() {
    int resp;
    cin>>resp;
    if(resp!=1)
        exit(1);
}

/* Initialize peoples strategy.
 * Buy all currencies, pay depts afterwards using the most valueable investments.
 */
int afterFirst(vector<string> &ops) {
//cout<<"log afterFirst()"<<endl;
    int count=0;
    for(int i=1; i<currencies; i++)
        if(xchg[i]==1)
            count++;

    int ans=0;
    ostringstream oss;
    if(count>0) {
        for(int p=1; p<=people; p++) {
            int amount=W[p][0]/(count+1);
            for(int c=1; c<currencies; c++) {
                if(xchg[c]==1) {
                    ans++;
                    oss<<"XCHG "<<p<<" "<< c+1<<" "<<amount<<endl;
                    W[p][c]+=amount;
                    W[p][0]-=amount;
                }
            }
        }
    }
    ops.push_back(oss.str());
    return ans;
}

int req0(vector<string> &ops) {
int ocount=0;
    ll a, b, k;
    cin>>a>>b>>k;
//cout<<"log req0, a="<<a<<" b="<<b<<" k="<<k<<endl;
    B[a]-=k;
    B[b]+=k;
    ostringstream oss;
    for(int cid=currencies-1; cid>=0; cid--) {
        int curr=xID[cid];
        int amount=min(W[a][curr], k/xchg[curr]);
//cout<<"log user a available of c="<<curr+1<<" am="<<W[a][curr]<<endl;
//cout<<"log xchg[c]="<<xchg[curr]<<endl;
//cout<<"log amount="<<amount<<endl;
        if(amount>0) {
            B[a]+=amount*xchg[curr];
            W[a][curr]-=amount;
            B[b]-=amount*xchg[curr];
            W[b][curr]+=amount;
            k-=amount*xchg[curr];
            oss<<"SEND "<<a<<" "<<b<<" "<<curr+1<<" "<<amount<<endl;
            ocount++;
        }

        if(k==0)
            break;
    }
    assert(k==0);

    ops.push_back(oss.str());
    return ocount;
}

int req1(vector<string> &ops) {
    int c, x;
    cin>>c>>x;
    c--;
//cout<<"log  req1, c="<<c<<" x="<<x<<endl;
    xchg[c]=x;

    sort(xID.begin(), xID.end(), [](auto idx1, auto idx2) {
        if(xchg[idx1]==xchg[idx2])
            return idx1<idx2;
        else
            return xchg[idx1]<xchg[idx2];
    });

/*
cout <<"log currencies in order, curr/xchg"<<endl;
for(int i=0; i<currencies; i++)
    cout<<xID[i]<<"/"<<xchg[xID[i]]<<" ";
cout<<endl;
*/
    return 0;
}

int main() {
    cin>>people>>currencies>>requests;

    B=vector<ll>(people+1);
    W=vector<vector<ll>>(people+1, vector<ll>(currencies));
    for(int i=1; i<=people; i++)
        W[i][0]=1000000000;

    xchg=vector<ll>(currencies, 1);
    xID=vector<int>(currencies);
    for(int i=0; i<currencies; i++)
        xID[i]=i;

    bool first=true;
    for(int i=0; i<requests; i++) {
        vector<string> ops;
        int opsCount=0;
        int t;
        cin>>t;
        if(t==0)
            opsCount+=req0(ops);
        else
            opsCount+=req1(ops);

        if(first) {
            opsCount+=afterFirst(ops);
            first=false;
        }
        cout<<opsCount<<endl;
        for(auto s:ops)
            cout<<s;
        readResp();
    }


}

