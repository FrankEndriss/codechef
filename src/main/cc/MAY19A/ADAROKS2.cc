
/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

//#define DEBUG

void answer(vector<vector<bool>> &f, int n) {
    int ans=0;
    for(int row=0; row<n; row++) {
        for(int col=0; col<n; col++) {
            cout<<(f[row][col]?"O":".");
            if(f[row][col])
                ans++;
        }
        cout<<endl;
    }

#ifdef DEBUG
    cout<<n*8<<endl;
    cout<<ans<<endl;
#endif
}

bool check(vector<vector<bool>> &f, int n) {
    bool ret=true;
    for(int row=0; row<n; row++) {
        for(int col=0; col<n; col++) {
            if(!f[row][col])
                continue;

            // left upper sector
            for(int y=0; y<row; y++) {
                for(int x=0; x<col; x++) {
                    if(f[row][x] && f[y][x] && f[y][col]) {
                        cout<<"checkfail row="<<row<<" col="<<col<<" y="<<y<<" x="<<x<<endl;
                        ret=false;
                    }
                }
            }
            // right upper sector
            for(int y=0; y<row; y++) {
                for(int x=col+1; x<n; x++) {
                    if(f[row][x] && f[y][x] && f[y][col]) {
                        cout<<"checkfail row="<<row<<" col="<<col<<" y="<<y<<" x="<<x<<endl;
                        ret=false;
                    }
                }
            }
            // left downer sector
            for(int y=row+1; y<n; y++) {
                for(int x=0; x<col; x++) {
                    if(f[row][x] && f[y][x] && f[y][col]) {
                        cout<<"checkfail row="<<row<<" col="<<col<<" y="<<y<<" x="<<x<<endl;
                        ret=false;
                    }
                }
            }
            // right downer sector
            for(int y=row+1; y<n; y++) {
                for(int x=col+1; x<n; x++) {
                    if(f[row][x] && f[y][x] && f[y][col]) {
                        cout<<"checkfail row="<<row<<" col="<<col<<" y="<<y<<" x="<<x<<endl;
                        ret=false;
                    }
                }
            }
        }
    }
    return ret;
}

void solve() {
int n;
    cin>>n;
    vector<vector<bool>> f(n, vector<bool>(n));
    
    // first row
    for(int c=0; c<n; c++)
        f[0][c]=c%7==0;
    
    for(int row=1; row<n; row++) {
        for(int col=row%7; col<n; col+=7) {
            bool good=true;
            for(int k=0; k<col; k++) { // check
                for(int j=1; j<=row; j++) {
                    if(f[row][k] && f[row-j][k] && f[row-j][col]) {
                        good=false;
                        break;
                    }
                }
                if(!good)
                    break;
            }
            f[row][col]=good;
        }
    }
    answer(f, n);
}

void solve2() {
int n;
    cin>>n;
    vector<vector<bool>> f(n, vector<bool>(n));

    int prim[]={ 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 57, 59, 61, 67 };
    int x=0;
    for(int pidx=0; x<n; x+=prim[pidx], pidx++) {
        int y=0;
        int lx=x;
        while(lx<n && y<n)
            f[y++][lx++]=true;
    };

    int y=0;
    for(int pidx=3; y<n; y+=prim[pidx], pidx++) {
        int x=0;
        int ly=y;
        while(x<n && ly<n)
            f[ly++][x++]=true;
    };

    check(f, n);
    answer(f, n);
}

bool canSet(vector<vector<bool>> &f, const int n, const int nRow, const int nCol) {
    for(int row=0; row<n; row++) {
        if(row==nRow)
            continue;
        if(f[row][nCol]) {
            for(int col=0; col<n; col++) {
                if(col==nCol)
                    continue;
                if(f[row][col] && f[nRow][col])
                    return false;
            }
        }
    }
    for(int col=0; col<n; col++) {
        if(col==nCol)
            continue;
        if(f[nRow][col]) {
            for(int row=0; row<n; row++) {
                if(row==nRow)
                    continue;
                if(f[row][col] && f[row][nCol])
                    return false;
            }
        }
    }
    return true;
}

void solve3() {
int n;
    cin>>n;
    vector<vector<bool>> f(n, vector<bool>(n, false));
    int cnt=0;
    srand(123);
    for(int i=0; i<n; i++) { // init
        f[i][i]=true;
        cnt++;
        if(i+2<n) {
            f[i+2][i]=true;
            cnt++;
        }
        if(i+3<n) {
            f[i][i+3]=true;
            cnt++;
        }
/*
        if(i+5<n) {
            f[i][i+5]=true;
            cnt++;
        }
*/
        if(i+7<n) {
            f[i][i+7]=true;
            cnt++;
        }
/*
        if(i+11<n) {
            f[i+11][i]=true;
            cnt++;
        }
*/
        if(i+13<n) {
            f[i+13][i]=true;
            cnt++;
        }
        if(i+19<n) {
            f[i+19][i]=true;
            cnt++;
        }
        if(i+41<n) {
            f[i][i+41]=true;
            cnt++;
        }
        if(i+37<n) {
            f[i+37][i]=true;
            cnt++;
        }
        if(i+49<n) {
            f[i+49][i]=true;
            cnt++;
        }
    }
    while(cnt<n*8) {
        int x=rand()%n;
        int y=rand()%n;
        if(!f[y][x] && canSet(f, n, y, x)) {
            f[y][x]=true;
            cnt++;
#ifdef DEBUG
cout<<"cnt="<<cnt<<endl;
#endif
        }
    }
#ifdef DEBUG
    if(!check(f, n)) {
        cout<<"checkfail"<<endl;
        return;
    }
#endif
    answer(f, n);
}

int main() {
int t;
    cin>>t;
    while(t--)
        solve3();

}

