/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

/* Subtask#2: The tree is a single linked list.
 *
 * Subtask#3: 
 */
#include <bits/stdc++.h>
using namespace std;

const bool unsyncedio=std::ios::sync_with_stdio(false);
typedef unsigned int uint;
typedef const int cint;
typedef long long ll;
typedef const long long cll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

//#define DEBUG

const int N=100003;
vector<int> tree[N];    /* node[i]== childs of node i */
int n;                  /* number of nodes in tree */

vector<int> parent(N);  // parent[i]==parent of node i
vector<int> level(N);   // length of path to/from root

vector<ll> k(N);
vector<ll> b(N);

ll quality(cint i, cint j) {
    auto ret= k[i]*j+b[i];
#ifdef DEBUG
cout<<"quality of type="<<j<<" in city="<<i<<" is: "<<ret<<endl;
#endif
    return ret;
}

void dfsParent(cint node, cint par, int depth) {
    parent[node]=par;
    level[node]=depth;
#ifdef DEBUG
cout<<"parent["<<node<<"]="<<par<<endl;
#endif
    for(auto c : tree[node]) 
        if(c!=par)  
            dfsParent(c, node, depth+1);
}

void indexParents() {
// Consider any node being the root.
// So simply use first one available.
// Alternativ is to burn tree from the leafs, last node burning
// is the shortest path to all leafs, so a good choice for root.

    int root=-1;
    fori(n) 
        if(tree[i].size()>0) {
            root=i;
            break;
        }

    dfsParent(root, 0, 1);
}
    
void doInPath(int u, int v, function<void (int)> action) {
    int levelU=level[u];    
    int levelV=level[v];
    while(levelU>levelV) {
        action(u);
        levelU--;
        u=parent[u];
    }
    while(levelV>levelU) {
        action(v);
        levelV--;
        v=parent[v];
    }
    while(u!=v) {
        action(u);
        u=parent[u];
        action(v);
        v=parent[v];
    }
    action(u);
}

/* On shortest path from u to v, update all nodes k[node]+=x and b[node]+=y */
void q1(int u, int v, cll x, cll y) {
#ifdef DEBUG
cout<<"upd u="<<u<<" v="<<v<<endl;
#endif
    doInPath(u, v, [&](int node) {
#ifdef DEBUG
cout<<"upd node="<<node<<endl;
#endif
        k[node]+=x;
        b[node]+=y;
    });
}

cll INF=1000000000000000007LL;

/* On shortest path from u to v, select max(quality(node, z)). */
void q2(cint u, cint v, cint z) {
#ifdef DEBUG
cout<<"findmax u="<<u<<" v="<<v<<endl;
#endif
    
    ll ma=-INF;
    doInPath(u, v, [&](cint node) {
#ifdef DEBUG
cout<<"findmax node="<<node<<endl;
#endif
        ma=max(ma, quality(node, z));
    });
    cout<<ma<<endl;;
}


int main() {
int q;
    cin>>n>>q;
    fori(n)
        cin>>k[i+1];
    fori(n)
        cin>>b[i+1];
    fori(n-1) {
        int u, v;
        cin>>u>>v;
        tree[u].push_back(v);
        tree[v].push_back(u);
    }
    indexParents();

    fori(q) {
        int t, u, v;
        cin>>t>>u>>v;
        if(t==1) {
            int x, y;
            cin>>x>>y;
            q1(u, v, x, y);
        } else {
            int z;
            cin>>z;
            q2(u, v, z);
        }
    }
}

