
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that order of the array does not
 * make any difference.
 * So make all values min of choice, then sort.
 * Then consecutivly swap the values of the leftmost
 * element, and recheck.
 *
 * WA, why?
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);
    cinai(b,n);

    multiset<pii> m;
    for(int i=0; i<n; i++) 
        m.emplace(min(a[i],b[i]), max(a[i],b[i]));

    int ans=INF;
    for(int i=0; i<=n; i++) {
        auto it=m.begin();
        auto [mi,s]=*it;
        auto [ma,t]=*m.rbegin();
        m.erase(it);
        m.emplace(s, mi);
        ans=min(ans, ma-mi);
    }
    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
