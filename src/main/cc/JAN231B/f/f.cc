
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * KSIZEGCD
 *
 * Consider the divisors of a[0]
 * To check for a seq of len=2 we have to check only those divisors
 * on a[1]...
 *
 * Observe, once we know the max gcd of len k is y, we need to 
 * check only few positions and values, that is all positions
 * with gcd>1 on len k, and then check only divisors lteq y.
 * ...hmm
 *
 * Maintain tuples of <gcd,start> and <start,gcd> of non overlapping 
 * positions.
 * That is, in first round its <a[i],i>
 * In second round we add next position to all tuples.
 * But we store only the ones <start,gcd>
 *
 * ****
 * From the other end:
 * Consider searching all longest subarrays with gcd>1.
 * So for all longer ones ans=1
 * And then?
 * **************
 *
 * idk :/
 */
void solve() {

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
