
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * COUNTING
 *
 * I guess we should find number of
 * multiples of a in L,R, same for b,
 * and number of multiples of a AND b in L,R,
 * to subtract that number.
 *
 * ...But, that only applies to max(a,b)<=L
 *
 * So, what if a,b >L
 * We need to consider divisors of a,b
 * -> Make it (at least) two probs.
 *
 * What about a,b==1?
 * How does definition of coprime work here?
 * -> I guess all numbers in range contribute.
 *
 * What about L,R in range 1...1e9
 * ...
 * Actually we have to consider 3 ranges/parts of L,R:
 * 1.: L..min(a,b)-1
 * 2.: min(a,b) .. max(a,b)-1
 * 3.: max(a,b) .. R
 *
 *
 *
 */
void solve() {
    vi ab(2);
    cin>>ab[0]>>ab[1];
    cini(L);
    cini(R);

    if(ab[0]>ab[1])
        swap(ab[0],ab[1]);

    if(ab[0]==1 || ab[1]==1) {
        cout<<R-L+1<<endl;
        return;
    }

    set<int> d;  /* divisors of a  and b */
    vector<set<int>> d2(2);
    for(int i=0; i<2; i++) {
        for(int j=2; j*j<=ab[i]; j++) {
            if(ab[i]%j==0) {
                d2[i].insert(j);
                d2[i].insert(ab[i]/j);
                d.insert(j);
                d.insert(ab[i]/j);
            }
        }
    }

    /* part 1, solution for interval max(L, max(a,b)),R */
    int ans3=0;
    int LL=max(ab[1],L);
    if(LL<=R) {
        ans3=R-LL+1;

        /* Then subtract all multiples of a */
        ans3-=(R/ab[0])-((LL-1)/ab[0]);
        /* Then subtract all multiples of b */
        ans3-=(R/ab[1])-((LL-1)/ab[1]);
        /* Then add all multiples of a AND b */

        const int lc=lcm(ab[0],ab[1]);
        ans3+=(R/lc)-((LL-1)/lc);
    }

    /* part1, find numbers in L,ab[0]-1 that are divisors of
     * ab[0] and/or ab[1].
     */
    int ans1=0;
    if(L<ab[0]) {
        ans1=ab[0]-L;
        for(int dd : d) 
            if(dd>=L && dd<ab[0])
                ans1--;
    }

    /* part2, range ab[0]..ab[1]-1
     * find numbers in L,R that are multiples of divisors of
     * ab[0], ab[1], or both.
     * ...but think again about... small a, big b...
     * which cases exist?
     **/
    int ans2=0;
    LL=max(L, ab[0]);
    int RR=min(R, ab[1]-1);
    if(LL<=RR) {
        ans2=RR-LL+1;
        /* remove multiples of ab[0] */
        ans2-=RR/ab[0];
        ans2+=(LL-1)/ab[0];

        /* remove divisors of ab[1], but not if multiple of ab[0] */
        for(int dd : d2[1]) {
            if(dd>=LL && dd<=RR && dd%ab[0]!=0)
                ans2--;
        }
    }
    int ans=ans1+ans2+ans3;
    cout<<ans<<endl;
}

signed main() {
        solve();
}