
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We can move values only left,
 * so we can make any prefix like
 * x+1, x+1, x+1,..., x,x,...
 * where the sum of all x+1 and x is the prefix
 * sum of the array.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,n);

    vi preR(n+1, 0);
    for(int i=0; i<n; i++) 
        preR[n-i-1]=max(preR[n-i], a[n-1-i]);

    int ans=INF;
    int sum=0;
    int ma=0;
    for(int i=0; i<n; i++) {
        sum+=a[i];      
        int x_1=(sum+i)/(i+1);
        x_1=max(x_1, ma);
        //cerr<<"sum="<<sum<<" x_1="<<x_1<<endl;
        ans=min(ans, max(x_1, preR[i+1]));
        ma=max(ma, x_1);
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}