
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We can move all allements to any position.
 * Prove:
 * if we want to move any b[] to a[0] we can simply swap.
 * Else if we want to move any a[1..n-1] to a[0], we
 * can swap that a[j] with b[0], then b[0] with a[0].
 *
 * So since we have to put n elements into a, we need to 
 * find the min spread segment of size n in all numbers.
 *
 * But WA. Why?
 * -> not that we need to iterate all left ends, which is
 *  0..n, not 0..n-1.
 */
const int INF=1e9;
void solve() {
    cini(n);
    cinai(a,2*n);
    sort(all(a));

    int ans=INF;
    for(int i=0; i<=n; i++) 
        ans=min(ans, a[i+n-1]-a[i]);

    cout<<ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
