
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that the swaps allways move the 2 smallest elements
 * leftmost.
 *
 * Consider permutation and indexing 0 based:
 * So, with 0 shift
 * 0,1,a[2],a[3]...,a[0],...,a[1],...
 *
 * But, since it is a permutation, we can simply construct all
 * possible outcomes on the fly, and compare.
 *
 * ...seems to complecated :/
 */
int n;
vi a;
vi epos;

void solve() {
    a.clear();
    epos.clear();
    cin>>n;
    for(int i=0; i<n; i++) {
        cini(aux);
        a.push_back(aux-1);
    }

    if(n<=4) {
        for(int i=1; i<=n; i++)
            cout<<i<<" ";
        cout<<endl;
        return;
    }

    epos=vi(n);
    for(int i=0; i<n; i++)
        epos[a[i]]=i;    /* position of elements */

    /* abstraction of an iterator over a rotated and
     * mostly twice swapped array based on a.
     */
    struct suba { 
        int off;    /* offset */
        int pos;    /* current position */
        vector<pii> ch;      /* <pos,newval> changed elements */

        suba(int poff):off(poff),pos(0) {
        }

        int next() {
            const int idx=(off+pos)%n;
            int v=a[idx];
            for(size_t i=0; i<ch.size(); i++) {
                if(ch[i].first==idx) {
                    v=ch[i].second;
                    //cerr<<"swapped pos="<<ch[i].first<<" nval="<<v<<endl;
                    break;
                }
            }
            //cerr<<"next(), idx="<<idx<<" pos="<<pos<<" v="<<v<<endl;

            if(v==pos) {    /* optimum, no swap */
                pos++;
                return pos-1;
            } else if(ch.size()==0) {    /* we can simply swap */
                //cerr<<"ch0, pos="<<pos<<" epos[pos]="<<epos[pos]<<" v="<<v<<endl;
                ch.emplace_back(epos[pos], v);
                pos++;
                return pos-1;
            } else if(ch.size()==1) {   /* consider the allready done swap */
                /* Two cases:
                 * 1. the element v=pos was swapped in first swap,
                 *    and therefore is at position sw[0].first
                 *    -> swap again
                 * 2. else the element 'pos' is at epos[pos]
                 */
                if(ch[0].second==pos) { 
                    //cerr<<"ch1, pos="<<pos<<" ch[0].first="<<ch[0].first<<" v="<<v<<endl;
                    ch[0].second=v;
                    //cerr<<"ch1 set ch[0].first="<<ch[0].first<<" second="<<v<<endl;
                    ch.emplace_back(-1,-1); /* insert dummy for second swap */
                    pos++;
                    return pos-1;
                } else {
                    //cerr<<"ch1, pos="<<pos<<" epos[pos]="<<epos[pos]<<" v="<<v<<endl;
                    ch.emplace_back(epos[pos], v);
                    pos++;
                    return pos-1;
                }
            }
            pos++;
            return v;
        }
    };

    vi id(n);
    iota(all(id), 0);

    int ans=*min_element(all(id), [&](int i1, int i2) {
        suba s1(i1);
        suba s2(i2);
        for(int i=0; i<n; i++) {
            int v1=s1.next();
            int v2=s2.next();
            if(v1<v2)
                return true;
            else if(v1>v2)
                return false;
        }
        return false;
    });

    //cerr<<"ans="<<ans<<endl;

    suba sans(ans);
    for(int i=0; i<n; i++)
        cout<<sans.next()+1<<" ";

    cout<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}