/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

void solve() {
    int n; cin>>n;
    vector<int> b(n);
    fori(n)
        cin>>b[i];
    vector<double> p(n);
        cin>>p[i];
}

int main() {
int t;
    cin>>t;
    while(t--)
        solve();

}

