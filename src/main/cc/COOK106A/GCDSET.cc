/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

void solve() {
    ll l, r, g;
    cin>>l>>r>>g;

    ll lg=l;
    while(lg%g!=0)
        lg+=g-lg%g;

    ll ans=0;
    if(r>=lg)
        ans=1+(r-lg)/g;

    if(ans==1 && lg!=g)
        ans=0;

    cout<<ans<<endl;
}

int main() {
int t; cin>>t;
    while(t--)
        solve();

}

