/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 *
 * AND WATCH OUT FOR OFF BY ONE!!!
 */

#include <bits/stdc++.h>
const bool unsyncedio=std::ios::sync_with_stdio(false);
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll (i)=0; (i)<(n); (i)++)

typedef struct node {
    int parent;
    unordered_map<int, int> c2d;    // c2d[i]= min distance to child of color i
} node;

void solve() {
    vector<node> tree(200001);
    vector<bool> known(200001, false);  // known colors
    int nodes=1;

    int q, c0;
    cin>>q>>c0;
    tree[1].parent=-1;
    tree[1].c2d[c0]=0;
    known[c0]=true;
    int ans=-1;
    fori(q) {
        string t;
        int u, c;
        cin>>t>>u>>c;
//cout<<"decq1 u="<<u<<" c="<<c<<" ans="<<ans<<endl;
        u=u ^ (ans+1);
        c=c ^ (ans+1);

//cout<<"decq2 u="<<u<<" c="<<c<<endl;

        if(t[0]=='+') {
            known[c]=true;
            nodes++;
            tree[nodes].parent=u;
            tree[nodes].c2d[c]=0;
            int node=nodes;
            int dist=1;
            while((node=tree[node].parent)>0) {
                auto got=tree[node].c2d.find(c);
                if(got==tree[node].c2d.end())
                    tree[node].c2d[c]=dist;
                else {
                    if(dist<got->second)
                        got->second=dist;
                    else
                        break;
                }
                dist++;
            }
        } else {
            // find min dist to any node with color c
            if(known[c]) {
                int minD=1000000;
                int ld=0;
                do {
                    auto got=tree[u].c2d.find(c);
                    if(got!=tree[u].c2d.end())
                        minD=min(minD, got->second+ld);
                    ld++;
                    if(ld>=minD)
                        break;
                } while((u=tree[u].parent)>0);

                if(minD==1000000)
                    ans=-1;
                else
                    ans=minD;
            } else
                ans=-1;

            cout<<ans<<endl;
            cout.flush();
        }
    }
}

int main() {
    int t;
    cin>>t;
    while(t--)
        solve();

}

