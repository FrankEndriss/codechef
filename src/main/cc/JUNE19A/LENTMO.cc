/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

void solve() {
    int n;
    cin>>n;
    vector<ll> a(n);

    ll sumA=0;
    fori(n) {
        cin>>a[i];
        sumA+=a[i];
    }

    int k, x;
    cin>>k>>x;

    vector<ll> b(n);   // negativ gain if switched
    fori(n)
        b[i]=a[i]-(a[i]^x);

/* Find rules on how we can switch the bags.
 * 1. We can switch k elements.
 * 2. If x elements are switched, we can switch x+k
 * 2. If x elements are switched, we can switch x-k, x-(k-2), x-(
 *
 */
    set<int> p; // number of possible switched elements of a
    if(k==n) { // switch all or nothing
        p.insert(n);
    } else if(k%2==0) {    // switch 2, 4, 6,...
        for(int i=2; i<=n; i+=2)
            p.insert(i);
    } else {    // switch any number
        for(int i=1; i<=n; i++)
            p.insert(i);
    }

    sort(b.begin(), b.end());
    
    ll ans=sumA;
    int idx=0;
    for(auto it=p.begin(); it!=p.end(); it++) {
        while(idx<*it)
            sumA-=b[idx++];

        ans=max(ans, sumA);
    }

    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

