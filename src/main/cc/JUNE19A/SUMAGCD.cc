/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

void solve() {
    int n;
    cin>>n;
    vector<int> a(n);
    fori(n)
        cin>>a[i];

    sort(a.begin(), a.end(), greater<int>());
    auto it=unique(a.begin(), a.end());
    a.resize(distance(a.begin(),it));

    if(a.size()==1) {  // all a[i] are equal
        cout<<2*a[0]<<endl;
        return;
    }

    int g1;
    int g2;
    int ans=a[0]+1;
    for(uint i=0; i<a.size(); i++) {
        if(a[i]*2<ans)
            break;
        g1=a[i];
        g2=-1;
        for(uint j=0; j<a.size(); j++) {
            if(i==j)
                continue;
            if(g2==-1)
                g2=a[j];
            else
                g2=__gcd(g2, a[j]);
            if(g2==1)
                break;
        }
        ans=max(ans, g1+g2);
    }

    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

