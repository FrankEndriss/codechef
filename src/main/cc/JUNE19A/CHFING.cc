/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;
typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

#define DEBUG

const int MOD=1000000007;

lll gauss(lll n) {
    return ((n*(n+1))/2)%MOD;
}

void solve() {
    ll n, k;
    cin>>n>>k;  // n ingredients, starting at k

/* We need to find the sum of (k-1)-(1*n) + (k-1)-(2*n) + (k-1)-(3*n) + ...
 * Every summand is >=0.
 */

/*
    ll ans=k-1;
    n--;
    ll nn=n;
    while(nn<k) {
        ans+=k-1-nn;
        nn+=n;
        ans%=MOD;
    }
*/

    lll d=(k-1)/(n-1);
    lll ans=((d+1)*(k-1))%MOD - (gauss(d)*(n-1))%MOD;
    while(ans<0)
        ans+=MOD;

#ifdef DEBUG
#endif
    ans%=MOD;
    
    cout<<(ll)ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);

    int t;
    cin>>t;
    while(t--)
        solve();
}

