/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

#define DEBUG
const int MOD=1000000007;

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int n, q;
    cin>>n>>q;
    vector<int> s(n);
    vector<int> sameLeft(n, -1);
    unordered_map<int, int> lastPos;
    for(int i=0; i<n; i++) {
        cin>>s[i];
        auto ent=lastPos.find(s[i]);
        if(ent!=lastPos.end()) {
            sameLeft[i]=ent->second;
            ent->second=i;
        } else
            lastPos[s[i]]=i;
    }
    
    ll ans=0;
    fori(q) {
        ll l1, l2, r1, r2;
        cin>>l1>>l2>>r1>>r2;
        ll l=(l1 * ans + l2)%n;
        ll r=(r1 * ans + r2)%n;
        if(l>r)
            swap(l, r);
        
/* query for number of single values in s[l] to s[r].
 *  This number is f(r-l+1) - sum_f(multiples)
 *  Where sum_f() is sum(f(num_of_multiples))
 *
 *  -> We need some kind of sqrt decomposition.
 */
#ifdef DEBUG
cout<<"query l="<<l<<" r="<<r<<endl;
#endif

        /* brute force */
        ans=1;
        ll d=r-l+1;
        vector<bool> vis(r-l+1, false);
        for(int j=(int)r; j>l; j--) {
            if(vis[j-l])
                continue;

/* TODO How to calculate number correct?
 * if in example one number is contained three times, result = n!/3!
 * But we cannot use division :/
 */
            int idx=j;
            int c=0;
            ll m=1;
            while(idx>=l) {
                vis[idx-l]=true;
                m*=(d-c);
                m%=MOD;
                idx=sameLeft[idx];
            }
            ans*=m;
            ans%=MOD;
            d--;
        }
        cout<<ans<<endl;
    }

}

