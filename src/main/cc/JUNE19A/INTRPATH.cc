/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;

//#define DEBUG

/* LCA based on euler tour with segment tree. */
struct LCA {
    vector<int> height, euler, first, segtree;
    vector<bool> visited;
    int n;

    LCA(vector<vector<int>> &adj, int root = 0) {
        n = (int)adj.size();
        height.resize(n);
        first.resize(n);
        euler.reserve(n * 2);
        visited.assign(n, false);
        dfs(adj, root);
        int m = (int)euler.size();
        segtree.resize(m * 4);
        build(1, 0, m - 1);
    }

    void dfs(vector<vector<int>> &adj, int node, int h = 0) {
        visited[node] = true;
        height[node] = h;
        first[node] = (int)euler.size();
        euler.push_back(node);
        for (auto to : adj[node]) {
            if (!visited[to]) {
                dfs(adj, to, h + 1);
                euler.push_back(node);
            }
        }
    }

    void build(int node, int b, int e) {
        if (b == e) {
            segtree[node] = euler[b];
        } else {
            int mid = (b + e) / 2;
            build(node << 1, b, mid);
            build(node << 1 | 1, mid + 1, e);
            int l = segtree[node << 1], r = segtree[node << 1 | 1];
            segtree[node] = (height[l] < height[r]) ? l : r;
        }
    }

    int query(int node, int b, int e, int L, int R) {
        if (b > R || e < L)
            return -1;
        if (b >= L && e <= R)
            return segtree[node];
        int mid = (b + e) >> 1;

        int left = query(node << 1, b, mid, L, R);
        int right = query(node << 1 | 1, mid + 1, e, L, R);
        if (left == -1) return right;
        if (right == -1) return left;
        return height[left] < height[right] ? left : right;
    }

    /* @return the lca of u and v */
    int lca(int u, int v) {
        int left = first[u], right = first[v];
        if (left > right)
            swap(left, right);
        return query(1, 0, (int)euler.size() - 1, left, right);
    }
};

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

void solve() {
    int n, q;
    cin>>n>>q;

    vector<vector<int>> tree(n+1);
    tree[1].push_back(0);   // artificial parent of ROOT
    fori(n-1) {
        int u, v;
        cin>>u>>v;
        tree[u].push_back(v);
        tree[v].push_back(u);
    }

    const int ROOT=1;
    vector<int> level(n+1);         /* level of node */
    vector<int> parent(n+1);        /* parent nodes */
    vector<ll> paths(n+1);          /* num nodes in subtree (ie for leaf==1) */
    vector<vector<ll>> cpaths(n+1); /* num perfectly crossing paths of this node, per child. */
    vector<vector<ll>> cpaths2root(n+1);    /* num perfect crossing paths on path from this node to root. */

    int maxLevel=0;
    function<void(int, int, int)> dfs=[&](int v, int p, int lv) {
        maxLevel=max(maxLevel, lv);
        level[v]=lv;
        parent[v]=p;
        paths[v]=1;
        cpaths[v].assign(tree[v].size(), 1);

        for(int c : tree[v]) {
            if(c!=p) {
                dfs(c, v, lv+1);
                paths[v]+=paths[c];
            }
        }
#ifdef DEBUG
        cout<<"v="<<v<<" paths="<<paths[v]<<endl;
#endif

/* For any child, multiply cpaths by paths of all siblings.
 * Note we do this for parent as well, which is the number of cpaths
 * if node is at end of path.
**/
        for(uint i=0; i<tree[v].size(); i++) {
            int c=tree[v][i];
            cpaths[v][i]=1;
            for(uint j=0; j<tree[v].size(); j++)  {
                int s=tree[v][j];
                if(s!=c && s!=p) {
                    cpaths[v][i]*=(paths[s]+1);
                }
            }
#ifdef DEBUG
            cout<<"cpaths["<<v<<"]["<<i<<"]="<<cpaths[v][i]<<endl;
#endif
        }
    };
    dfs(ROOT, 0, 1);
#ifdef DEBUG
cout<<"after dfs 1"<<endl;
#endif

/* Add up crossing paths top down.
 */
    function<void(int, int)> dfs2=[&](int vidx, int p) {
        int v=tree[p][vidx];
        cpaths2root[v].assign(tree[v].size(), 0);
#ifdef DEBUG
cout<<"add up cpaths for v="<<v<<" tree[v].size()="<<tree[v].size()<<" cpaths[v].size()="<<cpaths[v].size()<<" cpaths2root[v].size()="<<cpaths2root[v].size()<<endl;
cout<<"p="<<p<<" vidx="<<vidx<<" cpaths2root[p].size()="<<cpaths2root[p].size()<<endl;
#endif
        for(uint i=0; i<tree[v].size(); i++) {
            cpaths2root[v][i]=cpaths[v][i];
            if(p!=0)
                cpaths2root[v][i]+=cpaths2root[p][vidx];
            if(tree[v][i]!=p)
                dfs2(i, v);
        }
    };
    tree[0].push_back(1);
    dfs2(0, 0);

#ifdef DEBUG
cout<<"after dfs 2"<<endl;
#endif

    LCA lca(tree, ROOT);

#ifdef DEBUG
cout<<"after lca"<<endl;
#endif

    int logL=2;
    while((1<<logL)<=maxLevel)
        logL++;

#ifdef DEBUG
cout<<"logL="<<logL<<endl;
#endif

    vector<vector<int>> succ(n+1, vector<int>(logL+1));
    vector<int> log(maxLevel+1);
    for(int i=1; i<=maxLevel; i++)
        log[i]=log[i/2]+1;
#ifdef DEBUG
cout<<"after log table"<<endl;
#endif

/* @return Kth successor of x */
    function<void(int, int)> calcSuc=[&](int x, int logK) {
        if(logK==1) {
            succ[x][1]=parent[x];
            return;
        }
        //return succ[x][log[k]]=calcSuc(calcSuc(x, k/2), k/2);
        int suc2=succ[x][logK-1];
        succ[x][logK]=succ[suc2][logK-1];
    };

    int lk=1;
    while(lk<=maxLevel) {
        for(int i=1; i<=n; i++) {
            calcSuc(i, lk);
        }
        lk<<=1;
    }
#ifdef DEBUG
cout<<"after calcSuc for all"<<endl;
#endif

    /* returns the Kth successor of v */
    function<int(int, int)> getSuc=[&](int v, int k) {
#ifdef DEBUG
cout<<"getSuc, v="<<v<<" k="<<k<<endl;
#endif
        while(k>0) {
            v=succ[v][log[k]];
            k-=(1<<(log[k]-1));
        };
        return v;
    };

/* @return the idx of the parent of v in tree[v][] */
    function<uint(int)> indexOfParent=[&](int v) {
        for(uint i=0; i<tree[v].size(); i++)
            if(tree[v][i]==parent[v])
                return i;
        assert(false);
    };

/* @return the idx of v in the parent of v in tree[parent[v]][]) */
    function<uint(int)> indexOfChild=[&](int v) {
        for(uint i=0; i<tree[parent[v]].size(); i++)
            if(tree[parent[v]][i]==v)
                return i;
        assert(false);
    };


    fori(q) {
#ifdef DEBUG
cout<<"************* Query"<<endl;
#endif
        int u, v;
        cin>>u>>v;
        int p=lca.lca(u, v);
#ifdef DEBUG
        cout<<"u="<<u<<" v="<<v<<" p="<<p<<" parent[u]="<<parent[u]<<endl;
        cout<<" idxOfAdj="<<indexOfParent(u)<<endl;
#endif

        ll ans=0;
        if(u!=p)
            ans+=cpaths2root[u][indexOfParent(u)];
#ifdef DEBUG
        cout<<"1 ans="<<ans<<" level[u]="<<level[u]<<" level[p]="<<level[p]<<endl;
#endif
        int pp1=-1;
        if(level[u]>level[p]) {
            pp1=getSuc(u, level[u]-level[p]-1);
            ans-=cpaths2root[p][indexOfChild(pp1)];
#ifdef DEBUG
        cout<<"2 ans="<<ans<<" pp1="<<pp1<<" indexOfChild(pp1)="<<indexOfChild(pp1)<<endl;
#endif
        }
        if(v!=p)
            ans+=cpaths2root[v][indexOfParent(v)];
#ifdef DEBUG
        cout<<"3 ans="<<ans<<endl;
        cout<<"3.2 level[v]="<<level[v]<<" level[p]="<<level[p]<<endl;
#endif
        int pp2=-1;
        if(level[v]>level[p]) {
            pp2=getSuc(v, level[v]-level[p]-1);
            ans-=cpaths2root[p][indexOfChild(pp2)];
#ifdef DEBUG
        cout<<"4 ans="<<ans<<endl;
#endif
        }

/* Finally add the crossing paths of the lca. */
        ll lcaC=1;
        for(auto c : tree[p]) {
            if(c!=pp1 && c!=pp2)
                lcaC*=(paths[c]+1);
        }
#ifdef DEBUG
        cout<<"5 ans="<<ans<<" pp1="<<pp1<<" pp2="<<pp2<<" lcaC="<<lcaC<<endl;
#endif
        ans+=lcaC;

        cout<<ans<<endl;
    }



}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();

}

