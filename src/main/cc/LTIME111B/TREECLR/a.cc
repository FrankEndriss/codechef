
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;
#include <atcoder/modint>
using namespace atcoder;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Use any root wich c colors, all other vertex can have c-1 colors.
 * ...Path len is number of edges, so
 * dp, start with number of used colors, which is 0, 1 or 2.
 *
 * Note that there are trees with very short longest paths.
 */
using mint=modint1000000007;
void solve() {
    cini(n);
    cini(c);

    vvi adj(n);
    for(int i=0; i+1<n; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }

    /* vertex, parent, used colors in dist1 and dist2 */
    function<mint(int,int,int,int)> dfs=[&](int v, int p, int c1, int c2) {
        mint ans=max(0LL, c-c1-c2);
        int cnt=0;
        for(int chl : adj[v]) {
            if(chl!=p) {
                ans*=dfs(chl, v, 1, c1+cnt);
                cnt++;
            }
        }
        return ans;
    };

    mint ans=dfs(0, -1, 0, 0);
    cout<<ans.val()<<endl;
}

signed main() {
       solve();
}
