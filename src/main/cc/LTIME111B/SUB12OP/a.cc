
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that if a[i+1] is positive the operation decreases the sum.
 * Also, if both are positive, in decreases by 3.
 *
 * Greedy from left to right, use all possible 3-ops or two ops.
 * The again left to right use all 1 ops.
 *
 * Guess thats sol, but most likely there are edgecases.
 * How to prove anything here?
 */
void solve() {
    cini(n);
    cinai(a,n);

    //cerr<<"n="<<n<<endl;
    for(int i=0; i+1<n; i++) {
        if(a[i]>0 && a[i+1]>0) {
            int cnt=min(a[i], a[i+1]/2);
            //cerr<<"i1="<<i<<" cnt="<<cnt<<endl;
            a[i]-=cnt;
            a[i+1]-=cnt*2;
        }
    }

    for(int i=1; i<n; i++) {
        if(a[i]>1 || (a[i]>0 && a[i-1]>0)) {
            int cnt=(1+a[i])/2;
            //cerr<<"i2="<<i<<" cnt="<<cnt<<endl;
            a[i-1]-=cnt;
            a[i]-=cnt*2;
        }
    }

    int ans=0;
    for(int i=0; i<n; i++)
        ans+=abs(a[i]);

    cout<<ans<<endl;
}

signed main() {
   cini(t);
   for(int i=1; i<=t; i++) {
       solve();
   }
}
