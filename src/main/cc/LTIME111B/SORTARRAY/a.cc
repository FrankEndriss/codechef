
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

/* consider atan2(y,x) to find angle between x-axis and segment {(0,0),(x,y)} */

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider first element.
 * It can never be made smaller, so
 * if there is any smaller element right of it, that element must
 * be increased to some value for sure.
 *
 * However, we can update the prefix of the array to some position
 * where a[j]==a[i], and then go on starting at pos=j+1.
 * So thats a dp.
 * go(i)=min number of possible operations to sort the postfix 
 *  startring at i.
 */
const int INF=1e9+7;
void solve() {
    cini(n);
    cinai(a,n);

    vvi pos(n+1);
    for(int i=0; i<n; i++)
        pos[a[i]].push_back(i);

    vi dp(n+1, -1);
    dp[n]=0;
    function<int(int)> go=[&](int i) {
        if(dp[i]>=0)
            return dp[i];

        int ans=INF;
        if(i+1==n || a[i+1]>=a[i])
            ans=go(i+1);

        auto it=upper_bound(all(pos[a[i]]), i);

        while(it!=pos[a[i]].end()) {
            if(*it==n-1 || a[*it+1]>=a[i])
                ans=min(ans, 1+go(*it+1));

            it++;
        }
        return dp[i]=ans;
    };

    int ans=go(0);
    if(ans>=INF)
        ans=-1;
    cout<<ans<<endl;
}

signed main() {
   cini(t);
   for(int i=1; i<=t; i++) {
       //cout<<"Case #"<<i<<": ";
       solve();
   }
}
