
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Obviously, n-1 moves work.
 * Can we do better?
 * We need to find a[i], that is xor of some other consecutive elements.
 * Actually, the whole array is split into some blocks, where the xor
 * of each block is same.
 * Consider the block of numbers starting at a[0]
 * ...???
 * ****
 * It is not xor, it is AND !
 * So, each bit is set in all numbers, or it is 0 in the blocksums.
 */
const int N=31;
void solve() {
    cini(n);
    cinai(a,n);

    vi cnt(N);
    for(int i=0; i<n; i++) {
        for(int j=0; j<N; j++) {
            cnt[j]+=((a[i]>>j)&1);
        }
    }

    int m=0;
    for(int i=0; i<N; i++) 
        if(cnt[i]==n)
            m|=(1LL<<i);

    int ans=0;  /* count of 0 blocks */
    int sum=~0LL;
    for(int i=0; i<n; i++) {
        sum=sum&a[i];
        if(sum==m) {
            ans++;
            sum=~0LL;
        }
    }

    cout<<n-ans<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}
