
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to mark each 1st of an increasing subarray, 
 * then count the marks in intervals.
 */
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);

    cerr<<"n="<<n<<" q="<<q<<endl;
    if(n==1) {
        cout<<1<<endl;
        return;
    }

    vi inc(n);
    inc[0]=1;
    for(int i=1; i<n; i++) {
        if(a[i]>a[i-1])
            inc[i]=0;
        else
            inc[i]=1;
    }

    vi pre(n+1);
    for(int i=0; i<n; i++) 
        pre[i+1]=pre[i]+inc[i];

    for(int i=0; i<q; i++) {
        cini(l); l--;
        cini(r); 
        cerr<<"l="<<l<<" r="<<r<<endl;
        cout<<pre[r]-pre[l]<<endl;
    }
}

signed main() {
        solve();
}