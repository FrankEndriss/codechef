
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

#include <atcoder/segtree>
using namespace atcoder;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


using S=int; /* type of values */

S st_op(S a, S b) {
    return min(a,b);
}

const S INF=1e9;
S st_e() {
    return INF;
}

using stree=segtree<S, st_op, st_e>;

/**
 * We need to mark each 1st of an increasing subarray, 
 * then count the marks in intervals.
 * ...No, not so simple.
 * First, it is not increasing subarray, but consecutive
 * subarray, ie a[i]+1==a[i+1]
 * Then, by choosing a range l,r we change which elements
 * are consecutive.
 *
 * So, a pair of adj numbers are consecutive increasing, if
 * -they are increasing,
 * -and all numbers in between are not in the range
 * Foreach pair we can find the left/right border for the 
 * range in O(logN)
 * Then, how to find the number of such pairs in a range? Mo?
 *
 * Foreach range we want to find the number of segments 
 * that include that range completly. How?
 * Left must be lteq, right must be bgeq. How?
 *
 * Lets call the segments surounding.
 * Consider thow know 
 * -the number of left endpoints in the range,
 * -the number of right endpoints in the range,
 * -the number of active segment at the border of the range
 * Then number of surounding segments is 
 * allSegments - numberOfRightEndLeftOfRange - numberOfLeftEndRightOfRange
 *             + numberOfRightEndInRange - numberOfActiveAtLeftBorder
 */
void solve() {
    cini(n);
    cini(q);
    cinai(a,n);

    vector<pii> p(n-1);
    /* find foreach pair the left and right border */
    int l=0;
    for(int i=
}

signed main() {
        solve();
}
