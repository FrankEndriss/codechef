/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

void solve() {
    int n, k, d;
    cin>>n>>k>>d;
    vi x(n);
    fori(n) {
        cin>>x[i];
    }
    vi l(n);
    fori(n) {
        cin>>l[i];
    }

    int ans=0;
    for(int lane : { 1, 2 }) {
        int minS=0;
        bool bumped=false;
        for(int i=0; i<n; i++) {
            if(lane==l[i]) {
                if(x[i]<=minS) {
                    ans=max(ans, x[i]);
                    bumped=true;
                    break;
                }
                minS+=d;
                minS=max(minS, x[i]+1);

                if(lane==1)
                    lane=2;
                else
                    lane=1;
            } else {
                minS=max(minS, x[i]+1);
            }
        }
        if(!bumped) {
            ans=k;
            break;
        }

    }
    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

