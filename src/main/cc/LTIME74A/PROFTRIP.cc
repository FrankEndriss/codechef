/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef pair<int, ll> pill;
typedef pair<ll, int> plli;
typedef pair<ll, ll> pllll;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<ll> vll;
typedef vector<string> vs;

const ll INF=1e18;

//#define DEBUG
void solve() {
    int n, r;
    cin>>n>>r;
    vector<vector<pill>> graph(n);
    fori(r) {
        int u, v, w;
        cin>>u>>v>>w;
        u--;
        v--;
        graph[u].push_back({v, w});
        graph[v].push_back({u, w});
    }
    vll f(n);
    fori(n) {
        cin>>f[i];
    }
    int p, q;
    cin>>p>>q;
    p--;    // from city
    q--;    // to city

    vector<vector<pllll>> dp(n);    // min money, fuelprice

    function<bool(int, pllll)> merge=[&](int idx, pllll p) {
        for(auto it=dp[idx].begin(); it!=dp[idx].end(); it++) {
            if((it->first<=p.first && it->second<p.second) || 
                (it->first<p.first && it->second<=p.second))
                    return false;
        }

        for(auto it=dp[idx].begin(); it!=dp[idx].end(); it++) {
            if(( it->first>=p.first && it->second>p.second ) ||
                (it->first>p.first && it->second>=p.second))
                    it=dp[idx].erase(it);
            if(it==dp[idx].end())
                break;
        }

        for(auto it=dp[idx].begin(); it!=dp[idx].end(); it++) {
            if(it->first==p.first && it->second==p.second)
                return false;
        }

        dp[idx].push_back(p);
        return true;
    };

    dp[p].push_back({ 0, f[p] });

    priority_queue<plli> qu; // -money, city
    qu.push({ 0LL, p });

    int qOnQ=0;
    while(qu.size()) {
        auto node=qu.top();
        qu.pop();
        int ncity=get<1>(node);
#ifdef DEBUG
cout<<"ncity="<<ncity<<endl;
#endif
        if(ncity==q) {
            qOnQ--;
            if(qOnQ==0) {
                ll nmoney=INF;
                for(auto p1 : dp[q]) 
                    nmoney=min(nmoney, p1.first);
                cout<<nmoney<<endl;
                return;
            }
        }

        for(auto c : graph[ncity]) {
            for(auto p1 : dp[ncity]) {
                ll nextmoney=p1.first+p1.second*c.second;
                ll nextfuel=min(p1.second, f[c.first]);
#ifdef DEBUG
cout<<"nextfuel="<<nextfuel<<" nextmoney="<<nextmoney<<endl;
#endif
                if(merge(c.first, { nextmoney, nextfuel })) { 
                    qu.push({-nextmoney, c.first});
                    if(c.first==q)
                        qOnQ++;
                }
            }
        }
    }
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve(); 
}

