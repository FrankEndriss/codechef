/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;

//#define DEBUG

void solve() {
    int n;
    cin>>n;
    vi a(n);
    fori(n) {
        cin>>a[i];
    }

/* find len increasing prefix */
    int ipre=1;
    int ipost=1;
    for(int i=1; i<n; i++)  {
        if(a[i]>a[i-1])
            ipre++;
        else
            break;
    }
/* len increasing postfix */
    for(int i=1; i<n; i++)  {
        if(a[n-1-i]<a[n-i])
            ipost++;
        else
            break;
    }
    ll ans=min(ipre, n-1)+min(ipost, n-1);
    
#ifdef DEBUG
cout<<"ipre="<<ipre<<" ipost="<<ipost<<endl;
#endif
/* number of ways cutting something from the middle */
    for(int i=0; i<ipre && i<n-2; i++) {
        /* number of matching postfixes */
        auto it=upper_bound(a.begin()+max(n-ipost, i+2), a.end(), a[i]);
        int cnt=distance(it, a.end());
#ifdef DEBUG
cout<<"cnt="<<cnt<<endl;
#endif
        ans+=cnt;
    }
    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();

}

