/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

void solve() {
ll n, x, k;
    cin>>n>>x>>k;
    vector<ll> tl(n);
    fori(n)
        cin>>tl[i];

    vector<vector<int>> tree(n);

    fori(n-1) {
        int u, v;
        cin>>u>>v;
        u--;
        v--;
        tree[u].push_back(v);
        tree[v].push_back(u);
    }

    // ... boring TL;DR
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();

}

