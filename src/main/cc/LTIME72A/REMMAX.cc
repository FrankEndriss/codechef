/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

void solve() {
    string s;
    cin>>s;
    //cout<<"s="<<s<<" -> ";

    /* maximize the number of 9s left of the first digit of n */
    //int firstDigit=0;
    //while(s[firstDigit]=='0')
    //    firstDigit++;
    bool all9=true;
    for(uint i=1; i<s.size(); i++)
        if(s[i]!='9') {
            all9=false;
            break;
        }
    if(all9 || s.size()==1) {
        cout<<s<<endl;
        return;
    }

    if(s=="10") {
        cout<<"9"<<endl;
        return;
    }
    if(s[0]=='1' && s.size()==2) {
        cout<<s<<endl;
        return;
    }

    /* s >= "20" */

    bool allOther0=true;
    for(uint i=1; i<s.size(); i++)
        if(s[i]!='0') {
            allOther0=false;
            break;
        }

    if(s[0]!='1' || allOther0) {
        s[0]--;
        if(s[0]=='0') {
            s=s.substr(1, s.size());
            s[0]='9';
        }
        for(uint i=1; i<s.size(); i++)
            s[i]='9';
    } else {
        /* if n start with '1' we need to decrease next digit !=0 exept last digit.
         * Find digit >0, decrease, and set all other to '9'.
         * BUT: Dont decrease if all others _are_ '9'.
         */
        uint idx=1;
        while(idx<s.size()-1 && s[idx]=='0')
            idx++;

        if(idx<s.size()-1) {
            /* check if all others are '9' */
            all9=true;
            for(uint k=idx+1; k<s.size(); k++) {
                if(s[k]!='9') {
                    all9=false;
                    break;
                }
            }
            if(!all9) {
                s[idx]--;
                idx++;
                while(idx<s.size()) {
                    s[idx]='9';
                    idx++;
                }
            }
        }
    }
    cout<<s<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();

}

