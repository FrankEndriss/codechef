/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;
typedef unsigned int uint;
typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)
#define forn(i, n) for(ll i=0; i<(n); i++)
#define forn3(i, x, n) for(ll i=(x); i<(n); i++)

void solve() {
int n, q;
    cin>>n>>q;
    vector<ll> a(n);
    fori(n)
       cin>>a[i];
    
    fori(q) {
/* brute force */
        int l, r;
        cin>>l>>r;
        l--;
        r--;
        ll ans=0;
        for(int j=0; j+l<=r; j++)
            ans+=a[j+l]^j;
        cout<<ans<<endl;
    }
    

}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();

}

