
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Note that b[] must be non decreasing since we cannot make the mex(a[0..i]) smaller
 * by appending another value.
 * 
 * Consider the values up to the first b[i]>=0
 * We must not use b[i]
 * We must use all smaller numbers.
 *
 * So, at each position we must use the smallest possible value.
 * That is, a value that is not used later in b[j]
 *
 */
void solve() {
    cini(n);
    cinai(b,n);

    vi bb;

    multiset<int> a;
    for(int i=0; i<n; i++)  {
        a.insert(b[i]);
        if(b[i]>=0)
            bb.push_back(b[i]);
    }

    if(!is_sorted(all(bb))) {
        cout<<-1<<endl;
        return;
    }

    set<int> unused;
    int next=0;
    reverse(all(bb));
    for(int i=0; i<n; i++) {
        if(bb.size()==0) {
            assert(b[i]<0);
            ans[i]=42;
            continue;
        }

        if(unused.size() && *unused.begin()<bb.back()) {
            auto it=unused.begin();
            a[i]=*it;
            unused.erase(it);
            if(b[i]>=0)
                assert(b[i]==bb.back();
            bb.pop_back();
            continue;
        }
        
        if(next<bb.back()) {
            a[i]=next;
            next++;
            ... to complecated :/
        }
    }
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
