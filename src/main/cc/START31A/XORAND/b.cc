
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Consider MSB.
 * If set in a[i] and in a[j] the pair contributes.
 * If set in only one, it does not contribute.
 */
void solve() {
    cini(n);
    cinai(a,n);

    sort(all(a));
    vi cnt(32);
    for(int i=0; i<n; i++) {
        int c=0;
        while(a[i]) {
            c++;
            a[i]/=2;
        }
        cnt[c]++;
    }
    int ans=0;
    for(int i=0; i<32; i++) 
        ans+=cnt[i]*(cnt[i]-1);

    cout<<ans/2<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
