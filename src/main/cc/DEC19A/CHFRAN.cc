/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef pair<ll,ll> pllll;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/** TODO do it right, some of by one... */
void solve() {
    cini(n);
    vector<pllll> evt;
    ll mir=1e18;
    ll mal=-1;
    for(int i=0; i<n; i++) {
        cinll(l);    
        l*=2;
        cinll(r);
        r*=2;
        mir=min(mir,r);
        mal=max(mal,l);
        evt.push_back({l,1LL});
        evt.push_back({r,-1LL});
        evt.push_back({r+1,0LL});
    }
    sort(evt.begin(), evt.end());

    if(n<2) {
        cout<<-1<<endl;
        return;
    }

    vector<pllll> dp;
    dp.push_back({0LL,-1LL});

    int pprev=-2;
    for(pii p : evt) {
        if(p.first==pprev)
            dp[dp.size()-1].first+=p.second;
        else
            dp.push_back({dp.back().first+p.second, p.first});
        pprev=p.first;
    }

    ll ans=1e18;
    for(int i=1; i<dp.size(); i++) {
        if(dp[i].second>mir && dp[i].second<mal)
            ans=min(ans, dp[i].first);
    }

//cout<<"n="<<n<<" ans="<<ans<<endl;
    if(n-ans >= 2) 
        cout<<ans<<endl;
    else
        cout<<-1<<endl;

    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

