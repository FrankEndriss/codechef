/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;
const int N=100000;

vector<int> fac(N);


int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a, mod);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

int inv(const int x, const int mod=MOD) {
    return toPower(x, mod - 2);
}

void initFac(int mod=MOD) {
    fac[1]=1;
    for(int i=2; i<N; i++) {
        fac[i]=mul(fac[i-1], i, mod);
    }
}

/** see https://cp-algorithms.com/combinatorics/binomial-coefficients.html */
int nCr(int n, int k) {
    if(k==0 || k==n)
        return 1;
    if(k==1 || k==n-1)
        return n;
    if(k>n/2)
        return nCr(n, n-k);

    return mul(mul(fac[n], inv(fac[k])), inv(fac[n - k]));
}

void solve() {
    cini(n);
    cins(s1);
    cins(s2);
    int cnt1=0, cnt2=0;
    for(int i=0; i<n; i++) {
        if(s1[i]=='1')
           cnt1++; 
        if(s2[i]=='1')
           cnt2++; 
    }

/* We can create strings with at least mi 1s, ans mi+2, mi+4.. and max ma.
 **/
    int mi=max(cnt1,cnt2)-min(cnt1,cnt2);
    int ma=cnt1+cnt2;
    if(ma>n) 
        ma=n-(cnt1+cnt2-n);
    int ans=0;
    for(int i=mi; i<=ma; i+=2) {
        ans=pl(ans, nCr(n,i));
    }
    cout<<ans<<endl;

}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    initFac();
    cini(t);
    while(t--)
        solve();
}

