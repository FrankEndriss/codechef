/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    vvvi m(n, vvi(n, vi(n)));

    vi P(n);
    vi Q(n);
    vi R(n);

    for(int z=0; z<n; z++) {
        for(int y=0; y<n; y++) {
            cins(s);
            for(int x=0; x<n; x++)  {
                m[z][y][x]=s[x]-'0';
                if(m[z][y][x]) {
                    P[x]++;
                    Q[y]++;
                    R[z]++;
                }
            }
        }
    }
    vi Pid(n);
    vi Qid(n);
    vi Rid(n);
    for(int i=0; i<n; i++) {
        Pid[i]=i;
        Qid[i]=i;
        Rid[i]=i;
    }

    sort(Pid.begin(), Pid.end(), [&](int i1, int i2) {
        return P[i1]<P[i2];
    });
    sort(Qid.begin(), Qid.end(), [&](int i1, int i2) {
        return Q[i1]<Q[i2];
    });
    sort(Rid.begin(), Rid.end(), [&](int i1, int i2) {
        return R[i1]<R[i2];
    });

    for(int i : Pid)
        cout<<i+1<<" ";
    cout<<endl;
    for(int i : Qid)
        cout<<i+1<<" ";
    cout<<endl;
    for(int i : Rid)
        cout<<i+1<<" ";
    cout<<endl;

    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
        solve();
}

