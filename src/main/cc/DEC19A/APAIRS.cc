/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

vvi digits;
int offs;
void initDigits(int l, int r) {
    offs=l;
    digits.clear();
    digits.resize(r-l+1);
    for(int i=l;  i<=r; i++) {
        vi ai;
        int a=i;
        while(a) {
            digits[i-l].push_back(a%10);
            a/=10;
        }
        sort(digits[i-l].begin(), digits[i-l].end());
    }
    
}

int cdiff(int i, int j) {
    i-=offs;
    j-=offs;
    int k=0;
    int ans=0;
    if(digits[i].size()>digits[j].size())
        swap(i,j);

    while(digits[i].size()+k<digits[j].size()) {
        ans+=digits[j][k];
        k++;
    }

    for(int idx=0; idx<digits[i].size(); idx++) {
        ans+=abs(digits[i][idx]-digits[j][idx+k]);
    }

    return ans;
}

int ddiff(int i, int j) {
    vi ai;
    vi aj;
    while(i || j) {
        ai.push_back(i%10);
        aj.push_back(j%10);
        i/=10;
        j/=10;
    }
    sort(ai.begin(), ai.end());
    sort(aj.begin(), aj.end());
        
    int ans=0;
    for(size_t idx=0; idx<ai.size(); idx++) 
        ans+=abs(ai[idx]-aj[idx]);
    return ans;
}

void solve() {
    cini(l);
    cini(r);

    initDigits(l,r);
    int ans=0;
    for(int i=l; i<=r; i++) 
        for(int j=i+1; j<=r; j++) 
            ans+=cdiff(i,j)*2;

    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

