/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/*
string sxor(string &s1, string &s2) {
    int ma=max(s1.size(),s2.size());
    while(s1.size()<ma)
        s1="0"+s1;
    while(s2.size()<ma)
        s2="0"+s2;

    string ret(ma, '0');
    for(size_t i=0; i<ma; i++) {
        if((s1[i]=='1' && s2[i]=='0') || (s1[i]=='0' && s2[i]=='1'))
            ret[i]='1';
    }
}
string sand(string &s1, string &s2) {
    int ma=max(s1.size(),s2.size());
    while(s1.size()<ma)
        s1="0"+s1;
    while(s2.size()<ma)
        s2="0"+s2;

    string ret(ma, '0');
    for(size_t i=0; i<ma; i++) {
        if(s1[i]=='1' && s2[i]=='1')
            ret[i]='1';
    }
}

string mul2(string &s) {
    if(s[0]=='1')
        return s+"0";
    else
        return (s+"0").substr(1);
}

bool isset(string &s) {
    for(char c : s)
        if(c=='1')
            return true;
    return false;
}
*/

void solve() {
    cins(a);
    cins(b);

    reverse(a.begin(), a.end());
    reverse(b.begin(), b.end());

    if(b.find('1')==string::npos) {
        cout<<0<<endl;
        return;
    }

    int cnt=0;
    int ans=1;
    for(int i=0; i<=max(a.size(),b.size()); i++) {
        int ca=i<a.size()?a[i]-'0':0;
        int cb=i<b.size()?b[i]-'0':0;

        if(ca && cb) {
            cnt=2;
        } else if(ca || cb) {
            if(cnt>0)
                cnt++;
        } else {
            cnt=0;
        }
        ans=max(ans,cnt);
    }
    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

