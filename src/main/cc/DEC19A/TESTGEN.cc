/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cind(d) double d; cin>>d;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef pair<double,double> pdd;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

/* This has no input.
 * It produces a test case for https://www.codechef.com/OCT19A/problems/CNNCT2
 * The testcase can be checked against checker.cc
 */

void prand(vi &p) {
    srand(42);
    for(int i=0; i<p.size()*2; i++) {
        int i1=rand()%p.size();
        int i2;
        do {
            i2=rand()%p.size();
        } while(i1==i2);
        swap(p[i1], p[i2]);
    }
}

/** Bring checker1 down. Since that checks a randomized solution over all
 * permutations of the m edges, simply give enough "wrong" edges as possiblities.
 */
void solve1() {
    int n=10;
    int m=(n-2)*2;
    cout<<n<<" "<<m<<endl;
    vi p(n);
    for(int i=1; i<=n; i++)
        p[i-1]=i;

    for(int k=0; k<2; k++) {
        for(int i=0; i<n-2; i++) {
            cout<<p[i]<<" "<<p[i+1]<<endl;;
            cout<<p[n-1]<<" "<<p[i]<<endl;
        }

        prand(p);
    }

}

void solve2() {
    int n=100;
    int m=180;
    cout<<n<<" "<<m<<endl;
    vi p(n);
    for(int i=1; i<=n; i++)
        p[i-1]=i;

    /* Build 10 cycles with 10 members, connect the cycles. Cyclyc ;) */
    for(int i=0; i<2; i++) {
        for(int k=0; k<9; k++) {
            for(int j=0; j<9; j++) {
                cout<<j*10+k+1<<" "<<j*10+((k+1)%10)+1<<endl;
            }
            cout<<(k+1)*10<<" "<<(((k+1)%10)+1)*10<<endl;
        }
        prand(p);
    }

}


int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    solve2();
}

