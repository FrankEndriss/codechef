/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * a[i]&a[i+1] != a[i]|a[i+1]
 * -> a[i]!=a[i+1]
 * So a[] must be alternating.
 *
 * Note that to switch any bit, we need to have initially
 * at least one '1' bit.
 *
 * Note also that the statement says the switching indexes
 * i and j _must not_ be equal.
 */
void solve() {
	cini(n);
	cinai(a,n);
	for(int i=0; i<n; i++)  {
		a[i]&=1;
	}

	vi cnt(2);
	vvi ans(2);
	vi has1Idx(2,-1);
	for(int i=0; i<n; i++) {
		if(a[i]==(i%2)) {
			cnt[0]++;
			ans[0].push_back(i);
			if(a[i])
				has1Idx[1]=i;
		} else {
			cnt[1]++;
			ans[1].push_back(i);
			if(a[i])
				has1Idx[0]=i;
		}
	}

	if(cnt[0]==0 || cnt[1]==0)
		cout<<0<<endl;
	else {
		int aa=(cnt[0]>=cnt[1]);
		if(has1Idx[aa]<0)
			aa=!aa;
		if(has1Idx[aa]<0) {
			cout<<-1<<endl;
			return;
		}

		cout<<cnt[aa]<<endl;
		for(size_t i=0; i<ans[aa].size(); i++) {
			assert(ans[aa][i]!=has1Idx[aa]);
			cout<<ans[aa][i]+1<<" "<<has1Idx[aa]+1<<endl;
		}
	}



}

signed main() {
	cini(t);
	while(t--)
    		solve();
}