
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}
();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We can combine subarrays to the sum of it.
 * So we want to find the biggest number of disjoint
 * subarrays with same sum.
 *
 * There are not that much sums possible. Consider
 * the first subarray containing m elements.
 * Just find the biggest possible number of other arrays with that sum.
 *
 * Subarrays with sum zero can be ignored, because they simply merge
 * into adjacent subarrays.
 *
 * But we need to optimize the search for positions of "correct" sum,
 * else it is O(n^2).
 * So put foreach possible prefix-sum the indexes of that sum into
 * a map.
 *
 * ***
 * There seem to be some edgecases :/
 * -> Consider
 *  5
 *  1 1 1 1 -1
 * On searching for sum==3 starting at idx=2, it finds 2.
 * But it also should try the 4, because else it does not
 * find the optimal solution.
 *
 * We could try to iterate the list from back to front.
 * Since the sum over all n is y*sum.
 * So we can check if there is a subsequence sum, 2*sum, 3*sum,...,y*sum
 * in the prefix array.
 * ***
 * ...still one wrong testcase.
 */
void solve() {
    cini(n);
    cinai(a,n);

    int allZero=true;
    for(int i=0; i<n; i++)
        allZero&=(a[i]==0);

    if(allZero) {
        cout<<0<<endl;
        return;
    }

    map<int,vi> pre;
    int sumAll=0;
    for(int i=0; i<n; i++) {
        sumAll+=a[i];
        pre[sumAll].push_back(i);
    }

    int sum=0;
    int ans=n-1;
    for(int i=0; i+1<n; i++) {
        sum+=a[i];
        if(sum==0) {
            if(sumAll==0) {
                ans=min(ans, (int)(n-pre[0].size()));
            }
            continue;
        }

        if(sumAll%sum!=0)
            continue;

        int idx=i+1;
        int y=sumAll/sum;
        bool ok=true;
        for(int j=2; ok && j<=y; j++) {
            auto it=pre.find(sum*j);
            if(it==pre.end()) {
                ok=false;
            } else {
                auto it2=lower_bound(all(it->second), idx);
                if(it2!=it->second.end()) {
                    idx=*it2+1;
                } else
                    ok=false;
            }
        }
        if(ok)
            ans=min(ans, n-y);
    }
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
