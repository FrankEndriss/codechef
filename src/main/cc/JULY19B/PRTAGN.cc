/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

int numberOfSetBits(int i)
{
 return __builtin_popcount(i);
/*
    i = i - ((i >> 1) & 0x55555555);
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
    return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
*/
}

int N;
vi btab;

void solve() {
    int q;
    cin>>q;
    vi s1;
    vb s2(N, false);
    int bCnt[2]= { 0, 0 };
    fori(q) {
        int x;
        cin>>x;

        if(!s2[x]) {
            const int sz=s1.size();
            for(int j=0; j<sz; j++) {
                const int xval=s1[j]^x;
                if(!s2[xval]) {
                    s1.push_back(xval);
                    s2[xval]=true;
                    bCnt[btab[xval]]++;
                }
            }

            s1.push_back(x);
            s2[x]=true;
            bCnt[btab[x]]++;
        }

        cout<<bCnt[0]<<" "<<bCnt[1]<<endl;
    }
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    N=1;
    while(N<1e5)
        N<<=1;
    btab.resize(N, 0);
    for(int i=1; i<N; i++)
        btab[i]=numberOfSetBits(i)&1;

    int t;
    cin>>t;
    while(t--)
        solve();
}

