/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
        tree_order_statistics_node_update> indexed_set;

typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<pii> vpii;
typedef vector<vpii> vvpii;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<vll> vvll;

/* creates a list of 0 to K-1 based on distances given in plen. */
void travelingSalesman(vi& hamil, vvll& plen, int K) {
    vb vis(K, false);
    hamil.push_back(0);
    vis[0]=true;
    int last=0;
    for(int i=1; i<K; i++) {
        ll milen=1e9;
        int miidx=-1;
        for(int j=1; j<K; j++) {
            if(!vis[j] && last!=j && plen[last][j]<milen) {
                milen=plen[last][j];
                miidx=j;
            }
        }
        last=miidx;
        hamil.push_back(miidx);
        vis[miidx]=true;
    }
}

int N, M, K, S;
vvll plen; // plen[u][v]= len of shortest path from u to v
vi sc;     // special cities. ALL CITIES ZERO BASED
vi hamil;  // indexes into sc[]
vll l;
vll p;
vll q;     // non-eagerness: p[i]+q[i]*d
vll a, b;

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cin>>N>>M>>K>>S;
    sc=vi(K);   // special cities. ALL CITIES ZERO BASED
    fori(K) {
        cin>>sc[i];
        sc[i]--;
    }
    p=vll(N);
    q=vll(N); // non-eagerness: p[i]+q[i]*d
    fori(N) {
        cin>>p[i]>>q[i];
    }

    vvpii graph(N);    // edges of broken roads
    l=vll(M);
    a=vll(M);
    b=vll(M);
    fori(M) {
        int u, v;
        cin>>u>>v>>l[i]>>a[i]>>b[i];
        u--;
        v--;
        graph[u].push_back({ v, i} );
        graph[v].push_back({ u, i} );
    }

    /* We would like to create an hamiltonian path with all special cities.
     * Since we can connect virtually any city to any other city there are
     * like (n-1)! such paths. (or (n-1)!/n since n of them are the same).
     *
     * For creating new roads,
     * Step1 is to find the distances from any city
     * to any other city.
     */
    plen=vvll(N, vll(N, 1e18)); // plen[u][v]= len of shortest path from u to v
    vvvi ppath(N, vvi(N));      // pathes of plen, as vector<roadnumber>

    for(int i=0; i<N; i++) {
        priority_queue<tuple<ll, int, vi>> que;
        que.push({ 0LL, i, vi() });
        plen[i][i]=0LL;
        while(que.size()) {
            auto node=que.top();
            que.pop();
            for(auto e : graph[get<1>(node)]) {
                ll nlen=plen[i][get<1>(node)]+l[e.second];
                if(plen[i][e.first]>nlen) {
                    plen[i][e.first]=nlen;
                    vi npath=get<2>(node);
                    npath.push_back(e.second);
                    ppath[i][e.first]=npath;
                    que.push({ -nlen, e.first, npath });
                }
            }
        }
    }

    struct work {
        int roadnum;
        int hamilFrom;

        int hamilTo() {
            return (hamilFrom+1)%K;
        }

        int nodeFrom() {
            return sc[hamil[hamilFrom]];
        }

        int nodeTo() {
            return sc[hamil[hamilTo()]];
        }

        ll price(int startDay) {
            if(roadnum==-1) {
                return (fixPrice()+dailyPrice()*startDay)*days();
            } else {
                return (fixPrice()+dailyPrice()*startDay);
            }
        }

        ll days() {
            if(roadnum==-1) {
                return plen[nodeFrom()][nodeTo()];
            } else {
                return l[roadnum];
            }
        }

        ll dailyPrice() {
            if(roadnum==-1) {
                return q[nodeFrom()] + q[nodeTo()];
            } else {
                return b[roadnum];
            }
        }

        ll fixPrice() {
            if(roadnum==-1) {
                return p[nodeFrom()] + p[nodeTo()];
            } else {
                return a[roadnum];
            }
        }

    };

    /* Find a sequence of the special cities.
    * For all connections find if we want or have to build a new road
    * or repair the existing ones. Building a new one is simpler, so
    * we do that if allowed. If not then there is a single road we have
    * to repair instead.
    * Check for the sequence we want to repair/build the roads.
    * Create the output.
    */

    travelingSalesman(hamil, plen, K);

    vpii gmcrepairs;   // repairs[i]= first==day, second==roadnumber
    ll gmcosts=1e18;
    ll prevgmcosts=1e18;
    int hamSwapFrom=-1;
    int hamSwapTo=-1;
    vector<tuple<int, int, int>> gmcbuilds; // day, city-from, city-to
    /* swap cities in hamiltonian path */
    for(int optHamil=0; optHamil<12500; optHamil++) {
        if(optHamil>0) {
            if(prevgmcosts<=gmcosts) // last swap did no good, swap back
                swap(hamil[hamSwapFrom], hamil[hamSwapTo]);
        }

        hamSwapFrom=rand()%hamil.size();
        do {
            hamSwapTo=rand()%hamil.size();
        }
        while(hamSwapFrom==hamSwapTo);
        swap(hamil[hamSwapFrom], hamil[hamSwapTo]);
        prevgmcosts=gmcosts;

        /* check build or repair roads */
        vector<work> buildRoad;    // buildRoad[i]==-1 if road from sc[hamil[i]] to sc[hamil[i+1]]
        // will be build, roadnumber which will be repaired.
        for(int i=0; i<K; i++) {
            buildRoad.push_back({ -1, i });
            for(auto e : graph[sc[hamil[i]]]) {
                if(e.first==sc[hamil[(i+1)%K]]) { // if there is an old road we need to repair it
                    buildRoad[i].roadnum=e.second;
                    break;
                }
            }
        }

        /* now try to substitute build of new roads by repairs of existing ones.
         * check if possible with cheapest path, and less expensive.
         * Note: No road must be used twice on the hamiltonian path.
         */
        vb repRoadnum(M, false);
        for(work w : buildRoad)
            if(w.roadnum>=0)
                repRoadnum[w.roadnum]=true;

        int sz=(int)buildRoad.size();
        for(int i=0; i<sz; i++) {
            work w=buildRoad[i];
            if(w.roadnum==-1) {
                int ok=true;
                assert(ppath[w.nodeFrom()][w.nodeTo()].size()>1);
                for(int rnum : ppath[w.nodeFrom()][w.nodeTo()]) {
                    if(repRoadnum[rnum]) {
                        ok=false;
                        break;
                    }
                }
                if(!ok)
                    continue;

                buildRoad[i].roadnum=ppath[w.nodeFrom()][w.nodeTo()][0];
                repRoadnum[buildRoad[i].roadnum]=true;
                int psz=(int)ppath[w.nodeFrom()][w.nodeTo()].size();
                for(int j=1; j<psz; j++) {
                    buildRoad.push_back( { ppath[w.nodeFrom()][w.nodeTo()][j], -1 });
                    repRoadnum[ppath[w.nodeFrom()][w.nodeTo()][j]]=true;
                }
            }
        }
        /* Now search for alternative repairable roads for build connections.
         */

        /* returns a (shortest) list of roads which form a path from from to to.
         * size()<2 if notfound. 
         * Note that it is the path with lowest count of roads, not nesseccarily
         * the cheapest one.
         * */
        function<vi(int, int)> bfsFindAltPath=[&](int from, int to) {
            queue<pair<int, vi>> que;
            vi vis(N, false);
            vis[from]=true;
            vi emptyV;
            que.push({ from, emptyV });

            int cnt=0;
            while(que.size() && cnt++<2000) {
                auto qent=que.front();
                que.pop();
                int node=qent.first;
                for(auto ent : graph[node]) {
                    if(!vis[ent.first] && !repRoadnum[ent.second]) {
                        vi path=qent.second;
                        path.push_back(ent.second);
                        if(ent.first==to)
                            return path;
                        que.push({ ent.first, path });
                        vis[ent.first]=true;
                    }
                }
            }
            vi ret;
            return ret;  // empty
        };

        sz=(int)buildRoad.size();
        for(int i=0; i<sz; i++) {
            auto w=buildRoad[i];
            if(w.roadnum>=0)
                continue;

            int from=w.nodeFrom();
            int to=w.nodeTo();
            vi path=bfsFindAltPath(from, to);
            if(path.size()<2)
                continue;

            buildRoad[i].roadnum=path[0];
            repRoadnum[path[0]]=true;
            int psz=(int)path.size();
            for(int j=1; j<psz; j++) {
                buildRoad.push_back( { path[j], -1 });
                repRoadnum[path[j]]=true;
            }
        }
        /* end substitution */

        /* find a sorting which minimizes sum of prices. 
         * cheap and long to the end, expensive and short to the beginning.
         */
        sort(buildRoad.begin(), buildRoad.end(), [&](work& w1, work& w2) {
            double fac1=(-1.0d*w1.dailyPrice())/w1.days();
            if(w1.roadnum<0)
                fac1*=100;
            double fac2=(-1.0d*w2.dailyPrice())/w2.days();
            if(w2.roadnum<0)
                fac2*=100;
            return fac1<fac2;
        });

        /* Do the output.
         * schedul the jobs in the given sequence,
         * just need to check how long it takes.
         */
        vpii repairs, mcrepairs;   // repairs[i]= first==day, second==roadnumber
        vector<tuple<int, int, int>> builds, mcbuilds; // day, city-from, city-to
        ll mcosts=-1;
        int swapFrom=-1;
        int swapTo=-1;

        for(int optSched=0; optSched<21; optSched++) {
            repairs.clear();
            builds.clear();

            priority_queue<ll> jobs;
            fori(S) {
                jobs.push(-1);
            }

            ll costs=0;
            for(int i=0; i<(int)buildRoad.size(); i++) {
                ll d=-jobs.top();
                jobs.pop();

                if(buildRoad[i].roadnum==-1) {
                    builds.push_back({ d, buildRoad[i].nodeFrom(), buildRoad[i].nodeTo() });
                } else {
                    repairs.push_back({ d, buildRoad[i].roadnum });
                }

                costs+=buildRoad[i].price(d);
                jobs.push(-(d+buildRoad[i].days()));
            }

            /* Try to optimize the sorting by swapping pairs of build,
             * start with adjent ones.
             */
            if(mcosts==-1 || mcosts>costs) {
                mcosts=costs;
                mcrepairs=repairs;
                mcbuilds=builds;
            } else { /* swap back the last swap */
                swap(buildRoad[swapFrom], buildRoad[swapTo]);
            }
            if(buildRoad.size()<2)
                break;

            swapFrom=rand()%buildRoad.size();
            do {
                swapTo=rand()%buildRoad.size();
            } while(swapFrom==swapTo);

            swap(buildRoad[swapFrom], buildRoad[swapTo]);
        }
        if(mcosts<gmcosts) {
            gmcosts=mcosts;
            gmcrepairs=mcrepairs;
            gmcbuilds=mcbuilds;
        }
    }// end swap cities in hamiltonian path

    cout<<gmcrepairs.size()<<endl;
    for(auto r : gmcrepairs)
        cout<<r.first<<" "<<r.second+1<<endl;
    cout<<gmcbuilds.size()<<endl;
    for(auto bu : gmcbuilds)
        cout<<get<0>(bu)<<" "<<get<1>(bu)+1<<" "<<get<2>(bu)+1<<endl;

    return 0;
}

