/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;

void solve() {
    int n;
    cin>>n;
    vi a(n);
    ll sum=0;
    fori(n) {
        cin>>a[i];
        sum+=a[i];
    }
    if(sum%n!=0) {
        cout<<"Impossible"<<endl;
        return;
    }
    int mean=sum/n;
    fori(n) {
        if(mean==a[i]) {
            cout<<i+1<<endl;
            return;
        }
    }
    cout<<"Impossible"<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

