/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef pair<ll, int> plli;
typedef pair<ll, ll> pllll;
typedef vector<bool> vb;
typedef vector<ll> vll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vll> vvll;
typedef vector<pllll> vpllll;
typedef vector<vpllll> vvpllll;

template <typename E>
class SegmentTree {
private:
    vector<E> tree;
    E neutral;
    function<E (E, E)> cumul;

    int treeDataOffset;
    const int ROOT = 1;

    inline int leftChild(const int treeIdx) {
        return  treeIdx * 2;
    }
    inline int rightChild(const int treeIdx) {
        return treeIdx * 2 + 1;
    }
    inline int parent(const int treeIdx) {
        return treeIdx / 2;
    }
    inline bool isOdd(const int idx) {
        return (idx & 0x1);
    }

public:
/** SegmentTree. Note that you can hold your data in your own storage and give
 * an Array of indices to a SegmentTree.
 * @param beg, end The initial data.
 * @param pNeutral the Element neutral to cumul, ie 0 for sum, 1 for product etc
 * @param pCumul The cumulative function to create the "sum" of two nodes.
**/
    template<typename Iterator>
    SegmentTree(Iterator beg, Iterator end, E pNeutral, function<E (E, E)> pCumul) {
        neutral=pNeutral;
        cumul=pCumul;
        treeDataOffset=(int)distance(beg, end);
        tree=vector<E>(treeDataOffset*2, pNeutral);

        int i=treeDataOffset;
        for (auto it=beg; it!=end; it++)
            tree[i++] = *it;

        for (int j=treeDataOffset - 1; j>=1; j--)
            tree[j] = cumul(tree[leftChild(j)], tree[rightChild(j)]);
    }

    /** Updates the data at dataIdx to value. */
    void update(int dataIdx, E value) {
        int treeIdx = treeDataOffset + dataIdx;
        tree[treeIdx] = value;

        while (treeIdx != ROOT) {
            treeIdx = parent(treeIdx);
            tree[treeIdx] = cumul(tree[leftChild(treeIdx)], tree[rightChild(treeIdx)]);
        }
    }

    /** @return the cumul(idxL, idxR], iE idxL inclusive, idxR exclusive. */
    E get(int pIdxL, int pIdxR) {
        int idxL = pIdxL + treeDataOffset;
        int idxR = pIdxR + treeDataOffset;
        E cum = neutral;
        while (idxL < idxR) {
            if (isOdd(idxL)) { // left is odd
                cum = cumul(cum, tree[idxL]);
                idxL++;
            }
            if (isOdd(idxR)) {
                idxR--;
                cum = cumul(cum, tree[idxR]);
            }
            idxL = parent(idxL);
            idxR = parent(idxR);
        }
        return cum;
    }

};

//#define DEBUG

//#define SIMULATE

ll ans=0;
#ifdef SIMULATE
ll gans;
vi vgans;
int permidx;
vector<int> perm;
int nextFromPerm() {
    return perm[permidx++];
}
#endif

int n;
vll a;

#ifdef SIMULATE
vll sima;
bool simrun=false;
void initSimInput() {
    sima=a;
    simrun=true;
    perm=vi(n);
    fori(n) {
        perm[i]=(int)i;
    }
}

void simulateInput() {
    a=sima;
}
#endif

void readInput() {
    cin>>n;
    a=vll(n);
    fori(n) {
        cin>>a[i];
    }
}

ll solvedp() {
/* dp[i][j]= cheapest way to combine i nodes starting at j. ( i is ONE BASED) */
    vvpllll dp(n+1, vpllll(n+1));    
    for(int j=0; j<n; j++)  {
        dp[1][j]={ a[j], 0L };
//cout<<a[j]<<endl;
    }
    for(int j=0; j<n; j++)  {
        ll sum=dp[1][j].first+dp[1][(j+1)%n].first;
        dp[2][j]={ sum, sum };
    }

    for(int i=3; i<=n; i++) {
        for(int j=0; j<n; j++) {
/* We can split any group of n nodes into (1,n-1), (2,n-2),... (n-1,1)
 */
            ll mifi=1e18;
            ll misec=1e18;
            for(int k=1; k<i; k++)  {
                ll lfi=dp[k][j].first + dp[i-k][(j+k)%n].first;
                ll lsec=dp[k][j].second + dp[i-k][(j+k)%n].second + lfi;
                if(lsec<misec) {
                    mifi=lfi;
                    misec=lsec;
                }
            }

            dp[i][j]={ mifi, misec };
        }
    }

    ans=1e18;
    for(int i=0; i<n; i++)
        ans=min(ans, dp[n][i].second);

    return ans;
}

void solve() {

    const ll INF=1e18;
    vll s(n+1);
    vi next(n);
    vi prev(n);
    vi id(n+1);
    for(int i=0; i<n; i++)  {
        next[i]=(i+1)%n;
        prev[i]=(i-1+n)%n;
        id[i]=i;
    }
    fori(n)
        s[i]=a[i]+a[next[i]];

    id[n]=n;
    s[n]=INF;

    SegmentTree<int> st(id.begin(), id.end(), n, [&](int idx1, int idx2) {
        if(s[idx1] == s[idx2])
            return min(idx1, idx2);
        else if(s[idx1] < s[idx2])
            return idx1;
        return idx2;
    });

/* Find min sum, add sum to ans, update entries.
 * Repeat.
 */
    ans=0;
    fori(n-1) {
#ifdef SIMULATE
        int idx;
        if(simrun)
            idx=nextFromPerm();
        else
            idx=st.get(0, n);
#else
        int idx=st.get(0, n);
#endif

#ifdef DEBUG
    cout<<"idx="<<idx<<" a[idx]="<<a[idx]<<" s[idx]="<<s[idx]<<endl;
#endif

        // we remove entry idx
        ans+=s[idx];
        int nextidx=next[idx];
        int previdx=prev[idx];
        a[nextidx]+=a[idx];

        next[previdx]=nextidx;
        prev[nextidx]=previdx;

        s[idx]=INF;
        st.update(idx, n);

        s[previdx]=a[previdx]+a[nextidx];
        st.update(previdx, previdx);
        s[nextidx]=a[nextidx]+a[next[nextidx]];
        st.update(nextidx, nextidx);
    }
#ifdef SIMULATE
    if(ans<gans) {
        gans=ans;
        vgans=perm;
    }
#endif
    
}

// TODO We need to check/find the correct greedy.
// As a first step we can implement a brute force to
// check if sequence of idx really leads to smallest 
// possible solution.

#ifdef SIMULATE
int simmain() {
    int t;
    cin>>t;
    readInput();
    initSimInput();

    for(int simc=0; simc<100000; simc++) {
        fori(n)
            sima[i]=rand()%1000+1;

        gans=1e9;
        ll cans=0;
        do {
            permidx=0;
            cans++;
            simulateInput();
            solve();
        }while(next_permutation(perm.begin(), perm.end()));

        //cout<<"cans="<<cans<<endl;
//        cout<<"gans="<<gans<<endl;

        simrun=false;
        simulateInput();
        solve();
//        cout<<ans<<endl;

        if(gans!=ans) {
            cout<<"MATCH!!! ans="<<ans<<" gans="<<gans<<" simc="<<simc<<endl;
            for(int i=0; i<sima.size(); i++)
                cout<<"sima["<<i<<"]="<<sima[i]<<endl;
            for(int i=0; i<vgans.size(); i++)
                cout<<"perm["<<i<<"]="<<vgans[i]<<endl;
            exit(1);
        }

        //if(simc%100==0)
            cout<<"simc="<<simc<<endl;
    } // end for simc

}
#endif

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
#ifdef SIMULATE
    return simmain();
#endif
    int t;
    cin>>t;

    while(t--) {
        readInput();
        //solve();
        solvedp();
        cout<<ans<<endl;
    }
}
