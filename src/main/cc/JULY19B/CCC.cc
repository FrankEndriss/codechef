/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<ll> vll;
typedef vector<vll> vvll;
typedef vector<vi> vvi;

void solve() {
    int n, z;
    cin>>n>>z;
    vi a(n+1, 0);
    fori(n)
        cin>>a[i+1];
    sort(a.begin(), a.end());

    vi asma(n+1);   // asma[i]== next smaller a[i] 
    int next=0;
    fori(n) {
        if(a[i+1]!=a[i])
            next=a[i];
        asma[i+1]=next;
    }

    vvi dp(n+1, vi(z+1, 0));
    for(int j=1; j<=z; j++) {
        for(int i=j-1; i<=n; i++) {
            if(i<j) {
                dp[i][j]=1e9;
                continue;
            }

            dp[i][j]=min(dp[i-1][j]+asma[i], dp[i-1][j-1]+a[i]);

            for(int k=j; k<i; k++) {
                int val=dp[k][j]+a[k]*(i-k);
                int diff=val-dp[i][j];
                if(diff>=a[k])
                    k+=diff/a[k];
                dp[i][j]=min(dp[i][j], val);
            }
        }
    }
    cout<<dp[n][z]<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

