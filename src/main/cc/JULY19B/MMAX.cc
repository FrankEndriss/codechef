/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;


void solve() {
    ll n;
    string str;
    cin>>n>>str;

    reverse(str.begin(), str.end());
    ll mod=0;
    ll mul=1;
    for(char c : str) {
        mod=mod+(c-'0')*mul;
        mod%=n;
        mul*=10;
        mul%=n;
    }
    // mod= k%n

    ll ans=0;
    if(mod!=0) {
        int ones=mod;
        int nuls=n-ones;
        if(ones==nuls)
            ans=2*ones-1;
        else
            ans=2*min(ones,  nuls);
    }
    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve();
}

