/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/ */
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;

typedef unsigned int uint;
typedef long long ll;
//typedef __int128 lll;

#define fori(n) for(ll i=0; i<(n); i++)

typedef pair<int, int> pii;
typedef pair<int, ll> pill;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<pii> vpii;

//#define DEBUG

const int D=-1;
/* @return id of last soldier, hits on D */
pill simulate(list<int> &li, const vi &a) {
    ll hitD=0;
    auto it=li.begin();

#ifdef DEBUG
vi attlist;
#endif
    while(li.size()>2) {
        if(it==li.end()) {
            it=li.begin();
        } else if(*it==-1) {
#ifdef DEBUG
attlist.push_back(-1);
#endif
            it++;
        } else {
            int attacker=*it;
            it++;
            if(it==li.end())
                it=li.begin();

            if(*it==-1) {
                hitD+=a[attacker];
#ifdef DEBUG
attlist.push_back(a[attacker]);
#endif
                it++;
            } else {
                it=li.erase(it);
            }
        }
    }

    if(it==li.end())
        it=li.begin();

    if(*it==-1) {
        it++;
        if(it==li.end())
            it=li.begin();
    }

#ifdef DEBUG
cout<<"attackers={ ";
for(auto i : attlist)
    cout<<i<<", ";
cout<<"}"<<endl;
#endif
    
    return { *it, hitD };
}

/** @return hits taken if D is on pos pos and soldiers a */
ll simulate2(int pos, vi &a) {
/*
 * So...
 * Def: One round is that once any soldier was attacker or is dead. Note
 * that the last soldier could kill the first as last move of one round.
 *
 * pos=pos-(pos/2)   --since for any full pair of s left of D, one of the s dies
 * if(lastKillsFirst && pos>0) pos--;    --obviously
 *
 * In every round, D gets hit by the s left of him, or no hit.
 * if D==0, 2, 4... no one. if D==1, 3, 5: hit.
 * posIsOdd: Hitround, posIsEven: Nohitround
 *
 * Per round there are (sizeof(a)+1)/2 attacks.
 * sizeof(a)=sizeof(a)-(sizeof(a)+1)/2 + Hitround?1:0;
 *
 * Attacker pos.
 * Attackerpos works much same as D-pos for hitrounds.
 * apos=apos-(apos/2) --since for any full pair of s left of apos, one of the s dies
 * For Nohitrounds apos gets attacked, hence the s left of apos becomes the new apos.
 * So, which one is "the left of apos"?
 *
 *
 * For D==0/last
 * Case AOdd, sizeof(a) is odd : 
 * * D gets hit, attacker stays same
 * * sizeof(a)=sizeof(a)/2+1
 * Case AEven, sizeof(a) is even: 
 * * no hit, attacker dies hence new attacker
 * * sizeof(a)=sizeof(a)/2
 *
 * "new attacker", D-x is x positions left of D
 * AOdd:
 * died soldiers: D-2, D-4, ...
 * AEven: 
 * died soldiers: D-1, D-3, ...
 *
 * Some patterns of dieing soldiers:
 * AOdd, AOdd, AOdd: 
 *   R1: D-2, D-4, D-6,...   (alife: -1, -3, -5,...)
 *   R2: D-3, D-7, D-11,...  (alife: -1, -5, -9,...)
 *   R3: D-5, D-13, D-21,... (alife: -1, -9, -17,...)
 *   RN: D-(1+1<<(N-1)), +2^N, +2^N,...
 * AEven, AEven, AEven:
 *   R1: D-1, D-3, D-5,...   (alife: -2, -4, -6,...)
 *   R2: D-2, D-6, D-10,...  (alife: -4, -8, -12,...)
 *   R3: D-4, D-12, D-20,... (alife: -8, -16, -24,...)
 *   RN: D-(1<<(N-1)), +2^N, +2^N
 *
 * So, whatever happens, the distance between living soldiers doubles. In even
 * rounds the attacker-idx moves by that distance to the left.
 * But we need to "normalize" a (and the other values) so that D is at pos 0.
 * We do this by implementing a mapping function for the indexes.
 */

    int N=(int)a.size();
    ll hits=0;
    if(pos%2)       // pre round attacks
        hits+=a[(pos-1+N)%N];

    /** @calculate idx from normalized a[] to original a[].
 * Example, N=8, normSz=6:
 *               P
 * orig: 0 1 2 3 4 5 6 7
 *         x   x
 * norm: 4   5   0 1 2 3
 **/
    function<int(int)> normidx=[&](int idx) {
        if(idx<=N-pos-1) {   // 0 to 3
            return pos+idx;
        } else {    // 4 and more
            return (idx-(N-pos))*2;
        }
    };

    // assume normalized pos==0 and normalized a[]
    int normSz=N-pos/2;
    int att=normSz-1;
    int attdist=1;
    const int NN=normSz;
    while(normSz>1) {
        if(normSz%2) {  // AOdd
            hits+=a[normidx(att)];
            normSz=normSz/2+1;
        } else {    // AEven
            att=att-attdist;
            if(att<0)
                att+=NN;
            assert(att>=0);
            normSz=normSz/2;
        }
        attdist*=2;
    }
    return hits;
}

void solve2() {
    int n;
    cin>>n;
    vi a(n-1);
    fori(n-1)
        cin>>a[i];
    int f;
    cin>>f;

    ll minD=1e9;
    int posD=-1;
    for(int i=0; i<n-1; i++) { // positions of D
        if(a[i]>f)
            continue;

        ll d=simulate2(i, a);
        if(d<minD) {
            minD=d;
            posD=i;
        }
    }
            
    if(posD>=0) {
        cout<<"possible"<<endl;
        cout<<posD+1<<" "<<minD+f<<endl;
    } else 
        cout<<"impossible"<<endl;

}

void solve() {
    int n;
    cin>>n;
    vi a(n-1);
    fori(n-1)
        cin>>a[i];
    int f;
    cin>>f;

    ll minD=1e9;
    int posD=-1;
    for(int i=0; i<n-1; i++) { // positions of D
        if(a[i]>f)
            continue;

#ifdef DEBUG
cout<<"will simulate with posD="<<i<<endl;
#endif
        int idx=0;
        list<int> li;
        for(int j=0; j<n; j++)
            if(j==i)
                li.push_back(D);   // D
            else
                li.push_back(idx++);

/** simple brute force should be good for 30 points */
        auto res=simulate(li, a);
/** res has to fullfill two concerns:
 * 1. last soldier must be one with power less or equal to F.
 * 2. hits on D must be minimized.
 * 3. among minimized D, pos must be minimzed.
 *
 * +++ Observation:
 * Last soldier is allways the one just right of D !!!
 * So, the challenge is to find the minD.
 * We need to check all positions where a[pos+1]<f.
 * So, for big f, all positions. :/
 *
 */
        if(a[res.first]<=f) {   // rule 1.
            if(res.second<minD) {
                posD=i;
                minD=res.second;
            }
        }
    }

    if(posD>=0) {
        cout<<"possible"<<endl;
        cout<<posD+1<<" "<<minD+f<<endl;
    } else 
        cout<<"impossible"<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    int t;
    cin>>t;
    while(t--)
        solve2();
}

