
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"


const int primeAproxFactor=20;  // factor 20 is good for up to MAXN=5000000, ie max prime ==1e8
const int MAXN=1e6;
vector<int> pr(MAXN);
vector<bool> notpr(MAXN*primeAproxFactor, false);

const bool pr_init=[]() {
    ll c = 0;
    ll i = 2;
    while (i < MAXN * primeAproxFactor && c < MAXN) {
        if (!notpr[i]) {
            pr[c] = i;
            c++;
            ll j = 1LL*i * i;
            while (j < primeAproxFactor * MAXN) {
                notpr[j] = true;
                j += i;
            }
        }
        i++;
    }
    return true;
}();

/* The number with the most factors
 * must be merged until it has all factors,
 * then merge with all other numbers.
 *
 * How to find smallest possible number of 
 * sets to merge with biggest set?
 * -> each prime is in one set single element
 *  so all primes must be merged once.
 *  -> no, we have sets with more that one prime, so merging
 *  such set reduces the number of unmerged primes by 2.
 *  ???
 */
void solve() {
    cini(n);

    if(n==1) {
        cout<<1<<endl; 
        return;
    }
    if(n==2) {
        cout<<2<<endl;
        return;
    }

    int cnt=0;  /* count of disjoint sets */
    int idx=0;
    while(pr[idx]<=n) {
        int prod=pr[idx];
        while(prod*pr[idx+1]<=n)  {
            prod*=pr[idx+1];
            idx++;
        }
        cnt++;
        idx++;
    }

    int cnt2=0;
    while(cnt>1) {
        cnt2++;
        cnt-=cnt/2;
    }

    int ans=cnt2+(n-2);
    cout<<ans<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
