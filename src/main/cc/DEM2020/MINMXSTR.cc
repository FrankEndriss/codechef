
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = []() {
    ios_base::sync_with_stdio(false);
    cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

bool isPrefix(string &s1, string &s2) {
    if(s1.size()>s2.size())
        return false;

    for(size_t i=0; <s1.size(); i++) 
        if(s1[i]!=s2[i])
            return false;

    return true;
}
/* We maintain a set<string>
 * to be able to check after each update the
 * state of isPrefix.
 *
 * How to find indexes of first/last not prefix string?
 */
void solve() {
    cini(n);
    set<pair<string,int>> ss;
    for(int i=0; i<n; i++) {
        cins(s);
        ss.emplace(s,i);
    }

    vb isPrefix(n);

    pair<string,int> prev;
    prev.second=-1;
    for(auto ps : ss) {
        if(prev.second>=0 && isPrefix(prev.first, ss.first))
            isPrefix[prev.second]=true;

        prev=ss;
    }

    cini(q);
    while(q--) {
        cini(t);
        if(t==1) {
            cini(x);
            char c;
            cin>>c;
        } else if(t==2) {
        } else if(t==3) {
        } else assert(false);
    }

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
