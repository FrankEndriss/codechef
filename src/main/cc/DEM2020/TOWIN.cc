
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

const double PI = acos(-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) double d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<double, double>;
using vd= vector<double>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

void solve() {
    cini(n);
    cinai(a,n);
    sort(all(a), greater<int>());

    int sumF1=0;    /* second takes one */
    int sumS1=0;
    for(int i=0; i<n; i+=2)
        sumF1+=a[i];
    for(int i=1; i<n; i+=2)
        sumS1+=a[i];

    int sumF2=a[0];    /* second takes two */
    int sumS2=a[1];
    for(int i=3; i<n; i+=2) 
        sumF2+=a[i];

    for(int i=2; i<n; i+=2)
        sumS2+=a[i];

    //cerr<<"sumF1="<<sumF1<<" sumS1="<<sumS1<<" sumF2="<<sumF2<<" sumS2"<<sumS2<<endl;
    if(sumF1>sumS1 && sumF2>sumS2) {
        cout<<"first"<<endl;
    } else if(sumS1>sumF1 || sumS2>sumF2)
        cout<<"second"<<endl;
    else
        cout<<"draw"<<endl;

}

signed main() {
    cini(t);
    while(t--)
        solve();
}

// FIRST THINK, THEN CODE
// DO NOT JUMP BETWEEN PROBLEMS
