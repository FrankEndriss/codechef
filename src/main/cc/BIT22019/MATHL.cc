/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;
const int N=1000000;

int mul(ll i1, ll i2) {
    return (int)((i1*i2)%MOD);
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    vi ans(N+1);

    ans[1]=1;
    int row=1;
    for(int i=2; i<=N; i++) {
        row=mul(row, i);
        ans[i]=mul(ans[i-1], row);
    }

    cini(t);
    for(int i=0; i<t; i++) {
        cini(n);
        cout<<ans[n]<<endl;
    }

}

