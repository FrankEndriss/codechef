/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct node {
    int n;
    int o;

    int next;
    int prev;
};

/** see https://www.codechef.com/BIT22019/
 * That is to slow because we recalculate the beginning of 
 * the permutation more often than we have to.
 * We would need to do the calculations recursive for the
 * part right of i in p[i].
 */
void solve() {
    cins(s);
    vi nums;
    vector<char> ops;
    
    size_t idx=0;
    while(idx<s.size()) {
        string n="";
        while(s[idx]>='0' && s[idx]<='9')
            n+=s[idx++];
        istringstream is(n);
        int i;
        is>>i;
        nums.push_back(i);
        if(idx<s.size())
            ops.push_back(s[idx++]);
    }

    assert(ops.size()+1==nums.size());

    vi p(ops.size());
    for(size_t i=0; i<p.size(); i++)
        p[i]=(int)i;

    ll ma=-1e10;
    vector<node> tree(ops.size()*2+1);
    do {
//cout<<"next perm"<<endl;
        for(size_t i=0; i<ops.size(); i++) {
            int tsize=(int)tree.size();
            tree[i*2].n=nums[i];
            tree[i*2].prev=i*2-1;
            tree[i*2].next=i*2+1;

            tree[i*2+1].o=ops[i];
            tree[i*2+1].prev=i*2;
            tree[i*2+1].next=i*2+2;
        }
        node num;
        tree[ops.size()*2].n=nums.back();
        tree[ops.size()*2].next=-1;
        tree[ops.size()*2].prev=ops.size()*2-1;

        int ans=-1;
        for(size_t i=0; i<p.size(); i++) {
            int opidx=p[i]*2+1;
            int lIdx=tree[opidx].prev;
            int rIdx=tree[opidx].next;
//cout<<"opidx="<<opidx<<" lIdx="<<lIdx<<" rIdx="<<rIdx<<endl;
            assert(lIdx<rIdx);

            if(tree[opidx].o=='&')
                tree[lIdx].n&=tree[rIdx].n;
            else if(tree[opidx].o=='|')
                tree[lIdx].n|=tree[rIdx].n;
            else if(tree[opidx].o=='^')
                tree[lIdx].n^=tree[rIdx].n;
            else
                assert(false);

            ans=tree[lIdx].n;

//cout<<"ans="<<ans<<endl;

            tree[lIdx].next=tree[rIdx].next;
            if(tree[rIdx].next>=0)
                tree[tree[rIdx].next].prev = lIdx;
        }
        ma=max(ma, (ll)ans);
        
    } while(next_permutation(p.begin(), p.end()));

    cout<<ma<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

