/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cinai(c, n);
    cini(x);

    int aidx=0;

    int bidx=n-1;
    int bcnt=0;

    int amod=0;
    bool bAngefressen=false;
    while(aidx<=bidx) {
        if(aidx==bidx && bAngefressen) {
            break;
            bcnt++;
            break;
        }
        amod+=c[aidx];
        aidx++;

        while(bidx>=aidx && amod>=x) {
            int btmp=min(amod/x, c[bidx]);
            if(btmp>0)
                bAngefressen=true;
            amod-=btmp*x;
            c[bidx]-=btmp;
            if(c[bidx]==0) {
                bcnt++;
                bidx--;
                bAngefressen=false;
            }
        }
    }

    cout<<aidx<<" "<<bcnt<<endl;
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

