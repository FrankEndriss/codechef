
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * dp
 *
 * dp[i]=min const to reach vertex i
 *
 * Consider components, and the costs beteween components.
 *
 * Note that it is allways possible to reach vertex N 
 * with cost of N-1,
 * so cost is at most number of components -1.
 * Is there a better sol possible?
 * Yes.
 *
 * Consider components at dist 1 from first comp,
 * then dist 2, then dist 3 etc
 * There are not so much edges to traverse, at most N*sqrt(N)
 */
const int N=2e5+7;
const int INF=1e9;
void solve() {
    cini(n);
    cini(m);

    vector<vector<pii>> adj(n);
    for(int i=0; i<m; i++) {
        cini(u); u--;
        cini(v); v--;
        adj[u].emplace_back(v,0);
        adj[v].emplace_back(u,0);
    }

    for(int dist=1; dist<=2; dist++) {
        //cerr<<"adding edges for dist="<<dist<<endl;
        for(int v=0; v+dist<n; v++) {
                adj[v].emplace_back(v+dist, dist*dist);
                adj[v+dist].emplace_back(v, dist*dist);
        }
    }

    vi dp(n, INF);
    priority_queue<pii> q;  /* -dist,v */

    dp[0]=0;
    q.emplace(0,0);

    while(q.size()) {
        auto [di,v]=q.top();
        q.pop();
        if(-dp[v]!=di)
            continue;

        if(v==n-1)
            break;

        for(pii chl : adj[v]) {
            if(dp[v]+chl.second<dp[chl.first]) {
                dp[chl.first]=dp[v]+chl.second;
                q.emplace(-dp[chl.first], chl.first);
            }
        }
    }

    cout<<dp[n-1]<<endl;
}

signed main() {
    cini(t);
    while(t--)
        solve();
}