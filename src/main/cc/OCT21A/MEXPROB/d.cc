/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * Go upward with the mex values.
 * ie find number of pairs with MEX==0,
 * then with MEX==1 ... and so on.
 * Until k is reached.
 *
 * So, the number of pairs with MEX==0 is the number of ranges
 * without a 0 in it. So count the length of the seqs of non-0
 * values len, each contributes len*(len+1)/2
 *
 * The number of pairs with MEX==1 is the number of rangens 
 * with a 0 in it, but without a 1.
 * ...
 *
 * ****
 */

void solve() {
    cini(n);
    cini(k);
    cinai(a,n);

    // ???
}

/*
 * Brute force for 10 Points :)
 */
const int N=1e5+7;
void solve0() {
    cini(n);
    cini(k);

    cinai(a,n);

    vi ans(N);
    for(int l=0; l<n; l++) {
        int mex=0;
        vb vis(N);
        for(int r=l; r<n; r++) {
            vis[a[r]]=true;
            if(a[r]==mex) {
                while(vis[mex])
                    mex++;
            }
            ans[mex]++;
        }
    }

    for(int i=0; i<N; i++) {
        if(k>ans[i])
            k-=ans[i];
        else {
            cout<<i<<endl;
            return;
        }
    }
    assert(false);
}

signed main() {
    cini(t);
    while(t--)
        solve();
}
