
/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */
#include <bits/stdc++.h>
using namespace std;

const bool ready = [](){
    ios_base::sync_with_stdio(false); cin.tie(0);
    cout << fixed << setprecision(12);
    return true;
}();

using ld=long double;
const ld PI = acos((ld)-1);
using ll= long long;
#define int ll
#define all(v) (v).begin(), (v).end()
#define fori(n) for(int i=0; i<int(n); i++)

#define cini(i) int i; cin>>i;
#define cins(s) string s; cin>>s;
#define cind(d) ld d; cin>>d;
#define cinai(a,n) vi a(n); fori(n) { cin>>a[i]; }
#define cinas(s,n) vs s(n); fori(n) { cin>>s[i]; }
#define cinad(a,n) vd a(n); fori(n) { cin>>a[i]; }

using pii= pair<int, int>;
using pdd= pair<ld, ld>;
using vd= vector<ld>;
using vb= vector<bool>;
using vi= vector<int>;
using vvi= vector<vi>;
using vs= vector<string>;

#define endl "\n"

/**
 * We need to solve from left to right.
 *
 * if the current digit eq d, then we need to add
 * numbers until it changes to next digit.
 *
 * if d==0 then we need to add 111....111.
 *
 * How to implement?
 * Go from lowest to highest digit, and maintain
 * how much would be need to add to get to 0.
 * if current digit eq d then add all the sum so far.
 *
 *
 * fuck...thats nasty implementation problem.
 * Is there a simple brute force?
 *
 * still wrong...
 * ****************************
 * Let ans be the next number without digit d, then three cases:
 * 1. d is not contained in n, then ans=n
 * 2. d==0
 *   n is of form xx0yyyy with x!='0'
 *            ans=xx11111
 * 3. d!=0
 *   n is of form xxdyyyy withd x!=d
 *           ans=(xxd+1)0000
 *   But we need to take care that xxd+1 does not contain d, recursivly.
 *
 * Output: ans-n
 */


/* @return the next bigger number bgeq n that contains d in a more leftmost position :/ */
int solve(int n, int d) {
    const int nn=n;
    vi dig;
    bool hasd=false;
    while(n) {
        dig.push_back(n%10);
        if(dig.back()==d) {
            hasd=true;
        }
        n/=10;
    }

    if(!hasd)
        return nn;

    reverse(all(dig));

    int ans=0;
    for(size_t i=0; i<dig.size(); i++) {
        ans*=10;
        ans+=dig[i];

        if(dig[i]==d) {
            if(d!=0) {
                /* case 3 */
                ans++;
                for(size_t j=i+1; j<dig.size(); j++) 
                    ans*=10;
                return ans;
            } else {
                /* case 2 */
                int ones=1;
                for(size_t j=i+1; j<dig.size(); j++)  {
                    ones*=10;
                    ones++;
                    ans*=10;
                }
                return ans+ones;
            }
        }
    }
    assert(false);
}

signed main() {
    cini(t);
    while(t--) {
        cini(n);
        const int nn=n;
        cini(d);
        int nextn=n;
        do {
            n=nextn;
            nextn=solve(n,d);
        }while(nextn!=n);
        cout<<n-nn<<endl;
    }
}