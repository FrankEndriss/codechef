/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define DEBUG
#ifndef DEBUG
#define endl "\n"
#endif
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=1e9+7;

int mul(const int v1, const int v2, int mod=MOD) {
    return (int)((1LL * v1 * v2) % mod);
}

int toPower(int a, int p, int mod=MOD) {
    int res = 1;
    while (p != 0) {
        if (p & 1)
            res = mul(res, a);
        p >>= 1;
        a = mul(a, a);
    }
    return res;
}

int pl(int v1, int v2, int mod=MOD) {
    int res = v1 + v2;

    if (res < 0)
        res += mod;

    else if(res>=mod)
        res-=mod;

    return res;
}

/** @return the number of palindromes of len n */
int palins(int n) {
    if(n==0)
        return 0;

    int palins=toPower(10, (n+1)/2 ); // 1->1->10; 2->1->10; 3->2->100; ...
}

int solve6(int n) {
}

/* solve for k==5
 * "0" and all numbers ending (and hence starting) in 5 "21,61,42...,69")
 * Which are 1/100 of all
 */
int solve5(int n) {
    if(n==1) 
        return 2;   // "0,5"
    else if(n==2)
        return 3;   // "0,5" + "55"
    else {   // "0,5" + "55" + "5<p*>5" // p*==palindrome of len n-2
        int ans=3;
        return pl(3, palins(n-2));
    }
}

/* solve for k==4
 * All numbers dividable by 4 (end/start with "12,16,20!,24,...40!,60!,80!...96" vs "21,61,42...,69")
 * Which are 18/(100*100) of all.
 */
int solve4(int n) {
    if(n==1)
        return 3;   // "0,4,8"
    else if(n==2)
        return 5;   // "0,4,8" + "44,88"
    else if(n==3)
        return 25;  //  "0,4,8" + "44,88" + "212,232,252,272,292,404,424,444,464,484,616,636,656,676,696,808,828,848,868,888"
    else if(n==4)
        return mul(18, palins(n-4));
}


/* solve for k==3
 * All numbers dividable by 3 "0,3,6,9,12..."
 * (which are 1 + (10^^n)/3, * which are 3+30+300...)
 */
int solve3(int n) {
    int ans=1;
    // TODO
    while(n--)
        ans+=mul(toPower(10, n), 3);
    
    return ans;
}

/* solve for k==2 
 * ans=All numbers starting with {2,4,6,8} and ending in that number
 * So "{2,4,6,8}<p*>{2,4,6,8}"
 * Since "0" is the only number starting with 0 its only
 * starting with one of {2,4,6,8} and ending with one of {2,4,6,8}
 * So its 10^^(n-2) * 4 * 4
 **/
int solve2(int n) {
    if(n==1)
        return 5;   // 0,2,4,6,8
    else if(n==2) {
        return 5+4; //  0,2,4,6,8 + 22,44,66,88
    } else {    // {2,4,6,8}<p*>{8,6,4,2}
        pl(9, mul(palins(n-2), 4));
    }
}

void solve() {
    cini(n);
    cini(k);
    if(k==2)
        solve2(n);
    else if(k==3) 
        solve3(n);
    else if(k==4)
        solve4(n);
    else if(k==5)
        solve5(n);
    else if(k==6)
        solve6(n);
    else
        assert(false);
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

