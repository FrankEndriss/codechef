/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG

const double PI=3.1415926535897932384626433;
typedef long long ll;
#ifndef DEBUG
#define endl "\n"
#endif
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct edge {
    int au;
    int av;
    int bu;
    int bv;
};

void solve() {
    cini(n);
    cini(m);

    vector<edge> edges(m);
    vector<vector<pii>> treea(n);   // { node, edgenum }
    vector<vector<pii>> treeb(n);   // { node, edgenum }

    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        edges[i].au=u;
        edges[i].av=v;
        treea[u].push_back({ v, i });
        treea[v].push_back({ u, i });
    }

    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        edges[i].bu=u;
        edges[i].bv=v;
        treeb[u].push_back({ v, i });
        treeb[v].push_back({ u, i });
    }

    typedef set<int> nodeset;
    typedef set<int> edgeset;

/* Now find minimum set of edges[i] which make both graphs a single component.
 * We try to find the biggest set of edges[i] which make a single component
 * in both trees.
 * Then we remove the nodes in this component from the trees and repeat.
 *
 * So we get a list of component sizes. 
 * Each component needs sizeof_component-1 edges (for both trees).
 * We then need to connect those components with
 * num_components-1 edges per tree.
 */

    vb visa(n, false);    // nodes of a used in previous rounds, to be ignored
    vb visb(n, false);    // nodes of b used in previous rounds, to be ignored

/* TODO
 * If while bilding the maximum sized component, two edges can be added
 * to the trees, and in one tree both edges lead to the same vertex, but
 * in the other tree to two different vertex. 
 * Then we need to try both variants...
 */


    vi anssets;         // component sizes
    while(true) {
        edgeset meset;  // max set of edges so far

        /* try to build a maximus sized component, starting with edge f. */
        for(int f=0; f<m; f++) { 
            if(visa[edges[f].av] || visa[edges[f].au] || visb[edges[f].bv] || visb[edges[f].bu] ) {
                continue;
            }

            edgeset eset;
            nodeset nseta;
            nodeset nsetb;
#ifdef DEBUG
            cout<<"f="<<f<<endl;
#endif
            queue<int> q;
            q.push(f); // edges needed to be added to eset
            while(q.size()) {
                //while(q.size()) {
                    int e=q.front();
                    q.pop();
                    eset.insert(e);
                    nseta.insert(edges[e].av);
                    nseta.insert(edges[e].au);
                    nsetb.insert(edges[e].bv);
                    nsetb.insert(edges[e].bu);
#ifdef DEBUG
                    cout<<"add edge="<<e<<" av="<<edges[e].av<<" au="<<edges[e].au<<" bv="<<edges[e].bv<<" bu="<<edges[e].bu<<endl;
#endif
                    assert(nseta.size()==nsetb.size());
                //}


                edgeset eseta; // set of all edges connecting nodes in a with nodes not in a
                for(int node : nseta) {
                    for(pii c : treea[node]) {
                        if(!visa[c.first] && nseta.count(c.first)==0)
                            eseta.insert(c.second);
                    }
                }

                edgeset esetb; // set of all edges connecting nodes in b with nodes not in b
                for(int node : nsetb) {
                    for(pii c : treeb[node]) {
                        if(!visb[c.first] && nsetb.count(c.first)==0)
                            esetb.insert(c.second);
                    }
                }

                vi res;
                set_intersection(eseta.begin(),eseta.end(),esetb.begin(),esetb.end(), back_inserter(res));
                if(res.size())  {
                    q.push(res.front());
                }
            }

#ifdef DEBUG
            cout<<"eset.size()="<<eset.size()<<" nseta.size()="<<nseta.size()<<" nsetb.size()="<<nsetb.size()<<endl;
#endif
            if(eset.size()>meset.size()) {
                meset.swap(eset);
            }
        }
        for(int i : meset) {
            visa[edges[i].av]=true;
            visa[edges[i].au]=true;
            visb[edges[i].bv]=true;
            visb[edges[i].bu]=true;
        }
        if(meset.size()>0)
            anssets.push_back((int)meset.size());
        else
            break;
    }
#ifdef DEBUG
cout<<"anssets: ";
for(int i : anssets) 
    cout<<i<<" ";
cout<<endl;
#endif
    int ans=2*(n-1);
    for(int i: anssets) 
        ans-=i;
    cout<<ans<<endl;
}


int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

