/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef set<int> si;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

#define isOdd(x) ((x)&1)
void solve() {
    cini(n);
    cini(m);
    vvi tree(n);
    vi ec(n);
    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        tree[u].push_back(v);
        tree[v].push_back(u);
        ec[u]++;
        ec[v]++;
    }

    vi ans(n);  // ans[i]== set number node i belongs to

    /* If # edges is odd put one vertex with odd number of
     * edges into second set.
     * Which results in one set with 0 edges, other set with even # edges.
     */
    int setnumber=1;
    if(m&1) {
        setnumber++;
        /* Find odd numbered vertex, there might be none. (ie 3 vertex, 3 edges) */
        bool ok=false;
        for(int i=0; i<n; i++) {
            if(isOdd(ec[i])) {
                ans[i]=1;
                ok=true;
                break;
            }
        }

        if(!ok) {
            setnumber++;
            for(int i=0; i<n; i++) {
                if(ec[i]>0) {   /* Note ec[i] is even because all ec[i] are even. */
                    ans[i]=1;
                    ans[tree[i][0]]=2;
                    break;
                }
            }
        }
    }

    cout<<setnumber<<endl;
    for(int a : ans)
        cout<<a+1<<" ";
    cout<<endl;

}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

