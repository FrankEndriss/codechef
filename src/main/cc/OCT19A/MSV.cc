/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int N=1e6+1;

vi divs(N);

void adddivs(int a) {
    for(int i=1; i*i<=a; i++) {
        if(a%i==0) {
            divs[i]++;
            if(i*i!=a)
                divs[a/i]++;
        }
    }
}

void solve() {
    cini(n);
    cinai(a, n);

    int ans=0;
    for(int i=0; i<n; i++) {
        ans=max(ans, divs[a[i]]);
        adddivs(a[i]);
    }

    cout<<ans<<endl;
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--) {
        divs.resize(0, 0);
        divs.resize(N, 0);
        solve();
    }
}

