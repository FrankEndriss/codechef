/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

//#define DEBUG

const double PI=3.1415926535897932384626433;
typedef long long ll;
#ifndef DEBUG
#define endl "\n"
#endif
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct NextPrev {
    int first;
    int last;
    vector<int> next;
    vector<int> prev;

    NextPrev(size_t size) {
        assert(size>0);
        first=0;
        last=size-1;
        for(size_t i=0; i<size-1; i++) 
            next[i]=i+1;
        next[size-1]=-1;
        for(size_t i=1; i<size; i++) 
            prev[i]=i-1;
        prev[0]=-1;
    }

    void erase(int idx) {
        if(idx==first)
            first=next[first];
        else if(idx==last)
            last=prev[last];
        else {
            int lnext=next[idx];
            int lprev=prev[idx];
            next[lprev]=lnext;
            prev[lnext]=lprev;
        }
    }
};

class DSU {
public:
    vector<int> p;
    int sz;

    /* initializes each (0..size-1) to single set */
    DSU(int size) {
        p.resize(size);
        for(int i=0; i<size; i++)
            p[i]=i;
        sz=size;
    }

    /* finds set representative for member v */
    int find_set(int v) {
        if (v == p[v])
            return v;
        return p[v] = find_set(p[v]);
    }

    /* combines the sets of two members.
     *  * Use the bigger set as param a for optimized performance.  */
    void union_sets(int a, int b) {
        a = find_set(a);
        b = find_set(b);
        if (a != b) {
            p[b] = a;
            sz--;
        }
    }

    int count_sets() {
        return sz;
    }
};

struct edge {
    int au;
    int av;
    int bu;
    int bv;
};

void solve() {
    cini(n);
    cini(m);

    vector<edge> edges(m);
    vector<vector<pii>> treea(n);   // { node, edgenum }

    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        edges[i].au=u;
        edges[i].av=v;
        treea[u].push_back({ v, i });
        treea[v].push_back({ u, i });
    }

    for(int i=0; i<m; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        edges[i].bu=u;
        edges[i].bv=v;
    }

    typedef unordered_set<int> nodeset;
    typedef unordered_set<int> edgeset;

    const int INF=1e9+7;

    int cnt=0;
    function<int(edgeset&)> action=[&](edgeset &mst) {
        cnt++;
#ifdef DEBUG
        cout<<"action, cnt="<<cnt<<endl;
#endif

        DSU dsu(n);
        for(int i : mst)
            dsu.union_sets(edges[i].bv, edges[i].bu);

        return n-2+dsu.count_sets();
    };

    vi edgestate(m);    // 0=undecided; 1=in mst; 2=must not be in mst
    vi nodestate(n);    // 0=not in mst; 1=in mst;

    /* Now find minimum set of edges[i] which make both graphs a single component.
     *
     * Lets implement brute force by
     * -for all minimum spanning tree of a
     *   -count components in b, stop each count if bigger or equals min
     *
     * Parameters:
     * @edgetree all edges in graph until now
     * @nodetree all nodes in graph connected by edges edgetree
     * @adj all edges connecting one of the nodes of nodetree to one of the nodes not in nodetree
     */
    function<int(edgeset&,nodeset&,edgeset)> foreachMst=[&](edgeset &edgetree, nodeset &nodetree, edgeset adj) {
#ifdef DEBUG
        cout<<"foreachMst, n="<<n<<" edgetree.size()="<<edgetree.size()<<" nodetree.size()="<<nodetree.size()<<endl;
#endif
        if(n==(int)nodetree.size())
            return action(edgetree);

        int ans=INF;

        vi eraseedgeidx;
        for(int edgeidx : adj) {
            if(nodetree.count(edges[edgeidx].au) && nodetree.count(edges[edgeidx].av)) {
                eraseedgeidx.push_back(edgeidx);
                continue;
            }
#ifdef DEBUG
            assert(edgetree.count(edgeidx)==0);
#endif

            edgetree.insert(edgeidx);

            int insertednode;
            if(nodetree.count(edges[edgeidx].au))
                insertednode=edges[edgeidx].av;
            else
                insertednode=edges[edgeidx].au;

            nodetree.insert(insertednode);

            vi insertedAdj;
            for(pii ch : treea[insertednode]) {
                if(adj.count(ch.second)==0) {
                    adj.insert(ch.second);
                    insertedAdj.push_back(ch.second);
                }
            }
            ans=min(ans, foreachMst(edgetree, nodetree, adj));

            for(int r : insertedAdj)
                adj.erase(r);

            nodetree.erase(insertednode);
            edgetree.erase(edgeidx);
        }

        for(int edgeidx : eraseedgeidx)
            adj.erase(edgeidx);

        return ans;
    };

    /* find node with minimum number of adj, use this to start. */
    int mnode=INF;
    size_t msize=INF;
    for(int i=0; i<n ; i++) {
        if(treea[i].size()<msize) {
            msize=treea[i].size();
            mnode=i;
        }
    }

    int ans=INF;
    /* exactly one of the edges connected to mnode must be in the MST. */
    for(pii child : treea[mnode]) {
        int i=child.second;

        edgeset edgetree;
        edgetree.insert(i);
        nodeset nodetree;
        nodetree.insert(edges[i].au);
        nodetree.insert(edges[i].av);
        edgeset adj;
        for(pii ch : treea[edges[i].au])
            if(ch.second!=i)
                adj.insert(ch.second);
        for(pii ch : treea[edges[i].av])
            if(ch.second!=i)
                adj.insert(ch.second);

#ifdef DEBUG
        cout<<"mainloop, i="<<i<<" m="<<m<<endl;
#endif
        ans=min(ans, foreachMst(edgetree, nodetree, adj));
    }

    cout<<ans<<endl;
}


int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();
}

