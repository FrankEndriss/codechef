/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI=3.1415926535897932384626433;
typedef long long ll;

#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinc(c) char c; cin>>c;
#define cinll(l) ll l; cin>>l;

#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

//#define DEBUG

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(n);
    cini(q);
    vvi tree(n);
    for(int i=0; i<n-1; i++) {
        cini(u);
        cini(v);
        u--;
        v--;
        tree[u].push_back(v);
        tree[v].push_back(u);
    }
    cinai(ibac, n);

    const int ROOT=0;

    vi p(n);        // parent index
    vb leaf(n);     // isLeaf index
    p[ROOT]=-1; 
    function<void(int,int)> dfs1=[&](int node, int par) {
        p[node]=par;
        bool isLeaf=true;
        for(int c : tree[node])
            if(c!=par) {
                dfs1(c, node);
                isLeaf=false;
            }

        leaf[node]=isLeaf;
    };
    dfs1(ROOT, -1);

/*
 * We step up the parents.
 * Each step up increments second counter.
 * Note that this aproach takes no advatage from the fact that we
 * can process offline.
 *
 * Idea for offline:
 * We simulate the movement of bacteria, but ignore parts of
 * the tree where we know there is no query.
 * So, we can hold lists of nodes by level, or run them from
 * deepest level up to highest.
 * And more, we can "jump" over seconds which we know they do 
 * nothing.
 */

    struct ad {
        int v;
        int k;
    };


    vll sums(n);    // sums[i]==sum of all adds.k of node i so far
    vector<ad> adds;
    int curSec=0;      // current second

/** simply add up all sums in path to root. */
    function<ll(int)> calcForLeaf=[&](int node) {
#ifdef DEBUG
cout<<"calcForLeaf, node="<<node<<endl;
#endif
        ll ans=0;
        int sec=curSec;
        vll ladds(n);
        do {
            ans+=sums[node];  
            ans-=ladds[node];   // remove all which were added after that sec
            ans+=ibac[node];

            node=p[node];
            if(adds[sec].v>=0)  // add up all added at sec
                ladds[adds[sec].v]+=adds[sec].k;
            sec--;
        } while(node>=0 && sec>=0);
        return ans;
    };

/** add up all adds which are at the right seconds in the path to root. */
    function<ll(int)> calcForParent=[&](int node) {
#ifdef DEBUG
cout<<"calcForParent, node="<<node<<" curSec="<<curSec<<endl;
#endif
        ll ans=0;
        int sec=curSec-1;
        node=p[node];
        while(sec>=0 && node>=0) {
#ifdef DEBUG
cout<<"calcForParent loop, node="<<node<<" sec="<<sec<<endl;
#endif
            if(adds[sec].v==node)
                ans+=adds[sec].k;

            if(sec==0)
                ans+=ibac[node];

            node=p[node];
            sec--;
        }

        return ans;
    };

    curSec=0;
    adds.push_back({-1,0});

    for(int i=0; i<q; i++) {
        curSec++;

        cinc(t);
        if(t=='+') {
            cini(v);
            v--;
            cini(k);
#ifdef DEBUG
cout<<"adds.add, curSec="<<curSec<<" v="<<v<<" k="<<k<<endl;
#endif
            adds.push_back({v,k});
            sums[v]+=k;
        } else {
            adds.push_back({-1,0});
            cini(v);
            v--;
            ll ans=-1;
            if(leaf[v])
                ans=calcForLeaf(v);
            else 
                ans=calcForParent(v);

            cout<<ans<<endl;
        }
    }
}

