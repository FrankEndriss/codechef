/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

const double PI=3.1415926535897932384626433;
typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

const int MOD=998244353;

void solve() {
    cini(n);
    cini(m);
    cini(q);
    cini(z);

/* How to do q operations with an outcome of z odd cells?
 *
 * Observations:
 * A move done twice negates the previous one.
 * Moves can be done in any order with same outcome.
 * 4 clicks into 2x2 fields is a non-click.
 * (n*m-1)*2 clicks along a rectangualr path is a non click given that n am m are even.
 *
 * A klick into a cell of the empty grid makes     (r-1)+(c-1) cells switched.
 * A klick into another cell of the same row makes (r-1)+(c-1) +1 -(c-3)+(r-1) cells switched.
 *      which are                                   2*(r-1)+1
 *      ...
 *
 * Since before first klick the grid is symetrical in all directions, ie
 * it does not matter where to click, all same outcome. So we choose (0,0)
 * Same for second click, there are 4 different ones:
 * 1. Same cell again (0,0) 1
 * 2. Same row, other col, (0,1)   cols-1
 * 3. Same col, other row, (1,0)   rows-1
 * 4. Diff row/col (1,1) (cols-1)*(rows-1)
 * Third click:
 * 1. Any of the clicked cells, which swiches back to 1.
 * ...
 *
 * We assume only click-sequences with clicks into cells not clicked before,
 * which decreases the number of possible moves.
 *
 * Other one:
 * States of rows and cols.
 * Every row (and col) has a number of clicks.
 * So the state of the grid is determined by
 * these states clicks of row[0..cols], clicks of col[0..rows]
 * Every such combination has a fixed number of odd cells.
 * ...
 *
 **/
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t)
    while(t--)
        solve(t);
}

