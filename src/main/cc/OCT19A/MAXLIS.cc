/**
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

#include <bits/stdc++.h>
using namespace std;

#define DEBUG

#ifndef DEBUG
#define endl "\n"
#endif

const double PI=3.1415926535897932384626433;
typedef long long ll;
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

struct part {
    int first;  // position part starts
    int len;    // len of part (number of positions)
    int l;      // leftmost element of lis, usually first element
    int r;      // rightmost element of lis, usually last element
    int w;      // number of increasing elements, the weight/points
};

map<tuple<int,int,int>, pair<ll,vb>> knmemo;

/**  This runs O(n^2) on parts.size()
 * @param parts sorted by r, then l
 * @param idx of last used part, -1 at first recursion.
 * @return max possible sum of w
 * @return ret vector of size parts.size(), on return not more than maxParts entries set to true
 **/
ll knapsack(vector<part> &parts, int idx, int prevr, int maxParts, vb &ret) {
    if(maxParts==0)
        return 0LL;

    /* jump over all parts unusable because of l/r */
    idx++;
    while(idx<(int)parts.size() && parts[idx].l<=prevr)
        idx++;

    if(idx==(int)parts.size())
        return 0LL;   /* no more parts avail */

    auto cache=knmemo[make_tuple(idx, prevr, maxParts)];
    if(cache.second.size()>0) {
        for(int i=idx; i<(int)ret.size(); i++)
            ret[i]=cache.second[i];
        return cache.first;
    }

#ifdef DEBUG
cout<<"knapsack calc, idx="<<idx<<" prevr="<<prevr<<" maxParts="<<maxParts<<endl;
#endif

    vb ret1=ret;
    ll w1=knapsack(parts, idx, prevr, maxParts, ret1);                     /* do not use current part. */
    ll w2=knapsack(parts, idx, parts[idx].r, maxParts-1, ret) + parts[idx].w;    /* use current part. */
    ll w;
    if(w1>w2) {
        ret[idx]=false;
        for(int i=idx+1; i<(int)ret.size(); i++)
            ret[i]=ret1[i];
        w=w1;
    } else {
        ret[idx]=true;
        w=w2;
    }
    knmemo[make_tuple(idx, prevr, maxParts)]= {w,ret};
    return w;
}

/** Calc len of lis, for subarray a[lidx..ridx], but lis first element is allways a[lidx].
 * returns lengths of lis in ret.
 * retLen and retMax must be of min size ridx-lidx+1
 * @return retLen lengths of list
 * @return retMax max element for that lis
 */
void lis(vi &a, int lidx, int ridx, vi &retLen, vi &retMax) {
#ifdef DEBUG
    cout<<"lis, lidx="<<lidx<<" ridx="<<ridx<<endl;
#endif
    vi ans;
    ans.push_back(a[lidx]);
    retLen[0]=1;
    retMax[0]=a[lidx];
    for(int i=lidx+1; i<=ridx; i++) {
        retLen[i-lidx]=retLen[i-1-lidx];
        retMax[i-lidx]=retMax[i-1-lidx];
        if(a[i]<a[lidx])
            continue;

        int val=a[i];
        auto it=upper_bound(ans.begin(), ans.end(), val);
        if(it==ans.end()) {
            ans.push_back(val);
            retLen[i-lidx]++;
        } else
            *it=val;

        retMax[i-lidx]=ans.back();
    }
}

void countW(vi &a) {
    vi retLen(a.size());
    vi retMax(a.size());
    lis(a, 0, a.size()-1, retLen, retMax);
cout<<"countW, a.size="<<a.size()<<" len="<<retLen.back()<<endl;
}


/** Extended singlefix
 * We extend the idea of singlefix to parts.
 * We do not find the indexes of first k elements, but the indexes of
 * pivot elements 1, n/k*1, n/k*2, n/k*3... n/k*k-1.
 * From these indexes we define parts, every part is from one index to the next.
 * Then we simply reorder the parts by the pivot elements.
 * Since all other elements are still random positioned, that should increase
 * the outcome by k.
 * ...for some reason it does not :/
 */
void multifix(vi &a, int k) {
/* find indexes all */
#ifdef DEBUG
countW(a);
#endif
    vi pos(a.size()+1);   // a[pos[i]]=i
    for(size_t i=0; i<a.size(); i++)
        pos[a[i]]=i;

    vi ppos1;   // positions sorted by position
    vi ppos2;   // positions sorted by value
    for(int pivot=1; pivot<a.size(); pivot+=a.size()/(k-1)) {
#ifdef DEBUG
cout<<"pivot="<<pivot<<" pos="<<pos[pivot]<<endl;
#endif
        ppos1.push_back(pos[pivot]);
        ppos2.push_back(pos[pivot]);
    }

    sort(ppos1.begin(), ppos1.end());
    ppos1.push_back(a.size());

    vi ans;
    for(int i=0; i<ppos2.size(); i++)  {
        int next=*upper_bound(ppos1.begin(), ppos1.end(), ppos2[i]);
#ifdef DEBUG
cout<<"pos="<<ppos2[i]<<" next="<<next<<endl;
#endif
        for(int j=ppos2[i]; j<next; j++) {
            ans.push_back(a[j]);
        }
    }
#ifdef DEBUG
cout<<"pos="<<0<<" next="<<ppos1[0]<<endl;
#endif
    for(int i=0; i<ppos1[0]; i++)
        ans.push_back(a[i]);

#ifdef DEBUG
assert(ans.size()==a.size());
set<int> sset;
#endif
    for(int i : ans) {
        cout<<i<<" ";
#ifdef DEBUG
        sset.insert(i);
#endif
    }
    cout<<endl;
#ifdef DEBUG
assert(ans.size()==sset.size());
countW(ans);
#endif
}

/** Other Simple method.
 * We move Segments of data to the left (or right).
 * We move k/2-1 segments.
 * In simplest case we move k/2-1 segments of size 1.
 * But we can do better if we find a segment where n+1 is not
 * far right of n.
 */
void singlefix2(vi &a, int k) {
    vi pos(a.size());
    for(size_t i=0; i<a.size(); i++) {
        pos[a[i]]=i;
    }
    vector<pii> segs;
    set<int> used;
    int nextI=1;
    for(int i=0; i<k/2-1; i++)  {
        int len=pos[nextI+1]-pos[nextI]
        if(len>0 && len<10) 
            ; // TODO...
        
    }
}

/** Simple method.
 * We move the first k/2 elements to the left.
 * In place. Which is good for 42%
 */
void singlefix(vi &a, int k) {
/* find indexes of first k/2-1 elements */
    int k2=k/2-1;
    vi pos;   // positions of elements [1..k2]
    for(size_t i=0; i<a.size(); i++) {
        if(a[i]<=k2)
            pos.push_back(i);
    }
    int ridx=pos.back();
    int lidx=ridx-1;
    int rposidx=pos.size()-1;
    int lposidx=rposidx-1;

    while(lidx>=0) {
        while(lposidx>=0 && lidx==pos[lposidx]) {
            lidx--;
            lposidx--;
        }

        a[ridx]=a[lidx];
        ridx--;
        lidx--;
        
        if(rposidx>=0 && ridx==pos[rposidx]) {
            rposidx--;
        }
    }
    for(int i=1; i<=k2; i++)
        a[i-1]=i;

    for(size_t i=0; i<a.size(); i++)
        cout<<a[i]<<" ";
    cout<<endl;
}

int main() {
    cin.tie(nullptr);
    std::ios::sync_with_stdio(false);
    cini(n);
    cini(k);
    vi a(n);
    //vi apos(n);
    for(int i=0; i<n; i++) {
        cin>>a[i];
        //apos[a[i]]=i;
    }

    if(n<100000 && k<2000) {
        // countW(a);
        // singlefix(a, k);
        multifix(a, k);
    } else {
        // singlefix(a, k);
        singlefix(a, k);
    }
    return 0;


    /* We need to find parts of a[] with mostly increasing numbers in it.
     * Such parts have
     * -a number of increasing elements (longest increasing subseq)
     * -first
     * -last
     * -len
     *
     * ie Each non-increasing member is a negative point.
     * last-first should be small.
     * len should be big.
     *
     * We can then put together some of the parts, non overlapping l/r.
     * We need to find (via DP) the most valueable combination of K parts.
     *
     * Parts:
     * All parts with first element less than last element can be considered useful.
     *
     * How to DP the parts?
     * Sort parts by increasing r, tie by w desc.
     * Put in row by using first part with l big enough to be bigger than prev r.
     * Like with Knapsack, we can use a part or notuse a part.
     */

    /** Finding/Indexing parts strategy:
    * We try intervals, and calc LIS for them.
    * Len of lis is the worth.
     * Runs in log(n)^2
     * We can limit R and len by heuristics.
     */

/* NOTE:
 * Here we controll the number of calculations.
 *
 */
    const int avgsz=max(2, n/k);
    vi retLen(avgsz+1);
    vi retMax(avgsz+1);
    vector<part> parts;

    for(int lidx=0; lidx<n-avgsz; lidx+=10) {
        lis(a, lidx, lidx+avgsz-1, retLen, retMax);
        int cut=1;
        while(cut<avgsz && retMax[avgsz-cut]==retMax[avgsz-cut-1])
            cut++;
        parts.push_back({ lidx, avgsz, a[lidx], retMax[avgsz-cut], retLen[avgsz-cut] });
        continue;

        /* now we want max retLen with min retMax and min ridx.
         * so we run from left to right using all firsts of
         * group pair<retLen,retMax> */
        int prevLen=retLen[0];
        int prevMax=retMax[0];
        for(int i=avgsz/2; i<2*avgsz && lidx+i<n; i++)
            if(prevLen!=retLen[i] || prevMax!=retMax[i]) {
                parts.push_back({ lidx, i+1, a[lidx], retMax[i], retLen[i] });
                prevLen=retLen[i];
                prevMax=retMax[i];
/*
    int first;  // position part starts
    int len;    // len of part (number of positions)
    int l;      // leftmost element of lis, usually first element
    int r;      // rightmost element of lis, usually last element
    int w;      // number of increasing elements, the weight/points
*/
            }
    }

#ifdef DEBUG
    cout<<"parts.size()="<<parts.size()<<endl;
    for(size_t i=0; i<parts.size(); i++) {
        cout<<"lidx="<<parts[i].first<<" len="<<parts[i].len<<" w="<<parts[i].w<<" maxElement="<<parts[i].r<<endl;
    }
    cout<<"now ordering parts..."<<endl;
#endif

    sort(parts.begin(), parts.end(), [&](part &p1, part &p2) {
        if(p1.r==p2.r)
            return p1.l<p2.l;
        return p1.r<p2.r;
    });

    vb used(parts.size());
#ifdef DEBUG
cout<<"start knapsack phase..."<<endl;
#endif
    knapsack(parts, -1, -1, k, used);
#ifdef DEBUG
    for(size_t i=0; i<parts.size(); i++) {
        if(used[i]) {
            cout<<"i="<<i<<" first="<<parts[i].first<<" len="<<parts[i].len<<" l="<<parts[i].l<<" r="<<parts[i].r<<endl;
        }
    }
#endif

    /* extend parts to next used part */
    vi partidx;
    for(size_t i=0; i<parts.size(); i++) {
        if(used[i]) {
            partidx.push_back((int)i);
        }
    } // sort by startidx
    sort(partidx.begin(), partidx.end(), [&](int i1, int i2) {
        return parts[i1].first<parts[i2].first;
    });
    /* adjust first and len so that whole list is covered. */
    parts[partidx[0]].first=0;
    for(size_t i=1; i<partidx.size(); i++)
        parts[partidx[i-1]].len=parts[partidx[i]].first-parts[partidx[i-1]].first;
    size_t last=partidx.size()-1;
    parts[partidx[last]].len=n-parts[partidx[last]].first;

    for(size_t i=0; i<parts.size(); i++) {
        if(used[i]) {
            for(int j=parts[i].first; j<parts[i].first+parts[i].len; j++)
                cout<<a[j]<<" ";
        }
    }
    cout<<endl;

}

