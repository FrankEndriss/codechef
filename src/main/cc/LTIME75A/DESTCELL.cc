/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
//#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cini(m);

    cout<<n*m<<" ";
    for(int k=2; k<n*m; k++) {
        set<pii> g;
        int r1=0;
        int c1=0;
        int r2=0;
        int c2=0;
        for(int c=0; c<n*m; c+=k) {
            if(k>m) {
                r1+=c1/m;   
                c1%=m;
            } else {
                while(c1>=m) {
                    r1++;
                    c1-=m;
                }
            }

            if(k>m) {
                c2+=r2/n;
                r2%=n;
            } else {
                while(r2>=n) {
                    c2++;
                    r2-=n;
                }
            }

            g.insert({r1, c1});
            g.insert({r2, c2});

            c1+=k;
            r2+=k;
        }
        cout<<g.size()<<" ";
    }

    cout<<1<<endl;  // last cell destroyed by both
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

