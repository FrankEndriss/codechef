/** 
 * Dont raise your voice, improve your argument.
 * --Desmond Tutu
 */

/*
#pragma GCC target ("avx2")
#pragma GCC optimization ("O3")
#pragma GCC optimization ("unroll-loops")
*/

#include <bits/stdc++.h>
using namespace std;

/* see https://github.com/pllk/cphb/
#include <ext/pb_ds/assoc_container.hpp>
using namespace __gnu_pbds;
typedef tree<int,null_type,less<int>,rb_tree_tag,
    tree_order_statistics_node_update> indexed_set;
 **/

typedef long long ll;
#define endl "\n"
#define fori(n) for(ll i=0; i<(n); i++)

#define cins(s) string s; cin>>s;
#define cini(i) int i; cin>>i;
#define cinll(l) ll l; cin>>l;
#define cinai(a, n) vi a(n); fori(n) { cin>>a[i]; }
#define cinall(a, n) vll a(n); fori(n) { cin>>a[i]; }
#define cinas(s, n) vs s(n); fori(n) { cin>>s[i]; }

//typedef __int128 lll;
typedef pair<int, int> pii;
typedef vector<bool> vb;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<vvi> vvvi;
typedef vector<string> vs;
typedef vector<ll> vll;

void solve() {
    cini(n);
    cini(k);
    map<int, int> f;
    for(int i=0; i<n; i++) {
        cini(a);
        f[a]++;
    }
    map<int, vi> ff;    // ff[i]= list of numbers with freq==i
    for(auto ent : f) {
        ff[ent.second].push_back(ent.first);
    }

    vi ans(n);
    for(int i=0; i<k; i++) {
        int needed=(n+k-1-i)/k;
        for(int r=needed; r<n; r+=needed) {
            if(ff[r].size()>0) {
                ans[i]
            }
        }
        
    }

    int forfree=max(0, k*2-n);
    int freestart=
    for(auto ent : f) {
        forfree-=(ent.second%2);
        if(forfree<0)
            break;

        if(ent.second%2) {
            v2.push_back(ent.first);
            ent.second--;
        }
        while(ent.second) {
            v1.push_back(ent.first);
            ent.second-=2;
        }
    }

    if(forfree<0) {
        cout<<"NO"<<endl;
    } else {
        cout<<"YES"<<endl;
        for(int i : v1)
            cout<<i<<" ";
        for(int i : v2)
            cout<<i<<" ";
        for(int i : v1)
            cout<<i<<" ";
        cout<<endl;
    }
    
}

int main() {
cin.tie(nullptr);
std::ios::sync_with_stdio(false);
    cini(t);
    while(t--)
        solve();

}

